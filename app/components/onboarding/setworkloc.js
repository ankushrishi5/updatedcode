import React, { Component } from 'react';
import {
  Platform,
  View, Image, Dimensions, TouchableOpacity, Alert, StatusBar
} from 'react-native';
import { Container, Header, Button, Text } from 'native-base';
import Settings from '../../stores/settingsStore';
import DeviceInfo from 'react-native-device-info';
import theme from '../../theme/base-theme';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const work = require('../../img/work.png')
const location_icon = require('../../img/location_icon.png')
const loadingspinner = require('../../img/loading.gif')
const back_btn = require('../../img/back_btn.png')

const settings = new Settings()

class WorkLoc extends Component {
  constructor(props) {
    super(props);

    this.state = {
      uniqueId: '',
      selectedaddress: '',
      loading: false,
      showAutoComplete: false
    }
  }

  componentWillMount() {
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.baseUrl = settings.BaseUrl;
  }

  prepareAddress(addy) {
    this.setState({ selectedaddress: addy, showAutoComplete: false });
  }

  validateField = (myname) => {
    let tName = myname.length.toString();
    if (tName > 0) {
      return true;
    }
    else return false;
  };

  validateSave() {
    this.setState({ loading: true });
    if (!this.validateField(this.state.selectedaddress)) {
      Alert.alert(
        'Address missing',
        'Please enter your work address.')
      this.setState({ loading: false });
    }
    else {
      this.updateWorkLoc();
    }
  }

  updateWorkLoc() {
    this.props.UserActions.addHomeOrWorkLocations({
      selectedAdd: this.state.selectedaddress,
      idToUpdate: this.props.userData.id,
      selectedRowToEdit: 2
    })

    this.props.navigation.navigate('NotificationMain');

  }

  render() {
    return (
      <Container style={{ backgroundColor: theme.brandWhite }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />

        {
          this.state.showAutoComplete ?

            <View style={{ width: width * 0.9, height: height, alignSelf: 'center' }}>
              <TouchableOpacity style={{ marginTop: 20, marginBottom: 20 }} onPress={() => this.setState({ showAutoComplete: false })}>
                <Image source={back_btn} style={{ height: 25, width: 35 }} />
              </TouchableOpacity>
              <GooglePlacesAutocomplete
                placeholder='Enter your work address'
                minLength={2} // minimum length of text to search
                autoFocus={false}
                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                listViewDisplayed='true'    // true/false/undefined
                fetchDetails={true}
                renderDescription={row => row.description} // custom description render
                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                  this.prepareAddress(data.description);
                }}
                getDefaultValue={() => ''}
                query={{
                  // available options: https://developers.google.com/places/web-service/autocomplete
                  key: 'AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg',
                  language: 'en', // language of the results
                  types: 'address' // default: 'geocode'
                }}
                styles={{
                  textInputContainer: {
                    borderTopWidth: 0,
                    width: width * 0.80,
                    height: 50,
                    marginLeft: 10,
                    backgroundColor: 'rgba(256,256,256,0)' //
                  },
                  textInput: {
                    marginTop: 0,
                    marginLeft: 0,
                    marginBottom: 0,
                    height: 50,
                    color: '#000',
                    fontSize: 18,
                    borderBottomWidth: 1,
                    borderColor: 'rgba(179,179,179,1)',
                    backgroundColor: 'rgba(256,256,256,1)' //
                  },
                  description: {
                    fontWeight: '200',
                    fontSize: 15
                  },
                  predefinedPlacesDescription: {
                    color: '#1faadb'
                  }
                }}
                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                GoogleReverseGeocodingQuery={{
                  // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                }}
                GooglePlacesSearchQuery={{
                  // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                  rankby: 'distance',
                  types: 'food'
                }}
              />
            </View>
            :
            <View style={{ width: width, height: height }}>
              <View style={{ height: "70%", width: width * 0.90, alignSelf: 'center', paddingTop: 50 }}>
                <Image source={work} style={{ alignSelf: 'center', height: 50, width: 50 }} />
                <Text style={{ fontSize: 22, fontWeight: 'bold', color: theme.heading, marginTop: 30, marginBottom: 20, marginLeft: 20, marginRight: 20, textAlign: 'center' }}>Set Work Location</Text>
                <Text style={{ fontSize: 14, fontWeight: 'bold', color: theme.grey, marginBottom: 20, marginLeft: 20, marginRight: 20, textAlign: 'center' }}>So we can provide you with the most accurate weather alerts and forecasts</Text>

                {this.state.selectedaddress.length === 0 ?
                  <TouchableOpacity style={{ borderBottomColor: 'rgba(179,179,179,1)', borderBottomWidth: 1, marginLeft: 20, marginRight: 20, }}
                    onPress={() => this.setState({ showAutoComplete: true })}>
                    <Text style={{ color: theme.grey, fontSize: 18, marginTop: 50, padding: 10 }} >
                      Enter your work address
                    </Text>
                  </TouchableOpacity>
                  :
                  <View style={{ marginTop: 10 }}>
                    <Image source={location_icon} style={{ alignSelf: 'center', height: 50, width: 50 }} />
                    <Text style={{ color: '#000', textAlign: 'center', margin: 15, fontSize: 18 }}>{this.state.selectedaddress}</Text>
                    <TouchableOpacity onPress={() => this.setState({ showAutoComplete: true })}>
                      <Text style={{ color: '#4281e5', textAlign: 'center', fontWeight: 'bold' }}>Edit</Text>
                    </TouchableOpacity>
                  </View>
                }
              </View>

              <View style={{ height: "18%", justifyContent: 'flex-end' }}>
                {this.state.selectedaddress.length === 0 ?
                  null
                  :
                  !this.state.loading ?
                    <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, alignSelf: 'center', borderRadius: 10 }}><Text onPress={() => this.validateSave()}>SAVE LOCATION</Text></Button>
                    :
                    <Image source={loadingspinner} style={{ alignSelf: 'center', height: 75, width: 75 }} />
                }
                <TouchableOpacity style={{ marginTop: 15 }} onPress={() => this.props.navigation.navigate('NotificationMain')}>
                  <Text style={{ color: theme.grey, fontSize: 16, textAlign: 'center' }} >
                    SKIP
                  </Text>
                </TouchableOpacity>

              </View>
            </View>
        }

      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  getUnitData: state.user.getUserIdState,
  userData: state.user.userDetails
})

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(WorkLoc);
