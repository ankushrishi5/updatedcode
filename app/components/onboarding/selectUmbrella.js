import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, TouchableOpacity, TouchableHighlight, SafeAreaView, StatusBar
} from 'react-native';
import { Header, Content, Text } from 'native-base';
import LottieView from 'lottie-react-native';
import theme from '../../theme/base-theme';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')


export default class SelectUmbrella extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <Content style={{ width: width, height: height }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>
            <Image source={mylogo}
              style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 40, marginBottom: 5 }} />
            <ScrollView
              scrollEnabled={false}
              keyboardShouldPersistTaps="always" style={{ width: width * 0.90, marginTop: 20 }}>

              <TouchableOpacity
                style={{ width: width * 0.90, borderWidth: 2, borderColor: '#8497ab', borderRadius: 5, }}
                onPress={() => this.props.navigation.navigate('ConnectUmbrella')}>
                <View style={{ flexDirection: 'row' }}>
                  <LottieView
                    style={styles.image}
                    imageAssetsFolder={'images/'}
                    source={require('../../animation/pebblebee.json')}
                    autoPlay
                    loop
                  />

                  <View style={{
                    padding: 10, marginLeft: 10, alignSelf: 'center'
                  }}>
                    <Text style={{ color: theme.brandPrimary, fontSize: 18, textAlign: 'center' }}>Pebblebee</Text>
                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{ width: width * 0.90, borderWidth: 2, borderColor: '#8497ab', borderRadius: 5, marginTop: 10 }}
                onPress={() => this.props.navigation.navigate('WeathermanUmbrellaMain')} >
                <View style={{ flexDirection: 'row' }}>
                  <LottieView
                    style={styles.image}
                    imageAssetsFolder={'images/'}
                    source={require('../../animation/weatherman.json')}
                    autoPlay
                    loop
                  />

                  <View style={{
                    padding: 6, marginLeft: 10,
                    alignSelf: 'center'
                  }} >
                    <Text style={{
                      color: theme.brandPrimary, fontSize: 18, textAlign: 'center'
                    }}>Weatherman {'\n'}Droplet</Text>
                  </View>
                </View>
              </TouchableOpacity>

              <Text style={{ fontSize: 22, textAlign: 'center', color: theme.heading, marginTop: 25, marginLeft: 10, marginRight: 10 }}>
                Which device would you like to set up?
              </Text>

            </ScrollView>
          </SafeAreaView>
        </View >
      </Content >
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: width * 0.35,
    height: width * 0.35,
    alignSelf: 'center',
    resizeMode: 'cover',
  }
});
