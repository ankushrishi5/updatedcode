import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, Modal, TouchableOpacity, TextInput, ListView, StatusBar, WebView
} from 'react-native';
import { Container, Header, Content, Tabs, Tab,TabHeading, Footer, FooterTab, Button, Icon, Text, Left, Body, Right, List, ListItem } from 'native-base';
import DeviceInfo from 'react-native-device-info';
import theme from '../../theme/base-theme';
import Constants from '../../constants';
const loadingspinner = require('../../img/loading.gif')
const loadingEndspinner = require('../../img/loadingend.gif')
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';

class Loading extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      marketing: true,
      indicatorHeight: 'close',
      loading: false,
      spinnerloading: false,
      getMyID:this.props.getMyID,
      count:this.props.count
    }
    //console.log('count inside loading component ******* ',this.state.count)
  }

  componentWillMount() {
    this.DeviceUnique();
    this.getMyId()
  }

  componentWillReceiveProps(nextProps){
    this.setState({
      getMyID:nextProps.getMyID,
      count: nextProps.count.count
    },()=>{
      //this.getMyId();
      //this.forceUpdate();
    })
  }

  componentDidMount() {
    this.updateLastOpen()
  }

  DeviceUnique() {
    this.props.UserActions.deviceUnique({...this.state})
  }

  getMyId() {
    this.props.UserActions.uniqueUser({...this.state})
  }

  updateLastOpen() {
    //console.log('navigation function ***** ',this.state.getMyID)
    if(this.props.getMyID[0] != undefined){
      this.props.UserActions.storeLastOpened({idToUpdate: this.props.getMyID[0].id})
    }
  }

  render() {
    return (
      <Container style={{backgroundColor:theme.brandWhite}}>
        <Header style={{ height:(Platform.OS === 'ios' ) ? 20 : 0, backgroundColor:theme.brandPrimary}}/>
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <Content style={{width:Constants.BaseStyle.DEVICE_WIDTH, height:Constants.BaseStyle.DEVICE_HEIGHT}}>
        <View tyle={{width:Constants.BaseStyle.DEVICE_WIDTH, height:Constants.BaseStyle.DEVICE_HEIGHT, alignItems:'center'}}>
        {this.state.spinnerloading ? <Image source={loadingEndspinner} style={{alignSelf:'center',height:300, width:300, marginTop:100}} /> : null}
        </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) =>({
  getMyID: state.user.getUserIdState,
  count: state.user.count,
})

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Loading);