import React, { Component } from 'react';
import {
  View, Image, TouchableOpacity, StatusBar
} from 'react-native';
import { Container, Text } from 'native-base';
import AnimatedLinearGradient from 'react-native-animated-linear-gradient'
const mylogo = require('../../img/logo_white.png')


const bottom = '#7ac5ee';
const top = '#2e61c1';

const navColors = {
  backgroundColor: [
    top, bottom
  ]
};


const direction = {

  vertical: '{start: {x: 0, y: 0.4}, end: {x: 1, y: 0.6}}',
  slant: '{start: {x: 1, y: 0}, end: {x: 0, y: 1}}'
};

export default class Welcome extends Component {
  constructor(props) {
    super(props);

  }

  componentDidMount() {
    setTimeout(() => { this.props.navigation.navigate('Walkthrough'); }, 5000)
  }

  render() {
    return (
      <Container style={{ backgroundColor: bottom }}>

        <AnimatedLinearGradient customColors={navColors.backgroundColor} speed={10000} points={direction.vertical} />

        <StatusBar backgroundColor={top} barStyle='light-content' />

        <View style={{ height: "50%", justifyContent: 'flex-end', paddingBottom: 20, alignItems: 'center' }}>
          <Image source={mylogo} resizeMode='contain' style={{ height: 100, width: 200  }} />
          {/* <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', marginTop: 5 }}>WEATHERMAN</Text> */}
        </View>

        <View style={{ height: "20%", marginLeft: 30, marginRight: 30, marginTop: 20 }}>
          <Text style={{ color: 'white', fontSize: 18, textAlign: 'center' }} >
            Introducing a beautiful weather experience and perfect companion to the Weatherman umbrella.
            </Text>
        </View>

        {/* <View style={{ height: "15%", justifyContent: 'flex-end' }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Walkthrough')}>
            <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }} >
              GET STARTED!
            </Text>
          </TouchableOpacity>
        </View> */}

      </Container >
    );
  }
}

