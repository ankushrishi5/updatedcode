import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, TouchableOpacity, SafeAreaView, StatusBar
} from 'react-native';
import { Container, Header, Text } from 'native-base';
import Settings from '../../stores/settingsStore';
import theme from '../../theme/base-theme';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const settings = new Settings()
const mylogo = require('../../img/wxlogo.png')

import { registerKilledListener, registerAppListener } from "../../messaging/Listeners";

import LottieView from 'lottie-react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';

class LocationMain extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: props.deviceToken,
    }
  }

  componentWillMount() {
    this.state.baseUrl = settings.BaseUrl;
  }

  addToken() {
    registerAppListener(this.props.navigation);
    this.props.UserActions.addNotification({ deviceToken: this.state.token })

    this.props.navigation.navigate('Notify')
  }

  render() {
    return (
      <Container style={{ backgroundColor: theme.brandWhite }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />

        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>
            <Image source={mylogo}
              style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 30, marginBottom: 5 }} />

            <ScrollView
              scrollEnabled={false}
              keyboardShouldPersistTaps="always"
              style={{ width: width * 0.78, marginTop: 20 }}
              contentContainerStyle={{ alignItems: 'center' }}>

              <LottieView
                style={styles.image}
                imageAssetsFolder={'images/'}
                source={require('../../animation/notification_onboarding.json')}
                autoPlay
                loop
              />

              <Text style={{ fontSize: 22, textAlign: 'center', color: theme.heading, margin: 20 }}>
                Notifications
              </Text>

              <Text style={styles.subHeading}>
                Get custom forecasts and reminders to take your umbrella{'\n'}Would you like to receive notifications?
              </Text>

              <View style={{ width: '100%', flexDirection: 'row', alignSelf: 'center', marginTop: 40 }}>

                <View style={{ width: "50%", alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Success')}>
                    <Text style={{ color: theme.grey, fontWeight: 'bold' }}>NO</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ width: "50%", alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => this.addToken()} >
                    <Text style={{ color: '#4281e5', fontWeight: 'bold' }}>YES</Text>
                  </TouchableOpacity>
                </View>

              </View>

            </ScrollView>
          </SafeAreaView>
        </View >
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  subHeading: {
    fontSize: 15,
    color: theme.grey,
    textAlign: 'center'
  },
  image: {
    width: width * 0.60,
    height: width * 0.60,
    alignSelf: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
    marginBottom: 10
  }
});

const mapStateToProps = (state) => ({
  deviceToken: state.user.deviceToken
})

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationMain);