import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Animated,
  Easing,
  View, Image, Dimensions, ScrollView, TouchableOpacity, SafeAreaView, StatusBar
} from 'react-native';
import { Header, Content, Text, Button, InputGroup, Input } from 'native-base';
import LottieView from 'lottie-react-native';
import theme from '../../theme/base-theme';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')

const umbrella_icon_blue = require('../../img/umbrella_icon_blue.png')
const bag_icon_blue = require('../../img/bag_icon_blue.png')
const car_icon_blue = require('../../img/car_icon_blue.png')
const key_icon_blue = require('../../img/key_icon_blue.png')
const other_icon_blue = require('../../img/other_icon_blue.png')
const pet_icon_blue = require('../../img/pet_icon_blue.png')

const umbrella_icon_white = require('../../img/umbrella_icon_white.png')
const bag_icon_white = require('../../img/bag_icon_white.png')
const car_icon_white = require('../../img/car_icon_white.png')
const key_icon_white = require('../../img/key_icon_white.png')
const other_icon_white = require('../../img/other_icon_white.png')
const pet_icon_white = require('../../img/pet_icon_white.png')

const down_arrow = require('../../img/down_arrow.png')


export default class UmbrellaName extends Component {
  constructor(props) {
    super(props);
    this.spinValue = new Animated.Value(0)
    this.state = {
      newUmbrellaName: '',
      isIconsVisible: false,
      iconToUpdate: 'Umbrella',
    }

  }

  rotateIcon = () => {
    Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 150,
        easing: Easing.linear
      }
    ).start()
  }

  rotateBackIcon = () => {
    Animated.timing(
      this.spinValue,
      {
        toValue: 0,
        duration: 150,
        easing: Easing.linear
      }
    ).start()
  }

  showIconDialog() {
    this.setState({ isIconsVisible: !this.state.isIconsVisible }, () => { this.state.isIconsVisible ? this.rotateIcon() : this.rotateBackIcon() })

  }

  handleSubmit(){

    if (this.state.newUmbrellaName != undefined && this.state.newUmbrellaName.length == 0) {
      alert(" Name Field cannot be empty ")
      return
    }
    this.props.navigation.navigate('LocationMain')
  }

  render() {

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '-180deg']
    })

    return (
      <Content style={{ width: width, height: height }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>
            <Image source={mylogo}
              style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 40, marginBottom: 5 }} />
            <ScrollView
              scrollEnabled={false}
              keyboardShouldPersistTaps="always" style={{ width: width * 0.9, marginTop: 20 }}>

              {/* <LottieView
                style={styles.image}
                imageAssetsFolder={'images/'}
                source={require('../../animation/umbrella_ongoing.json')}
                autoPlay
                loop
              /> */}

              <View style={{ width: width * 0.9, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ justifyContent: 'flex-end', marginRight: 2, padding: 5, paddingRight: 18 }}>

                  {this.state.iconToUpdate == 'Umbrella' ? <Image source={umbrella_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                  {this.state.iconToUpdate == 'Keys' ? <Image source={key_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                  {this.state.iconToUpdate == 'Bag' ? <Image source={bag_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                  {this.state.iconToUpdate == 'Car' ? <Image source={car_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                  {this.state.iconToUpdate == 'Pet' ? <Image source={pet_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                  {this.state.iconToUpdate == 'Other' ? <Image source={other_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                  <TouchableOpacity style={{ position: 'absolute', alignSelf: 'flex-end' }} onPress={() => this.showIconDialog()}>
                    <Animated.Image style={{ height: 25, width: 25, transform: [{ rotate: spin }] }} source={down_arrow} />
                  </TouchableOpacity>

                </View>
                <View style={{ margin: 5, width: "80%" }}>
                  <Text style={{ fontSize: 16, color: theme.heading, marginTop: 20 }}>
                    Please name your device:
              </Text>
                  <InputGroup style={{ marginBottom: 10, width: width * 0.55, backgroundColor: "rgba(38, 38, 38, 0.0)", borderBottomWidth: 1, borderColor: theme.brandPrimary }} boarderType='round'>
                    <Input style={{ color: '#000' }}
                      maxLength={20}
                      value={this.state.newUmbrellaName}
                      placeholder='Device Name'
                      placeholderTextColor={theme.brandTertiary}
                      onChangeText={(val) => { this.setState({ newUmbrellaName: val }) }} />
                  </InputGroup>
                </View>

              </View>

              {this.state.isIconsVisible ?
                <View style={{ width: "96%", flexDirection: 'row', alignSelf: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Umbrella' })}>
                    {this.state.iconToUpdate == 'Umbrella' ?
                      <Image source={umbrella_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                      :
                      <Image source={umbrella_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}

                  </TouchableOpacity>
                  <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Keys' })}>
                    {this.state.iconToUpdate == 'Keys' ?
                      <Image source={key_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                      :
                      <Image source={key_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                  </TouchableOpacity>
                  <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Bag' })}>
                    {this.state.iconToUpdate == 'Bag' ?
                      <Image source={bag_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                      :
                      <Image source={bag_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                  </TouchableOpacity>
                  <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Car' })}>
                    {this.state.iconToUpdate == 'Car' ?
                      <Image source={car_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                      :
                      <Image source={car_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                  </TouchableOpacity>
                  <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Pet' })}>
                    {this.state.iconToUpdate == 'Pet' ?
                      <Image source={pet_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                      :
                      <Image source={pet_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                  </TouchableOpacity>
                  <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Other' })}>
                    {this.state.iconToUpdate == 'Other' ?
                      <Image source={other_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                      :
                      <Image source={other_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                  </TouchableOpacity>
                </View>
                : null}

              <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 40, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.handleSubmit()}>CONTINUE SETUP</Text></Button>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                <Text style={{ fontSize: 15, textAlign: 'center', fontWeight: 'bold', color: theme.grey, marginTop: 20, marginBottom: 10 }}>
                  PAIR ANOTHER UMBRELLA
              </Text>
              </TouchableOpacity>

            </ScrollView>
          </SafeAreaView>
        </View >
      </Content >
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: width * 0.50,
    height: width * 0.40,
    alignSelf: 'center',
    resizeMode: 'cover',
    marginBottom: 20,
    // backgroundColor: 'red'
  }
});
