import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, TouchableOpacity, SafeAreaView, StatusBar
} from 'react-native';
import { Header, Content, Text, Button } from 'native-base';
import LottieView from 'lottie-react-native';
import theme from '../../theme/base-theme';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')


export default class WeathermanUmbrellaMain extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <Content style={{ width: width, height: height }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>
            <Image source={mylogo}
              style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 40, marginBottom: 5 }} />
            <ScrollView
              scrollEnabled={false}
              keyboardShouldPersistTaps="always" style={{ width: width * 0.78, marginTop: 20 }}>

              <LottieView
                style={styles.image}
                imageAssetsFolder={'images/'}
                source={require('../../animation/weatherman_main.json')}
                autoPlay
                loop
              />

              <Text style={{ fontSize: 22, textAlign: 'center', color: theme.heading, marginTop: 40, marginBottom: 20 }}>
                If you have not already, please remove the battery insulated tab.
              </Text>

              <Button style={{ width: width * 0.5, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 30, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.props.navigation.navigate('ConnectWeathermanUmbrella')}>CONTINUE</Text></Button>

            </ScrollView>
          </SafeAreaView>
        </View >
      </Content >
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: width * 0.50,
    height: width * 0.50,
    alignSelf: 'center',
    resizeMode: 'cover',
    marginBottom: 20,
  }
});
