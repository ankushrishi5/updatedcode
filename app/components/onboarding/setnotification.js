import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, Modal, TouchableOpacity, Alert, StatusBar
} from 'react-native';
import { Container, Header, Content, Button, Text, Left, Body, ListItem } from 'native-base';
import Settings from '../../stores/settingsStore';
import FlipToggle from 'react-native-flip-toggle-button';
import MC from 'react-native-vector-icons/MaterialCommunityIcons'
import DeviceInfo from 'react-native-device-info';
import OneDateTimePicker from 'react-native-modal-datetime-picker';
import TwoDateTimePicker from 'react-native-modal-datetime-picker';
import theme from '../../theme/base-theme';
import FCM from "react-native-fcm";
import OpenAppSettings from 'react-native-app-settings'

var moment = require('moment-timezone');
var { DateTime } = require('luxon');

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const notify = require('../../img/notification.png')
const oorUmbrella = require('../../img/darkUmbrella.png')
const loadingspinner = require('../../img/loading.gif')
const settings = new Settings()


export default class WorkLoc extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      uniqueId: '',
      firstlat: '',
      firstlon: '',
      selectedaddress: '',
      varianType: "",
      modalVisible: false,
      firstNotification: false,
      secondNotification: false,
      displayDays: "Mo,Tu,We,Th,Fr",
      oorAlerts: false,
      daysSunday: false,
      daysMonday: true,
      daysTuesday: true,
      daysWednesday: true,
      daysThursday: true,
      daysFriday: true,
      daysSaturday: false,
      timeOne: '',
      timeTwo: '',
      savetimeOne: '07:00',
      savetimeTwo: '16:00',
      isOneDateTimePickerVisible: false,
      isTwoDateTimePickerVisible: false,
      loading: false
    }
  }

  _showOneDateTimePicker = () => this.setState({ isOneDateTimePickerVisible: true });

  _hideOneDateTimePicker = () => this.setState({ isOneDateTimePickerVisible: false });

  _handleOneDatePicked = (date) => {
    var timeConverted = moment.tz(date, DeviceInfo.getTimezone()).format();
    var timeStamp = moment(timeConverted).format("h:mm a")

    this.setState({ savetimeOne: DateTime.fromISO(timeConverted).setZone('utc').minus({ minutes: 15 }).toLocaleString(DateTime.TIME_24_SIMPLE) });
    // this.setState({ timeOne: DateTime.fromISO(timeConverted).setZone(DeviceInfo.getTimezone()).toLocaleString(DateTime.TIME_SIMPLE) });
    this.setState({ timeOne: timeStamp });
    this._hideOneDateTimePicker();
  };

  _showTwoDateTimePicker = () => this.setState({ isTwoDateTimePickerVisible: true });

  _hideTwoDateTimePicker = () => this.setState({ isTwoDateTimePickerVisible: false });


  _handleTwoDatePicked = (date) => {
    var timeConverted = moment.tz(date, DeviceInfo.getTimezone()).format();
    var timeStamp = moment(timeConverted).format("h:mm a")
    this.setState({ savetimeTwo: DateTime.fromISO(timeConverted).setZone('utc').minus({ minutes: 15 }).toLocaleString(DateTime.TIME_24_SIMPLE) });
    // this.setState({ timeTwo: DateTime.fromISO(timeConverted).setZone(DeviceInfo.getTimezone()).toLocaleString(DateTime.TIME_SIMPLE) });
    this.setState({ timeTwo: timeStamp });
    this._hideTwoDateTimePicker();
  };

  componentWillMount() {
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.baseUrl = settings.BaseUrl;
    this.locateMe();
  }

  async  locateMe() {

    navigator.geolocation.getCurrentPosition(
      (position) => {
        if (position.coords && position.coords.latitude && position.coords.longitude) {
          this.setState({ firstlat: position.coords.latitude, firstlon: position.coords.longitude });
        }
      },
      (error) => console.log(error),
      { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
    );
  }

  prepareAddress(addy) {
    this.setState({ selectedaddress: addy });
  }

  validateField = (myname) => {
    let tName = myname.length.toString();
    if (tName > 0) {
      return true;
    }
    else return false;
  };

  validateSave() {
    this.setState({ loading: true });
   /* if (this.state.displayDays.length < 1) {
        Alert.alert(
                 'No days selected',
                 'Please enter the days you want to receive notifications.')
        this.setState({loading: false});
      }
   else if (this.state.timeOne.length == 0 && this.state.timeTwo.length == 0) {
    Alert.alert(
      'No times selected',
      'Please enter the days you want to receive notifications.')
this.setState({loading: false});
   }   
   else*/ if (1 == 1) {
      this.getMyId();
    }
  }

  async getMyId() {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification?filter[where][wdeviceid]=' + this.state.uniqueId, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        this.addNotificationSettings(parsed[0].id);


      } else {
        let errors = res;
        throw errors;
      }
    } catch (error) {
      this.setState({ errors: error });
      console.log(error);
    }
  }


  async addNotificationSettings(idToUpdate) {

    try {

      let response = await fetch(this.state.baseUrl + 'wn_notification/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wcurlat: this.state.firstlat,
          wcurlon: this.state.firstlon,
          wnotifyOOR: this.state.oorAlerts,
          wnotifySunday: this.state.daysSunday,
          wnotifyMonday: this.state.daysMonday,
          wnotifyTuesday: this.state.daysTuesday,
          wnotifyWednesday: this.state.daysWednesday,
          wnotifyThursday: this.state.daysThursday,
          wnotifyFriday: this.state.daysFriday,
          wnotifySaturday: this.state.daysSaturday,
          wnotify1: this.state.firstNotification,
          wnotifyTime1: this.state.savetimeOne,
          wnotifyTime1Show: this.state.timeOne,
          wnotify2: this.state.secondNotification,
          wnotifyTime2: this.state.savetimeTwo,
          wnotifyTime2Show: this.state.timeTwo,
        })
      });
      let res = await response.text();

      if (response.status >= 200 && response.status < 300) {
        this.setState({ loading: false });
        this.setState({ error: "" });
        let parsed = JSON.parse(res);
        this.props.navigation.navigate('Success');

      } else {
        let errors = res;
        throw errors;
      }

    } catch (errors) {
      let formErrors = JSON.parse(errors);
      let errorsArray = [];
      for (let key in formErrors) {
        if (formErrors[key].length > 1) {
          formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
        } else {
          errorsArray.push(`${key} ${formErrors[key]}`)
        }
      }
      this.setState({ errors: errorsArray });
    }
  }

  openModal(variant) {

    this.setState({ varianType: variant });

    this.setState({ modalVisible: true, subScreenLoad: false });
  }

  closeModal() {
    this.setState({ modalVisible: false, subScreenLoad: true });
  }

  dayToggler(day) {
    if (day == 'Sunday') {
      if (this.state.daysSunday == true) {
        this.setState({ daysSunday: false });
      }
      if (this.state.daysSunday == false) {
        this.setState({ daysSunday: true });
      }
    }
    else if (day == 'Monday') {
      if (this.state.daysMonday == true) {
        this.setState({ daysMonday: false });
      }
      if (this.state.daysMonday == false) {
        this.setState({ daysMonday: true });
      }
    }
    else if (day == 'Tuesday') {
      if (this.state.daysTuesday == true) {
        this.setState({ daysTuesday: false });
      }
      if (this.state.daysTuesday == false) {
        this.setState({ daysTuesday: true });
      }
    }
    else if (day == 'Wednesday') {
      if (this.state.daysWednesday == true) {
        this.setState({ daysWednesday: false });
      }
      if (this.state.daysWednesday == false) {
        this.setState({ daysWednesday: true });
      }
    }
    else if (day == 'Thursday') {
      if (this.state.daysThursday == true) {
        this.setState({ daysThursday: false });
      }
      if (this.state.daysThursday == false) {
        this.setState({ daysThursday: true });
      }
    }
    else if (day == 'Friday') {
      if (this.state.daysFriday == true) {
        this.setState({ daysFriday: false });
      }
      if (this.state.daysFriday == false) {
        this.setState({ daysFriday: true });
      }
    }
    else if (day == 'Saturday') {
      if (this.state.daysSaturday == true) {
        this.setState({ daysSaturday: false });
      }
      if (this.state.daysSaturday == false) {
        this.setState({ daysSaturday: true });
      }
    }
  }

  dayBuilder() {
    this.state.displayDays = ""
    if (this.state.daysSunday == true) {
      this.state.displayDays = this.state.displayDays + 'Su,';
    }
    if (this.state.daysMonday == true) {
      this.state.displayDays = this.state.displayDays + 'Mo,';
    }
    if (this.state.daysTuesday == true) {
      this.state.displayDays = this.state.displayDays + 'Tu,';
    }
    if (this.state.daysWednesday == true) {
      this.state.displayDays = this.state.displayDays + 'We,';
    }
    if (this.state.daysThursday == true) {
      this.state.displayDays = this.state.displayDays + 'Th,';
    }
    if (this.state.daysFriday == true) {
      this.state.displayDays = this.state.displayDays + 'Fr,';
    }
    if (this.state.daysSaturday == true) {
      this.state.displayDays = this.state.displayDays + 'Sa,';
    }
    this.state.displayDays = this.state.displayDays.substr(0, this.state.displayDays.length - 1)
    this.closeModal();
  }

  _renderTimeofDay(isVisible, time, showDateTimePicker) {
    if (isVisible) {
      return (
        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }} onPress={showDateTimePicker} >
          <Text style={{ color: theme.subTitle, fontWeight: "700" }}>Time of day</Text>
          <Text style={{ textAlign: 'right', color: theme.title }}>{time}</Text>
        </TouchableOpacity>
      )
    }
  }

  async setNotification(type) {
    let response = ""
    await FCM.requestPermissions().then(val => {
      return response = ""
    }).catch(err => {
      console.log(" get dssfssponse -- " + err)
      return response = err

    })// for iOS
    // this.
    if (response === "") {
      FCM.getFCMToken().then(token => {
        console.log(token, "token generated in set notification file");
        if (token) {
          this.props.UserActions.setDeviceToken(token)
          this.setState({ deviceToken: token })
        }
      });
      registerAppListener(this.props.navigation);
      this.props.UserActions.addNotification({ deviceToken: this.state.deviceToken })

    } else {
      this.setState({ firstNotification: false, secondNotification: false })
      Alert.alert(
        'Notification',
        "Please allow notifications from your mobile settings.",
        [
          { text: 'OK', onPress: () => this.openSettings(type) },
        ],
      )
    }

  }

  openSettings = (type) => {
    OpenAppSettings.open()
    // if (Platform.OS === 'ios') {
    //   Linking.openURL('app-settings:')

    // } else {
    //   Linking.openURL('app-settings:')
    //   // NativeModules.OpenNotification.open();
    //   // NotificationSetting.open();

    // }
  }

  render() {
    return (
      <Container style={{ backgroundColor: theme.brandWhite }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <Content style={{ width: width, height: height }}>
          <View style={{ width: width * 0.92, height: height, alignSelf: 'center', marginTop: 20 }}>
            <View style={{ width: width * 0.80, alignSelf: 'center', zIndex: 0 }}>
              <Image source={notify} style={{ alignSelf: 'center', height: 45, width: 45, marginBottom: 5 }} />
              <Text style={{ fontSize: 18, fontWeight: 'bold', color: theme.heading, marginTop: 10, marginBottom: 10, textAlign: 'center' }}>Notifications</Text>
              <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.grey, marginBottom: 20, textAlign: 'center' }}>So we can provide you with the most accurate weather and umbrella alerts</Text>
            </View>
            <ScrollView keyboardShouldPersistTaps={"always"} scrollEnabled={false} style={{ width: width * 0.92, backgroundColor: 'rgba(256, 256, 256, 1)' }}>
              <View>
                <View style={styles.line_separator} />
                <TouchableOpacity style={{ flexDirection: 'row', padding: 18, justifyContent: 'space-between' }} onPress={() => this.openModal('days')} >
                  <Text style={{ color: theme.title, fontWeight: '500' }}>Days</Text>
                  <Text style={{ textAlign: 'right', color: theme.subTitle }}>{this.state.displayDays}</Text>
                </TouchableOpacity>
                <View style={styles.line_separator} />

                <View style={{ padding: 18 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: theme.title, fontSize: 16, fontWeight: '500' }}>First notification</Text>
                    <FlipToggle
                      value={this.state.firstNotification}
                      buttonWidth={45}
                      buttonHeight={17}
                      buttonRadius={50}
                      sliderWidth={25}
                      sliderHeight={25}
                      sliderOffColor={'#ecebf2'}
                      sliderOnColor={theme.sliderOn}
                      buttonOffColor={'#aeaeae'}
                      buttonOnColor={theme.buttonOn}
                      onToggle={(value) => {
                        this.setState({ firstNotification: value }, () => {
                          this.state.firstNotification ? this.setNotification("firstNotify") : null
                        })
                      }}
                    />
                  </View>
                  {this._renderTimeofDay(this.state.firstNotification, this.state.timeOne, this._showOneDateTimePicker)}
                </View>
                <View style={styles.line_separator} />

                <View style={{ padding: 18 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: theme.title, fontSize: 16, fontWeight: '500' }}>Second notification</Text>
                    <FlipToggle
                      value={this.state.secondNotification}
                      buttonWidth={45}
                      buttonHeight={17}
                      buttonRadius={50}
                      sliderWidth={25}
                      sliderHeight={25}
                      sliderOffColor={'#ecebf2'}
                      sliderOnColor={theme.sliderOn}
                      buttonOffColor={'#aeaeae'}
                      buttonOnColor={theme.buttonOn}
                      onToggle={(value) => {
                        this.setState({ secondNotification: value }, () => {
                          this.state.secondNotification ? this.setNotification("secondNotify") : null
                        });
                      }}
                    />
                  </View>
                  {this._renderTimeofDay(this.state.secondNotification, this.state.timeTwo, this._showTwoDateTimePicker)}
                </View>
                <View style={styles.line_separator} />

                <View style={{ flexDirection: 'row', paddingTop: 20, width: "100%", paddingBottom: 10 }}>
                  <View style={{ width: "12%" }}>
                    <Image source={oorUmbrella} style={{ height: 30, width: 30 }} />
                  </View>
                  <View style={{ width: "65%" }}>
                    <Text style={{ fontSize: 15, color: theme.title, fontWeight: '600' }}>Receive umbrella out-of-range alerts</Text>
                    <Text style={{ fontSize: 13, color: theme.subTitle, marginTop: 5 }}>We’ll let you know that you may have left your umbrella behind.</Text>
                  </View>
                  <View style={{ width: "18%", alignItems: 'flex-end' }}>
                    <FlipToggle
                      value={this.state.oorAlerts}
                      buttonWidth={45}
                      buttonHeight={17}
                      buttonRadius={50}
                      sliderWidth={25}
                      sliderHeight={25}
                      sliderOffColor={'#ecebf2'}
                      sliderOnColor={theme.sliderOn}
                      buttonOffColor={'#aeaeae'}
                      buttonOnColor={theme.buttonOn}
                      onToggle={(value) => {
                        this.setState({ oorAlerts: value });
                      }}
                    />
                  </View>
                </View>
                <View style={styles.line_separator} />
              </View>

              <View style={{ marginTop: 20, alignItems: 'center' }}>
                {this.state.loading == true ? <Image source={loadingspinner} style={{ alignSelf: 'center', height: 75, width: 75 }} /> :
                  <Button style={{ width: width * 0.75, justifyContent: 'center', backgroundColor: theme.brandPrimary, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.validateSave()}>SAVE NOTIFICATION</Text></Button>
                }
              </View>

              <View style={{ alignItems: 'center' }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Success')}>
                  <Text style={{ fontSize: 16, fontWeight: 'bold', color: theme.grey, marginTop: 20 }}>SKIP</Text>
                </TouchableOpacity>
              </View>

              <Modal
                visible={this.state.modalVisible}
                animationType={'slide'}
                transparent={true}
                onRequestClose={() => this.closeModal()}>
                <View style={{ width: width, height: height, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems: 'center' }}>
                  {this.state.varianType == 'days' ? <View style={{ width: width * 0.82, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 60, borderWidth: 1, borderRadius: 10 }}>
                    <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(179,179,179,1)', borderBottomWidth: 0.5, alignItems: 'center' }}>
                      <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5 }}>Days</Text>
                    </View>
                    <View>
                      <ListItem onPress={() => this.dayToggler('Sunday')}>
                        <Left>
                          {this.state.daysSunday ? <MC name="checkbox-marked-outline" size={30} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={30} color={theme.brandPrimary} />}
                        </Left>
                        <Body>
                          <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily }}>Sunday</Text>
                        </Body>
                      </ListItem>
                      <ListItem onPress={() => this.dayToggler('Monday')}>
                        <Left>
                          {this.state.daysMonday ? <MC name="checkbox-marked-outline" size={30} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={30} color={theme.brandPrimary} />}
                        </Left>
                        <Body>
                          <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily }}>Monday</Text>
                        </Body>
                      </ListItem>
                      <ListItem onPress={() => this.dayToggler('Tuesday')}>
                        <Left>
                          {this.state.daysTuesday ? <MC name="checkbox-marked-outline" size={30} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={30} color={theme.brandPrimary} />}
                        </Left>
                        <Body>
                          <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily }}>Tuesday</Text>
                        </Body>
                      </ListItem>
                      <ListItem onPress={() => this.dayToggler('Wednesday')}>
                        <Left>
                          {this.state.daysWednesday ? <MC name="checkbox-marked-outline" size={30} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={30} color={theme.brandPrimary} />}
                        </Left>
                        <Body>
                          <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily }}>Wednesday</Text>
                        </Body>
                      </ListItem>
                      <ListItem onPress={() => this.dayToggler('Thursday')}>
                        <Left>
                          {this.state.daysThursday ? <MC name="checkbox-marked-outline" size={30} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={30} color={theme.brandPrimary} />}
                        </Left>
                        <Body>
                          <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily }}>Thursday</Text>
                        </Body>
                      </ListItem>
                      <ListItem onPress={() => this.dayToggler('Friday')}>
                        <Left>
                          {this.state.daysFriday ? <MC name="checkbox-marked-outline" size={30} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={30} color={theme.brandPrimary} />}
                        </Left>
                        <Body>
                          <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily }}>Friday</Text>
                        </Body>
                      </ListItem>
                      <ListItem onPress={() => this.dayToggler('Saturday')}>
                        <Left>
                          {this.state.daysSaturday ? <MC name="checkbox-marked-outline" size={30} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={30} color={theme.brandPrimary} />}
                        </Left>
                        <Body>
                          <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily }}>Saturday</Text>
                        </Body>
                      </ListItem>
                    </View>
                    <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
                      <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                        <Text style={{ fontSize: 18, marginTop: 12, color: theme.brandPrimary }}>Cancel</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.dayBuilder()}>
                        <Text style={{ fontSize: 18, marginTop: 12, color: theme.brandPrimary }}>Save</Text>
                      </TouchableOpacity>
                    </View>
                  </View> : null}
                </View>
              </Modal>
              <OneDateTimePicker
                titleIOS='Select a time'
                mode='time'
                isVisible={this.state.isOneDateTimePickerVisible}
                onConfirm={this._handleOneDatePicked}
                onCancel={this._hideOneDateTimePicker}
              />
              <TwoDateTimePicker
                titleIOS='Select a time'
                mode='time'
                isVisible={this.state.isTwoDateTimePickerVisible}
                onConfirm={this._handleTwoDatePicked}
                onCancel={this._hideTwoDateTimePicker}
              />
            </ScrollView>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  line_separator: {
    width: "100%",
    height: 0.5,
    backgroundColor: theme.separator
  }
});