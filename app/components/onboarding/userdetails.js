import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, TouchableOpacity, StatusBar, Alert, SafeAreaView
} from 'react-native';
import { Header, Content, Text, InputGroup, Input } from 'native-base';
import Settings from '../../stores/settingsStore'
import theme from '../../theme/base-theme';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';
import Validations from '../../utilities/Regex'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')
const loadingspinner = require('../../img/loading.gif')
const check_box = require('../../img/check_box.png')
const uncheck_box = require('../../img/uncheck_box.png')
const settings = new Settings()

const sub_text_color = '#53595f'
const line_color = '#c4c8cc'

class UserDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      baseUrl: '',
      fname: '',
      lname: '',
      email: '',
      marketing: true,
      indicatorHeight: 'close',
      loading: false,
      emailValidation: 'New',
    }
  }

  componentWillMount() {
    this.state.baseUrl = settings.BaseUrl;
    //

  }

  uhoh() {
    Alert.alert('Uh Oh!', 'Something went wrong. Please make sure you are connected to the internet and try again.');
    this.setState({ loading: false });
  }

  validateEmail = (email) => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  validateField = (myname) => {
    let tName = myname.length.toString();
    if (tName > 0) {
      return true;
    }
    else return false;
  };

  validateLength = (myname) => {
    let tName = myname.length.toString();
    if (tName >= 2) {
      return true;
    }
    else return false;
  };

  marketingSelect() {

    this.setState({ marketing: !this.state.marketing });

  }

  async emailUnique() {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_users/count?[where][wEmail]=' + this.state.email, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();

      if (response.status >= 200 && response.status < 300) {

        this.setState({ error: "" });
        let parsed = JSON.parse(res)
        let count = parsed.count;


        if (count > 0) {
          //console.log ("Email is taken");
          this.state.emailValidation = "Taken"

        }
        else {
          //
          this.state.emailValidation = "New"
        }

      } else {
        let errors = res;
        throw errors;
      }

    } catch (error) {
      this.setState({ errors: error });
      //
    }
  }

  onRegisterClick() {
    const { fname, lname, email } = this.state;
    if (fname == '') {
      alert("Please enter first name");
      return;
    } else if (lname == '') {
      alert("Please enter last name");
      return;
    } else if (email == '') {
      alert("Please enter email");
      return;
    } else if (Validations.validateEmail(email) == false) {
      alert("Please enter valid email");
    } else
      this.setState({ loading: true });
    //commented as not using email as unique value
    //this.emailUnique();
    // setTimeout(() => { this.onRegisterAfterWaiting(); }, 1000)
    this.onRegisterAfterWaiting();
  }

  onRegisterAfterWaiting = async () => {

    if (!this.validateField(this.state.fname)) {
      Alert.alert(
        'Sign up',
        'Please enter your First Name.')
      this.setState({ loading: false });
    }

    else if (!this.validateLength(this.state.fname)) {
      Alert.alert(
        'Sign up',
        'Your First Name must be at least 2 characters.')
      this.setState({ loading: false });
    }

    else if (!this.validateField(this.state.lname)) {
      Alert.alert(
        'Sign up',
        'Please enter your Last Name.')
      this.setState({ loading: false });
    }

    else if (!this.validateLength(this.state.lname)) {
      Alert.alert(
        'Sign up',
        'Your Last Name must be at least 2 characters.')
      this.setState({ loading: false });
    }

    else if (!this.validateField(this.state.email)) {
      Alert.alert(
        'Sign up',
        'Please enter your email adress.')
      this.setState({ loading: false });
    }

    else if (!this.validateEmail(this.state.email)) {
      Alert.alert(
        'Sign Up',
        'This email is invalid.')
      this.setState({ loading: false });
    }
    //  else if (this.state.emailValidation === "Taken") {
    //   Alert.alert(
    //     'Sign Up',
    //     'This email is already taken.')
    //   this.setState({loading: false});
    //  }

    else {
      this.state.email = this.state.email.toLowerCase();

      if (this.state.marketing) {
        await this.addToAppListMailChimp(this.state.fname, this.state.lname, this.state.email);
        await this.addToMainListMailChimp(this.state.fname, this.state.lname, this.state.email);
      }
      this.props.UserActions.registerUser({ ...this.state });
      //this.props.navigation.navigate('LocationMain');
    }
  }

  async addToMainListMailChimp(fname, lname, email) {

    try {

      let response = await fetch(settings.wmmcurl + 'members', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': settings.mckey
        },
        body: JSON.stringify({
          email_address: email,
          status: "subscribed",
          merge_fields: {
            FNAME: fname,
            LNAME: lname
          }
        })
      });
      let res = await response.text();
      //

      if (response.status >= 200 && response.status < 300) {


      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {

    }
  }

  async addToAppListMailChimp(fname, lname, email) {

    try {

      let response = await fetch(settings.appmcurl + 'members', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': settings.mckey
        },
        body: JSON.stringify({
          email_address: email,
          status: "subscribed",
          merge_fields: {
            FNAME: fname,
            LNAME: lname
          }
        })
      });
      let res = await response.text();
      //

      if (response.status >= 200 && response.status < 300) {


      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {

    }
  }


  render() {
    return (
      <Content style={{ width: width, height: height }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>

            <Image source={mylogo} style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 40, marginBottom: 5 }} />
            <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: 10, color: theme.heading }}>Welcome to Weatherman</Text>
            <Text style={styles.sub_text}>{'\n'}Let's get to know each other.{'\n'} What's your name?</Text>

            <ScrollView
              scrollEnabled={false}
              keyboardShouldPersistTaps="always" style={{ width: width * 0.78, marginTop: 20 }}>

              <InputGroup style={styles.input_group} boarderType='round'>
                <Input style={{ color: '#000' }}
                  maxLength={20}
                  placeholder='First Name'
                  autoCorrect={false}
                  autoCapitalize={'words'}
                  clearButtonMode='while-editing'
                  placeholderTextColor={theme.grey}
                  onChangeText={(val) => { this.setState({ fname: val }) }} />
              </InputGroup>

              <InputGroup style={styles.input_group} boarderType='round'>
                <Input style={{ color: "#000" }}
                  maxLength={20}
                  placeholder='Last Name'
                  autoCorrect={false}
                  autoCapitalize={'words'}
                  clearButtonMode='while-editing'
                  placeholderTextColor={theme.grey}
                  onChangeText={(val) => { this.setState({ lname: val }) }} />
              </InputGroup>

              <InputGroup style={styles.input_group} boarderType='round'>
                <Input style={{ color: "#000" }}
                  keyboardType='email-address'
                  autoCorrect={false}
                  autoCapitalize={'none'}
                  clearButtonMode='while-editing'
                  placeholder='Email'
                  placeholderTextColor={theme.grey}
                  onChangeText={(val) => { this.setState({ email: val }) }} />
              </InputGroup>

              <View style={{ marginTop: 15, flexDirection: 'row' }}>
                <TouchableOpacity onPress={() => this.marketingSelect()} >
                  {
                    this.state.marketing ?
                      <Image source={check_box} style={styles.check_box_style} />
                      :
                      <Image source={uncheck_box} style={styles.check_box_style} />
                  }
                </TouchableOpacity>
                <Text style={styles.check_box_text}>Get Weatherman news and updates</Text>
              </View>

              {this.state.loading == true ?
                <Image source={loadingspinner} style={{ alignSelf: 'center', height: 75, width: 75 }} />
                :
                <TouchableOpacity style={{
                  width: "100%", height: 40, marginTop: 20, backgroundColor: theme.brandPrimary,
                  borderRadius: 10, justifyContent: 'center', alignItems: 'center'
                }} onPress={() => this.onRegisterClick()}>
                  <Text style={{ color: 'white' }}>SUBMIT</Text>

                </TouchableOpacity>
              }
              {/* <Button style={{ width: width * 0.5, justifyContent: 'center', backgroundColor: theme.brandPrimary, alignSelf: 'center', borderRadius: 10 }}><Text >SUBMIT</Text></Button> */}
            </ScrollView>
          </SafeAreaView>
        </View>
      </Content>

    );
  }
}

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch)
});

const styles = StyleSheet.create({

  input_group: {
    marginBottom: 15,
    borderBottomWidth: 2,
    borderColor: line_color
  },
  sub_text: {
    fontSize: 16,
    color: sub_text_color,
    textAlign: 'center'
  },
  check_box_text: {
    fontSize: 16,
    color: sub_text_color,
    textAlign: 'center',
    marginLeft: 10
  },
  check_box_style: {
    width: 20,
    height: 20
  }
});

export default connect(null, mapDispatchToProps)(UserDetails);