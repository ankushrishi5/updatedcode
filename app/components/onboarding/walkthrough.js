import React, { Component } from 'react';
import {
  Platform,
  View, Image, Dimensions, TouchableOpacity, StatusBar
} from 'react-native';
import { Container, Header, Text } from 'native-base';
import Swiper from 'react-native-swiper'
import theme from '../../theme/base-theme'
import LottieView from 'lottie-react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const mylogo = require('../../img/wxlogo.png')
const next_color = '#4281e5';
const dot_color = '#d3dee8';
const active_dot_color = '#92adc4';

export default class Walkthrough extends Component {
  constructor(props) {
    super(props);

    this.state = {
      swipeIndex: 0,
    }
  }

  changeIndex(myIndex) {
    this.setState({
      swipeIndex: myIndex
    })

  }

  render() {

    return (
      <Container style={{ width: width, height: height, backgroundColor: '#fff' }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <Image source={mylogo} style={{ alignSelf: 'center', height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 20, marginBottom: 5 }} />
        <Swiper style={styles.wrapper}
          onMomentumScrollEnd={(e, state, context) => this.changeIndex(state.index)}
          showsPagination={true}
          showsButtons={true}
          buttonWrapperStyle={{ backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'flex-end', marginTop: 12 }}
          dotStyle={{ width: 10, height: 10, backgroundColor: dot_color, borderRadius: 5 }}
          activeDotStyle={{ width: 12, height: 12, backgroundColor: active_dot_color, borderRadius: 10 }}
          nextButton={<Text style={{ color: next_color, fontSize: 18 }}>Next</Text>}
          prevButton={<Text style={{ color: next_color, fontSize: 1 }}></Text>}
          loop={false}
          paginationStyle={{
            bottom: 0, left: 0, right: 0
          }} >

          <View style={styles.slide}>
            <LottieView
              style={styles.image}
              imageAssetsFolder={'images/'}
              source={require('../../animation/animation_cities.json')}
              autoPlay
              loop
            />
            <View style={styles.slide_lowerView}>
              <Text style={styles.heading}> Always be in the know{'\n'} </Text>
              <Text style={styles.subHeading}>
                See live weather conditions for your favorite locations and look ahead with hourly and extended seven-day forecasts
              </Text>
            </View>
          </View>

          <View style={styles.slide}>
            <LottieView
              style={styles.image}
              imageAssetsFolder={'images/'}
              source={require('../../animation/animation_weather.json')}
              autoPlay
              loop
            />
            <View style={styles.slide_lowerView}>
              <Text style={styles.heading}> Never get caught in the rain again{'\n'} </Text>
              <Text style={styles.subHeading}>
                Get accurate weather alerts so you know to take your umbrella before you go
               </Text>
            </View>
          </View>

          <View style={styles.slide}>
            <LottieView
              style={styles.image}
              imageAssetsFolder={'images/'}
              source={require('../../animation/animation_umbrella.json')}
              autoPlay
              loop
            />
            <View style={styles.slide_lowerView}>
              <Text style={styles.heading}>Find your lost umbrella{'\n'} </Text>
              <Text style={styles.subHeading}>
                Connect to your Weatherman and see where you last left your umbrella
               </Text>
            </View>
          </View>
        </Swiper>

        {
          this.state.swipeIndex === 2 ?
            <TouchableOpacity style={{ width: "90%", marginBottom: 30, marginTop: -20, alignSelf: 'center', alignItems: 'flex-end' }} onPress={() => this.props.navigation.navigate('UserDetails')}>
              <Text style={{ color: next_color, fontSize: 18, fontWeight: 'bold' }}>GET STARTED!</Text>
            </TouchableOpacity>
            :
            <TouchableOpacity style={{ width: "40%", marginBottom: 30, marginTop: -20, marginLeft: 10 }} onPress={() => this.props.navigation.navigate('UserDetails')} >
              <Text style={{ color: theme.grey, fontSize: 18 }}>Skip</Text>
            </TouchableOpacity>
        }

      </Container >
    );
  }
}

const styles = {

  wrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 0,
  },
  slide: {
    height: height * 0.75,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: width * 0.65,
    height: width * 0.65,
    marginTop: -10,
    alignItems: 'center',
    resizeMode: 'contain',
  },
  slide_lowerView: {
    marginTop: 20,
    marginLeft: 30,
    marginRight: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  heading: {
    fontSize: 20,
    fontWeight: 'bold',
    color: theme.heading
  },
  subHeading: {
    fontSize: 14,
    color: theme.grey,
    textAlign: 'center'
  }
}