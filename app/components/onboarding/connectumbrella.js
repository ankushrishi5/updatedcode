import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, FlatList, TouchableOpacity, SafeAreaView, Alert, StatusBar,
} from 'react-native';
import { Container, Header, Button, Text } from 'native-base';
import * as Progress from 'react-native-progress';
import theme from '../../theme/base-theme';
// import RNPbWrapper from 'react-native-pb-wrapper';
import LottieView from 'lottie-react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')
const tick = require('../../img/tick_icon.png')
const cross = require('../../img/cross_icon.png')

export default class ConnectUmbrella extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isScanning: false,
      isPairing: false,
      isCancelled: false,
      scannedDevices: [{ id: 1, status: "Ready to Pair" }, { id: 2, status: "Already Paired" }],
      pairedDevices: [{ id: 1, status: "Ready to Pair" }],
      target: null,
    }
  }

  componentDidMount() {
    this.onScanStarted();

  }

  onScanStarted() {

    this.setState({ isScanning: true });

    setTimeout(() => {
      this.setState({ isScanning: false });
    }, 4000);
  }

  onCancelClick() {

    if (this.state.isScanning) {
      this.props.navigation.goBack(null);
    } else if (this.state.isPairing) {
      this.setState({ isPairing: false, isCancelled: true })
    }
  }

  selectDevice(id) {
    let deviceData = this.state.scannedDevices
    if (this.state.target === id) {
      deviceData[id].isSelected = false
      this.setState({ target: null })
    } else {
      if (this.state.target === null) {
        deviceData[id].isSelected = true
        this.setState({ target: id })
      }
      else {
        deviceData[this.state.target].isSelected = false
        deviceData[id].isSelected = true
        this.setState({ target: id })
      }
    }
    this.setState({ scannedDevices: deviceData })

  }

  connect(devices) {

    if (this.state.target == null) {

      Alert.alert(
        'Device',
        'Please select device to connect.')

    } else {
      this.setState({ isPairing: true, isCancelled: false });
      setTimeout(() => {
        if (!this.state.isCancelled) {
          this.onPairingCompleted();
        }
      }, 4000);

    }

  }

  onPairingCompleted() {
    if (this.state.pairedDevices == null || this.state.pairedDevices.length == 0) {
      this.setState({ isPairing: false, isFailed: true, isSuccess: false });
    }
    else {
      this.setState({ isPairing: false, isSuccess: true, isFailed: false });
    }

  }

  onTryAgain() {

    this.setState({
      isScanning: false,
      isPairing: false,
      isSuccess: false,
      isFailed: false
    })
  }

  onNextStep() {
    this.props.navigation.navigate('UmbrellaName')
    this.setState({
      isScanning: false,
      isPairing: false,
      isSuccess: false,
      isFailed: false
    })
  }

  row(image, text) {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, marginLeft: 5 }}>
        <Image source={image}
          style={{ height: 15, width: 15, resizeMode: 'contain', margin: 5 }} />
        <Text style={{ fontSize: 15, marginLeft: 5 }}>{text}</Text>
      </View>
    );
  }


  render() {
    return (
      <Container style={{ backgroundColor: theme.brandWhite }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>
            <Image source={mylogo}
              style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 30, marginBottom: 5 }} />

            {
              this.state.isScanning || this.state.isPairing ?
                <View>
                  <View style={{ width: width * 0.8, height: height * 0.25, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                    <LottieView
                      style={styles.image}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/pebblebee_connect.json')}
                      autoPlay
                      loop
                    />

                  </View>
                  <View style={{ width: width * 0.8, height: height * 0.35, marginTop: 20, alignItems: 'center' }}>
                    <View style={{ height: "60%" }}>
                      <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 10, marginBottom: 10 }}>
                        {this.state.isScanning ? 'Scanning your devices' : 'Pairing your devices'}
                      </Text>
                      <Progress.Bar style={{ marginTop: 30 }} width={width * 0.65} height={2} indeterminate={true} useNativeDriver={true} color={theme.brandPrimary} />
                    </View>
                    <View style={{ height: "40%", justifyContent: 'flex-end' }}>
                      <TouchableOpacity onPress={() => this.onCancelClick()}>
                        <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 10, marginBottom: 10 }}>
                          Cancel
                       </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                :
                null}

            {
              !this.state.isScanning && !this.state.isPairing && !this.state.isFailed && !this.state.isSuccess ?
                <View>
                  <View style={{ width: width * 0.8, height: height * 0.25, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                    <LottieView
                      style={styles.image}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/pebblebee_connect.json')}
                      autoPlay
                      loop
                    />

                  </View>
                  <View style={{ width: width * 0.8, height: height * 0.35 }}>

                    <FlatList
                      style={{ marginBottom: 20, height: "60%" }}
                      data={this.state.scannedDevices}
                      renderItem={({ item, index }) => {
                        console.log(' item ', item)
                        return (

                          <TouchableOpacity style={{ flexDirection: "row", paddingVertical: 10, paddingHorizontal: 10 }} onPress={() => this.selectDevice(index)}>
                            <View style={{ width: 20, height: 20, borderRadius: 20, backgroundColor: "white", borderColor: item.isSelected ? theme.sliderOn : "grey", borderWidth: 6, alignSelf: "center" }} />
                            <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, textAlign: 'center', alignSelf: "center", marginHorizontal: 15 }}>Device {index} - {item.status}  </Text>
                          </TouchableOpacity>
                        )
                      }}
                      keyExtractor={(item, index) => index.toString()}
                      extraData={this.state}
                    />
                    <View style={{ height: "40%" }}>
                      <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 10, marginBottom: 10 }}>
                        Select device(s) you would like to connect.
                      </Text>

                      <Button style={{ width: width * 0.5, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 20, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.connect(this.state.scannedDevices)}>CONNECT</Text></Button>

                    </View>
                  </View>
                </View>
                :
                null
            }

            {
              this.state.isFailed ?
                <View>
                  <View style={{ width: width * 0.8, height: height * 0.25, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                    <LottieView
                      style={styles.image}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/pebblebee_fail.json')}
                      autoPlay
                      loop
                    />

                  </View>
                  <View style={{ width: width * 0.7, height: height * 0.35 }}>
                    <Text style={{ fontSize: 20, textAlign: 'center', color: 'red', marginTop: 5, marginBottom: 5 }}>
                      Pairing Failed
                  </Text>
                    {this.row(tick, "Bluetooth enabled on phone")}
                    {this.row(cross, "Tracker within range")}
                    {this.row(cross, "Tracker turned on")}

                    <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 30, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.onTryAgain()}>TRY AGAIN</Text></Button>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('LocationMain')}>
                      <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 15, marginBottom: 10 }}>
                        Skip
                    </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                : null
            }

            {
              this.state.isSuccess ?

                <View>
                  <View style={{ width: width * 0.8, height: height * 0.25, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                    <LottieView
                      style={styles.image}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/pebblebee_success.json')}
                      autoPlay
                      loop
                    />

                  </View>

                  <View style={{ width: width * 0.8, height: height * 0.35 }}>
                    <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 15, marginBottom: 5 }}>
                      Pairing Complete!!
                  </Text>

                    <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 15, marginBottom: 10 }}>
                    Please don't forget to place the tracker in the zippered pocket of your umbrella or attach to the item you do not want to lose.
                  </Text>

                    <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 30, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.onNextStep()}>NEXT STEP</Text></Button>

                  </View>
                </View>
                : null
            }

          </SafeAreaView>
        </View >
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: width * 0.50,
    height: width * 0.40,
    alignSelf: 'center',
    resizeMode: 'cover',
    marginBottom: 20,
  }
});