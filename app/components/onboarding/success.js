import React, { Component } from 'react';
import {
  View, Image, StatusBar, Dimensions
} from 'react-native';
import { Container, Text } from 'native-base';
import AnimatedLinearGradient from 'react-native-animated-linear-gradient'
import { connect } from "react-redux";

const mylogo = require('../../img/logo_white.png')
var height = Dimensions.get('window').height; //full height
var width = Dimensions.get('window').width; //full width

const bottom = '#7ac5ee';
const top = '#2e61c1';

const navColors = {
  backgroundColor: [
    top, bottom
  ]
};


const direction = {

  vertical: '{start: {x: 0, y: 0.4}, end: {x: 1, y: 0.6}}',
  slant: '{start: {x: 1, y: 0}, end: {x: 0, y: 1}}'
};

class Success extends Component {
  constructor(props) {
    super(props);

  }

  componentDidMount() {
    setTimeout(() => { this.props.navigation.navigate('Home'); }, 3000)
  }


  render() {
    return (
      <Container style={{ backgroundColor: bottom }}>

        <AnimatedLinearGradient customColors={navColors.backgroundColor} speed={10000} points={direction.vertical} />

        <StatusBar backgroundColor={top} barStyle='light-content' />

        <View style={{ height: height, width: width * 0.7, justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
          <Image source={mylogo} resizeMode='contain' style={{ height: 100, width: 200 }} />
          <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', marginTop: 40 }} >
            You’re all set up!
            </Text>
          <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', marginTop: 10 }} >
            Let’s get to it.
            </Text>
        </View>

      </Container >
    );
  }
}

const mapStateToProps = (state) => ({
  getUnitData: state.user.getUserIdState,
  userData: state.user.userDetails
})

export default connect(mapStateToProps, null)(Success);
