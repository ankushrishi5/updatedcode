/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  DeviceEventEmitter,
  NativeEventEmitter,
  AppState,
  Alert,
  ActivityIndicator,
  Dimensions,
  NativeModules,
} from 'react-native';
// import RNPbWrapper from 'react-native-pb-wrapper';
var {height, width} = Dimensions.get('window');

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      number: 0,
      deviceArray: [],
      isDeviceAvailable: false,
      connectedMacAddress: '',
      appState: AppState.currentState,
      isBlutoothEnabled: false,
      isResetEnabled: true,
      isScanning:false,
    }
  }

  componentDidMount() {
    if( Platform.OS === 'ios'){
      this.setListenersForiOS();
    }else{
      this.setListeners();
    }    
  }

  setListenersForiOS(){
    // const myModuleEvt = new NativeEventEmitter(NativeModules.RNPbWrapper)
    // myModuleEvt.addListener('ON_DEVICE_ADDED', (data) => {
    //   var dArray = this.state.deviceArray;
    //   dArray.push(data);
    //   this.setState({ deviceArray: dArray, isResetEnabled: false  });
    // })
  }

  setListeners() {   
    AppState.addEventListener('change', this._handleAppStateChange);
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_DEVICE_ADDED, (data) => {
    //   var dArray = this.state.deviceArray;
    //   dArray.push(data);
    //   this.setState({ deviceArray: dArray, isResetEnabled: false  });
    // });
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_RSS_SIGNAL_CHANGED, (data) => {
    //   let dArray = this.state.deviceArray;
    //   dArray = dArray.map((item) => {
    //     if (item.macAddress == data.macAddress) {
    //       return data;
    //     } else {
    //       return item;
    //     }
    //   });
    //   this.setState({ deviceArray: dArray });
    // });
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_DEVICE_REMOVED, (data) => {
    //   let dArray = this.state.deviceArray;
    //   let index = -1;
    //   for (var i = 0; i < dArray.length; i++) {
    //     var element = dArray[i];
    //     if (data.rss_signal == element.rss_signal) {
    //       index = i;
    //     }
    //   }
    //   if (index != 1) {
    //     dArray.splice(index, 1);
    //   }
    //   this.setState({ deviceArray: dArray });
    // });
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STARTED, (data) => {
    //     this.setState({isScanning:true});
    // });
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_SCANNING_SUCCESS, (data) => {
    //     this.setState({isScanning:false});
    // });
    //  DeviceEventEmitter.addListener(RNPbWrapper.ON_SCANNING_FAILED, (data) => {
    //     this.setState({isScanning:false});
    //     alert(RNPbWrapper.ON_SCANNING_FAILED);
    // });
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_ON, (data) => {
    //     alert(RNPbWrapper.ON_BLUETOOTH_ON);
    // });
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_OFF, (data) => {
    //     alert(RNPbWrapper.ON_BLUETOOTH_OFF);
    // });
    // DeviceEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STOPPED, (data) => {
    //     this.setState({isScanning:false}); 
    // });    
 }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      // RNPbWrapper.onAttach();
    } else {
      console.log('App has come to the background!')
      // RNPbWrapper.onDettach();
    }
    this.setState({ appState: nextAppState });
  }

  render() {
    return (

      <View style={styles.container}>
        <Text style={{ fontSize: 20, alignSelf: 'center' }}>Available Devices</Text>
        {/*<Text style={{ fontSize: 20, alignSelf: 'center' }}>Connected Devices: {this.state.connectedMacAddress}</Text>*/}
        {this.renderDevices()}
        {this.renderScan()}
        {this.renderLoading()}
      </View>
    )
  }

  renderScan() {
    if (this.state.isResetEnabled)
      return (
        <TouchableOpacity onPress={() => this.onScanPressed()}
          style={{width: 100, height: 100, borderRadius: 50, backgroundColor: 'black', alignItems: 'center', justifyContent: 'center',marginTop:50 }}>
          <Text style={{ color: 'white', fontSize: 25 }}>SCAN</Text>
        </TouchableOpacity>
      );
  }

  onScanPressed() {
    // RNPbWrapper.isBlutoothEnabled((isBlutoothEnabled) => {
    //   this.setState({ isBlutoothEnabled: isBlutoothEnabled });
    //   if (!isBlutoothEnabled) {
    //     alert('Please enable blutooth');
    //   } else {
    //     RNPbWrapper.startScanning()
    //   }
    // });
  }

  renderDevices() {
    return this.state.deviceArray.map((data) => {
      return (
        <View style={{ alignSelf: 'stretch', backgroundColor: 'tan', marginTop: 5 }}>
          <TouchableOpacity style={{ alignSelf: 'stretch', backgroundColor: 'tan', marginTop: 5 }}
            onPress={() => {
              // RNPbWrapper.connectWithDevice(data.macAddress, (macAddress) => { console.log('Connected mac address: ', macAddress); this.setState({ connectedMacAddress: macAddress }) });
              this.setState({ isDeviceAvailable: true });
            }} 
            >
            <Text style={styles.itemStyle}>{data.macAddress} {data.rss_signal}
            </Text></TouchableOpacity>
          {this.state.isDeviceAvailable ? <View style={{ flexDirection: 'row', marginTop: 5 }}>
            {/* <TouchableOpacity style={{ flex: 1 }} onPress={() => { 
              RNPbWrapper.beepDevice(data.macAddress) 
              }}><Text style={styles.textStyle}>BEEP</Text></TouchableOpacity>
            <TouchableOpacity style={{ flex: 1 }} onPress={() => { 
              RNPbWrapper.startLightDevice(data.macAddress)
               }}><Text style={styles.textStyle}>FLASH</Text></TouchableOpacity> */}
          </View> : null}
       
        </View>
      )
    })
  }

  renderLoading(){
    if(this.state.isScanning)
      return(<TouchableOpacity onPress={()=>this.setState({isScanning:false})}
       style={styles.loader}>
      <ActivityIndicator size="large" color="#00ff00"/>
      </TouchableOpacity>)
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#3b5998',
  },
  textStyle: {
    fontSize: 20,
    color: 'red',
    textAlign: 'center',
    padding: 10,
    backgroundColor: '#fff',
    margin: 5,
    borderRadius: 10
  },
  itemStyle: {
    fontSize: 20,
    color: 'red',
    textAlign: 'center',
    padding: 10,
    alignSelf: 'stretch',
    backgroundColor: 'black',
    margin: 5
  },
  loader:{
    width:width,
    height:height,
    position:'absolute',
    backgroundColor:'#00000080',
    alignItems:'center',
    alignSelf:'center',
    justifyContent:'center'
  }
});
