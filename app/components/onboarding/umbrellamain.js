import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, TouchableOpacity, SafeAreaView, StatusBar
} from 'react-native';
import { Header, Content, Text } from 'native-base';
import LottieView from 'lottie-react-native';
import theme from '../../theme/base-theme';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')


export default class UmbrellaMain extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <Content style={{ width: width, height: height }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>
            <Image source={mylogo}
              style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 40, marginBottom: 5 }} />
            <ScrollView
              scrollEnabled={false}
              keyboardShouldPersistTaps="always" style={{ width: width * 0.78, marginTop: 20 }}>

              <LottieView
                style={styles.image}
                imageAssetsFolder={'images/'}
                source={require('../../animation/umbrella_ongoing.json')}
                autoPlay
                loop
              />

              <Text style={{ fontSize: 22, textAlign: 'center', color: theme.heading, marginTop: 20, marginBottom: 20 }}>
                Would you like to connect your Weatherman umbrella?
              </Text>

              <View style={{ width: '100%', flexDirection: 'row', alignSelf: 'center', marginTop: 40 }}>

                <View style={{ width: "50%", alignItems: 'center' }}>
                  <TouchableOpacity style={{
                    width: 90, height: 45, borderRadius: 5, borderWidth: 2, borderColor: '#8497ab',
                    justifyContent: 'center', alignItems: 'center'
                  }} onPress={() => this.props.navigation.navigate('LocationMain')}>
                    <Text style={{ color: '#092f57' }}>NO</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ width: "50%", alignItems: 'center' }}>
                  <TouchableOpacity style={{
                    width: 90, height: 45, backgroundColor: theme.brandPrimary,
                    borderRadius: 5, justifyContent: 'center', alignItems: 'center'
                  }} onPress={() => this.props.navigation.navigate('SelectUmbrella')} >
                    <Text style={{
                      color: 'white',
                    }}>YES</Text>
                  </TouchableOpacity>
                </View>

              </View>

            </ScrollView>
          </SafeAreaView>
        </View >
      </Content >
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: width * 0.60,
    height: width * 0.50,
    alignSelf: 'center',
    resizeMode: 'cover',
    marginBottom: 20,
  }
});
