import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, TouchableOpacity, SafeAreaView, StatusBar, PermissionsAndroid, Alert
} from 'react-native';
import { Container, Header, Text } from 'native-base';

import LottieView from 'lottie-react-native';
import theme from '../../theme/base-theme'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')

export default class LocationMain extends Component {
  constructor(props) {
    super(props);

  }

  handleNoBtn() {
    Alert.alert(
      'Locations',
      "This feature can be enabled at any time. Visit the Weather tab to add location. ",
      [
        { text: 'OK', onPress: () => this.props.navigation.navigate('NotificationMain') },
      ],
    )

  }

  handleYesBtn() {
    Alert.alert(
      'Locations',
      "For current location updates to work properly select “Always” in the system pop-up.",
      [
        { text: 'OK', onPress: () => this.getAuthorizationIfNeeded() },
      ],
    )

  }

  getAuthorizationIfNeeded() {
    Platform.select({
      ios: () => this.testLocationforAuthorization(),
      android: () => this.requestLocationPermission(),
    })();
  }

  async  requestLocationPermission() {
    try {
      var granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Weatherman Location Services',
          'message': 'Weatherman would like to access to your location to provide accurate weather alerts and forecasts.'
        }
      )
      var granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (granted) {
        this.props.navigation.navigate('HomeLoc')
      } else {
        Alert.alert(
          'Location Services unavailable',
          'Please make sure your location services are on and try again?.')
      }
    } catch (err) {
      console.warn(err)
    }
  }

  async  testLocationforAuthorization() {

    navigator.geolocation.getCurrentPosition(
      (position) => {

        this.props.navigation.navigate('HomeLoc');
      },
      (error) => {
        Alert.alert(
          'Location Services unavailable',
          'Please enable location permissions from settings.')
      },
      { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
    );
  }


  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />

        <View style={{ width: width, height: height, alignItems: 'center', backgroundColor: '#fff' }}>

          <SafeAreaView style={{ alignItems: 'center' }}>
            <Image source={mylogo}
              style={{ height: height * 0.12, width: height * 0.15, resizeMode: 'contain', marginTop: 30, marginBottom: 5 }} />

            <ScrollView
              scrollEnabled={false}
              keyboardShouldPersistTaps="always"
              style={{ width: width * 0.78, marginTop: 20 }}
              contentContainerStyle={{ alignItems: 'center' }}>

              <LottieView
                style={styles.image}
                imageAssetsFolder={'images/'}
                source={require('../../animation/location_onboarding.json')}
                autoPlay
                loop
              />

              <Text style={{ fontSize: 22, textAlign: 'center', color: theme.heading, margin: 20 }}>
                My Locations
              </Text>

              <Text style={styles.subHeading}>
                Display weather conditions for your current and favorite locations{'\n'}Would you like to enable this feature?
              </Text>

              <View style={{ width: '100%', flexDirection: 'row', alignSelf: 'center', marginTop: 40 }}>

                <View style={{ width: "50%", alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => this.handleNoBtn()}>
                    <Text style={{ color: theme.grey, fontWeight: 'bold' }}>NO</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ width: "50%", alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => this.handleYesBtn()} >
                    <Text style={{ color: '#4281e5', fontWeight: 'bold' }}>YES</Text>
                  </TouchableOpacity>
                </View>

              </View>

            </ScrollView>
          </SafeAreaView>
        </View >
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  subHeading: {
    fontSize: 15,
    color: theme.grey,
    textAlign: 'center'
  },
  image: {
    width: width * 0.60,
    height: width * 0.60,
    alignSelf: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
    marginBottom: 10
  }
});

