import React, { Component } from 'react';
import {
  Platform,
  View, Image, ScrollView, Modal, TouchableOpacity, ListView
} from 'react-native';
export default class AddWeatherModal extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      personalLoc: [],
      mycities: [],
      fullList: [],
      indicator_personal: [],
      indicator_data: [],
      index: 0,
      index_indicator: 0,
      data: getData(-1, dataArray0),
      tickArray: getTickValue(-1, dataArray0),
      modalVisible: false,
      search: '',
      searchText: '',
      buttonType: '',
      uniqueId: '',
      indicatorHeight: 'close',
      selectedItem: undefined,
      selectedAddress: '',
      selectedCity: '',
      selectedLat: '',
      selectedLon: '',
      deletedPL: '',
      deletedAL: '',
      deletedCity: '',
      editedPL: '',
      editedCity: '',
      reults: '',
      copy: '',
      showMax: true,
      refreshing: false,
      doYouNeedUmbrella: false,
      loading: false,
      deviceArray: [],
      scanning: false,
      fail: false,
      success: false,
      progress: 0,
      indeterminate: true,
      devicesToConnect: [],
      justDevices: [],
      isDeviceAvailable: false,
      connectedMacAddress: '',
      appState: AppState.currentState,
      isBlutoothEnabled: false,
      isResetEnabled: true,
      isScanning: false,
      appState: AppState.currentState,
      listloading: false,
      initlat: '',
      initlon: '',
    }
  }

  prepareCity(city, lat, lon) {
    this.setState({ selectedCity: city, selectedLat: lat, selectedLon: lon });
  }

  render() {
    return (
      <Modal
        visible={this.state.modalVisible}
        animationType={'slide'}
        transparent={true}
        onRequestClose={() => this.closeModal()}>
        <View style={{ width: width, height: height, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems: 'center' }}>
          {this.state.buttonType == 'addWeather' ? <View style={{ width: width * 0.82, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
            <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(0, 0, 0, 0.7)', alignItems: 'center' }}>
              <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5 }}>Add Location</Text>
            </View>
            <ScrollView keyboardShouldPersistTaps={"always"} scrollEnabled={false} style={{ width: width * 0.80, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)' }}>
              <GooglePlacesAutocomplete
                placeholder='City'
                minLength={2} // minimum length of text to search
                autoFocus={false}
                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                listViewDisplayed='true'    // true/false/undefined
                fetchDetails={true}
                renderDescription={row => row.description} // custom description render
                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                  this.prepareCity(data.structured_formatting.main_text, details.geometry.location.lat, details.geometry.location.lng);
                }}
                getDefaultValue={() => ''}
                query={{
                  // available options: https://developers.google.com/places/web-service/autocomplete
                  key: 'AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg',
                  language: 'en', // language of the results
                  types: '(cities)' // default: 'geocode'
                }}
                styles={{
                  textInputContainer: {
                    borderWidth: 0,
                    width: width * 0.80,
                    height: 50,
                    marginLeft: 10,
                    marginRight: 0,
                    backgroundColor: 'rgba(256,256,256,0)'
                  },
                  textInput: {
                    marginTop: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    marginBottom: 0,
                    height: 50,
                    color: '#000',
                    fontSize: 16,
                    borderBottomWidth: 1,
                    borderColor: 'rgba(179,179,179,1)',
                    backgroundColor: 'rgba(256,256,256,1)'
                  },
                  description: {
                    fontWeight: '100'
                  },
                  predefinedPlacesDescription: {
                    color: '#1faadb'
                  }
                }}
                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                GoogleReverseGeocodingQuery={{
                  // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                }}
                GooglePlacesSearchQuery={{
                  // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                  rankby: 'distance',
                  types: 'food'
                }}
              />
            </ScrollView>
            {this.state.loading ? <Spinner color='#003057' /> : null}
            <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.validateSave()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Save</Text>
              </TouchableOpacity>
            </View>
          </View> : null}
          {this.state.buttonType == 'editWeather' ? <View style={{ width: width * 0.82, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
            <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(179,179,179,1)', borderBottomWidth: 0.5, alignItems: 'center' }}>
              <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5 }}>Edit Locations</Text>
            </View>
            {this.state.listloading ? <Spinner color='#003057' /> : <List scrollEnabled={false} enableEmptySections={true} dataSource={this.ds.cloneWithRows(this.state.personalLoc)} style={{ height: 0, backgroundColor: '#fff' }}
              renderRow={data =>
                <ListItem>
                  <View style={{ marginLeft: 15 }}>
                    <Text numberOfLines={2} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '200' : '300', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'flex-start', backgroundColor: 'rgba(0, 0, 0, 0)', textAlign: 'center' }}> {data} </Text>
                  </View>
                </ListItem>}
              renderLeftHiddenRow={(data, secId, rowId, rowMap) =>
                <Button warning onPress={() => this.editPersonalLoc(rowId)}>
                  <Feather size={20} name="edit" color='#fff' />
                </Button>}
              renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                <Button full danger onPress={() => this.deletePersonalLoc(data)}>
                  <Feather size={20} name="trash-2" color='#fff' />
                </Button>}
              leftOpenValue={75}
              rightOpenValue={-75}
            />}
            {this.state.listloading ? <Spinner color='#003057' /> : <List scrollEnabled={true} enableEmptySections={true} dataSource={this.ds.cloneWithRows(this.state.mycities)} style={{ height: 0, backgroundColor: '#ffffff' }}
              renderRow={data =>
                <ListItem>
                  <View style={{ marginLeft: 15 }}>
                    <Text numberOfLines={2} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '200' : '300', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}> {data} </Text>
                  </View>
                </ListItem>}
              renderLeftHiddenRow={data =>
                <Button full onPress={() => alert(data)}>
                  <Icon active name="information-circle" />
                </Button>}
              renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                <Button full danger onPress={() => this.deleteAdditionalLoc(data)}>
                  <Feather size={20} name="trash-2" color='#fff' />
                </Button>}
              leftOpenValue={0}
              rightOpenValue={-75}
            />}
            {this.state.listloading ? null : <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
              <TouchableOpacity style={{ width: width * 0.82, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Close</Text>
              </TouchableOpacity>
            </View>}
          </View> : null}
          {this.state.buttonType == 'editPersonalLoc' ? <View style={{ width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 60, borderWidth: 1, borderRadius: 10 }}>
            <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(0, 0, 0, 0.7)', alignItems: 'center' }}>
              {this.state.editedPL == 1 ? <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5 }}>Enter Home Address</Text> :
                <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5 }}>Enter Work Address</Text>}
            </View>
            <ScrollView keyboardShouldPersistTaps={"always"} scrollEnabled={false} style={{ width: width * 0.80, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)' }}>
              <GooglePlacesAutocomplete
                placeholder='Enter Address'
                minLength={2} // minimum length of text to search
                autoFocus={false}
                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                listViewDisplayed='true'    // true/false/undefined
                fetchDetails={true}
                renderDescription={row => row.description} // custom description render
                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                  this.prepareAddressforEdit(data.description);
                }}
                getDefaultValue={() => ''}
                query={{
                  // available options: https://developers.google.com/places/web-service/autocomplete
                  key: 'AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg',
                  language: 'en', // language of the results
                  types: 'address' // default: 'geocode'
                }}
                styles={{
                  textInputContainer: {
                    borderWidth: 0,
                    width: width * 0.80,
                    height: 50,
                    marginLeft: 10,
                    marginRight: 0,
                    backgroundColor: 'rgba(256,256,256,0)'
                  },
                  textInput: {
                    marginTop: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    marginBottom: 0,
                    height: 50,
                    color: '#000',
                    fontSize: 12,
                    borderBottomWidth: 1,
                    borderColor: 'rgba(179,179,179,1)',
                    backgroundColor: 'rgba(256,256,256,1)'
                  },
                  description: {
                    fontWeight: '100',
                    fontSize: 12
                  },
                  predefinedPlacesDescription: {
                    color: '#1faadb'
                  }
                }}
                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                GoogleReverseGeocodingQuery={{
                  // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                }}
                GooglePlacesSearchQuery={{
                  // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                  rankby: 'distance',
                  types: 'food'
                }}
              />
            </ScrollView>
            {this.state.loading ? <Spinner color='#003057' /> : null}
            <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.returntoEdit()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.validateEdit()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Save</Text>
              </TouchableOpacity>
            </View>
          </View> : null}
          {this.state.buttonType == 'addTracking' && this.state.scanning == false && this.state.fail == false && this.state.success == false ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1.0)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
            <View style={{ width: width * 0.80, alignSelf: 'center' }}>
              <Image source={pairingconnect} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain' }} />
              <Text style={{ fontSize: 18, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 0, marginBottom: 20, textAlign: 'center' }}>Let’s connect your Weatherman</Text>
              <Text style={{ fontSize: 14, fontWeight: '400', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginBottom: 20, textAlign: 'center' }}>First, enable Bluetooth on your device. Next, turn on your tracker by holding down the button for five seconds until you hear two beeps.</Text>
            </View>
            <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row', bottom: 0, position: 'absolute' }}>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.startscan()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Search</Text>
              </TouchableOpacity>
            </View>
          </View> : null}
          {this.state.buttonType == 'addTracking' && this.state.scanning == true && this.state.fail == false ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
            <View style={{ width: width * 0.80, alignSelf: 'center', alignItems: 'center' }}>
              <Image source={pairing} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain', marginBottom: 20 }} />
              <Progress.Bar progress={this.state.progress} width={200} indeterminate={this.state.indeterminate} color={theme.brandPrimary} />
            </View>
          </View> : null}
          {this.state.buttonType == 'addTracking' && this.state.scanning == false && this.state.fail == true ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
            <View style={{ width: width * 0.80, alignSelf: 'center', alignItems: 'center' }}>
              <Image source={pairingfail} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain' }} />
            </View>
            <View style={{ alignItems: 'center', marginTop: 5 }}>
              <Text style={{ fontSize: 24, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandDanger, marginBottom: 10 }}>Pairing Failed</Text>
              <Text style={{ fontSize: 16, fontWeight: '300', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 10 }}>Is Bluetooth enabled?</Text>
              <Text style={{ fontSize: 16, fontWeight: '300', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 10 }}>Is tracker turned on?</Text>
              <Text style={{ fontSize: 16, fontWeight: '300', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 10 }}>Is tracker in range?</Text>
            </View>
            <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row', bottom: 0, position: 'absolute' }}>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.startscan()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Try again</Text>
              </TouchableOpacity>
            </View>
          </View> : null}
          {this.state.buttonType == 'addTracking' && this.state.scanning == false && this.state.success == true && this.state.fail == false ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
            <View style={{ width: width * 0.92, alignSelf: 'center', alignItems: 'center' }}>
              <Image source={pairingfound} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain' }} />
            </View>
            <View style={{ alignItems: 'center', marginTop: 5 }}>
              <Text style={{ fontSize: 24, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginBottom: 25 }}>Device(s) Found</Text>
              <ListView scrollEnabled={true} enableEmptySections={true} dataSource={this.ds.cloneWithRows(this.state.devicesToConnect)} style={{ height: 100, width: width * 0.80, backgroundColor: 'transparent' }}
                renderRow={(data, sectionID, rowID) =>
                  <ListItem>
                    <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, textAlign: 'center' }}>My Umbrella{rowID} - {data.status}  </Text>
                  </ListItem>}
              />
            </View>
            <View style={{ marginTop: 10, alignItems: 'center' }}>
              {this.state.loading == true ? <Image source={loadingspinner} style={{ alignSelf: 'center', height: 75, width: 75 }} /> : null}
            </View>
            <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row', bottom: 0, position: 'absolute' }}>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.connect(this.state.devicesToConnect)}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Connect</Text>
              </TouchableOpacity>
            </View>
          </View> : null}
        </View>
      </Modal>
    )
  }
}