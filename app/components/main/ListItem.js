import React from 'react';
import { View, Text, StyleSheet, Animated, Dimensions, PanResponder, TouchableOpacity, FlatList, Image, Platform } from 'react-native';
import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient'
import { formatDate, getDayoftheWeekHourly, getDayoftheWeek } from '../../functions/datetimefuncs';
import { round, getpercent, getBearing, getCardPercent } from '../../functions/mathandweatherfuncs';
import { Button } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';

const { width } = Dimensions.get('window');
const pin = require('../../img/currentLocation.png')
const Home = require('../../img/home_icon.png')
const Work = require('../../img/work_Icon.png')
const close = require('../../img/close.png')
const cloud = require('../../img/cloudy.png')
const temp = require('../../img/temp.png')
const humidity = require('../../img/humidity.png')
const wind = require('../../img/wind.png')
const windGust = require('../../img/windGust.png')

const weatherClearDay = require('../../img/weatherClearDay.png')
const weatherClearNight = require('../../img/weatherClearNight.png')
const weatherRain = require('../../img/weatherRain.png')
const weatherSnow = require('../../img/weatherSnow.png')
const weatherSleet = require('../../img/weatherHail.png')
const weatherWind = require('../../img/weatherWind.png')
const weatherFog = require('../../img/weatherFog.png')
const weatherCloudy = require('../../img/weatherCloudy.png')
const weatherPartlyCloudyDay = require('../../img/weatherPartlyCloudyDay.png')
const weatherPartlyCloudyNight = require('../../img/weatherPartlyCloudyNight.png')
const weatherHail = require('../../img/weatherHail.png')
const weatherThunderstorm = require('../../img/weatherThunderstorm.png')
const weatherTornado = require('../../img/weatherTornado.png')
const loadingspinner = require('../../img/loading.gif')

const edit_icon = require('../../img/edit_button.png')
const tracking_icon = require('../../img/tracking_button.png')
const tracking_off_icon = require('../../img/tracking_off_button.png')
const delete_icon = require('../../img/delete_button.png')

const subNavColors = {
  active:
    'rgba(256,256,256,0.2)'
  ,
  inactive:
    'rgba(256,256,256,0)'
};

const direction = {

  vertical: '{start: {x: 0, y: 0.4}, end: {x: 1, y: 0.6}}',
  slant: '{start: {x: 1, y: 0}, end: {x: 0, y: 1}}'
};


const myWeather = {
  weatherClearDay: [
    '#2e61c1', '#7ac5ee'
  ],
  weatherPartlyCloudyDay: [
    '#678ccf', '#8fc8e6'
  ],
  weatherCloudy: [
    '#5770a1', '#90c1dc'
  ],
  weatherClearNight: [
    '#211e57', '#5d59be'
  ],
  weatherPartlyCloudyNight: [
    '#112158', '#443f82', '#9762a2'
  ],
  weatherWind: [
    '#2c74d5', '#82aee9'
  ],
  weatherFog: [
    '#a79bbe', '#cfbed5'
  ],
  weatherRain: [
    '#2b5685', '#6bb2d5'
  ],
  weatherHail: [
    '#3c5c80', '#a1b8d1'
  ],
  weatherSleet: [
    '#67738d', '#aeb5c3'
  ],
  weatherSnow: [
    '#7d9cbc', '#b6c9dc'
  ],
  weatherThunderstorm: [
    '#3d426c', '#b19ab4'
  ],
  weatherTornado: [
    '#21324f', '#b5a8a3'
  ]

};

const myTracking = {
  inRange: [
    '#368baf', '#68b2d1'
  ],
  outOfRange: [
    '#1a3153', '#274777'
  ],
  untracked: [
    '#828286', '#9b9b9f'
  ]

};

export default class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.gestureDelay = -35;
    this.scrollViewEnabled = true;

    const position = new Animated.ValueXY();
    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => false,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderTerminationRequest: (evt, gestureState) => false,
      onPanResponderMove: (evt, gestureState) => {
        if (gestureState.dx > 35) {
          this.setScrollViewEnabled(false);
          let newX = gestureState.dx + this.gestureDelay;
          position.setValue({ x: newX, y: 0 });
        }
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (gestureState.dx < 150) {
          Animated.timing(this.state.position, {
            toValue: { x: 0, y: 0 },
            duration: 150,
          }).start(() => {
            this.setScrollViewEnabled(true);
          });
        } else {
          Animated.timing(this.state.position, {
            toValue: { x: width * 0.2, y: 0 },  //width
            duration: 300,
          }).start(() => {
            // this.props.success(this.props.item);
            this.setScrollViewEnabled(true);
          });
        }
      },
    });

    this.panResponder = panResponder;
    this.state = { position };
  }

  setScrollViewEnabled(enabled) {
    if (this.scrollViewEnabled !== enabled) {
      this.props.setScrollEnabled(enabled);
      this.scrollViewEnabled = enabled;
    }
  }

  backgroundGradient(item) {

    return (

      <View>
        {item.currently.icon == 'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
        {item.currently.icon == 'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical} /> : null}
        {item.currently.icon == 'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical} /> : null}
      </View>
    )
  }

  // async storeLocation(LocType, lat, lon) {

  //   try {

  //     let response = await fetch(this.state.baseUrl + 'wn_location/', {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Accept': 'application/json'
  //       },
  //       body: JSON.stringify({
  //         wuserid: this.state.uniqueId,
  //         wloctypeid: LocType,
  //         wlat: lat,
  //         wlon: lon,
  //         wloctime: Date.now(),
  //       })
  //     });

  //     let res = await response.text();
  //     if (response.status >= 200 && response.status < 300) {
  //       this.setState({ loading: false });
  //       this.setState({ error: "" });
  //       let parsed = JSON.parse(res);
  //     } else {
  //       let errors = res;
  //       throw errors;
  //     }

  //   } catch (errors) {
  //     let formErrors = JSON.parse(errors);
  //     let errorsArray = [];
  //     for (let key in formErrors) {
  //       if (formErrors[key].length > 1) {
  //         formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
  //       } else {
  //         errorsArray.push(`${key} ${formErrors[key]}`)
  //       }
  //     }
  //     this.setState({ errors: errorsArray });
  //   }
  // }

  // currentView = (item, index) => {

  //   return (
  //     <View>
  //       <View style={{ width: width - 80, flexDirection: "row", alignSelf: 'center', marginVertical: 20 }}>
  //         <View style={{ width: (width - 80) / 3 }}>
  //           <Image source={temp} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
  //           <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Feels Like</Text>
  //           <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.apparentTemperature)}˚</Text>
  //         </View>
  //         <View style={{ width: (width - 80) / 3 }}>
  //           <Image source={windGust} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
  //           <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Wind / Gust</Text>
  //           <Text numberOfLines={1} style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{getBearing(item.currently.windBearing)} {round(item.currently.windSpeed)}/{round(item.currently.windGust)}</Text>
  //         </View>
  //         <View style={{ width: (width - 80) / 3 }}>
  //           <Image source={humidity} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
  //           <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Humidity</Text>
  //           <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.humidity * 100)}%</Text>
  //         </View>
  //       </View>
  //       <View style={{ width: width - 80, flexDirection: "row", alignSelf: 'center', paddingBottom: 20 }}>
  //         <View style={{ width: (width - 80) / 3 }}>
  //           <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Pressure</Text>
  //           <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.pressure)}</Text>
  //         </View>
  //         <View style={{ width: (width - 80) / 3 }}>
  //           <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Visibility</Text>
  //           <Text numberOfLines={1} style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.visibility)}</Text>
  //         </View>
  //         <View style={{ width: (width - 80) / 3 }}>
  //           <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Dew Point</Text>
  //           <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>
  //             {round(item.currently.dewPoint)}˚
  //                   </Text>
  //         </View>
  //       </View>
  //     </View>
  //   )
  // }

  // hourlyView = (item, index, listIndex) => {
  //   // console.log(item, "list from hour list")
  //   const { forecast } = this.state
  //   var timeZone = ''

  //   if (listIndex === 0) {
  //     timeZone = this.state.currentLocationData.timezone;

  //   } else {
  //     timeZone = forecast[item.listIndex].timezone;
  //   }

  //   return (
  //     <View style={{ alignItems: "center", paddingHorizontal: 15 }}>
  //       <Text style={{ fontWeight: '400', color: '#fff', fontSize: 15, marginBottom: 2, alignSelf: 'center', }}>{formatDate(item.time, timeZone)}</Text>
  //       <Text style={{ fontWeight: '400', fontSize: 12, color: '#fff', marginTop: 4, marginBottom: 15, alignSelf: 'center' }}>{getpercent(round(item.precipProbability * 100))}</Text>
  //       {item.icon == 'clear-day' ? <Image source={weatherClearDay} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'clear-night' ? <Image source={weatherClearNight} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'rain' ? <Image source={weatherRain} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'snow' ? <Image source={weatherSnow} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'sleet' ? <Image source={weatherSleet} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'wind' ? <Image source={weatherWind} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'fog' ? <Image source={weatherFog} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'cloudy' ? <Image source={weatherCloudy} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'hail' ? <Image source={weatherHail} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'thunderstorm' ? <Image source={weatherThunderstorm} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       {item.icon == 'tornado' ? <Image source={weatherTornado} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
  //       <Text style={{ fontWeight: '600', fontSize: 14, color: '#fff', marginBottom: 2, alignSelf: 'center' }}>{round(item.temperature)}˚</Text>
  //       <Text style={{ fontWeight: '400', color: '#fff', fontSize: 15, marginRight: 5 }}>{getDayoftheWeekHourly(item.time, timeZone)}</Text>
  //     </View >
  //   )
  // }

  // extendedView = (item, index, listIndex) => {
  //   const { forecast } = this.state

  //   var timeZone = ''

  //   if (listIndex === 0) {
  //     timeZone = this.state.currentLocationData.timezone;

  //   } else {
  //     timeZone = forecast[item.listIndex].timezone;
  //   }

  //   return (
  //     <View style={{ alignItems: "center", paddingHorizontal: 15 }}>
  //       <Text style={{ fontWeight: '400', color: '#fff' }}>{getDayoftheWeek(item.time, timeZone)}</Text>
  //       <Text style={{ fontWeight: '400', fontSize: 12, color: '#fff', marginTop: 4, marginBottom: 15 }}>{getpercent(round(item.precipProbability * 100))}</Text>
  //       {item.icon == 'clear-day' ? <Image source={weatherClearDay} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'clear-night' ? <Image source={weatherClearNight} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'rain' ? <Image source={weatherRain} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'snow' ? <Image source={weatherSnow} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'sleet' ? <Image source={weatherSleet} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'wind' ? <Image source={weatherWind} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'fog' ? <Image source={weatherFog} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'cloudy' ? <Image source={weatherCloudy} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'hail' ? <Image source={weatherHail} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'thunderstorm' ? <Image source={weatherThunderstorm} style={{ height: 28, width: 28 }} /> : null}
  //       {item.icon == 'tornado' ? <Image source={weatherTornado} style={{ height: 28, width: 28 }} /> : null}
  //       <Text style={{ fontWeight: '700', fontSize: 16, color: '#fff', marginTop: 10 }}>{round(item.apparentTemperatureHigh)}˚</Text>
  //       <Text style={{ fontWeight: '300', fontSize: 14, color: '#fff', marginTop: 5, paddingBottom: 5 }}>{round(item.apparentTemperatureLow)}˚</Text>
  //     </View>
  //   )
  // }

  // renderForecastData(item, index) {
  //   const { citynames } = this.state
  //   return (
  //     <View style={{
  //       zIndex: 1, width: width - 30, paddingTop: 20, paddingHorizontal: 20, marginBottom: 10, paddingBottom: item.isExpandable ? 20 : 2,
  //       borderRadius: 10, overflow: 'hidden', alignSelf: 'center', flexDirection: 'column'
  //     }}>
  //       {item.currently.icon == 'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
  //       {item.currently.icon == 'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical} /> : null}
  //       {item.currently.icon == 'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical} /> : null}
  //       <TouchableOpacity style={{ paddingTop: 10, }} onPress={() => this.props.collapsible(item, index)} >
  //         <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
  //           <View style={{ flexDirection: "row", marginLeft: 10 }}>
  //             {item.type === "Current" || item.type === "Home" || item.type === "Work" ?
  //               <Image source={item.type === "Current" ? pin : item.type === "Home" ? Home : item.type === "Work" ? Work : null} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
  //               :
  //               null
  //             }
  //             <Text numberOfLines={1} style={{ fontSize: 28, marginLeft: 15, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>{item.address}</Text>
  //           </View>
  //           <Image source={close} style={{ alignSelf: "center", height: 28, width: 28, transform: [{ rotate: !item.isExpandable ? '180deg' : '0deg' }] }} />
  //         </View>
  //         <View style={{ flexDirection: "row", marginVertical: 10, width: "100%", justifyContent: "space-between" }}>
  //           <Text
  //             style={{ width: "47%", fontSize: 70, paddingLeft: 10, fontWeight: (Platform.OS === 'ios') ? '200' : '300', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop: 5 }}
  //           >
  //             {round(item.currently.temperature)}˚
  //                   </Text>
  //           <View style={{ width: "53%", alignItems: "baseline", justifyContent: "flex-end", marginVertical: 20, marginLeft: 10, marginRight: 10 }}>
  //             {item.currently.icon === 'clear-day' ? <Image source={weatherClearDay} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'clear-night' ? <Image source={weatherClearNight} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'rain' ? <Image source={weatherRain} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'snow' ? <Image source={weatherSnow} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'sleet' ? <Image source={weatherSleet} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'wind' ? <Image source={weatherWind} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'fog' ? <Image source={weatherFog} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'cloudy' ? <Image source={weatherCloudy} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'hail' ? <Image source={weatherHail} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'thunderstorm' ? <Image source={weatherThunderstorm} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             {item.currently.icon === 'tornado' ? <Image source={weatherTornado} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
  //             <Text style={{ fontWeight: '300', fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop: 0, alignSelf: "center" }}>{item.currently.summary}</Text>
  //           </View>
  //         </View>
  //       </TouchableOpacity>
  //       {item.isExpandable ?
  //         <View>
  //           <View style={{ width: width - 80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf: 'center', flexDirection: 'row', borderWidth: 1, borderColor: 'rgba(256,256,256,0.4)', borderRadius: 5 }}>
  //             <TouchableOpacity
  //               style={{
  //                 width: (width - 80) / 3,
  //                 paddingVertical: 10,
  //                 backgroundColor: item.subNav === "Current" ? subNavColors.active : subNavColors.inactive,
  //                 borderWidth: item.subNav === "Current" ? 0.5 : 0,
  //                 borderBottomLeftRadius: 5, borderTopLeftRadius: 5,
  //                 borderColor: 'rgba(256,256,256,0.4)', alignItems: 'center', justifyContent: 'center'
  //               }}
  //               onPress={() => this.props.selectSubNav("Current", item, index)}
  //             >
  //               <Text style={{ color: '#fff', fontWeight: '200', fontSize: 14 }}>CURRENT</Text>
  //             </TouchableOpacity>
  //             <TouchableOpacity
  //               style={{
  //                 width: (width - 80) / 3,
  //                 backgroundColor: item.subNav === "Hourly" ? subNavColors.active : subNavColors.inactive,
  //                 borderWidth: item.subNav === "Hourly" ? 0.5 : 0,
  //                 borderColor: 'rgba(256,256,256,0.4)', alignItems: 'center', justifyContent: 'center'
  //               }}
  //               onPress={() => this.props.selectSubNav("Hourly", item, index)}  >
  //               <Text style={{ color: '#fff', fontWeight: '200', fontSize: 14 }}>HOURLY</Text>
  //             </TouchableOpacity>
  //             <TouchableOpacity
  //               style={{
  //                 width: (width - 80) / 3,
  //                 borderBottomRightRadius: 5,
  //                 borderTopRightRadius: 5,
  //                 backgroundColor: item.subNav === "Extended" ? subNavColors.active : subNavColors.inactive,
  //                 borderWidth: item.subNav === "Extended" ? 0.5 : 0,
  //                 borderColor: 'rgba(256,256,256,0.4)', alignItems: 'center', justifyContent: 'center'
  //               }}
  //               onPress={() => this.props.selectSubNav("Extended", item, index)}  >
  //               <Text style={{ color: '#fff', fontWeight: '200', fontSize: 14 }}>EXTENDED</Text>
  //             </TouchableOpacity>
  //           </View>
  //           {item.subNav === "Current" ? this.currentView(item, index) :
  //             item.subNav === "Hourly" ?
  //               <View style={{ marginVertical: 10 }}>
  //                 <FlatList
  //                   data={item.hourly.data}
  //                   renderItem={({ item, subIndex }) => this.hourlyView(item, subIndex, index)}
  //                   keyExtractor={(item, subIndex) => subIndex.toString()}
  //                   extraData={this.state}
  //                   horizontal={true}
  //                   contentContainerStyle={{ marginTop: 10 }}
  //                 // ItemSeparatorComponent={() => {
  //                 //     return (
  //                 //         <View style={{ width: 1, backgroundColor: "#fff" }} />
  //                 //     )
  //                 // }}
  //                 />
  //               </View>
  //               :
  //               item.subNav === "Extended" ?
  //                 <View style={{ marginVertical: 10 }}>
  //                   <FlatList
  //                     data={item.daily.data}
  //                     renderItem={({ item, subIndex }) => this.extendedView(item, subIndex, index)}
  //                     keyExtractor={(item, index) => index.toString()}
  //                     extraData={this.state}
  //                     horizontal={true}
  //                     contentContainerStyle={{ marginTop: 10 }}
  //                   // ItemSeparatorComponent={() => {
  //                   //     return (
  //                   //         <View style={{ width: 1, backgroundColor: "#fff" }} />
  //                   //     )
  //                   // }}
  //                   />
  //                 </View>
  //                 :
  //                 null}
  //         </View>
  //         : null
  //       }
  //     </View>
  //   )
  // }
  // 
  //
  render() {
    const item = this.props.item;
    const type = this.props.item.type;

    console.log('ListItem item: ', item)

    return (
      <View style={styles.listItem}>

        <Animated.View style={[this.state.position.getLayout()]} {...this.panResponder.panHandlers}>

          {
            this.props.screen === 'Tracking' ?
              <View style={styles.absoluteCell}>

                <View style={{ zIndex: 1, width: 55, overflow: 'hidden', justifyContent: 'center', flex: 0.3, marginTop: 10, borderRadius: 6, marginBottom: 5 }}>
                  {item.wtracking == true && item.winrange == true ? <AnimatedLinearGradient customColors={myTracking.inRange} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
                  {item.wtracking == true && item.winrange == false ? <AnimatedLinearGradient customColors={myTracking.outOfRange} speed={10000} points={direction.vertical} /> : null}
                  {item.wtracking == false ? <AnimatedLinearGradient customColors={myTracking.untracked} speed={10000} points={direction.vertical} /> : null}
                  <TouchableOpacity style={{ width: 50, justifyContent: 'center', position: 'absolute' }}
                    onPress={() => this.props.editItem(this.props.item)}>
                    <Image source={edit_icon} style={{ height: 30, width: 30, alignSelf: "center" }} resizeMode="contain" />
                  </TouchableOpacity>
                </View>

                <View style={{ zIndex: 1, width: 55, overflow: 'hidden', justifyContent: 'center', flex: 0.3, marginTop: 10, borderRadius: 6, marginBottom: 5 }}>
                  {item.wtracking == true && item.winrange == true ? <AnimatedLinearGradient customColors={myTracking.inRange} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
                  {item.wtracking == true && item.winrange == false ? <AnimatedLinearGradient customColors={myTracking.outOfRange} speed={10000} points={direction.vertical} /> : null}
                  {item.wtracking == false ? <AnimatedLinearGradient customColors={myTracking.untracked} speed={10000} points={direction.vertical} /> : null}
                  <TouchableOpacity style={{ width: 50, justifyContent: 'center', position: 'absolute' }}
                    onPress={() => this.props.updateTracking(this.props.item.id, this.props.item.wtracking)}>
                    <Image source={this.props.item.wtracking ? tracking_off_icon : tracking_icon} style={{ height: 30, width: 30, alignSelf: "center" }} resizeMode="contain" />
                  </TouchableOpacity>
                </View>

                <View style={{ zIndex: 1, width: 55, overflow: 'hidden', justifyContent: 'center', flex: 0.3, marginBottom: 15, marginTop: 5, borderRadius: 6 }}>
                  {item.wtracking == true && item.winrange == true ? <AnimatedLinearGradient customColors={myTracking.inRange} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
                  {item.wtracking == true && item.winrange == false ? <AnimatedLinearGradient customColors={myTracking.outOfRange} speed={10000} points={direction.vertical} /> : null}
                  {item.wtracking == false ? <AnimatedLinearGradient customColors={myTracking.untracked} speed={10000} points={direction.vertical} /> : null}
                  <TouchableOpacity style={{ width: 50, justifyContent: 'center', position: 'absolute' }}
                    onPress={() => this.props.deleteItem(this.props.item.id, this.props.item.wtrackername)}>
                    <Image source={delete_icon} style={{ height: 30, width: 30, alignSelf: "center" }} resizeMode="contain" />
                  </TouchableOpacity>
                </View>
              </View>
              :
              <View style={styles.absoluteCell}>
                {type === 'Home' || type === 'Work' ?
                  <View style={{ zIndex: 1, width: 55, overflow: 'hidden', justifyContent: 'center', flex: 0.5, marginTop: 10, borderRadius: 6, marginBottom: 5 }}>
                    {item.currently.icon == 'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
                    {item.currently.icon == 'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical} /> : null}
                    {item.currently.icon == 'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical} /> : null}
                    <TouchableOpacity style={{ width: 55, justifyContent: 'center', position: 'absolute' }}
                      onPress={() => this.props.editItem(this.props.item, this.props.index)}>
                      <Image source={edit_icon} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" />
                    </TouchableOpacity>
                  </View>
                  // <View  style={styles.absoluteCellText}>
                  //   <Button full warning onPress={() => this.props.editItem(this.props.item, this.props.index)}>
                  //     <Feather size={30} name="edit" color='#fff' />
                  //   </Button>
                  // </View>
                  : null
                }
                <View style={{ zIndex: 1, width: 55, overflow: 'hidden', justifyContent: 'center', flex: 0.5, marginBottom: 15, marginTop: 5, borderRadius: 6 }}>
                  {item.currently.icon == 'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
                  {item.currently.icon == 'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical} /> : null}
                  {item.currently.icon == 'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical} /> : null}
                  <TouchableOpacity style={{ width: 55, justifyContent: 'center', position: 'absolute' }}
                    onPress={() => this.props.deleteItem(this.props.item, this.props.index)}>
                    <Image source={delete_icon} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" />
                  </TouchableOpacity>
                </View>
                {/* <View style={styles.absoluteCellText}>
              <Button full danger onPress={() => this.props.deleteItem(this.props.item, this.props.index)}>
                <Feather size={30} name="trash-2" color='#fff' />
              </Button>
            </View> */}
              </View>
          }

          <View style={styles.innerCell}>
            {this.props.renderData(this.props.item, this.props.index)}
          </View>
        </Animated.View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    marginLeft: -60,
    justifyContent: 'center',
    // backgroundColor: 'red',
  },
  absoluteCell: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    width: 180,
    flex: 1,
    // flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: 'red'
  },
  absoluteCellText: {
    width: 50,
    justifyContent: 'center',
    flex: 0.5,
    backgroundColor: 'blue',
    // alignItems: 'center'
  },
  innerCell: {
    // width: width,
    // height: 80,
    marginLeft: 60,
    // backgroundColor: 'yellow',
    justifyContent: 'center',
    alignItems: 'center',
  },
});