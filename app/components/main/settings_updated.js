
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  View, Dimensions, Platform, Image, Linking, Alert
} from 'react-native';
import { Container, Header, Content, Tabs, Tab, TabHeading, Card, CardItem, cardBody, Badge, Body, Button, Segment, List, ListItem, Left, Right, Separator } from 'native-base';

import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient'
import SettingsStore from '../../stores/settingsStore';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import DeviceInfo from 'react-native-device-info';
import FlipToggle from 'react-native-flip-toggle-button';
import theme from '../../theme/base-theme';
import { registerKilledListener, registerAppListener } from "../../messaging/Listeners";
// import FCM, { FCMEvent } from "react-native-fcm";
import { patchNotifications } from '../../functions/weatherfuncs';
const settings = new SettingsStore()
const settingsad = require('../../img/setting-ad.png')
const dslogo = require('../../img/poweredby-ds.png')
import Notifications from './notifications_updated';
import Idx from '../../utilities/Idx';
import Banner from './banner';
const loadingspinner = require('../../img/loading.gif')

class Settings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "",
      temploaded: true,
      units: this.props.wUnits || this.props.getMyID[0].wUnits,
      myID: Idx(this.props, _ => _.getMyID[0].id) || '',
      isFahrenheit: true,
      testing: false,
      resetbutton: true,
      token: this.props.deviceToken,
      imageUrl: null,
      isLoading: false
    }
  }

  componentWillMount() {
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.baseUrl = settings.BaseUrl;
    // this.state.baseUrl = "https://wnstgapi01.azurewebsites.net/api/"
    // this.getMyId();
    this.setInitial();

  }


  async componentDidMount() {
    this.setState({ isLoading: true })
    this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true });
    let response = await fetch(`${settings.baseurl}wn_images`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    let res = await response.text();
    let parsed = JSON.parse(res)
    if (response.status >= 200 && response.status < 300) {
      this.setState({ imageUrl: parsed[0].wlocation, isLoading: false })
    } else {
      this.setState({ isLoading: false })
      // alert(" Error ")
    }
  }

  changeUnits(val) {
    if (val) {
      this.setState({ isFahrenheit: true, units: 'us' })
      setTimeout(() => { this.updateUnits(); }, 200)
    } else {
      this.setState({ isFahrenheit: false, units: 'si' })
      setTimeout(() => { this.updateUnits(); }, 200)
    }
  }

  setInitial() {
    let context = this;
    if (context.state.units == 'us') {
      this.setState({ isFahrenheit: true, temploaded: true });

    }
    if (context.state.units == 'si') {
      this.setState({ isFahrenheit: false, temploaded: true });
    }
  }

  updateUnits() {
    this.props.UserActions.convertUnits(this.state.units)
    //this.forceUpdate();
  }

  render() {
    if (this.state.testing) {
      return (
        <View style={{ width: width, height: height, backgroundColor: '#fff', alignItems: 'center' }}>
          <Banner />
          <Text>Settings</Text>
        </View>
      )
    }


    else
      return (
        <ScrollView ref='_scrollView' scrollEnabled={false} style={{ backgroundColor: '#fff' }}>
          <Banner />
          <View>
            <Text style={{ paddingLeft: 12, paddingTop: 16, color: theme.subTitle, fontSize: 14, fontWeight: '500' }}> Units </Text>
          </View>
          <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ color: theme.title, fontSize: 16 }}>Temperature</Text>
            {this.state.temploaded ? <FlipToggle
              value={this.state.isFahrenheit}
              buttonWidth={48}
              buttonHeight={18}
              buttonRadius={50}
              sliderWidth={28}
              sliderHeight={28}
              sliderOffColor={'#ecebf2'}
              sliderOnColor={'#f1f1f1'}
              buttonOffColor={'#aeaeae'}
              buttonOnColor={"#092f57"}
              onLabel={'˚F'}
              offLabel={'˚C'}
              labelStyle={this.state.isFahrenheit ? { color: '#fff', fontSize: 12, fontWeight: 'bold', width: 40 } : { color: '#fff', fontSize: 12, fontWeight: 'bold', marginLeft: 20 }}
              onToggle={(value) => { this.changeUnits(value) }}
            /> : null}
          </View>
          <View style={styles.line_separator} />
          <View>
            <Text style={{ paddingLeft: 12, paddingTop: 16, color: theme.subTitle, fontSize: 14, fontWeight: '500' }}> About </Text>
          </View>
          <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ color: theme.title, fontSize: 16 }}>Version</Text>
            <Text style={{ color: theme.title }}>2.1.0</Text>
          </View>
          <View style={styles.line_separator} />
          <View style={{ width: width * 0.80, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 22, marginBottom: 5 }}>
            <Text style={{ textAlign: 'center', fontSize: 15, marginBottom: 5, color: theme.brandPrimary }} onPress={() => { Linking.openURL('https://weathermanumbrella.com/pages/app') }}>Support/FAQs</Text>
            <Text style={{ textAlign: 'center', fontSize: 15, marginBottom: 5, color: theme.brandPrimary }} onPress={() => { Linking.openURL('https://weathermanumbrella.com/pages/privacy-policy') }}>Privacy Policy</Text>
            <Text style={{ textAlign: 'center', fontSize: 15, marginBottom: 5, color: theme.brandPrimary }} onPress={() => { Linking.openURL('https://weathermanumbrella.com/pages/terms-of-service') }}>T&amp;Cs</Text>
          </View>
          {this.state.isLoading === false && this.state.imageUrl !== null ?
            <TouchableOpacity style={{ marginTop: 20 }} onPress={() => { Linking.openURL('https://weathermanumbrella.com') }}>
              <Image source={{ uri: this.state.imageUrl }} resizeMode="stretch" style={{ height: 225, width: width - 28, alignSelf: 'center', }} />
              <Text style={{ fontWeight: 'bold', color: 'white', marginTop: -40, alignSelf: 'center', fontSize: 20 }}>SHOP NOW</Text>
            </TouchableOpacity>
            : this.state.isLoading === true && this.state.imageUrl === null ?
              <Image source={loadingspinner} style={{ width: 100, height: 100, alignSelf: "center" }} resizeMode="contain" />
              : <Text> No data found </Text>
          }
          <View style={{ alignSelf: 'center', marginTop: 30 }}>
            <Image source={dslogo} style={{ height: 40, width: width * 0.5 }} />
          </View>
        </ScrollView >
      );
  }
}


const mapStateToProps = (state) => ({
  getMyID: state.user.getUserIdState,
  wUnits: state.user.wUnits,
  deviceToken: state.user.deviceToken

})

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch),
});

const styles = StyleSheet.create({
  line_separator: {
    width: "100%",
    height: 0.5,
    backgroundColor: theme.separator
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
