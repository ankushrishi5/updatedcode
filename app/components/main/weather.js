/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  View, Dimensions, Platform, Image, Button, SafeAreaView, TouchableHighlight, PermissionsAndroid, RefreshControl, Alert
} from 'react-native';
import { Container, Header, Content, Tabs, Tab,TabHeading, Card, CardItem, cardBody, Badge, Body, List, ListItem, Spinner, StyleProvider, getTheme } from 'native-base';
import Settings from '../../stores/settingsStore'
import AnimatedLinearGradient, {presetColors} from 'react-native-animated-linear-gradient'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import DeviceInfo from 'react-native-device-info';
import {formatDate, getDayoftheWeekHourly, getDayoftheWeek} from '../../functions/datetimefuncs';
import {round,getpercent,getBearing} from '../../functions/mathandweatherfuncs';
import theme from '../../theme/base-theme';
import Idx from '../../utilities/Idx';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as WeatherActions from '../../redux/modules/weather';
import * as UserActions from '../../redux/modules/user';
import * as userLocation from '../../redux/modules/location';
import {writeToLog} from '../../functions/umbrellafuncs';

var moment = require('moment-timezone');
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const pin = require('../../img/currentLocation.png')
const close = require('../../img/close.png')
const cloud = require('../../img/cloudy.png')
const temp = require('../../img/temp.png')
const humidity = require('../../img/humidity.png')
const wind = require('../../img/wind.png')
const windGust = require('../../img/windGust.png')

const weatherClearDay = require('../../img/weatherClearDay.png')
const weatherClearNight = require('../../img/weatherClearNight.png')
const weatherRain = require('../../img/weatherRain.png')
const weatherSnow = require('../../img/weatherSnow.png')
const weatherSleet = require('../../img/weatherHail.png')
const weatherWind = require('../../img/weatherWind.png')
const weatherFog = require('../../img/weatherFog.png')
const weatherCloudy = require('../../img/weatherCloudy.png')
const weatherPartlyCloudyDay = require('../../img/weatherPartlyCloudyDay.png')
const weatherPartlyCloudyNight = require('../../img/weatherPartlyCloudyNight.png')
const weatherHail = require('../../img/weatherHail.png')
const weatherThunderstorm = require('../../img/weatherThunderstorm.png')
const weatherTornado = require('../../img/weatherTornado.png')
const loadingspinner = require('../../img/loading.gif')
const settings = new Settings();

const subNavColors = {
  active:
    'rgba(256,256,256,0.2)'
  ,
  inactive:
    'rgba(256,256,256,0)'
};


class Weather extends Component {
  constructor(props) {
    super(props)
    this.state = {
      baseUrl: '',
      dsUrl:'',
      curTemp: "",
      myUnits: this.props.wUnits || '',
      mylatitude: "",
      mylongitude: "",
      mylocations: 3,
      navCurrentColor: subNavColors.active,
      navHourlyColor: subNavColors.inactive,
      navExtendedColor: subNavColors.inactive,
      expand1: 'True',
      expanded1: '180deg',
      myheight1: 180,
      expand2: 'True',
      expanded2: '180deg',
      myheight2: 180,
      expand3: 'True',
      expanded3: '180deg',
      myheight3:180,
      expand4: 'True',
      expanded4: '180deg',
      myheight4: 180,
      expand5: 'True',
      expanded5: '180deg',
      myheight5: 180,
      expand6: 'True',
      expanded6: '180deg',
      myheight6: 180,
      subNav: "Current",
      loading: true,
      weatherLoading: false,
      locationRetry: false,
      forecast: [],
      citynames: [],
      allcities:[],
      personalLoc:[],
      mycities:[],
      mylatlons:[],
      getUnitData:props.getUnitData == [] ? props.userData : props.getUnitData
    }

  }

  componentWillReceiveProps(nextProps){
   // console.log('here are the next props *********** ',nextProps)
    //this.setState({locationRetry: false});
    this.setState({
      getUnitData:nextProps.getUnitData,
      wUnits:nextProps.wUnits
    })
    this.forceUpdate();
    //this.getCities()
  }

  componentDidMount(){
      this.state.uniqueId = DeviceInfo.getUniqueID();
      this.state.baseUrl = settings.BaseUrl;
      this.state.dsUrl = settings.DSUrl;
      this.state.forecast.push('');
      this.state.citynames.push('');
      //this.locateMe();
      this.getCities();
      this.updateLastOpen();
      writeToLog('','',DeviceInfo.getUniqueID(),1,'appopen')

    //  this.getUnits();
  }

  updateLastOpen() {
    //console.log('inside weather screen loading update last open ********')
    if(this.state.getUnitData[0] != undefined){
      this.props.UserActions.storeLastOpened({idToUpdate: this.state.getUnitData[0].id})
    }
  }

  componentWillUnmount() {
    //delete this.state;
    // this.state.forecast= [];
    // this.state.citynames= [];
    // this.state.allcities=[];
    // this.state.mycities=[];
    // this.state.personalLoc=[];
    // this.state.forecast.push('');
    // this.state.citynames.push('');
  }

  _onRefresh() {
    this.setState({locationRetry: false});
    this.state.forecast= [];
    this.state.citynames= [];
    this.state.allcities=[];
    this.state.mycities=[];
    this.state.personalLoc=[];
    this.state.forecast.push('');
    this.state.citynames.push('');
    //this.getUnits();
    this.getCities();

  }

  // getUnits() {
  //   // console.log('inside get units ******** ',Idx(this.props,_=>_.getUnitData[0].wUnits))
  //   this.setState({myUnits:this.state.getUnitData[0].wUnits});
  // }


locateMe() {
  //console.log('location data ****** ',this.props.location)
    navigator.geolocation.getCurrentPosition(
        (position) => {
          //console.log("currentLocation***", position)
          if(position.coords && position.coords.latitude && position.coords.longitude){
            this.getTemp(position.coords.latitude, position.coords.longitude, 1);
            //
            this.getCityName(position.coords.latitude, position.coords.longitude, 0);
            this.setState({currentRegion: {
              mylatitude: position.coords.latitude,
              mylongitude: position.coords.longitude,
            }});
          }
        },
        (error) => this.setState({locationRetry: true}),
        {enableHighAccuracy: false, timeout: 10000, maximumAge: 1000}
      );
      // console.log("this.state.currentRegion****", this.state.currentRegion)
  }

  getCities() {
    this.locateMe();
        let parsed = this.state.getUnitData || this.props.getUnitData;
      //console.log("getCities******",parsed)
      //console.log("cities weather js",this.state.allcities)

        //this.setState({myUnits:parsed[0].wUnits});
        if(parsed.length > 0){
          if (parsed[0].wHomeLoc != null) {
          this.state.personalLoc.push(parsed[0].wHomeLoc);
          this.state.allcities.push(parsed[0].wHomeLoc);
        }
        if (parsed[0].wWorkLoc != null) {
            this.state.personalLoc.push(parsed[0].wWorkLoc);
            this.state.allcities.push(parsed[0].wWorkLoc);
          }
        if (parsed[0].wCNAdd1 != null) {
            this.state.mycities.push(parsed[0].wCNAdd1);
            this.state.allcities.push(parsed[0].wCNAdd1);
            this.state.mylatlons.push(parsed[0].wLLAdd1);
          }
        if (parsed[0].wCNAdd2 != null) {
            this.state.mycities.push(parsed[0].wCNAdd2);
            this.state.allcities.push(parsed[0].wCNAdd2);
            this.state.mylatlons.push(parsed[0].wLLAdd2);
          }
        if (parsed[0].wCNAdd3 != null) {
            this.state.mycities.push(parsed[0].wCNAdd3);
            this.state.allcities.push(parsed[0].wCNAdd3);
            this.state.mylatlons.push(parsed[0].wLLAdd3);
          }
          if (this.state.allcities[0] == null && this.state.allcities[1] == null && this.state.allcities[2] == null && this.state.allcities[3] == null && this.state.allcities[4] == null  ) {
            //
            this.setState({expand1: 'False', myheight1:464, expanded1:'180deg'});
          }
          if (this.state.personalLoc[0] != null) {
            setTimeout(() => {this.getCNLL(this.state.personalLoc[0]);}, 500)
          }
          if (this.state.personalLoc[1] != null) {
            //this.getCNLL(this.state.personalLoc[1]);
            setTimeout(() => {this.getCNLL(this.state.personalLoc[1]);}, 1000)
          }
          if (this.state.mycities[0] != null) {
            setTimeout(() => {this.getAdditionalTemp(this.state.mycities[0],this.state.mylatlons[0]);}, 1500)
          }
          if (this.state.mycities[1] != null) {
            setTimeout(() => {this.getAdditionalTemp(this.state.mycities[1],this.state.mylatlons[1]);}, 2000)
          }
          if (this.state.mycities[2] != null) {
            setTimeout(() => {this.getAdditionalTemp(this.state.mycities[2],this.state.mylatlons[2]);}, 2500)
          }
        }
          
  }

async getCityName(lat, lon, id) {
try {
  let response = await fetch(settings.geourl + 'latlng=' + lat + ',' + lon + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  let res = await response.text();
  if(response.status >= 200 && response.status < 300) {
    let parsed = JSON.parse(res)
    //console.log("getcity name *****", parsed.results);
    this.props.userLocation.setCurrentLocation(parsed.results)
    if (parsed.results[0].address_components[1].types[0] == "locality" || parsed.results[0].address_components[1].types[1] == "sublocality") { // locality type
      this.state.citynames.push(parsed.results[0].address_components[1].long_name);
     }
     else if (parsed.results[0].address_components[2].types[0] == "locality"|| parsed.results[0].address_components[2].types[1] == "sublocality") { // locality type
      this.state.citynames.push(parsed.results[0].address_components[2].long_name);
     }
     else if (parsed.results[0].address_components.length > 3) {
      if (parsed.results[0].address_components[3].types[0] == "locality"|| parsed.results[0].address_components[3].types[1] == "sublocality") { // locality type
       this.state.citynames.push(parsed.results[0].address_components[3].long_name);
      }
     }
     //
    //  console.log("getcity name ***** from reducer", this.props.location); 

  } else {
    let errors = res;
    throw errors;
  }

} catch(error) {
  this.setState({errors: error});
  
  }
}

async getCNLL (myAddress) {

  try {
    let response = await fetch(settings.geourl + 'address=' + myAddress + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    let res = await response.text();

    if(response.status >= 200 && response.status < 300) {
      let parsed = JSON.parse(res)
      //
      if (parsed.results[0].address_components[1].types[0] == "locality" || parsed.results[0].address_components[1].types[1] == "sublocality") { // locality type
       this.state.citynames.push(parsed.results[0].address_components[1].long_name);
      }
      else if (parsed.results[0].address_components[2].types[0] == "locality" || parsed.results[0].address_components[2].types[1] == "sublocality") { // locality type
       this.state.citynames.push(parsed.results[0].address_components[2].long_name);
      }
      else if (parsed.results[0].address_components.length > 3) {
       if (parsed.results[0].address_components[3].types[0] == "locality" || parsed.results[0].address_components[3].types[1] == "sublocality") { // locality type
        this.state.citynames.push(parsed.results[0].address_components[3].long_name);
       }
     }

      //
      //
      this.getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, 2);

    } else {
      let errors = res;
      throw errors;
    }

  } catch(error) {
    this.setState({errors: error});
    
    }
}

 async getAdditionalTemp (city, latlon) {
   var location = latlon.split(",");
   //
   this.state.citynames.push(city);
   this.getTemp(location[0], location[1], 3);
 }


  async getTemp(lat, lon, id) {
    //console.log (id )
    try {
      let response = await fetch(this.state.dsUrl  + lat + ',' + lon + '?units=' + this.state.myUnits, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      if(response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        let DateArray = [];
        //console.log (id + ' ' + parsed.currently.temperature)
        //console.log (id + ' ' + parsed.currently.icon)
        //
        this.state.forecast.push(parsed);

        this.storeLocation(id, lat, lon);

      } else {
        let errors = res;
        throw errors;
      }

    } catch(error) {
      this.setState({errors: error});
      
    }
  }

  selectSubNav (selectedSubNav) {
    this.state.subNav = selectedSubNav;
    if (selectedSubNav == 'Current') {
      this.setState({navCurrentColor: subNavColors.active, navHourlyColor:subNavColors.inactive, navExtendedColor:subNavColors.inactive});
    }
    if (selectedSubNav == 'Hourly') {
      this.setState({navCurrentColor: subNavColors.inactive, navHourlyColor:subNavColors.active, navExtendedColor:subNavColors.inactive});
    }
    if (selectedSubNav == 'Extended') {
      this.setState({navCurrentColor: subNavColors.inactive, navHourlyColor:subNavColors.inactive, navExtendedColor:subNavColors.active});
    }

    return selectedSubNav;
  }


  expandDetails1() {
    if (this.state.expand1 == 'True') {
    //open
    this.setState({expand1: 'False', myheight1:464, expanded1:'0deg'});
    }
    else if (this.state.expand1 == 'False') {
    //close
    this.setState({expand1: 'True', myheight1:180, expanded1:'180deg'});
    }

  }

  expandDetails2() {
    if (this.state.expand2 == 'True') {
    //open
    this.setState({expand2: 'False', myheight2:464, expanded2:'0deg'});
    }
    else if (this.state.expand2 == 'False') {
    //close
    this.setState({expand2: 'True', myheight2:180, expanded2:'180deg'});
    }

  }

  expandDetails3() {
    if (this.state.expand3 == 'True') {
    //open
    this.setState({expand3: 'False', myheight3:464, expanded3:'0deg'});
    }
    else if (this.state.expand3 == 'False') {
    //close
    this.setState({expand3: 'True', myheight3:180, expanded3:'180deg'});
    }

  }

  expandDetails4() {
    if (this.state.expand4 == 'True') {
    //open
    this.setState({expand4: 'False', myheight4:464, expanded4:'0deg'});
    }
    else if (this.state.expand4 == 'False') {
    //close
    this.setState({expand4: 'True', myheight4:180, expanded4:'180deg'});
    }

  }

  expandDetails5() {
    if (this.state.expand5 == 'True') {
    //open
    this.setState({expand5: 'False', myheight5:464, expanded5:'0deg'});
    }
    else if (this.state.expand5 == 'False') {
    //close
    this.setState({expand5: 'True', myheight5:180, expanded5:'180deg'});
    }

  }

  expandDetails6() {
    if (this.state.expand6 == 'True') {
    //open
    this.setState({expand6: 'False', myheight6:464, expanded6:'0deg'});
    }
    else if (this.state.expand6 == 'False') {
    //close
    this.setState({expand6: 'True', myheight6:180, expanded6:'180deg'});
    }

  }


  async storeLocation (LocType, lat, lon) {

    try {

      let response = await fetch(this.state.baseUrl + 'wn_location/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wuserid: this.state.uniqueId,
          wloctypeid: LocType,
            wlat: lat,
            wlon: lon,
            wloctime: Date.now(),
          })
      });
      
      let res = await response.text();
      //

      if(response.status >= 200 && response.status < 300) {
        this.setState({loading: false});
        this.setState({error:""});
        let parsed = JSON.parse(res);
        //

      } else {
        let errors = res;
        throw errors;
      }

    } catch(errors) {

      
      //this.uhoh();
      let formErrors = JSON.parse(errors);
      let errorsArray = [];
      for(let key in formErrors) {
        if(formErrors[key].length > 1) {
          formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
        } else {
            errorsArray.push(`${key} ${formErrors[key]}`)
        }
      }
       this.setState({errors: errorsArray});
    }
  }


  render() {
    //console.log("this propssssss*****", this.props)
    // console.log("this.props.locations*******weather", this.state.getUnitData)
    // console.log("this.state.forecast[2]****", this.state.forecast[2])
    if (this.state.locationRetry) {
      return (
        <View style={{width:width, height:height, backgroundColor:'#fff', alignItems:'center'}}>
        <Text style={{marginTop:70, marginLeft:20, marginRight:20, marginBottom:60, textAlign:'center', color:theme.brandPrimary}}>We were unable to get your current location. If you would like to use the weather features, please ensure location services are tuned on from your settings menu and retry.</Text>
          <TouchableOpacity>
          <View style={{alignSelf:'center', backgroundColor:'transparent'}}>
              <MaterialIcons name="autorenew" size={100} color="#000" onPress={()=>this._onRefresh()}/>
          </View>
          </TouchableOpacity>
        </View>
      )
    }

    // else if (!this.state.weatherLoading){
    //   return (<Image source={loadingspinner} style={{alignSelf:'center',height:200, width:200, marginTop:10}} />)
    //   //return null;
    // }

    else
    return (
      <SafeAreaView>
    <ScrollView scrollEnabled={false}  style={{elevation: 0,shadowOpacity: 0, backgroundColor: 'transparent'}}>
      
   {this.state.forecast[1] != null ? <View style={{zIndex: 1, height:this.state.myheight1, width:width*0.92,marginTop:5, marginBottom:10, borderRadius:10, overflow:'hidden', alignSelf:'center', flexDirection:'column'}}>
          {this.state.forecast[1].currently.icon ==  'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{borderRadius:5}}/> : null }
          {this.state.forecast[1].currently.icon ==  'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical}/> : null }
          {this.state.forecast[1].currently.icon ==  'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical}/> : null }
         <TouchableOpacity onPress={()=>this.expandDetails1()}>
         <View style={{height:40, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row', marginBottom:0, marginTop:15}}>
          <View style={{width:width*0.08, height: 56, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
           <View style={{width:width*0.075, height: 54, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:3}}>
             <Image source={pin} style={{height:28, width:28}} />
           </View>
           <View style={{width:width*0.65, height:40, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:0}}>
             <Text numberOfLines={1} style={{ fontSize: 28, fontWeight: (Platform.OS === 'ios' ) ? '400' : '500', fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{this.state.citynames[1]}</Text>
           </View>
           <View style={{width:width*0.1, height:25, backgroundColor:'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop:0}}>
             <Image source={close} style={{justifyContent:'flex-end',height:28, width:28, transform: [{ rotate: this.state.expanded1}]}} />
           </View>
         </View>
         <View style={{height:110, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row'}}>
         <View style={{width:width*0.08, height: 80, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
           <View style={{width:width*0.35, height: 94, backgroundColor:'rgba(256, 0, 256, 0)', alignItems:'flex-start'}}>
             <Text style={{ fontSize: 68, fontWeight: (Platform.OS === 'ios' ) ? '200' : '300',  fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop:5}}>{round(this.state.forecast[1].currently.temperature)}˚ </Text>

           </View>
           <View style={{width:width*0.05, height:94, backgroundColor:'rgba(0, 256, 256, 0)'}}>


           </View>
           <View style={{width:width*0.45, height:140, backgroundColor:'rgba(0, 0, 256, 0.0)', marginTop:10}}>
           {this.state.forecast[1].currently.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:42, width:42}} /> : null }
           {this.state.forecast[1].currently.icon ==  'rain' ? <Image source={weatherRain} style={{height:42, width:42}} /> : null }
           {this.state.forecast[1].currently.icon ==  'snow' ? <Image source={weatherSnow} style={{height:42, width:42}} /> : null }
           {this.state.forecast[1].currently.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:42, width:42}} /> : null }
           {this.state.forecast[1].currently.icon ==  'wind' ? <Image source={weatherWind}  style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'fog' ? <Image source={weatherFog} style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'hail' ? <Image source={weatherHail} style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:42, width:42}}/> : null }
           {this.state.forecast[1].currently.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:42, width:42}}/> : null }
            <View style={{width:width*0.4, height:60, backgroundColor:'rgba(256, 256, 256, 0.0)', marginTop:0}}>
             <Text numberOfLines={2} style={{ fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop:0}}>{this.state.forecast[1].currently.summary}</Text>
            </View>
           </View>

         </View>
         <View style={{width:width*0.92, height:25, backgroundColor:'rgba(256, 0,0, 0)', flexDirection:'row'}}>

         </View>
         </TouchableOpacity>
         <View style={{height:60, width:width*0.80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf:'center', flexDirection:'row'}}>
           <TouchableOpacity onPress={()=>this.selectSubNav("Current")}>
             <View style={{height:50, width:width*0.266, backgroundColor: this.state.navCurrentColor, borderWidth:1, borderTopLeftRadius: 5, borderBottomLeftRadius: 5,  borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
               <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>CURRENT</Text>
             </View>
           </TouchableOpacity>
           <TouchableOpacity onPress={()=>this.selectSubNav("Hourly")}>
             <View style={{height:50, width:width*0.266, backgroundColor: this.state.navHourlyColor, borderTopWidth:1, borderBottomWidth:1, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
               <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>HOURLY</Text>
             </View>
           </TouchableOpacity>
           <TouchableOpacity onPress={()=>this.selectSubNav("Extended")}>
             <View style={{height:50, width:width*0.266, backgroundColor: this.state.navExtendedColor, borderWidth:1, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
               <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>EXTENDED</Text>
             </View>
           </TouchableOpacity>
         </View>
          {this.state.navCurrentColor == subNavColors.active ? <View style={{height:175, width:width*0.8, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
          <View style={{height:26, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
           <View style={{height:70, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
           <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0)'}}>
             <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems:'center'}}>
             <Image source={temp} style={{height:28, width:28}} />
           </View>
             <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems:'center'}}>
             <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Feels Like</Text>
             </View>
             <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems:'center'}}>
             <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[1].currently.apparentTemperature)}˚</Text>
             </View>
           </View>

             <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}>
               <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
               <Image source={windGust} style={{height:28, width:28}} />
             </View>
               <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
               <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Wind / Gust</Text>
               </View>
               <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
               <Text numberOfLines={1} style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{getBearing(this.state.forecast[1].currently.windBearing)} {round(this.state.forecast[1].currently.windSpeed)}/{round(this.state.forecast[1].currently.windGust)}</Text>
               </View>
             </View>
             <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
               <View style={{height:30, width:width*0.2566, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                 <Image source={humidity} style={{height:28, width:28}} />
               </View>
               <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
               <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Humidity</Text>
               </View>
               <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
               <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[1].currently.humidity*100)}%</Text>
               </View>
             </View>
           </View>
           <View style={{height:40, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0)'}}/>
           <View style={{height:75, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
           <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
             <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0)' , alignItems:'center'}}>
             <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Pressure</Text>
             </View>
             <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems:'center'}}>
             <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[1].currently.pressure)}</Text>
             </View>
           </View>
           <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
             <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
             <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Visibility</Text>
             </View>
             <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
             <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[1].currently.visibility)}</Text>
             </View>
           </View>
             <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
               <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
               <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Dew Point</Text>
               </View>
               <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
               <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[1].currently.dewPoint)}˚</Text>
               </View>
             </View>
           </View>

           </View> : null}
          {this.state.navHourlyColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
          <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
            <List horizontal ={true} dataArray={this.state.forecast[1].hourly.data} renderRow={(item) =>

              <View style={{height:175, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
              <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                 <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{formatDate(item.time, this.state.forecast[1].timezone)}</Text>
              </View>
                <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                </View>
                <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

              </View>
              <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                <Text style={{fontWeight:'700', fontSize:15, color:'#fff'}}>{round(item.temperature)}˚</Text>
              </View>

                <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                   <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{getDayoftheWeekHourly(item.time, this.state.forecast[1].timezone)}</Text>
                </View>

              </View>}
            />
          </View>
           : null}
          {this.state.navExtendedColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
          <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                      <List horizontal ={true} dataArray={this.state.forecast[1].daily.data} renderRow={(item) =>

                        <View style={{height:170, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                        <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Text style={{fontWeight:'400', color:'#fff'}}>{getDayoftheWeek(item.time, this.state.forecast[1].timezone)}</Text>
                        </View>

                          <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                          </View>
                          <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                          {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                        </View>
                        <View style={{height:20, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Text style={{fontWeight:'700', fontSize:16, color:'#fff'}}>{round(item.apparentTemperatureHigh)}˚</Text>
                        </View>
                        <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Text style={{fontWeight:'normal', fontSize:14, color:'#fff'}}>{round(item.apparentTemperatureLow)}˚</Text>
                        </View>

                        </View>}
                      />
          </View>
           : null}
         </View> : null}
        
         {this.state.forecast[2] != null ? <View style={{zIndex: 1, height:this.state.myheight2, width:width*0.92,marginTop:5, marginBottom:10, alignSelf:'center', borderRadius:10, overflow:'hidden', flexDirection:'column'}}>
                   {this.state.forecast[2].currently.icon ==  'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{borderRadius:5}}/> : null }
                   {this.state.forecast[2].currently.icon ==  'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[2].currently.icon ==  'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical}/> : null }
                  <TouchableOpacity onPress={()=>this.expandDetails2()}>
                  <View style={{height:40, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row', marginBottom:0, marginTop:15}}>
                   <View style={{width:width*0.08, height: 56, backgroundColor:'rgba(0, 256, 0, 0)'}}/>

                    <View style={{width:width*0.65, height:40, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:0}}>
                      <Text numberOfLines={1} style={{ fontSize: 28, fontWeight: (Platform.OS === 'ios' ) ? '400' : '500', fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{this.state.citynames[2]}</Text>
                    </View>
                    <View style={{width:width*0.075, height: 54, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:3}}>
                    </View>
                    <View style={{width:width*0.1, height:25, backgroundColor:'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop:0}}>
                      <Image source={close} style={{justifyContent:'flex-end',height:28, width:28, transform: [{ rotate: this.state.expanded2}]}} />
                    </View>
                  </View>
                  <View style={{height:110, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row'}}>
                  <View style={{width:width*0.08, height: 80, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                    <View style={{width:width*0.35, height: 94, backgroundColor:'rgba(256, 0, 256, 0)', alignItems:'flex-start'}}>
                      <Text style={{ fontSize: 68, fontWeight: (Platform.OS === 'ios' ) ? '200' : '300',  fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop:5}}>{round(this.state.forecast[2].currently.temperature)}˚ </Text>

                    </View>
                    <View style={{width:width*0.05, height:94, backgroundColor:'rgba(0, 256, 256, 0)'}}>


                    </View>
                    <View style={{width:width*0.45, height:140, backgroundColor:'rgba(0, 0, 256, 0.0)', marginTop:10}}>
                    {this.state.forecast[2].currently.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[2].currently.icon ==  'rain' ? <Image source={weatherRain} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[2].currently.icon ==  'snow' ? <Image source={weatherSnow} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[2].currently.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[2].currently.icon ==  'wind' ? <Image source={weatherWind}  style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'fog' ? <Image source={weatherFog} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'hail' ? <Image source={weatherHail} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[2].currently.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:42, width:42}}/> : null }
                     <View style={{width:width*0.4, height:60, backgroundColor:'rgba(256, 256, 256, 0.0)', marginTop:0}}>
                      <Text numberOfLines={2} style={{ fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop:0}}>{this.state.forecast[2].currently.summary}</Text>
                     </View>
                    </View>

                  </View>
                  <View style={{width:width*0.92, height:25, backgroundColor:'rgba(256, 0,0, 0)', flexDirection:'row'}}>

                  </View>
                  </TouchableOpacity>
                  <View style={{height:60, width:width*0.80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf:'center', flexDirection:'row'}}>
                    <TouchableOpacity onPress={()=>this.selectSubNav("Current")}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navCurrentColor, borderWidth:1, borderTopLeftRadius: 5, borderBottomLeftRadius: 5,  borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>CURRENT</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectSubNav("Hourly")}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navHourlyColor, borderTopWidth:1, borderBottomWidth:1, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>HOURLY</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectSubNav("Extended")}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navExtendedColor, borderWidth:1, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>EXTENDED</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                   {this.state.navCurrentColor == subNavColors.active ? <View style={{height:175, width:width*0.8, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                   <View style={{height:26, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                    <View style={{height:70, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                    <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                      <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Image source={temp} style={{height:28, width:28}} />
                    </View>
                      <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Feels Like</Text>
                      </View>
                      <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[2].currently.apparentTemperature)}˚</Text>
                      </View>
                    </View>

                      <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}>
                        <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Image source={windGust} style={{height:28, width:28}} />
                      </View>
                        <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Wind / Gust</Text>
                        </View>
                        <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text numberOfLines={1} style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{getBearing(this.state.forecast[2].currently.windBearing)} {round(this.state.forecast[2].currently.windSpeed)}/{round(this.state.forecast[2].currently.windGust)}</Text>
                        </View>
                      </View>
                      <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                        <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Image source={humidity} style={{height:28, width:28}} />
                        </View>
                        <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Humidity</Text>
                        </View>
                        <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[2].currently.humidity*100)}%</Text>
                        </View>
                      </View>
                    </View>
                    <View style={{height:40, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                    <View style={{height:75, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                    <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                      <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)' , alignItems:'center'}}>
                      <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Pressure</Text>
                      </View>
                      <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[2].currently.pressure)}</Text>
                      </View>
                    </View>
                    <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                      <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Visibility</Text>
                      </View>
                      <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[2].currently.visibility)}</Text>
                      </View>
                    </View>
                      <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                        <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Dew Point</Text>
                        </View>
                        <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[2].currently.dewPoint)}˚</Text>
                        </View>
                      </View>
                    </View>

                    </View> : null}
                   {this.state.navHourlyColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                   <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                     <List horizontal ={true} dataArray={this.state.forecast[2].hourly.data} renderRow={(item) =>

                       <View style={{height:175, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                       <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{formatDate(item.time,this.state.forecast[2].timezone)}</Text>
                       </View>
                         <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                         </View>
                         <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                         {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                       </View>
                       <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{fontWeight:'700', fontSize:15, color:'#fff'}}>{round(item.temperature)}˚</Text>
                       </View>

                         <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                            <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{getDayoftheWeekHourly(item.time, this.state.forecast[2].timezone)}</Text>
                         </View>

                       </View>}
                     />
                   </View>
                    : null}
                   {this.state.navExtendedColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                   <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                               <List horizontal ={true} dataArray={this.state.forecast[2].daily.data} renderRow={(item) =>

                                 <View style={{height:170, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                                 <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    <Text style={{fontWeight:'400', color:'#fff'}}>{getDayoftheWeek(item.time,  this.state.forecast[2].timezone)}</Text>
                                 </View>

                                   <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                                   </View>
                                   <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                                 </View>
                                 <View style={{height:20, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'700', fontSize:16, color:'#fff'}}>{round(item.apparentTemperatureHigh)}˚</Text>
                                 </View>
                                 <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'normal', fontSize:14, color:'#fff'}}>{round(item.apparentTemperatureLow)}˚</Text>
                                 </View>

                                 </View>}
                               />
                   </View>
                    : null}



                  </View>  : null}
        {this.state.forecast[3] != null ?  <View style={{zIndex: 1, height:this.state.myheight3, width:width*0.92,marginTop:5, marginBottom:10, alignSelf:'center', borderRadius:10, overflow:'hidden', flexDirection:'column'}}>
                    {this.state.forecast[3].currently.icon ==  'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{borderRadius:5}}/> : null }
                    {this.state.forecast[3].currently.icon ==  'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical}/> : null }
                    {this.state.forecast[3].currently.icon ==  'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical}/> : null }
                   <TouchableOpacity onPress={()=>this.expandDetails3()}>
                   <View style={{height:40, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row', marginBottom:0, marginTop:15}}>
                    <View style={{width:width*0.08, height: 56, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                     <View style={{width:width*0.65, height:40, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:0}}>
                       <Text numberOfLines={1} style={{ fontSize: 28, fontWeight: (Platform.OS === 'ios' ) ? '400' : '500', fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{this.state.citynames[3]}</Text>
                     </View>
                     <View style={{width:width*0.075, height: 54, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:3}}>
                     </View>
                     <View style={{width:width*0.1, height:25, backgroundColor:'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop:0}}>
                       <Image source={close} style={{justifyContent:'flex-end',height:28, width:28, transform: [{ rotate: this.state.expanded3}]}} />
                     </View>
                   </View>
                   <View style={{height:110, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row'}}>
                   <View style={{width:width*0.08, height: 80, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                     <View style={{width:width*0.35, height: 94, backgroundColor:'rgba(256, 0, 256, 0)', alignItems:'flex-start'}}>
                       <Text style={{ fontSize: 68, fontWeight: (Platform.OS === 'ios' ) ? '200' : '300',  fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop:5}}>{round(this.state.forecast[3].currently.temperature)}˚ </Text>

                     </View>
                     <View style={{width:width*0.05, height:94, backgroundColor:'rgba(0, 256, 256, 0)'}}>


                     </View>
                     <View style={{width:width*0.45, height:140, backgroundColor:'rgba(0, 0, 256, 0.0)', marginTop:10}}>
                     {this.state.forecast[3].currently.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:42, width:42}} /> : null }
                     {this.state.forecast[3].currently.icon ==  'rain' ? <Image source={weatherRain} style={{height:42, width:42}} /> : null }
                     {this.state.forecast[3].currently.icon ==  'snow' ? <Image source={weatherSnow} style={{height:42, width:42}} /> : null }
                     {this.state.forecast[3].currently.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:42, width:42}} /> : null }
                     {this.state.forecast[3].currently.icon ==  'wind' ? <Image source={weatherWind}  style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'fog' ? <Image source={weatherFog} style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'hail' ? <Image source={weatherHail} style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:42, width:42}}/> : null }
                     {this.state.forecast[3].currently.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:42, width:42}}/> : null }
                      <View style={{width:width*0.4, height:60, backgroundColor:'rgba(256, 256, 256, 0.0)', marginTop:0}}>
                       <Text numberOfLines={2} style={{ fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop:0}}>{this.state.forecast[3].currently.summary}</Text>
                      </View>
                     </View>

                   </View>
                   <View style={{width:width*0.92, height:25, backgroundColor:'rgba(256, 0,0, 0)', flexDirection:'row'}}>

                   </View>
                   </TouchableOpacity>
                   <View style={{height:60, width:width*0.80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf:'center', flexDirection:'row'}}>
                      <TouchableOpacity onPress={()=>this.selectSubNav("Current")}>
                        <View style={{height:50, width:width*0.266, backgroundColor: this.state.navCurrentColor, borderWidth:1, borderTopLeftRadius: 5, borderBottomLeftRadius: 5,  borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>CURRENT</Text>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>this.selectSubNav("Hourly")}>
                        <View style={{height:50, width:width*0.266, backgroundColor: this.state.navHourlyColor, borderTopWidth:1, borderBottomWidth:1, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>HOURLY</Text>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>this.selectSubNav("Extended")}>
                        <View style={{height:50, width:width*0.266, backgroundColor: this.state.navExtendedColor, borderWidth:1, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                          <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>EXTENDED</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                    {this.state.navCurrentColor == subNavColors.active ? <View style={{height:175, width:width*0.8, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                    <View style={{height:26, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                     <View style={{height:70, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                     <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                       <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                       <Image source={temp} style={{height:28, width:28}} />
                     </View>
                       <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                       <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Feels Like</Text>
                       </View>
                       <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                       <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[3].currently.apparentTemperature)}˚</Text>
                       </View>
                     </View>

                       <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}>
                         <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Image source={windGust} style={{height:28, width:28}} />
                       </View>
                         <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Wind / Gust</Text>
                         </View>
                         <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text numberOfLines={1} style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{getBearing(this.state.forecast[3].currently.windBearing)} {round(this.state.forecast[3].currently.windSpeed)}/{round(this.state.forecast[3].currently.windGust)}</Text>
                         </View>
                       </View>
                       <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                         <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Image source={humidity} style={{height:28, width:28}} />
                         </View>
                         <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Humidity</Text>
                         </View>
                         <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[3].currently.humidity*100)}%</Text>
                         </View>
                       </View>
                     </View>
                     <View style={{height:40, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                     <View style={{height:75, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                     <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                       <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)' , alignItems:'center'}}>
                       <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Pressure</Text>
                       </View>
                       <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                       <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[3].currently.pressure)}</Text>
                       </View>
                     </View>
                     <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                       <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                       <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Visibility</Text>
                       </View>
                       <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                       <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[3].currently.visibility)}</Text>
                       </View>
                     </View>
                       <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                         <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Dew Point</Text>
                         </View>
                         <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[3].currently.dewPoint)}˚</Text>
                         </View>
                       </View>
                     </View>

                     </View> : null}
                    {this.state.navHourlyColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                    <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                      <List horizontal ={true} dataArray={this.state.forecast[3].hourly.data} renderRow={(item) =>

                        <View style={{height:175, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                        <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{formatDate(item.time, this.state.forecast[3].timezone)}</Text>
                        </View>
                          <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                          </View>
                          <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                          {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                          {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                          {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                        </View>
                        <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Text style={{fontWeight:'700', fontSize:15, color:'#fff'}}>{round(item.temperature)}˚</Text>
                        </View>

                          <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{getDayoftheWeekHourly(item.time, this.state.forecast[3].timezone)}</Text>
                          </View>

                        </View>}
                      />
                    </View>
                     : null}
                    {this.state.navExtendedColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                    <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                                <List horizontal ={true} dataArray={this.state.forecast[3].daily.data} renderRow={(item) =>

                                  <View style={{height:170, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                                  <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                     <Text style={{fontWeight:'400', color:'#fff'}}>{getDayoftheWeek(item.time,  this.state.forecast[3].timezone)}</Text>
                                  </View>

                                    <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                                    </View>
                                    <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                                    {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                                    {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                                    {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                                    {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                                    {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                                  </View>
                                  <View style={{height:20, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    <Text style={{fontWeight:'700', fontSize:16, color:'#fff'}}>{round(item.apparentTemperatureHigh)}˚</Text>
                                  </View>
                                  <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    <Text style={{fontWeight:'normal', fontSize:14, color:'#fff'}}>{round(item.apparentTemperatureLow)}˚</Text>
                                  </View>

                                  </View>}
                                />
                    </View>
                     : null}



                           </View> : null}
                 {this.state.forecast[4] != null ?  <View style={{zIndex: 1, height:this.state.myheight4, width:width*0.92,marginTop:5, marginBottom:10, alignSelf:'center', borderRadius:10, overflow:'hidden', flexDirection:'column'}}>
                             {this.state.forecast[4].currently.icon ==  'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{borderRadius:5}}/> : null }
                             {this.state.forecast[4].currently.icon ==  'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical}/> : null }
                             {this.state.forecast[4].currently.icon ==  'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical}/> : null }
                            <TouchableOpacity onPress={()=>this.expandDetails4()}>
                            <View style={{height:40, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row', marginBottom:0, marginTop:15}}>
                             <View style={{width:width*0.08, height: 56, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                              <View style={{width:width*0.65, height:40, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:0}}>
                                <Text numberOfLines={1} style={{ fontSize: 28, fontWeight: (Platform.OS === 'ios' ) ? '400' : '500', fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{this.state.citynames[4]}</Text>
                              </View>
                              <View style={{width:width*0.075, height: 54, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:3}}>
                              </View>
                              <View style={{width:width*0.1, height:25, backgroundColor:'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop:0}}>
                                <Image source={close} style={{justifyContent:'flex-end',height:28, width:28, transform: [{ rotate: this.state.expanded4}]}} />
                              </View>
                            </View>
                            <View style={{height:110, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row'}}>
                            <View style={{width:width*0.08, height: 80, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                              <View style={{width:width*0.35, height: 94, backgroundColor:'rgba(256, 0, 256, 0)', alignItems:'flex-start'}}>
                                <Text style={{ fontSize: 68, fontWeight: (Platform.OS === 'ios' ) ? '200' : '300',  fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop:5}}>{round(this.state.forecast[4].currently.temperature)}˚ </Text>

                              </View>
                              <View style={{width:width*0.05, height:94, backgroundColor:'rgba(0, 256, 256, 0)'}}>


                              </View>
                              <View style={{width:width*0.45, height:140, backgroundColor:'rgba(0, 0, 256, 0.0)', marginTop:10}}>
                              {this.state.forecast[4].currently.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:42, width:42}} /> : null }
                              {this.state.forecast[4].currently.icon ==  'rain' ? <Image source={weatherRain} style={{height:42, width:42}} /> : null }
                              {this.state.forecast[4].currently.icon ==  'snow' ? <Image source={weatherSnow} style={{height:42, width:42}} /> : null }
                              {this.state.forecast[4].currently.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:42, width:42}} /> : null }
                              {this.state.forecast[4].currently.icon ==  'wind' ? <Image source={weatherWind}  style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'fog' ? <Image source={weatherFog} style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'hail' ? <Image source={weatherHail} style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:42, width:42}}/> : null }
                              {this.state.forecast[4].currently.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:42, width:42}}/> : null }
                               <View style={{width:width*0.4, height:60, backgroundColor:'rgba(256, 256, 256, 0.0)', marginTop:0}}>
                                <Text numberOfLines={2} style={{ fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop:0}}>{this.state.forecast[4].currently.summary}</Text>
                               </View>
                              </View>

                            </View>
                            <View style={{width:width*0.92, height:25, backgroundColor:'rgba(256, 0,0, 0)', flexDirection:'row'}}>

                            </View>
                            </TouchableOpacity>
                            <View style={{height:60, width:width*0.80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf:'center', flexDirection:'row'}}>
                              <TouchableOpacity onPress={()=>this.selectSubNav("Current")}>
                                <View style={{height:50, width:width*0.266, backgroundColor: this.state.navCurrentColor, borderWidth:1, borderTopLeftRadius: 5, borderBottomLeftRadius: 5,  borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                                  <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>CURRENT</Text>
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity onPress={()=>this.selectSubNav("Hourly")}>
                                <View style={{height:50, width:width*0.266, backgroundColor: this.state.navHourlyColor, borderTopWidth:1, borderBottomWidth:1, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                                  <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>HOURLY</Text>
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity onPress={()=>this.selectSubNav("Extended")}>
                                <View style={{height:50, width:width*0.266, backgroundColor: this.state.navExtendedColor, borderWidth:1, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                                  <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>EXTENDED</Text>
                                </View>
                              </TouchableOpacity>
                            </View>
                             {this.state.navCurrentColor == subNavColors.active ? <View style={{height:175, width:width*0.8, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                             <View style={{height:26, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                              <View style={{height:70, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                              <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                                <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                <Image source={temp} style={{height:28, width:28}} />
                              </View>
                                <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Feels Like</Text>
                                </View>
                                <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[4].currently.apparentTemperature)}˚</Text>
                                </View>
                              </View>

                                <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}>
                                  <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                  <Image source={windGust} style={{height:28, width:28}} />
                                </View>
                                  <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                  <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Wind / Gust</Text>
                                  </View>
                                  <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                  <Text numberOfLines={1} style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{getBearing(this.state.forecast[4].currently.windBearing)} {round(this.state.forecast[4].currently.windSpeed)}/{round(this.state.forecast[4].currently.windGust)}</Text>
                                  </View>
                                </View>
                                <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                                  <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    <Image source={humidity} style={{height:28, width:28}} />
                                  </View>
                                  <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                  <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Humidity</Text>
                                  </View>
                                  <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                  <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[4].currently.humidity*100)}%</Text>
                                  </View>
                                </View>
                              </View>
                              <View style={{height:40, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                              <View style={{height:75, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                              <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                                <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)' , alignItems:'center'}}>
                                <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Pressure</Text>
                                </View>
                                <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[4].currently.pressure)}</Text>
                                </View>
                              </View>
                              <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                                <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Visibility</Text>
                                </View>
                                <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[4].currently.visibility)}</Text>
                                </View>
                              </View>
                                <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                                  <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                  <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Dew Point</Text>
                                  </View>
                                  <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                  <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[4].currently.dewPoint)}˚</Text>
                                  </View>
                                </View>
                              </View>

                              </View> : null}
                             {this.state.navHourlyColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                             <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                               <List horizontal ={true} dataArray={this.state.forecast[4].hourly.data} renderRow={(item) =>

                                 <View style={{height:175, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                                 <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{formatDate(item.time, this.state.forecast[4].timezone)}</Text>
                                 </View>
                                   <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                                   </View>
                                   <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                                 </View>
                                 <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'700', fontSize:15, color:'#fff'}}>{round(item.temperature)}˚</Text>
                                 </View>

                                   <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                      <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{getDayoftheWeekHourly(item.time, this.state.forecast[4].timezone)}</Text>
                                   </View>

                                 </View>}
                               />
                             </View>
                              : null}
                             {this.state.navExtendedColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                             <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                                         <List horizontal ={true} dataArray={this.state.forecast[4].daily.data} renderRow={(item) =>

                                           <View style={{height:170, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                                           <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                              <Text style={{fontWeight:'400', color:'#fff'}}>{getDayoftheWeek(item.time,  this.state.forecast[4].timezone)}</Text>
                                           </View>

                                             <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                             <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                                             </View>
                                             <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                             {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                                             {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                                             {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                                             {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                                             {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                                             {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                                           </View>
                                           <View style={{height:20, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                             <Text style={{fontWeight:'700', fontSize:16, color:'#fff'}}>{round(item.apparentTemperatureHigh)}˚</Text>
                                           </View>
                                           <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                             <Text style={{fontWeight:'normal', fontSize:14, color:'#fff'}}>{round(item.apparentTemperatureLow)}˚</Text>
                                           </View>

                                           </View>}
                                         />
                             </View>
                              : null}



                                      </View> : null}
            {this.state.forecast[5] != null ?  <View style={{zIndex: 1, height:this.state.myheight5, width:width*0.92,marginTop:5, marginBottom:10, alignSelf:'center', borderRadius:10, overflow:'hidden', flexDirection:'column'}}>
                        {this.state.forecast[5].currently.icon ==  'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{borderRadius:5}}/> : null }
                        {this.state.forecast[5].currently.icon ==  'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical}/> : null }
                        {this.state.forecast[5].currently.icon ==  'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical}/> : null }
                       <TouchableOpacity onPress={()=>this.expandDetails5()}>
                       <View style={{height:40, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row', marginBottom:0, marginTop:15}}>
                        <View style={{width:width*0.08, height: 56, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                         <View style={{width:width*0.65, height:40, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:0}}>
                           <Text numberOfLines={1} style={{ fontSize: 28, fontWeight: (Platform.OS === 'ios' ) ? '400' : '500', fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{this.state.citynames[5]}</Text>
                         </View>
                         <View style={{width:width*0.075, height: 54, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:3}}>
                         </View>
                         <View style={{width:width*0.1, height:25, backgroundColor:'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop:0}}>
                           <Image source={close} style={{justifyContent:'flex-end',height:28, width:28, transform: [{ rotate: this.state.expanded5}]}} />
                         </View>
                       </View>
                       <View style={{height:110, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row'}}>
                       <View style={{width:width*0.08, height: 80, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                         <View style={{width:width*0.35, height: 94, backgroundColor:'rgba(256, 0, 256, 0)', alignItems:'flex-start'}}>
                           <Text style={{ fontSize: 68, fontWeight: (Platform.OS === 'ios' ) ? '200' : '300',  fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop:5}}>{round(this.state.forecast[5].currently.temperature)}˚ </Text>

                         </View>
                         <View style={{width:width*0.05, height:94, backgroundColor:'rgba(0, 256, 256, 0)'}}>


                         </View>
                         <View style={{width:width*0.45, height:140, backgroundColor:'rgba(0, 0, 256, 0.0)', marginTop:10}}>
                         {this.state.forecast[5].currently.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:42, width:42}} /> : null }
                         {this.state.forecast[5].currently.icon ==  'rain' ? <Image source={weatherRain} style={{height:42, width:42}} /> : null }
                         {this.state.forecast[5].currently.icon ==  'snow' ? <Image source={weatherSnow} style={{height:42, width:42}} /> : null }
                         {this.state.forecast[5].currently.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:42, width:42}} /> : null }
                         {this.state.forecast[5].currently.icon ==  'wind' ? <Image source={weatherWind}  style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'fog' ? <Image source={weatherFog} style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'hail' ? <Image source={weatherHail} style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:42, width:42}}/> : null }
                         {this.state.forecast[5].currently.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:42, width:42}}/> : null }
                          <View style={{width:width*0.4, height:60, backgroundColor:'rgba(256, 256, 256, 0.0)', marginTop:0}}>
                           <Text numberOfLines={2} style={{ fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop:0}}>{this.state.forecast[5].currently.summary}</Text>
                          </View>
                         </View>

                       </View>
                       <View style={{width:width*0.92, height:25, backgroundColor:'rgba(256, 0,0, 0)', flexDirection:'row'}}>

                       </View>
                       </TouchableOpacity>
                       <View style={{height:60, width:width*0.80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf:'center', flexDirection:'row'}}>
                        <TouchableOpacity onPress={()=>this.selectSubNav("Current")}>
                          <View style={{height:50, width:width*0.266, backgroundColor: this.state.navCurrentColor, borderWidth:1, borderTopLeftRadius: 5, borderBottomLeftRadius: 5,  borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                            <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>CURRENT</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.selectSubNav("Hourly")}>
                          <View style={{height:50, width:width*0.266, backgroundColor: this.state.navHourlyColor, borderTopWidth:1, borderBottomWidth:1, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                            <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>HOURLY</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.selectSubNav("Extended")}>
                          <View style={{height:50, width:width*0.266, backgroundColor: this.state.navExtendedColor, borderWidth:1, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                            <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>EXTENDED</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                        {this.state.navCurrentColor == subNavColors.active ? <View style={{height:175, width:width*0.8, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                        <View style={{height:26, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                         <View style={{height:70, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                         <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                           <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Image source={temp} style={{height:28, width:28}} />
                         </View>
                           <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Feels Like</Text>
                           </View>
                           <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[5].currently.apparentTemperature)}˚</Text>
                           </View>
                         </View>

                           <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}>
                             <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Image source={windGust} style={{height:28, width:28}} />
                           </View>
                             <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Wind / Gust</Text>
                             </View>
                             <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Text numberOfLines={1} style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{getBearing(this.state.forecast[5].currently.windBearing)} {round(this.state.forecast[5].currently.windSpeed)}/{round(this.state.forecast[5].currently.windGust)}</Text>
                             </View>
                           </View>
                           <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                             <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                               <Image source={humidity} style={{height:28, width:28}} />
                             </View>
                             <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Humidity</Text>
                             </View>
                             <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[5].currently.humidity*100)}%</Text>
                             </View>
                           </View>
                         </View>
                         <View style={{height:40, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                         <View style={{height:75, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                         <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                           <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)' , alignItems:'center'}}>
                           <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Pressure</Text>
                           </View>
                           <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[5].currently.pressure)}</Text>
                           </View>
                         </View>
                         <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                           <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Visibility</Text>
                           </View>
                           <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                           <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[5].currently.visibility)}</Text>
                           </View>
                         </View>
                           <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                             <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Dew Point</Text>
                             </View>
                             <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                             <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[5].currently.dewPoint)}˚</Text>
                             </View>
                           </View>
                         </View>

                         </View> : null}
                        {this.state.navHourlyColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                        <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                          <List horizontal ={true} dataArray={this.state.forecast[5].hourly.data} renderRow={(item) =>

                            <View style={{height:175, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                            <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                               <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{formatDate(item.time, this.state.forecast[5].timezone)}</Text>
                            </View>
                              <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                              <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                              </View>
                              <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                              {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                              {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                              {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                              {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                              {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                              {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                              {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                              {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                              {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                              {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                              {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                              {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                              {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                            </View>
                            <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                              <Text style={{fontWeight:'700', fontSize:15, color:'#fff'}}>{round(item.temperature)}˚</Text>
                            </View>

                              <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                 <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{getDayoftheWeekHourly(item.time, this.state.forecast[5].timezone)}</Text>
                              </View>

                            </View>}
                          />
                        </View>
                         : null}
                        {this.state.navExtendedColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                        <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                                    <List horizontal ={true} dataArray={this.state.forecast[5].daily.data} renderRow={(item) =>

                                      <View style={{height:170, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                                      <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                         <Text style={{fontWeight:'400', color:'#fff'}}>{getDayoftheWeek(item.time,  this.state.forecast[5].timezone)}</Text>
                                      </View>

                                        <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                        <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                                        </View>
                                        <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                        {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                                        {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                                        {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                                        {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                                        {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                                        {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                                      </View>
                                      <View style={{height:20, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                        <Text style={{fontWeight:'700', fontSize:16, color:'#fff'}}>{round(item.apparentTemperatureHigh)}˚</Text>
                                      </View>
                                      <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                        <Text style={{fontWeight:'normal', fontSize:14, color:'#fff'}}>{round(item.apparentTemperatureLow)}˚</Text>
                                      </View>

                                      </View>}
                                    />
                        </View>
                         : null}



                       </View> : null}
       {this.state.forecast[6] != null ?  <View style={{zIndex: 1, height:this.state.myheight6, width:width*0.92,marginTop:5, marginBottom:10, alignSelf:'center', borderRadius:10, overflow:'hidden', flexDirection:'column'}}>
                   {this.state.forecast[6].currently.icon ==  'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{borderRadius:5}}/> : null }
                   {this.state.forecast[6].currently.icon ==  'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical}/> : null }
                   {this.state.forecast[6].currently.icon ==  'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical}/> : null }
                  <TouchableOpacity onPress={()=>this.expandDetails6()}>
                  <View style={{height:40, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row', marginBottom:0, marginTop:15}}>
                   <View style={{width:width*0.08, height: 56, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                    <View style={{width:width*0.65, height:40, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:0}}>
                      <Text numberOfLines={1} style={{ fontSize: 28, fontWeight: (Platform.OS === 'ios' ) ? '400' : '500', fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{this.state.citynames[6]}</Text>
                    </View>
                    <View style={{width:width*0.075, height: 54, backgroundColor:'rgba(0, 0, 0, 0)', marginTop:3}}>
                    </View>
                    <View style={{width:width*0.1, height:25, backgroundColor:'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop:0}}>
                      <Image source={close} style={{justifyContent:'flex-end',height:28, width:28, transform: [{ rotate: this.state.expanded6}]}} />
                    </View>
                  </View>
                  <View style={{height:110, width:width*0.92, backgroundColor: 'rgba(0, 0, 0, 0.0)', flexDirection: 'row'}}>
                  <View style={{width:width*0.08, height: 80, backgroundColor:'rgba(0, 256, 0, 0)'}}/>
                    <View style={{width:width*0.35, height: 94, backgroundColor:'rgba(256, 0, 256, 0)', alignItems:'flex-start'}}>
                      <Text style={{ fontSize: 68, fontWeight: (Platform.OS === 'ios' ) ? '200' : '300',  fontFamily: (Platform.OS === 'ios' ) ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop:5}}>{round(this.state.forecast[6].currently.temperature)}˚ </Text>

                    </View>
                    <View style={{width:width*0.05, height:94, backgroundColor:'rgba(0, 256, 256, 0)'}}>


                    </View>
                    <View style={{width:width*0.45, height:140, backgroundColor:'rgba(0, 0, 256, 0.0)', marginTop:10}}>
                    {this.state.forecast[6].currently.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[6].currently.icon ==  'rain' ? <Image source={weatherRain} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[6].currently.icon ==  'snow' ? <Image source={weatherSnow} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[6].currently.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:42, width:42}} /> : null }
                    {this.state.forecast[6].currently.icon ==  'wind' ? <Image source={weatherWind}  style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'fog' ? <Image source={weatherFog} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'hail' ? <Image source={weatherHail} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:42, width:42}}/> : null }
                    {this.state.forecast[6].currently.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:42, width:42}}/> : null }
                     <View style={{width:width*0.4, height:60, backgroundColor:'rgba(256, 256, 256, 0.0)', marginTop:0}}>
                      <Text numberOfLines={2} style={{ fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop:0}}>{this.state.forecast[6].currently.summary}</Text>
                     </View>
                    </View>

                  </View>
                  <View style={{width:width*0.92, height:25, backgroundColor:'rgba(256, 0,0, 0)', flexDirection:'row'}}>

                  </View>
                  </TouchableOpacity>
                   <View style={{height:60, width:width*0.80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf:'center', flexDirection:'row'}}>
                    <TouchableOpacity onPress={()=>this.selectSubNav("Current")}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navCurrentColor, borderWidth:1, borderTopLeftRadius: 5, borderBottomLeftRadius: 5,  borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>CURRENT</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectSubNav("Hourly")}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navHourlyColor, borderTopWidth:1, borderBottomWidth:1, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>HOURLY</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectSubNav("Extended")}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navExtendedColor, borderWidth:1, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>EXTENDED</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                   {this.state.navCurrentColor == subNavColors.active ? <View style={{height:175, width:width*0.8, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                   <View style={{height:26, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                    <View style={{height:70, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                    <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                      <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Image source={temp} style={{height:28, width:28}} />
                    </View>
                      <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Feels Like</Text>
                      </View>
                      <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[6].currently.apparentTemperature)}˚</Text>
                      </View>
                    </View>

                      <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}>
                        <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Image source={windGust} style={{height:28, width:28}} />
                      </View>
                        <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Wind / Gust</Text>
                        </View>
                        <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text numberOfLines={1} style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{getBearing(this.state.forecast[6].currently.windBearing)} {round(this.state.forecast[6].currently.windSpeed)}/{round(this.state.forecast[6].currently.windGust)}</Text>
                        </View>
                      </View>
                      <View style={{height:70, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                        <View style={{height:30, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Image source={humidity} style={{height:28, width:28}} />
                        </View>
                        <View style={{height:18, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Humidity</Text>
                        </View>
                        <View style={{height:26, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[6].currently.humidity*100)}%</Text>
                        </View>
                      </View>
                    </View>
                    <View style={{height:40, width:width*0.8, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                    <View style={{height:75, width:width*0.8, backgroundColor:'transparent', flexDirection:'row'}}>
                    <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                      <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)' , alignItems:'center'}}>
                      <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Pressure</Text>
                      </View>
                      <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[6].currently.pressure)}</Text>
                      </View>
                    </View>
                    <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                      <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Visibility</Text>
                      </View>
                      <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                      <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[6].currently.visibility)}</Text>
                      </View>
                    </View>
                      <View style={{height:75, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                        <View style={{height:20, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{color:'rgba(256, 256, 256, 0.7)', fontWeight:'200', fontSize:12}}>Dew Point</Text>
                        </View>
                        <View style={{height:40, width:width*0.266, backgroundColor: 'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                        <Text style={{ fontSize: 14, fontWeight:'500', color: '#fff', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0)'}}>{round(this.state.forecast[6].currently.dewPoint)}˚</Text>
                        </View>
                      </View>
                    </View>

                    </View> : null}
                   {this.state.navHourlyColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                   <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                     <List horizontal ={true} dataArray={this.state.forecast[6].hourly.data} renderRow={(item) =>

                       <View style={{height:175, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                       <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                          <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{formatDate(item.time, this.state.forecast[6].timezone)}</Text>
                       </View>
                         <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                         </View>
                         <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                         {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                         {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                         {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                       </View>
                       <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                         <Text style={{fontWeight:'700', fontSize:15, color:'#fff'}}>{round(item.temperature)}˚</Text>
                       </View>

                         <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                            <Text style={{fontWeight:'400', color:'#fff', fontSize:15}}>{getDayoftheWeekHourly(item.time, this.state.forecast[6].timezone)}</Text>
                         </View>

                       </View>}
                     />
                   </View>
                    : null}
                   {this.state.navExtendedColor == subNavColors.active ? <View style={{height:175, width:width*0.9, marginTop:10, alignSelf:'center', backgroundColor: 'rgba(256, 256, 256, 0.0)'}}>
                   <View style={{height:26, width:width*0.92, backgroundColor: 'rgba(256, 0, 256, 0.0)'}}/>
                               <List horizontal ={true} dataArray={this.state.forecast[6].daily.data} renderRow={(item) =>

                                 <View style={{height:170, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', borderLeftWidth:1, borderColor:'#fff'}}>
                                 <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                    <Text style={{fontWeight:'400', color:'#fff'}}>{getDayoftheWeek(item.time,  this.state.forecast[6].timezone)}</Text>
                                 </View>

                                   <View style={{height:25, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'400', fontSize:12, color:'#fff'}}>{getpercent(round(item.precipProbability*100))}</Text>
                                   </View>
                                   <View style={{height:40, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   {item.icon ==  'clear-day' ? <Image source={weatherClearDay} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'clear-night' ? <Image source={weatherClearNight} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'rain' ? <Image source={weatherRain} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'snow' ? <Image source={weatherSnow} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'sleet' ? <Image source={weatherSleet} style={{height:28, width:28}} /> : null }
                                   {item.icon ==  'wind' ? <Image source={weatherWind}  style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'fog' ? <Image source={weatherFog} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'cloudy' ? <Image source={weatherCloudy} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'hail' ? <Image source={weatherHail} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'thunderstorm' ? <Image source={weatherThunderstorm} style={{height:28, width:28}}/> : null }
                                   {item.icon ==  'tornado' ? <Image source={weatherTornado} style={{height:28, width:28}}/> : null }

                                 </View>
                                 <View style={{height:20, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'700', fontSize:16, color:'#fff'}}>{round(item.apparentTemperatureHigh)}˚</Text>
                                 </View>
                                 <View style={{height:30, width:width*0.15, backgroundColor:'rgba(256, 256, 256, 0.0)', alignItems:'center'}}>
                                   <Text style={{fontWeight:'normal', fontSize:14, color:'#fff'}}>{round(item.apparentTemperatureLow)}˚</Text>
                                 </View>

                                 </View>}
                               />
                   </View>
                    : null}

                  </View> : null}
   </ScrollView>
</SafeAreaView>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }, row:{
    flexDirection:'row',
    alignItems:'center',
    margin:7,
  }
});

const vertical = {
  start: {x: 0.5, y: 0},
  end: {x: 0.5, y: 1}
};

const navColors = {
  active:
    '#4f93e2'
  ,
  inactive:
    '#77787a'

};

const direction = {

  vertical: '{start: {x: 0, y: 0.4}, end: {x: 1, y: 0.6}}',
  slant: '{start: {x: 1, y: 0}, end: {x: 0, y: 1}}'
};



const myWeather = {
  weatherClearDay: [
    '#2e61c1', '#7ac5ee'
  ],
  weatherPartlyCloudyDay: [
    '#678ccf', '#8fc8e6'
  ],
  weatherCloudy: [
    '#5770a1', '#90c1dc'
  ],
  weatherClearNight: [
    '#211e57', '#5d59be'
  ],
  weatherPartlyCloudyNight: [
    '#112158', '#443f82', '#9762a2'
  ],
  weatherWind: [
    '#2c74d5', '#82aee9'
  ],
  weatherFog: [
    '#a79bbe', '#cfbed5'
  ],
  weatherRain: [
    '#2b5685', '#6bb2d5'
  ],
  weatherHail: [
    '#3c5c80', '#a1b8d1'
  ],
  weatherSleet: [
    '#67738d', '#aeb5c3'
  ],
  weatherSnow: [
    '#7d9cbc', '#b6c9dc'
  ],
  weatherThunderstorm: [
    '#3d426c', '#b19ab4'
  ],
  weatherTornado: [
    '#21324f', '#b5a8a3'
  ]

};

const mapStateToProps = (state) =>({
  userData: state.user.userDetails,
  getUnitData: state.user.getUserIdState,
  location: state.location,
  wUnits: state.user.wUnits,
})

const mapDispatchToProps = dispatch => ({
  WeatherActions: bindActionCreators(WeatherActions, dispatch),
  UserActions: bindActionCreators(UserActions, dispatch),
  userLocation: bindActionCreators(userLocation, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Weather);