/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  View, Dimensions, Platform, Image, Linking, Alert
} from 'react-native';
import { Container, Header, Content, Tabs, Tab,TabHeading, Card, CardItem, cardBody, Badge, Body, Button, Segment, List, ListItem, Left, Right, Separator } from 'native-base';

import AnimatedLinearGradient, {presetColors} from 'react-native-animated-linear-gradient'
import SettingsStore from '../../stores/settingsStore'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import DeviceInfo from 'react-native-device-info';
import FlipToggle from 'react-native-flip-toggle-button';
import theme from '../../theme/base-theme';
import {registerKilledListener, registerAppListener} from "../../messaging/Listeners";
import FCM, { FCMEvent } from "react-native-fcm";
import {patchNotifications} from '../../functions/weatherfuncs';
const settings = new SettingsStore()
const settingsad = require('../../img/setting-ad.png')
const dslogo = require('../../img/poweredby-ds.png')
import Notifications from './notifications_updated';

export default class Settings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "",
      temploaded:false,
      units:'',
      myID:'',
      isFahrenheit:true,
      testing:false,
      resetbutton:true

    }

  }

  componentWillMount(){
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.baseUrl = settings.BaseUrl;
    this.getMyId();
   
  }

  componentWillUnmount(){

  }

  componentDidMount(){
    this.refs._scrollView.scrollTo({x:0,y:0,animated:true});
  }

  changeUnits(val) {
      if (val == false) {
        this.setState({isFahrenheit:false, units:'si'})
        //this.updateUnits();
        setTimeout(() => {this.updateUnits();}, 1000)
      } 

      if (val == true) {
        this.setState({isFahrenheit:true, units:'us'})
        setTimeout(() => {this.updateUnits();}, 1000)
      }
  }

  setInitial() {
    if (this.state.units == 'us') {
      this.setState({isFahrenheit:true, temploaded:true});

    }
    if (this.state.units == 'si') {
      this.setState({isFahrenheit:false, temploaded:true});
    }
  }

  async getMyId() {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_users?filter[where][wDeviceID]=' + this.state.uniqueId, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      
      //
      if(response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        //
        this.setState({myID:parsed[0].id, units:parsed[0].wUnits})
        this.setInitial();

      } else {
        let errors = res;
        throw errors;
      }
    } catch(error) {
      this.setState({errors: error});
      
    }
  }

  async getMyNotifyId() {
    
    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification?filter[where][wdeviceid]=' + this.state.uniqueId, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      
      //
      if(response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)

        if (parsed.length == 0) {

            
            this.requesNotificationPermission();
        }

        else {
          
            this.deleteNotification(parsed[0].id);
       }
      } else {
        let errors = res;
        throw errors;
      }
    } catch(error) {
      this.setState({errors: error});
      
    }
  }

  async deleteNotification(id) {
    try {

      let response = await fetch(this.state.baseUrl + 'wn_notification/' +  id, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        }
      });
      
      let res = await response.text();
      //

      if(response.status >= 200 && response.status < 300) {

        this.requesNotificationPermission();

      } else {
        let errors = res;
        throw errors;
      }

    } catch(errors) {
      
      let formErrors = JSON.parse(errors);
      let errorsArray = [];
      for(let key in formErrors) {
        if(formErrors[key].length > 1) {
          formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
        } else {
            errorsArray.push(`${key} ${formErrors[key]}`)
        }
      }
       this.setState({errors: errorsArray});
    }

  }

  resetNotifications() {
    this.setState({resetbutton: false});
    Alert.alert(
      'Reset Notifications',
      'In order for this process to work, please be sure you’re connected to the internet.',
      [
        {text: 'Cancel', onPress: () => this.setState({resetbutton: true}), style: 'cancel'},
        {text: 'Continue', onPress: () =>  this.getMyNotifyId()},
      ],
      { cancelable: false }
    )
  }

  async  requesNotificationPermission() {
    this.setState({notificationLoading:true});
    registerAppListener();

    try{
      let result = await FCM.requestPermissions({badge: false, sound: true, alert: true});
    } catch(e){
      
    }


  // New token
  FCM.on(FCMEvent.RefreshToken, (token) => { 
    
    this.setState({token: token || ""});
    //this.addToken();
});

FCM.getFCMToken().then(token => {
  
  this.setState({token: token || ""});

  this.addToken();
});
  
    

    if (Platform.OS === 'ios') {
      FCM.getAPNSToken().then(token => {
        
      });
    }
}

async addToken() {
  
  try {

    let response = await fetch(this.state.baseUrl + 'wn_notification/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        wdeviceid:DeviceInfo.getUniqueID(),
        wtoken:this.state.token,
        wnotifyOOR:false,
        wnotifySunday:false,
        wnotifyMonday:true,
        wnotifyTuesday:true,
        wnotifyWednesday:true,
        wnotifyThursday:true,
        wnotifyFriday:true,
        wnotifySaturday:false,
        wnotify1:false,
        wnotifyTime1:'07:00',
        wnotifyTime1Show:'',
        wnotify2:false,
        wnotifyTime2:'16:00',
        wnotifyTime2Show:'',
        })
    });
    
    let res = await response.text();
    //

    if(response.status >= 200 && response.status < 300) {
      this.setState({notificationLoading: false, skipped:false});
      this.setState({error:""});
      let parsed = JSON.parse(res);
      
      if (this.state.token.length > 1) {
        Alert.alert(
          'Reset successful',
          'Please return to the Notifications tab to turn on your daily alerts.'
        )
      }
      else {
        Alert.alert(
          'Reset not successful',
          'Please check your internet connection and try again.'
        )
      }
      this.setState({resetbutton: true});
      this.getCurPos();      

    } else {
      let errors = res;
      throw errors;
    }

  } catch(errors) {
    
    let formErrors = JSON.parse(errors);
    let errorsArray = [];
    for(let key in formErrors) {
      if(formErrors[key].length > 1) {
        formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
      } else {
          errorsArray.push(`${key} ${formErrors[key]}`)
      }
    }
     this.setState({errors: errorsArray});
  }
}

 async  getCurPos() {

  navigator.geolocation.getCurrentPosition(
      (position) => {
        if(position.coords && position.coords.latitude && position.coords.longitude){
          patchNotifications(settings.BaseUrl, position.coords.latitude,position.coords.longitude,DeviceInfo.getUniqueID());
        }
      },
      (error) => console.log("error",error),
      {enableHighAccuracy: false, timeout: 10000, maximumAge: 1000}
    );
}

  async updateUnits() {

      try {

        let response = await fetch(this.state.baseUrl + 'wn_users/' +  this.state.myID, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({
              wUnits: this.state.units,
            })
        });
        
        let res = await response.text();
        //

        if(response.status >= 200 && response.status < 300) {
          // console.log('updateUnits*****',response)
        } else {
          let errors = res;
          throw errors;
        }

      } catch(errors) {
        
        let formErrors = JSON.parse(errors);
        let errorsArray = [];
        for(let key in formErrors) {
          if(formErrors[key].length > 1) {
            formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
          } else {
              errorsArray.push(`${key} ${formErrors[key]}`)
          }
        }
         this.setState({errors: errorsArray});
      }
    }

  render() {

    if (this.state.testing) {
      return (
        <View style={{width:width, height:height, backgroundColor:'#fff', alignItems:'center'}}>
        <Text>Settings</Text>
        </View>
      )
    }


    else
    return (
      <ScrollView ref='_scrollView' scrollEnabled={false} style={{backgroundColor:'#fff'}}>
      <List style={{backgroundColor:'#d9e0e2'}} ref={(ref) => { this.listRef = ref; }}>
        <Separator bordered style={{height:40}}>
          <Text style={{color:theme.brandPrimary}}>Settings</Text>
        </Separator>
      </List>
      <List>
        <ListItem style={{width:width-20, height: 50}}>
        <Left>
          <Text style={{color:theme.brandPrimary}}>Temperature</Text>
          </Left>
            <Right>
              {this.state.temploaded ? <FlipToggle
              value={this.state.isFahrenheit}
              buttonWidth={65}
              buttonHeight={25}
              buttonOnColor={theme.brandPrimary}
              buttonOffColor={theme.brandPrimary}
              sliderWidth={20}
              sliderHeight={20}
              sliderOnColor={theme.brandWhite}
              sliderOffColor={theme.brandWhite}
              onLabel={'F˚'}
              offLabel={'C˚'}
              labelStyle={{ color: '#fff', fontSize: 16, fontWeight: 'bold'}}
              onToggle={(value) => {this.changeUnits(value)}}
          /> : null}
          </Right> 
        </ListItem>
      </List>
      <List>
        <ListItem style={{width:width-20, height: 50}}>
          <Left>
            <Text style={{color:theme.brandPrimary}}>Reset Notifications</Text>
          </Left>
          <Right>
            {this.state.resetbutton == true ? <Button onPress={() => this.resetNotifications()} style={{width:65, backgroundColor:theme.brandPrimary, alignSelf:'center',justifyContent:'center', borderRadius:10, marginLeft:5}}><Text style={{color:theme.brandWhite, alignSelf:'center', justifyContent:'center'}}>Reset</Text></Button>: null}
          </Right> 
        </ListItem>
      </List>
      <List style={{backgroundColor:'#d9e0e2'}}>
        <Separator bordered style={{height:40}}>
          <Text style={{color:theme.brandPrimary}}>About</Text>
        </Separator>
      </List>
      <List>
      <ListItem style={{width:width-20, height: 50}}>
       <Left>
         <Text style={{color:theme.brandPrimary}}>Version</Text>
        </Left>
      <Right>
      <Text style={{color:theme.brandPrimary}}>2.0.4</Text>
      </Right>
      </ListItem>
      <Separator bordered style={{height:40}}>
        <Text style={{color:theme.brandPrimary}}></Text>
      </Separator>
      {/* <Separator bordered style={{height:40}}>
        <Text style={{color:theme.brandPrimary}}>Notifications</Text>
      </Separator> */}
      </List>
      {/* <Notifications /> */}
         <View style={{width:width*0.90, alignSelf:'center', flexDirection:'row', justifyContent:'space-between', marginTop: 10, marginBottom:5}}>
          <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:5, color:theme.brandPrimary}} onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com/pages/app')}}>Support/FAQs</Text>
          <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:5, color:theme.brandPrimary}} onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com/pages/privacy-policy')}}>Privacy Policy</Text>
          <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:5, color:theme.brandPrimary}} onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com/pages/terms-of-service')}}>Legal</Text>
         </View>  
         <View style={{alignSelf:'center', marginTop:10}}>
         <TouchableOpacity onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com')}}>
        <Image source={settingsad} resizeMode='contain' style={{height:225, width:300}}/>
        </TouchableOpacity>
      </View>
      <View style={{alignSelf:'center', marginTop:5}}>
        <Image source={dslogo} style={{height:40, width:width*0.5}}/>
      </View>
      </ScrollView>
    );
  }
}

