/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  View, Dimensions, Platform, Image, Linking, Alert
} from 'react-native';
import { Container, Header, Content, Tabs, Tab,TabHeading, Card, CardItem, cardBody, Badge, Body, Button, Segment, List, ListItem, Left, Right, Separator } from 'native-base';

import AnimatedLinearGradient, {presetColors} from 'react-native-animated-linear-gradient'
import SettingsStore from '../../stores/settingsStore';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import DeviceInfo from 'react-native-device-info';
import FlipToggle from 'react-native-flip-toggle-button';
import theme from '../../theme/base-theme';
import {registerKilledListener, registerAppListener} from "../../messaging/Listeners";
import FCM, { FCMEvent } from "react-native-fcm";
import {patchNotifications} from '../../functions/weatherfuncs';
const settings = new SettingsStore()
const settingsad = require('../../img/setting-ad.png')
const dslogo = require('../../img/poweredby-ds.png')
import Notifications from './notifications_updated';
import Idx from '../../utilities/Idx';

 class Settings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "",
      temploaded:true,
      units: this.props.wUnits || this.props.getMyID[0].wUnits,
      myID: Idx(this.props,_=>_.getMyID[0].id) || '',
      isFahrenheit:true,
      testing:false,
      resetbutton:true,
      token:this.props.deviceToken
    }
  }

  componentWillMount(){
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.baseUrl = settings.BaseUrl;
    // this.getMyId();
    this.setInitial();
   
  }

  componentWillUnmount(){

  }

  componentDidMount(){
    this.refs._scrollView.scrollTo({x:0,y:0,animated:true});
  }

  changeUnits(val) {
    if(val){
      this.setState({isFahrenheit:true, units:'us'})
      setTimeout(() => {this.updateUnits();}, 200)
    }else{
      this.setState({isFahrenheit:false, units:'si'})
      setTimeout(() => {this.updateUnits();}, 200)
    }
  }

  setInitial() {
    let context = this;
    if (context.state.units == 'us') {
      this.setState({isFahrenheit:true, temploaded:true});

    }
    if (context.state.units == 'si') {
      this.setState({isFahrenheit:false, temploaded:true});
    }
  }

  updateUnits() {
    //console.log("this.state.unitsthis.state.units", this.state.units)
    this.props.UserActions.convertUnits(this.state.units)
    //this.forceUpdate();
  }

  render() {
    if (this.state.testing) {
      return (
        <View style={{width:width, height:height, backgroundColor:'#fff', alignItems:'center'}}>
        <Text>Settings</Text>
        </View>
      )
    }


    else
    return (
      <ScrollView ref='_scrollView' scrollEnabled={false} style={{backgroundColor:'#fff'}}>
      <List style={{backgroundColor:'#d9e0e2'}} ref={(ref) => { this.listRef = ref; }}>
        <Separator bordered style={{height:40}}>
          <Text style={{color:theme.brandPrimary}}>Settings</Text>
        </Separator>
      </List>
      <List>
        <ListItem style={{width:width-20, height: 50}}>
        <Left>
          <Text style={{color:theme.brandPrimary}}>Temperature</Text>
          </Left>
            <Right>
              {this.state.temploaded ? <FlipToggle
              value={this.state.isFahrenheit}
              buttonWidth={65}
              buttonHeight={25}
              buttonOnColor={theme.brandPrimary}
              buttonOffColor={theme.brandPrimary}
              sliderWidth={20}
              sliderHeight={20}
              sliderOnColor={theme.brandWhite}
              sliderOffColor={theme.brandWhite}
              onLabel={'F˚'}
              offLabel={'C˚'}
              labelStyle={{ color: '#fff', fontSize: 16, fontWeight: 'bold'}}
              onToggle={(value) => {this.changeUnits(value)}}
              onToggleLongPress={() => console.log("onToggleLongPress") }
          /> : null}
          </Right> 
        </ListItem>
      </List>
      <List style={{backgroundColor:'#d9e0e2'}}>
        <Separator bordered style={{height:40}}>
          <Text style={{color:theme.brandPrimary}}>About</Text>
        </Separator>
      </List>
      <List>
      <ListItem style={{width:width-20, height: 50}}>
       <Left>
         <Text style={{color:theme.brandPrimary}}>Version</Text>
        </Left>
      <Right>
      <Text style={{color:theme.brandPrimary}}>2.0.9</Text>
      </Right>
      </ListItem>
      <Separator bordered style={{height:40}}>
        <Text style={{color:theme.brandPrimary}}></Text>
      </Separator>
      {/* <Separator bordered style={{height:40}}>
        <Text style={{color:theme.brandPrimary}}>Notifications</Text>
      </Separator> */}
      </List>
      {/* <Notifications /> */}
         <View style={{width:width*0.90, alignSelf:'center', flexDirection:'row', justifyContent:'space-between', marginTop: 10, marginBottom:5}}>
          <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:5, color:theme.brandPrimary}} onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com/pages/app')}}>Support/FAQs</Text>
          <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:5, color:theme.brandPrimary}} onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com/pages/privacy-policy')}}>Privacy Policy</Text>
          <Text style={{textAlign:'center', fontSize:16, fontWeight:'bold', marginBottom:5, color:theme.brandPrimary}} onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com/pages/terms-of-service')}}>Legal</Text>
         </View>  
         <View style={{alignSelf:'center', marginTop:10}}>
         <TouchableOpacity onPress={ ()=>{ Linking.openURL('https://weathermanumbrella.com')}}>
        <Image source={settingsad} resizeMode='contain' style={{height:225, width:300}}/>
        </TouchableOpacity>
      </View>
      <View style={{alignSelf:'center', marginTop:5}}>
        <Image source={dslogo} style={{height:40, width:width*0.5}}/>
      </View>
      </ScrollView>
    );
  }
}


const mapStateToProps = (state) =>({
  getMyID: state.user.getUserIdState,
  wUnits: state.user.wUnits,
  deviceToken: state.user.deviceToken

})

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
