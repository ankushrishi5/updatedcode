import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View, Image, Dimensions, ScrollView, TouchableOpacity, ListView, SafeAreaView, StatusBar, Alert, AppState, RefreshControl, Linking,
    NativeEventEmitter,
    NativeModules,
    Modal,
    FlatList,
    Animated,
    Easing, ActivityIndicator
} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Left, Body, Right, InputGroup, Input, Spinner, Radio } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
// import Weather from './weather'
import Tracking from './tracking'
import Notifications from './notifications_updated'
import AppSettings from './settings_updated'
import Settings from '../../stores/settingsStore'
import theme from '../../theme/base-theme';
import DeviceInfo from 'react-native-device-info';
//import PullToRefresh from 'react-native-pull-refresh';
import Svg, { Stop, Defs, LinearGradient } from "react-native-svg";
import { VictoryAxis, VictoryChart, VictoryLine, VictoryArea, VictoryScatter, } from "victory-native";
import { formatDate } from '../../functions/datetimefuncs';
import { multiDimensionalUnique, removeDuplicates, writeToLog } from '../../functions/umbrellafuncs';
import { patchNotifications } from '../../functions/weatherfuncs';
// import RNPbWrapper from 'react-native-pb-wrapper';
// import { BleManager } from 'react-native-ble-plx';
import * as Progress from 'react-native-progress';
import { getData, getTickValue } from '../../functions/indicatorfuncs';
import _ from 'lodash';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';
import * as userLocation from '../../redux/modules/location';
import Idx from '../../utilities/Idx';
import BackgroundTimer from 'react-native-background-timer';
import LottieView from 'lottie-react-native'
import Weather from './weatherChange'
import FCM from 'react-native-fcm';
import { registerAppListener } from "../../messaging/Listeners";

// import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient'
var moment = require('moment-timezone');

var progress = 0;
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const Home_icon = require('../../img/home_icon.png')
const Work_icon = require('../../img/work_Icon.png')
const mylogo = require('../../img/wxlogo.png')
const add = require('../../img/add_button.png')
const edit = require('../../img/edit.png')
const weather_on = require('../../img/weatherActive.png')
const addButton = require('../../img/addButton.png')
const weather_off = require('../../img/weatherInactive.png')
const tracking_on = require('../../img/umbrellaActive.png')
const tracking_off = require('../../img/umbrellaInactive.png')
const pairingconnect = require('../../img/pairingconnect.png')
const cross_icon = require('../../img/cross_icon.png')
const pairing = require('../../img/pairing.gif')
const notifications_on = require('../../img/notificationActive.png')
const notifications_off = require('../../img/notificationInactive.png')
const settings_on = require('../../img/settingsActive.png')
const settings_off = require('../../img/settingsInactive.png')
const needUmbrella = require('../../img/rain.gif')
const dontNeedUmbrella = require('../../img/umbrellaClose.png')
const pairingfail = require('../../img/pairingfail.png')
const pairingfound = require('../../img/pairingfound.png')
const loadingspinner = require('../../img/loading.gif')

const umbrella_icon_blue = require('../../img/umbrella_icon_blue.png')
const bag_icon_blue = require('../../img/bag_icon_blue.png')
const car_icon_blue = require('../../img/car_icon_blue.png')
const key_icon_blue = require('../../img/key_icon_blue.png')
const other_icon_blue = require('../../img/other_icon_blue.png')
const pet_icon_blue = require('../../img/pet_icon_blue.png')

const umbrella_icon_white = require('../../img/umbrella_icon_white.png')
const bag_icon_white = require('../../img/bag_icon_white.png')
const car_icon_white = require('../../img/car_icon_white.png')
const key_icon_white = require('../../img/key_icon_white.png')
const other_icon_white = require('../../img/other_icon_white.png')
const pet_icon_white = require('../../img/pet_icon_white.png')

const down_arrow = require('../../img/down_arrow.png')

const settings = new Settings();

const navColors = {
    active:
        '#092f57'
    ,
    inactive:
        '#77787a'

};


const dataArray0 = [{ time: 4, value: 0 },
{ time: 5, value: 0 },
{ time: 6, value: 10 },
{ time: 7, value: 10 },
{ time: 8, value: 30 },
{ time: 9, value: 20 },
{ time: 10, value: 50 },
{ time: 11, value: 80 },
{ time: 12, value: 100 },
{ time: 1, value: 70 },
{ time: 2, value: 30 },
{ time: 3, value: 0 }];

const buttonImages = [{
    normal: require('../../img/btn1.png'),
    clicked: require('../../img/btn1_clicked.png')
},
{
    normal: require('../../img/btn2.png'),
    clicked: require('../../img/btn2_clicked.png')
},
{
    normal: require('../../img/btn3.png'),
    clicked: require('../../img/btn3_clicked.png')
}];
var fcmUrl = 'https://fcm.googleapis.com/fcm/send';
var alertUrl = 'http://52.207.131.10:3000/newalert';


const tick = require('../../img/tick_icon.png')
const cross = require('../../img/cross_icon.png')

const home = require('../../img/home.png')
const location_icon = require('../../img/location_icon.png')
const work = require('../../img/work.png')

class Home extends Component {
    constructor(props) {
        super(props);
        console.log('Home change ', props);
        // this.manager = new BleManager();
        // console.log('manager ', this.manager);
        this.animatedValue = new Animated.Value(0);
        this.spinValue = new Animated.Value(0)
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            baseUrl: '',
            dsUrl: '',
            myID: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].id : '',
            myNotifyID: '',
            myUnits: this.props.wUnits || '',
            subScreenLoad: true,
            subScreen: <Weather getUserLocation={this.getUserLocation} RefreshIndicator={this.RefreshIndicator.bind(this)} editPersonalLoc={this.editPersonalLoc.bind(this)} />,
            colorWeather: "Active",
            colorTracking: "Inactive",
            colorNotifications: "Inactive",
            colorSettings: "Inactive",
            curScreen: 'Weather',
            personalLoc: [],
            mycities: [],
            fullList: [],
            homeLocation: Idx(this.props.location, _ => _.IdAndAllLocationData[0].wHomeLoc) || null,
            workLocation: Idx(this.props.location, _ => _.IdAndAllLocationData[0].wWorkLoc) || null,
            // indicator_personal: Idx(this.props.location,_=>_.IdAndAllLocationData.iwUnitsd) || [],
            indicator_data: [],
            index: 0,
            index_indicator: 0,
            data: getData(-1, dataArray0),
            tickArray: getTickValue(-1, dataArray0),
            modalVisible: false,
            search: '',
            searchText: '',
            buttonType: '',
            uniqueId: '',
            indicatorHeight: 'close',
            selectedItem: undefined,
            selectedAddress: '',
            selectedCity: '',
            selectedLat: '',
            selectedLon: '',
            deletedPL: '',
            deletedAL: '',
            deletedCity: '',
            editedPL: '',
            editedTitle: '',
            editedCity: '',
            reults: '',
            copy: '',
            showMax: true,
            refreshing: false,
            doYouNeedUmbrella: false,
            loading: false,
            deviceArray: [],
            // scanning: false,
            fail: false,
            success: false,
            progress: 0,
            // indeterminate: true,
            // devicesToConnect: [],
            scannedDevices: [{ id: 1, status: "Ready to Pair" }],
            pairedDevices: [{ id: 1, status: "Ready to Pair" }],
            justDevices: [],
            isDeviceAvailable: false,
            connectedMacAddress: '',
            appState: AppState.currentState,
            isBlutoothEnabled: false,
            isResetEnabled: true,
            isScanning: false,
            isPairing: false,
            isCancelled: false,
            isFailed: false,
            isSuccess: false,
            appState: AppState.currentState,
            listloading: false,
            initlat: '',
            initlon: '',
            openEditLocModal: false,
            addAdditionalLoc: '',
            selectedHomeAdd: '',
            selectedWorkAdd: '',
            wHomeLoc: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wHomeLoc : null,
            wWorkLoc: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wWorkLoc : null,
            loc1State: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wCNAdd1 : null,
            loc2State: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wCNAdd2 : null,
            loc3State: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wCNAdd3 : null,
            sendDataToTracking: [],
            deviceToken: props.deviceToken,
            target: null,
            wbroadcast: 0,
            wMessage: "",
            // isDeviceConnected: false,
            isDeviceNamed: false,
            firstSelected: false,
            thirdSelected: false,
            isDeviceSelected: false,
            isLoadingDevice: false,
            dataLoaded: false,
            showAddButton: false,
            isPebblebeeSelected: false,
            isWeathermanSelected: false,
            newUmbrellaName: '',
            isIconsVisible: false,
            iconToUpdate: 'Umbrella',
            showAutoComplete: false
            //setNotiId: props.notificationId
        }

        //ble-manager-lib
        // console.log('tracker id from props ******* ', props)
        // const subscription = this.manager.onStateChange((state) => {
        //     console.log('state ', state)
        //     if (state === 'PoweredOn') {
        //         this.scanAndConnect();
        //         subscription.remove();
        //     }
        // }, true);
    }

    scanAndConnect() {
        // console.log('scanAndConnect');
        // this.manager.startDeviceScan(null, null, (error, device) => {
        //     if (error) {
        //         console.log('error ', error);
        //         // Handle error (scanning will be stopped automatically)
        //         return
        //     }
        //     // Check if it is a device you are looking for based on advertisement data
        //     // or other criteria.
        //     if (device.name === 'TI BLE Sensor Tag' ||
        //         device.name === 'SensorTag' || device.name === 'PebbleBee') {
        //         // console.log('device ', device);
        //         // Alert.alert('device ',device)
        //         // Stop scanning as it's not necessary if you are scanning for one device.
        //         this.manager.stopDeviceScan();

        //         // Proceed with connection.
        //     }
        // });
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.getUnitData && nextProps.getUnitData.length > 0) {
            this.setState({
                myUnits: nextProps.wUnits,
                //getUnitData:nextProps.getUnitData,
                wHomeLoc: this.state.selectedHomeAdd == '' ? nextProps.getUnitData[0].wHomeLoc : this.state.selectedHomeAdd,
                wWorkLoc: this.state.selectedWorkAdd == '' ? nextProps.getUnitData[0].wWorkLoc : this.state.selectedWorkAdd
            })
            this.forceUpdate()
        }
        if (!nextProps.isLoading) {
            this.state.dataLoaded ? this.closeModal() :
                this.setState({ dataLoaded: false, modalVisible: false })

        }

        if (nextProps.wnotify1 && nextProps.wnotifyTime1Show && nextProps.wnotifyTime1Show !== "") {
            this.getFCMNotify(nextProps.wnotifyTime1Show, "first_Notification");
        }

        if (nextProps.wnotify2 && nextProps.wnotifyTime1Show && nextProps.wnotifyTime2Show !== "") {
            this.getFCMNotify(nextProps.wnotifyTime2Show, "second_Notification");
        }

    }

    getFCMNotify(time, type) {

        FCM.createNotificationChannel({
            id: 'default',
            name: 'Default',
            description: 'used for example',
            priority: 'high'
        })
        registerAppListener(this.props.navigation);
        FCM.getInitialNotification().then(res => {
            // console.log(res, "response from getInitialNotifiction")
            // console.log("FCM get initalNotification =======>" + JSON.stringify(res))
        })
        FCM.requestPermissions({
            badge: false,
            sound: true,
            alert: true
        });
        this.showScheduleLocalNotification(time, type)
    }

    showScheduleLocalNotification(time, type) {
        var date = this.getTime(time);
        FCM.scheduleLocalNotification({
            id: "test",
            fire_date: date.getTime(),
            vibrate: 500,
            title: "Weatherman Umbrella",
            body: "Don't forget to take your umbrella with you.",
            priority: "high",
            large_icon: "../../img/wxlogo.png",
            channel: "default",
            groupSummary: true,
            // "https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg",
            show_in_foreground: true,
            wake_screen: true,
            // extra1: { a: 1 },
            // extra2: 1
        }, (data) => {
            // console.log("fcm showScheduleLocalNotification response" + data)
        });
    }

    getTime(time) {

        var timeSplit1 = time.split(":");
        var timeSpit2 = timeSplit1[1].split(" ");
        var hours = timeSplit1[0];
        var minutes = timeSpit2[0];
        var aa = timeSpit2[1];

        if (aa == "PM" || aa == "pm" && hours < 12) hours = parseInt(hours) + 12;
        if (aa == "AM" || aa == "am" && hours == 12) hours = parseInt(hours) - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;

        var data = new Date()
        data.setHours(sHours)
        data.setMinutes(sMinutes)
        data.setSeconds("00")

        return data;

    }

    componentWillMount() {
        this.state.baseUrl = settings.BaseUrl;
        this.state.uniqueId = DeviceInfo.getUniqueID();
        this.state.dsUrl = settings.DSUrl;

        this.setListeners();
        this.getNotiId();
    }

    async getNotiId() {
        try {
            let response = await fetch(this.state.baseUrl + 'wn_notification?filter[where][wdeviceid]=' + this.state.uniqueId, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)

                if (parsed.length == 0) {

                    // console.log("No values for this ID");
                } else {
                    this.props.UserActions.updateDeviceToken({ notiId: parsed[0].id, deviceToken: this.state.deviceToken })
                }

            } else {
                let errors = res;
                throw errors;
            }
        } catch (error) {
            this.setState({ errors: error });
            console.log(error);
        }
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    componentDidMount() {
        console.log('componentDidMount home', this.props.deviceToken)
        this.getMyIdandIndicatorData();

    }

    uniqFilterAccordingToProp(prop) {
        if (prop)
            return (ele, i, arr) => arr.map(ele => ele[prop]).indexOf(ele[prop]) === i
        else
            return (ele, i, arr) => arr.indexOf(ele) === i
    }

    setListeners() {
        // const mNativeEventEmitter = new NativeEventEmitter(NativeModules.RNPbWrapper)
        // AppState.addEventListener('change', this._handleAppStateChange);
        // let trackingArr = [];
        // console.log('RNPbWrapper ', RNPbWrapper)
        // mNativeEventEmitter.addListener(RNPbWrapper.ON_RSS_SIGNAL_CHANGED, (data) => {
        //     console.log('ON_RSS_SIGNAL_CHANGED ****** ', data)

        //     trackingArr.push(data);

        //     let finalArr = _.uniqBy(trackingArr, 'macAddress')

        //     this.setState({
        //         sendDataToTracking: finalArr
        //     })

        //     let dArray = this.state.deviceArray;
        //     dArray = dArray.map((item) => {
        //         if (item.macAddress == data.macAddress) {
        //             return data;
        //         } else {
        //             return item;
        //         }
        //     });

        //     console.log('Home dArray', dArray)
        //     console.log('Home deviceArray ', this.state.deviceArray)
        // });


        // mNativeEventEmitter.addListener(RNPbWrapper.ON_DEVICE_ADDED, (data) => {
        //     console.log('Home inside ON_DEVICE_ADDED ******** ', data)
        //     trackingArr.push(data);

        //     let finalArr = _.uniqBy(trackingArr, 'macAddress')

        //     console.log('trackingArr ********** ', finalArr)
        //     this.setState({
        //         sendDataToTracking: finalArr,
        //         deviceArray: finalArr,
        //         isResetEnabled: false
        //     }, () => {
        //         console.log('sendDataToTracking --->><<--- ', this.state.sendDataToTracking);
        //         this.setState({ isLoadingDevice: true })
        //         this.getDevicetoConnect(this.state.deviceArray);
        //     })

        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STARTED, (data) => {
        //     console.log('Home ON_SCANNING_STARTED');
        //     this.setState({ isScanning: true });
        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_SUCCESS, (data) => {
        //     console.log('Home ON_SCANNING_SUCCESS');
        //     this.setState({ isScanning: false }, () => {
        //         this.scanAction();
        //     });
        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_FAILED, (data) => {
        //     console.log('Home ON_SCANNING_FAILED');
        //     this.setState({ isScanning: false });
        //     alert(RNPbWrapper.ON_SCANNING_FAILED);
        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_ON, (data) => {
        //     console.log('Home ON_BLUETOOTH_ON');
        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_OFF, (data) => {
        //     console.log('Home ON_BLUETOOTH_OFF');
        //     alert(RNPbWrapper.ON_BLUETOOTH_OFF);
        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STOPPED, (data) => {
        //     console.log('Home ON_SCANNING_STOPPED');
        //     this.setState({ isScanning: false });

        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_OUT_OF_RANGE, (data) => {
        //     console.log(" gett data aout ssasasa")
        // });

        // mNativeEventEmitter.addListener(RNPbWrapper.ON_DEVICE_DISCONNECT, (data) => {
        //     console.log('device disconnected ************ ', data)
        //     trackingArr = [];
        //     this.setState({
        //         sendDataToTracking: []
        //     })
        //     if (data.device_state != 'Disconnected') {
        //         trackingArr.push(data);

        //         let finalArr = _.uniqBy(trackingArr, 'macAddress')

        //         console.log('trackingArr ********** ', finalArr)
        //         this.setState({
        //             sendDataToTracking: finalArr
        //         })

        //         let dArray = this.state.deviceArray;
        //         dArray = dArray.map((item) => {
        //             if (item.macAddress == data.macAddress) {
        //                 return data;
        //             } else {
        //                 return item;
        //             }
        //         });
        //         console.log('dArray ', dArray);
        //         console.log('deviceArray ', this.state.deviceArray)
        //     }

        // });
    }


    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.setState({ subScreenLoad: false });
            this.closeIndicator();
            this.RefreshIndicator(null);
            if (Platform.OS === 'ios') {
                BackgroundTimer.stopBackgroundTimer();
            } else {
                BackgroundTimer.clearInterval(this.state.backgroundTimeInterval);
            }

            setTimeout(() => { this.setState({ subScreenLoad: true }); }, 1000);

        }
        else {
            // this.locateMe();
            if (Platform.OS === 'ios') {
                BackgroundTimer.runBackgroundTimer(() => {
                    patchNotifications(settings.BaseUrl, this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID());
                    this.getOutOfRange(this.state.initlat, this.state.initlon);
                    writeToLog(this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID(), 2, 'backgroundIOS')
                }, 900000)

            } else {
                intervalId = BackgroundTimer.setInterval(() => {
                    patchNotifications(settings.BaseUrl, this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID());
                    this.getOutOfRange(this.state.initlat, this.state.initlon);
                    writeToLog(this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID(), 2, 'backgroundAndroid')
                }, 900000)
                this.setState({ backgroundTimeInterval: intervalId });
            }

        }
        this.setState({ appState: nextAppState });
    }



    async getOutOfRange(lat, lon) {
        // console.log('getOutOfRange lat and long in get out of range ******* ', lat, lon)

        //Fix for 2.0 wdeviceid from wDeviceID
        //console.log(settings.BaseUrl + 'wn_view_oor_curpost?filter[where][wdeviceid]=' + DeviceInfo.getUniqueID());
        try {
            let response = await fetch(settings.BaseUrl + 'wn_view_oor_notifier?filter[where][wdeviceid]=' + DeviceInfo.getUniqueID(), {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                if (parsed == null || parsed.length == 0) {
                    console.log("No one out of range");
                }

                else {

                    let { sendDataToTracking } = this.state;
                    _.each(parsed, (data) => {
                        if (sendDataToTracking.length > 0) {
                            _.each(sendDataToTracking, (element) => {
                                if (element.macAddress != undefined) {
                                    if (data.wtrackerid == element.macAddress) {
                                        this.getUpdatedUmbrellaInfo(data.mytrackerID, lat, lon);
                                        this.patchRange(data.mytrackerID, true);
                                    } else {
                                        this.patchRange(data.mytrackerID, false);
                                        this.checkDistancefromLocations(lat, lon, data.whomeLoc, data.wWorkLoc, data.wtoken, data.wtrackername)
                                    }
                                }
                            })
                        }
                        else {
                            this.patchRange(data.mytrackerID, false);
                            this.checkDistancefromLocations(lat, lon, data.whomeLoc, data.wWorkLoc, data.wtoken, data.wtrackername)
                        }
                    })
                }
            } else {
                let errors = res;
                throw errors;
            }
        } catch (error) {
            console.log(error);
        }
    }

    getUpdatedUmbrellaInfo(id, lat, lng) {
        let latlng = lat + ',' + lng;
        this.getAddress(latlng, id);
    }

    async getAddress(latlon, id) {
        try {
            let response = await fetch(settings.geourl + 'latlng=' + latlon + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                this.patchInRange(latlon, parsed.results[0].formatted_address, id);


            } else {
                let errors = res;
                throw errors;
            }

        } catch (error) {
            this.setState({ errors: error });
            console.log("error: " + error);
        }
    }

    patchRange(idToUpdate, toggle) {
        this.props.UserActions.patchRangeFunc({ idToUpdate: idToUpdate, winrange: toggle });
    }


    async patchInRange(latlon, address, idToUpdate) {
        try {
            let response = await fetch(this.state.baseUrl + 'wn_tracking/' + idToUpdate, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({
                    wlatlon: latlon,
                    waddress: address,
                    wlastconnected: Date.now(),
                })
            });

            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                console.log("Updated InRage Umbrella")
            } else {
                let errors = res;
                throw errors;
            }
        } catch (errors) {
            console.log(errors);
        }
    }

    async checkDistancefromLocations(lat, lon, home, work, token, uname) {
        let combinedHomeandWork = [];
        if (home != null) {
            combinedHomeandWork = '[' + home + ']'
        }

        if (work != null) {
            combinedHomeandWork = '[' + work + ']'
        }

        if (home != null && work != null) {
            combinedHomeandWork = '[' + home + '|' + work + ']'

        }
        try {
            let response = await fetch('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + lat + ',' + lon + '&destinations=' + combinedHomeandWork + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();

            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)

                if (parsed != null && parsed.rows[0] != null && parsed.rows[0].elements != null) {

                    if (parsed.rows[0].elements.length > 1) {
                        if (parsed.rows[0].elements[0].distance.value > 3218 && parsed.rows[0].elements[1].distance.value > 3218) {
                            this.sendNotification(token, uname);
                        }
                    }
                    else if (parsed.rows[0].elements.length == 1) {
                        if (parsed.rows[0].elements[0].distance.value > 3218) {
                            this.sendNotification(token, uname);
                        }
                    }
                } else {
                    let errors = res;
                    console.log("error: " + error);
                }
            } else {
                let errors = res;
                throw errors;
            }

        } catch (error) {
            console.log("error: " + error);
        }
    }


    async sendNotification(token, umbrella) {

        try {

            let response = await fetch(fcmUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'key=AAAAM-j2f4c:APA91bEKm7cfnew5ZHTw3vDI3P66z64oQ9wJa9oXb2LCBJTPj8UTmi9EUtDyfJahe7FvMZw5p6ZfGxNPfuyfOfsH3khkvqy364fPYkd7WGnHu7U95qpAPspHLMv7Ac1upAy6ZNC4tKH7'
                },
                body: JSON.stringify({
                    notification: {
                        title: "Out Of Range",
                        body: 'Looks like you may have left ' + umbrella + ' behind! Open the app to locate it.',
                        sound: "default"
                    },

                    to: token,
                    priority: "high"
                })
            });

            let res = await response.text();

            if (response.status >= 200 && response.status < 300) {

                let parsed = JSON.parse(res);
            } else {
                let errors = res;
                throw errors;
            }

        } catch (errors) {
            patchRespnse(id, errors);
            console.log(errors);
        }
    }


    onScanPressed() {

        // RNPbWrapper.isBlutoothEnabled((isBlutoothEnabled) => {
        //     this.setState({ isBlutoothEnabled: isBlutoothEnabled });
        //     if (!isBlutoothEnabled) {
        //         Alert.alert(
        //             'Bluetooth',
        //             'Please enable bluetooth.')
        //         console.log('Please enable bluetooth');
        //     } else {
        //         RNPbWrapper.startScanning()
        //         this.setState({ fail: false, success: false });

        //     }
        // });
    }

    getDevicetoConnect(devices) {
        if (devices != null)
            this.doesTrackerExist(this.state.uniqueId, devices[devices.length - 1].macAddress)
    }

    scanAction() {
        if (this.state.deviceArray != null && this.state.deviceArray.length > 0) {
            // this.setState({ scanning: false, success: true });
            this.setState({ success: true });
            //alert("Scanning Failed to find a device");
        }
        else {
            // this.setState({ scanning: false, fail: true });
            this.setState({ fail: true });
        }
    }

    connect(target, devices) {
        if (target !== null) {

            this.setState({ isPairing: true, isCancelled: false });
            setTimeout(() => {
                if (!this.state.isCancelled) {
                    this.onPairingCompleted();
                }
            }, 4000);

            // if (!this.state.dataLoaded) {
            //     this.setState({ dataLoaded: true });
            //     if (devices[target].status === 'Ready to Pair') {
            //         this.addDevice(this.state.uniqueId, devices[target].id, "Devices " + devices[target].id)
            //     } else if (devices[target].status == 'Already Paired') {
            //         this.setState({ dataLoaded: false });
            //         this.closeModal();

            //     }
            // }

        } else {
            Alert.alert(
                'Device',
                'Please select device to connect.')
        }
        // console.log('Home connect ', devices)
        // if (!this.state.loading) {
        //   console.log('Home isLoading ', this.state.loading)
        //   this.setState({ loading: true });
        //   for (let key in devices) {
        //     if (devices[key].status == 'Ready to Pair') {
        //       this.addDevice(this.state.uniqueId, devices[key].id, "My Umbrella" + key)
        //     }
        //   }
        //   setTimeout(() => {
        //     this.setState({ loading: false });
        //     this.closeModal();
        //   }, 4000);
        //   // setTimeout(() => { this.closeModal(); }, 4000);
        // }
    }

    onPairingCompleted() {
        if (this.state.pairedDevices == null || this.state.pairedDevices.length == 0) {
            this.setState({ isPairing: false, isFailed: true, isSuccess: false });
        }
        else {
            this.setState({ isPairing: false, isSuccess: true, isFailed: false });
        }

    }

    onTryAgain() {

        this.setState({
            isScanning: false,
            isPairing: false,
            isSuccess: false,
            isFailed: false
        })
    }

    onNextStep() {

        this.setState({
            isScanning: false,
            isPairing: false,
            isSuccess: false,
            isFailed: false,
            isDeviceNamed: true,
            newUmbrellaName: ''
        })
    }

    handleSubmit(){

        if (this.state.newUmbrellaName != undefined && this.state.newUmbrellaName.length == 0) {
          alert(" Name Field cannot be empty ")
          return
        }
        this.closeModal()
      }

    async doesTrackerExist(deviceID, trackerid) {
        try {
            let response = await fetch(this.state.baseUrl + 'wn_tracking/count?[where][wdeviceid]=' + deviceID + '&[where][wtrackerid]=' + trackerid + '&[where][wactive]=true', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                let newItem = '';
                if (parsed.count == 0) {
                    newItem = { id: trackerid, status: "Ready to Pair" };
                    this.state.scannedDevices.push(newItem);
                    this.state.justDevices.push(trackerid);
                }
                else {
                    newItem = { id: trackerid, status: "Already Paired" };
                    this.state.scannedDevices.push(newItem);
                    this.state.justDevices.push(trackerid);
                }
                this.setState({ isLoadingDevice: false })
                this.state.scannedDevices = multiDimensionalUnique(this.state.scannedDevices);

            } else {
                let errors = res;
                this.setState({ isLoadingDevice: false })
                throw errors;
            }
        } catch (error) {
            this.setState({ isLoadingDevice: false })

        }
    }

    addDevice(deviceid, trackerid, trackername) {
        this.props.UserActions.addNewTracker({ deviceId: deviceid, trackerId: trackerid, trackerName: trackername, lat: this.state.initlat, lng: this.state.initlon });
    }

    _onRefresh() {
        this.closeIndicator();
        // this.setState({refreshing:true,subScreenLoad:false});
        this.setState({ refreshing: true, subScreenLoad: false });
        // this.onScanPressed();
        setTimeout(() => { this.setState({ subScreenLoad: true, refreshing: false }); }, 1000);

    }

    RefreshIndicator(type) {
        this.setState({ doYouNeedUmbrella: false, subScreenLoad: true });
        this.setState({ data: getData(-1, dataArray0), tickArray: getTickValue(-1, dataArray0) });
        // this.state.indicator_data = [];
        this.state.indicator_data.splice(1, 2)
        this.setState({ indicator_data: this.state.indicator_data }
            , () => console.log('aray after ', this.state.indicator_data))
        // this.state.indicator_personal = [];
        if (type == "Home") {
            this.setState({
                wHomeLoc: null,
                homeLocation: null
            })
        }
        if (type == "Work") {
            this.setState({
                wWorkLoc: null,
                workLocation: null
            })
        }
        this.getMyIdandIndicatorData();
        if (this.state.showAddButton)
            this.OpenClosedAddButton()
    }

    async getMyIdandIndicatorData() {
        console.log('homechange getMyIdandIndicatorData')
        try {
            let response = await fetch(this.state.baseUrl + 'wn_users?filter[where][wDeviceID]=' + this.state.uniqueId, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            //
            // this.locateMe();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                this.props.userLocation.setDetails(parsed);

                //this.setState({myID:parsed[0].id, myUnits:parsed[0].wUnits});
                // this.props.UserActions.storeLastOpened({idToUpdate: this.props.getUnitData[0].id})
                // if (parsed[0].wHomeLoc == null) {
                //   //this.state.indicator_data.push([]);
                // }
                // if (parsed[0].wWorkLoc == null) {
                //   //this.state.indicator_data.push([]);
                // }
                console.log('homechange parsed[0] ', parsed[0])
                if (parsed[0].wHomeLoc != null) {
                    // this.state.homeLocation = parsed[0].wHomeLoc;
                    setTimeout(() => { this.getCNLL(this.state.homeLocation, 'Home'); }, 2000)
                }
                if (parsed[0].wWorkLoc != null) {
                    // this.state.workLocation = parsed[0].wWorkLoc;
                    setTimeout(() => { this.getCNLL(this.state.workLocation, 'Work'); }, 2000)

                }
            } else {
                let errors = res;
                throw errors;
            }
        } catch (error) {
            this.setState({ errors: error });

        }
    }

    locateMe() {
        navigator.geolocation.watchPosition(
            (position) => {

                if (position.coords && position.coords.latitude && position.coords.longitude) {
                    this.getTemp(position.coords.latitude, position.coords.longitude, 1, "Cur");
                    patchNotifications(this.state.baseUrl, position.coords.latitude, position.coords.longitude, this.state.uniqueId);
                    this.setState({
                        currentRegion: {
                            mylatitude: position.coords.latitude,
                            mylongitude: position.coords.longitude,
                        }
                    });
                    this.setState({ initlat: position.coords.latitude, initlon: position.coords.longitude })
                }
            },
            (error) => {
                this.RefreshIndicator(null);

            },
            { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
        );
    }

    getUserLocation = (position) => {

        if (position.coords && position.coords.latitude && position.coords.longitude) {
            this.getTemp(position.coords.latitude, position.coords.longitude, 1, "Cur");
            patchNotifications(this.state.baseUrl, position.coords.latitude, position.coords.longitude, this.state.uniqueId);
            this.setState({
                currentRegion: {
                    mylatitude: position.coords.latitude,
                    mylongitude: position.coords.longitude,
                }
            });
            this.setState({ initlat: position.coords.latitude, initlon: position.coords.longitude })
        }
    }


    async getCNLL(myAddress, loctype) {

        try {
            let response = await fetch(settings.geourl + 'address=' + myAddress + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)

                this.getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, 2, loctype);
                //setTimeout(() => {this.setState({weatherLoading: false}) }, 8000)

            } else {
                let errors = res;
                throw errors;
            }

        } catch (error) {
            this.setState({ errors: error });

        }
    }

    async getTemp(lat, lon, id, loctype) {
        try {
            let response = await fetch(this.state.dsUrl + lat + ',' + lon + '?units=' + this.state.myUnits, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                let hourscheck = 0;

                var tempData = [];
                while (hourscheck < 12) {
                    if (parsed.hourly.data[hourscheck].precipIntensity >= 0.02 || parsed.currently.icon == 'rain' || parsed.currently.icon == 'snow') {
                        this.setState({ doYouNeedUmbrella: true });
                    }
                    var newItem = { time: formatDate(parsed.hourly.data[hourscheck].time, parsed.timezone), value: (parsed.hourly.data[hourscheck].precipProbability * 100) };
                    tempData.push(newItem)
                    hourscheck++;
                }
                if (loctype == 'Cur') {
                    this.state.indicator_data[0] = tempData;
                    this.sendAlert(lat, lon, parsed.currently.temperature)
                }
                if (loctype == 'Home') {
                    this.state.indicator_data[1] = tempData;
                }
                if (loctype == 'Work') {
                    this.state.indicator_data[2] = tempData;
                }
                this.state.index_indicator = this.state.index_indicator + 1;
                this.setState({ index: 0, subScreenLoad: true });


            } else {
                let errors = res;
                throw errors;
            }

        } catch (error) {
            this.setState({ errors: error });

        }
    }

    async sendAlert(lat, long, temp) {
        try {

            let response = await fetch(alertUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjOWQ4ZDAwYmQ0YTEwMzM2MjIwNDMyYSIsImlhdCI6MTU1NjI2ODUyN30.LB2ugvsTPwjiYKzWLQ-R3zTU_05W5tHw0KVr-PuuZ1k'
                },
                body: JSON.stringify({
                    "alertType": "1",
                    "temperature": "" + temp,
                    "coordinates": [
                        long, lat
                    ]
                })
            });

            let res = await response.text();
            console.log('homechange sendAlert res ', res)
            if (response.status >= 200 && response.status < 300) {

                let parsed = JSON.parse(res);
            } else {
                let errors = res;
                throw errors;
            }

        } catch (errors) {
            console.log(errors);
        }

    }

    openModal(variant) {
        this.setState({ buttonType: variant + this.state.curScreen });
        // this.setState({ scanning: false, progress: 0, indeterminate: true, fail: false, success: false });
        this.setState({ isScanning: false, fail: false, success: false });
        this.setState({ modalVisible: true, subScreenLoad: false, selectedCity: '', selectedAddress: '', showAutoComplete: false });

        setTimeout(() => {

            if (variant == 'edit') {
                this.getCities();
            }
        }, 1000);

        this.closeIndicator();
        if (this.state.showAddButton)
            this.OpenClosedAddButton()

    }

    closeModal() {
        this.setState({
            modalVisible: false, subScreenLoad: true,
            isDeviceSelected: false, isPebblebeeSelected: false, isWeathermanSelected: false,
            isScanning: false, isPairing: false, isFailed: false, isSuccess: false, isCancelled: true,
            firstSelected: false, thirdSelected: false, dataLoaded: false, isDeviceNamed: false
        });
        this.RefreshIndicator(null);
        this.state.justDevices = [];
        // this.state.deviceArray = [];
        // RNPbWrapper.resetAll();
        // setTimeout(() => { this.setListeners(); }, 2000)
        // setTimeout(() => { this.startscan(); }, 2000)
    }

    onPebblebeeSelected() {
        //onScanStarted
        this.setState({ isPebblebeeSelected: true, isWeathermanSelected: false, isScanning: true })

        // this.onScanPressed();

        //onScanStopped
        setTimeout(() => {
            this.setState({ isScanning: false });
        }, 4000);
    }

    onWeathermanSelected() {
        this.setState({ isWeathermanSelected: true, isPebblebeeSelected: false, insulationSetup: true })

    }

    handleContinueClick() {

        //onScanStarted
        this.setState({ isWeathermanSelected: true, isPebblebeeSelected: false, insulationSetup: false, isScanning: true })

        //onScanStopped
        setTimeout(() => {
            this.setState({ isScanning: false });
        }, 4000);
    }

    manageIndicator() {
        if (this.state.indicatorHeight == 'close') {
            this.onButtonPress(0);
            this.openIndicator();
        }
        if (this.state.indicatorHeight == 'open') {
            this.closeIndicator();
        }
    }

    openIndicator() {
        this.setState({ indicatorHeight: 'open' })
    }

    closeIndicator() {
        this.setState({ indicatorHeight: 'close' })
    }


    validateField = (myname) => {
        let tName = myname.length.toString();
        if (tName > 0) {
            return true;
        }
        else return false;
    };

    validateSave() {
        if (!this.state.loading) {
            this.setState({ loading: true });
            if (!this.validateField(this.state.selectedCity)) {
                Alert.alert(
                    'City Missing',
                    'Please enter your city before saving.')
                this.setState({ loading: false });
            }
            else {
                this.getAvailability();
            }
        }
    }


    validateEdit() {
        if (!this.state.loading) {
            this.setState({ loading: true });
            if (!this.validateField(this.state.selectedAddress)) {
                Alert.alert(
                    'Address Missing',
                    'Please enter your address.')
                this.setState({ loading: false });
            }
            else {
                if (this.state.editedPL == 1) {
                    this.setState({
                        selectedHomeAdd: this.state.selectedAddress,
                        wHomeLoc: this.state.selectedAddress,
                        homeLocation: this.state.selectedAddress
                    }, () => {
                        this.updateHomeLoc();
                    })
                }
                if (this.state.editedPL == 2) {
                    this.setState({
                        selectedWorkAdd: this.state.selectedAddress,
                        wWorkLoc: this.state.selectedAddress,
                        workLocation: this.state.selectedAddress
                    }, () => {
                        this.updateWorkLoc();
                    })

                }
            }
        }
    }

    updateHomeLoc() {
        this.props.UserActions.addHomeOrWorkLocations({ idToUpdate: this.state.myID, selectedAdd: this.state.selectedAddress, selectedRowToEdit: this.state.editedPL });
        this.setState({ buttonType: 'editWeather', loading: false });
        setTimeout(() => {
            this.getCities();
            this.closeModal()
        }, 2000);

    }

    updateWorkLoc() {
        this.props.UserActions.addHomeOrWorkLocations({ idToUpdate: this.state.myID, selectedAdd: this.state.selectedAddress, selectedRowToEdit: this.state.editedPL });
        this.setState({ buttonType: 'editWeather', loading: false });
        setTimeout(() => {
            this.getCities();
            this.closeModal()
        }, 2000);
    }

    editPersonalLoc(row, title) {
        console.log('editPersonalLoc ****** ', row)
        this.setState({
            modalVisible: true, subScreenLoad: false, editedTitle: title,
            editedPL: row, buttonType: 'editPersonalLoc', selectedAddress: '',
            showAutoComplete: false
        });
        if (this.state.showAddButton)
            this.OpenClosedAddButton()
    }

    returntoEdit() {
        this.setState({ buttonType: 'editWeather' });
    }

    switchScreen(screen) {
        if (screen == "Weather") {
            this.setState({ subScreen: <Weather currentRegion={this.state.currentRegion} getUserLocation={this.getUserLocation} RefreshIndicator={this.RefreshIndicator.bind(this)} editPersonalLoc={this.editPersonalLoc.bind(this)} />, colorWeather: "Active", colorTracking: "Inactive", colorNotifications: "Inactive", colorSettings: "Inactive" })
        }
        if (screen == "Tracking") {
            this.setState({ subScreen: <Tracking trackingData={this.state.sendDataToTracking} />, colorWeather: "Inactive", colorTracking: "Active", colorNotifications: "Inactive", colorSettings: "Inactive" });
            // this.onScanPressed();
        }
        if (screen == "Notifications") {
            this.setState({ subScreen: <Notifications />, colorWeather: "Inactive", colorTracking: "Inactive", colorNotifications: "Active", colorSettings: "Inactive" });
        }
        if (screen == "AppSettings") {
            this.setState({ subScreen: <AppSettings />, colorWeather: "Inactive", colorTracking: "Inactive", colorNotifications: "Inactive", colorSettings: "Active" });
        }
        this.setState({ curScreen: screen });
        this.closeIndicator();
    }

    prepareAddressforEdit(addy) {
        this.setState({ selectedAddress: addy, showAutoComplete: false });
    }

    getCities() {
        this.setState({ listloading: true }, () => {
            let { personalLoc, mycities } = this.state;
            this.setState({
                mycities: []
            })
            let parsed = this.props.getUnitData;

            if (parsed != null && parsed.length > 0) {
                if (parsed[0].wHomeLoc != null) {
                    personalLoc[1] = this.state.selectedHomeAdd == '' ? this.state.wHomeLoc : this.state.selectedHomeAdd;
                }
                if (parsed[0].wWorkLoc != null) {
                    personalLoc[2] = this.state.selectedWorkAdd == '' ? this.state.wWorkLoc : this.state.selectedWorkAdd;
                }

                if (parsed[0].wHomeLoc == null) {
                    personalLoc[1] = "Swipe right to enter a home address";
                }
                if (parsed[0].wWorkLoc == null) {
                    personalLoc[2] = "Swipe right to enter a work/other address";
                }
                this.setState({
                    loc1State: parsed[0].wCNAdd1,
                    loc2State: parsed[0].wCNAdd2,
                    loc3State: parsed[0].wCNAdd3,
                }, () => {
                    if (this.state.loc1State != null || this.state.loc2State != null || this.state.loc3State != null) {
                        let citiesArr = [...this.state.mycities];
                        citiesArr.push(this.state.loc1State, this.state.loc2State, this.state.loc3State);
                        let uniqueArr = _.uniq(citiesArr);
                        let finalArr = _.without(uniqueArr, null);
                        this.setState({
                            mycities: finalArr
                        })
                    }

                }
                )
                setTimeout(() => { this.setState({ listloading: false }); }, 2000)
            } else {
                personalLoc[1] = "Swipe right to enter a home address";
                personalLoc[2] = "Swipe right to enter a work/other address";
                setTimeout(() => { this.setState({ listloading: false }); }, 2000)
            }

        });

    }

    getAvailability() {

        let parsed = this.props.getUnitData;
        if (parsed != null && parsed.length > 0) {
            if (parsed[0].wCNAdd1 == null) {
                this.setState({
                    addAdditionalLoc: 'add1'
                }, () => {
                    this.AddLoc(parsed[0].id);
                })
            }
            else if (parsed[0].wCNAdd2 == null) {
                this.setState({
                    addAdditionalLoc: 'add2'
                }, () => {
                    this.AddLoc(parsed[0].id);
                })
            }
            else if (parsed[0].wCNAdd3 == null) {
                this.setState({
                    addAdditionalLoc: 'add3'
                }, () => {
                    this.AddLoc(parsed[0].id);
                })
            }
            else if (this.state.addAdditionalLoc == '') {
                Alert.alert(
                    'Limit Reached',
                    'You can only add 3 additional cities at most')
                this.setState({ loading: false });
            }
            else {
                Alert.alert(
                    'Limit Reached',
                    'You can only add 3 additional cities at most')
                this.setState({ loading: false });
            }
        }
    }

    AddLoc(idToUpdate) {
        let { addAdditionalLoc, selectedCity, selectedLat, selectedLon } = this.state;
        this.props.UserActions.addAdditionalLocations({
            addAdditionalLoc: addAdditionalLoc,
            selectedCity: selectedCity,
            selectedLat: selectedLat,
            selectedLon: selectedLon,
            idToUpdate: idToUpdate
        });
        setTimeout(() => {
            this.setState({ loading: false }, () => {
                this.closeModal();
                this.getCities();
                this.forceUpdate();
            });
        }, 1000);

    }

    prepareCity(city, lat, lon) {
        this.setState({ selectedCity: city, selectedLat: lat, selectedLon: lon, showAutoComplete: false });
    }

    onButtonPress(i) {
        if (this.state.indicator_data != null && this.state.indicator_data.length > 0) {
            this.setState({
                index: i,
                data: getData(i, this.state.indicator_data[i]),
                tickArray: getTickValue(i, this.state.indicator_data[i])
            });
            setTimeout(() => this.onUpdate(), 500);
        }
    }

    onUpdate() {
        this.forceUpdate();
    }

    getImage(i) {
        if (i == this.state.index) {
            return buttonImages[i].clicked;
        }
        return buttonImages[i].normal;
    }

    // connectAction(type) {
    //     if (type === "iHaveDevice") {
    //         this.setState({ isDeviceConnected: true })
    //     }

    // }

    selectDevice(id) {
        let deviceData = this.state.scannedDevices
        if (this.state.target === id) {
            deviceData[id].isSelected = false
            this.setState({ target: null })
        } else {
            if (this.state.target === null) {
                deviceData[id].isSelected = true
                this.setState({ target: id })
            }
            else {
                deviceData[this.state.target].isSelected = false
                deviceData[id].isSelected = true
                this.setState({ target: id })
            }
        }
        this.setState({ scannedDevices: deviceData })

    }

    rotateIcon = () => {
        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 150,
                easing: Easing.linear
            }
        ).start()
    }

    rotateBackIcon = () => {
        Animated.timing(
            this.spinValue,
            {
                toValue: 0,
                duration: 150,
                easing: Easing.linear
            }
        ).start()
    }

    showIconDialog() {
        this.setState({ isIconsVisible: !this.state.isIconsVisible }, () => { this.state.isIconsVisible ? this.rotateIcon() : this.rotateBackIcon() })

    }

    OpenClosedAddButton = () => {
        this.setState({ showAddButton: !this.state.showAddButton }, () => { this.state.showAddButton ? this.rotateIcon() : this.rotateBackIcon() })
    }

    row(image, text) {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, marginLeft: 5 }}>
                <Image source={image}
                    style={{ height: 15, width: 15, resizeMode: 'contain', margin: 5 }} />
                <Text style={{ fontSize: 15, marginLeft: 5 }}>{text}</Text>
            </View>
        );
    }

    ListEmptyView = () => {
        return (
            <View>
                <Text style={{ textAlign: 'center' }}> No Devices Found.</Text>
            </View>

        );
    }

    render() {

        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '-45deg']
        })

        _.set(this.refs, 'Content._scrollview.resetCoords', { x: 0, y: this.state.scrollY })
        return (
            <Container>
                <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
                <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />

                {/* {Floating Buttons} */}
                {((this.state.curScreen == 'Weather' || this.state.curScreen == 'Tracking') && this.state.subScreenLoad) &&
                    <View style={{ position: 'absolute', zIndex: 2, bottom: height * 0.16, right: 15 }}>
                        <View style={this.state.showAddButton ? styles.openModel : styles.closeModel}>

                            {
                                this.state.showAddButton && this.state.curScreen == 'Tracking' || this.state.showAddButton && this.state.curScreen == 'Weather' ?
                                    <TouchableOpacity
                                        onPress={() => this.openModal('add')}>
                                        <Image source={add} style={{ height: 28, width: 28, margin: 12 }} />
                                    </TouchableOpacity> : null
                            }
                            {
                                this.state.showAddButton && this.state.curScreen == 'Weather' && this.state.wHomeLoc == null ? <TouchableOpacity
                                    onPress={() => this.editPersonalLoc(1, "Enter")}>
                                    <Image source={Home_icon} style={{ height: 28, width: 28, margin: 12 }} />
                                </TouchableOpacity> : null
                            }
                            {
                                this.state.showAddButton && this.state.curScreen == 'Weather' && this.state.wWorkLoc == null ? <TouchableOpacity
                                    onPress={() => this.editPersonalLoc(2, "Enter")}>
                                    <Image source={Work_icon} style={{ height: 27, width: 27, margin: 12 }} />
                                </TouchableOpacity> : null
                            }
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => this.OpenClosedAddButton()}
                                style={{ backgroundColor: '#2b5685', width: 45, height: 45, borderRadius: 30, justifyContent: 'center', alignItems: 'center', }} >
                                <Animated.Image source={addButton} style={{ width: 25, height: 25, transform: [{ rotate: spin }] }} />
                            </TouchableOpacity>
                        </View>
                    </View>}


                {/*Top Header*/}
                <View style={{ width: width, height: 50, backgroundColor: '#fff', borderBottomColor: navColors.inactive, borderBottomWidth: 1, flexDirection: 'row' }}>
                    <View style={{ width: width * 0.9, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 5, paddingLeft: width * 0.4 }}>
                        <Image source={mylogo} style={{ marginTop: 2.5, height: 38.285, width: 66.142 }} />
                    </View>
                    {this.state.index >= 0 ? <TouchableOpacity onPress={() => this.manageIndicator()}>
                        <View style={{ width: width * 0.1, height: 40, backgroundColor: 'rgba(100, 150, 100, 0)', marginTop: 10, alignItems: 'flex-end', paddingRight: 10 }}>
                            {this.state.doYouNeedUmbrella ? <Image source={needUmbrella} style={{ height: 28, width: 28 }} /> : <Image source={dontNeedUmbrella} style={{ height: 28, width: 28 }} />}
                        </View>
                    </TouchableOpacity> : null}
                </View>

                {this.state.indicatorHeight == 'open' ? <View style={styles.container}>
                    <View style={styles.buttonGroup}>
                        <TouchableOpacity onPress={() => this.onButtonPress(0)}>
                            <Image source={this.getImage(0)} style={styles.checkboxImage} />
                        </TouchableOpacity>
                        {this.state.wHomeLoc != null
                            ? <TouchableOpacity onPress={() => this.onButtonPress(1)}
                                style={{ marginLeft: 10, marginRight: 10 }}>
                                <Image source={this.getImage(1)} style={styles.checkboxImage} />
                            </TouchableOpacity> : null}
                        {this.state.wWorkLoc != null
                            ? <TouchableOpacity onPress={() => this.onButtonPress(2)}>
                                <Image source={this.getImage(2)} style={styles.checkboxImage} />
                            </TouchableOpacity> : null}
                    </View>

                    <ScrollView style={styles.chartView} horizontal={true} showsHorizontalScrollIndicator={false}>
                        <VictoryChart
                            height={100}
                            width={this.state.data.length * 50}
                            domain={{ x: [-1, this.state.data.length], y: [-2, 180] }}
                            padding={{ left: 0, top: 0, right: 0, bottom: 40 }}>

                            <Defs>
                                <LinearGradient id='gradientFill'
                                    x1='0%'
                                    x2='0%'
                                    y1='0%'
                                    y2='100%'
                                >
                                    <Stop offset='0%' stopColor={"#557aa0"} stopOpacity='0.9' />
                                    <Stop offset='25%' stopColor={"#557aa0"} stopOpacity='0.7' />
                                    <Stop offset='50%' stopColor={"#557aa0"} stopOpacity='0.5' />
                                    <Stop offset='70%' stopColor={"#557aa0"} stopOpacity='0.3' />
                                    <Stop offset='90%' stopColor={"#557aa0"} stopOpacity='0.1' />
                                </LinearGradient>
                            </Defs>

                            <VictoryArea
                                data={this.state.data}
                                interpolation="cardinal"
                                labels={(data) => `${Math.round(data.y)}%`}
                                style={{
                                    data: {
                                        stroke: theme.brandSecondary,
                                        strokeWidth: 1,
                                        fill: 'url(#gradientFill)',
                                    },
                                    labels: { fontFamily: theme.fontFamily, fontWeight: '400', fontSize: 14, fill: "white" },
                                }}
                            />

                            <VictoryScatter
                                style={{ data: { fill: theme.brandSecondary } }}
                                data={this.state.data}
                                size={2} />

                            <VictoryAxis
                                style={{
                                    axis: {
                                        stroke: "transparent",
                                    },
                                    tickLabels: { fontFamily: theme.fontFamily, fontWeight: '400', fontSize: 14, fill: "white" },
                                }}
                                tickValues={this.state.tickArray}
                                tickFormat={(t) => `${Idx(this.state, _ => _.data[t].time)}`}
                            />

                        </VictoryChart>
                    </ScrollView>
                </View> : null}


                {/* Main Content */}
                {this.state.curScreen == 'Tracking' || this.state.curScreen == 'Weather' ?
                    <ScrollView scrollEnabled={true} style={{ height: (Platform.OS == 'ios') ? height - 120 : height - 100, backgroundColor: '#fff', flexDirection: 'column' }} refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh.bind(this)}
                        />
                    }>
                        {this.state.subScreenLoad ? this.state.subScreen :
                            <Image source={loadingspinner} style={{ alignSelf: 'center', height: 200, width: 200, marginTop: 10 }} />
                        }
                    </ScrollView> :
                    <ScrollView scrollEnabled={true} style={{ height: (Platform.OS == 'ios') ? height - 120 : height - 100, backgroundColor: '#fff', flexDirection: 'column' }}>
                        {this.state.subScreenLoad ? this.state.subScreen :
                            <Image source={loadingspinner} style={{ alignSelf: 'center', height: 200, width: 200, marginTop: 10 }} />
                        }
                    </ScrollView>
                }


                <Modal
                    visible={this.state.modalVisible}
                    animationType={'slide'}
                    transparent={true}
                    onRequestClose={() => this.closeModal()}>

                    <View style={{ width: width, height: height, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems: 'center' }}>

                        {this.state.buttonType == 'addWeather' ?

                            <View style={{ width: width * 0.82, height: height * 0.75, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 70, borderWidth: 1, borderRadius: 10 }}>

                                {
                                    this.state.showAutoComplete ?

                                        <View style={{ width: width * 0.7, height: height * 0.75, alignSelf: 'center' }}>
                                            <TouchableOpacity style={{ marginTop: 20, marginBottom: 20 }} onPress={() => this.setState({ showAutoComplete: false })}>
                                                <Image source={cross_icon} style={{ height: 20, width: 20 }} />
                                            </TouchableOpacity>
                                            <GooglePlacesAutocomplete
                                                placeholder='Enter your address'
                                                minLength={2} // minimum length of text to search
                                                autoFocus={false}
                                                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                                listViewDisplayed='true'    // true/false/undefined
                                                fetchDetails={true}
                                                renderDescription={row => row.description} // custom description render
                                                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                                    this.prepareCity(data.structured_formatting.main_text, details.geometry.location.lat, details.geometry.location.lng);
                                                }}
                                                getDefaultValue={() => ''}
                                                query={{
                                                    // available options: https://developers.google.com/places/web-service/autocomplete
                                                    key: 'AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg',
                                                    language: 'en', // language of the results
                                                    types: 'address' // default: 'geocode'
                                                }}
                                                styles={{
                                                    textInputContainer: {
                                                        borderTopWidth: 0,
                                                        width: width * 0.70,
                                                        height: 50,
                                                        marginLeft: 10,
                                                        backgroundColor: 'rgba(256,256,256,0)' //
                                                    },
                                                    textInput: {
                                                        marginTop: 0,
                                                        marginLeft: 0,
                                                        marginBottom: 0,
                                                        height: 50,
                                                        color: '#000',
                                                        fontSize: 18,
                                                        borderBottomWidth: 1,
                                                        borderColor: 'rgba(179,179,179,1)',
                                                        backgroundColor: 'rgba(256,256,256,1)' //
                                                    },
                                                    description: {
                                                        fontWeight: '100',
                                                        fontSize: 15,
                                                        color: '#000'
                                                    },
                                                    predefinedPlacesDescription: {
                                                        color: '#1faadb'
                                                    }
                                                }}
                                                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                                GoogleReverseGeocodingQuery={{
                                                    // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                                }}
                                                GooglePlacesSearchQuery={{
                                                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                                    rankby: 'distance',
                                                    types: 'food'
                                                }}
                                            />
                                        </View>
                                        :
                                        <View style={{ width: width * 0.8, height: height * 0.75, alignItems: 'center' }}>
                                            <View style={{ height: "78%", width: width * 0.70, alignSelf: 'center', paddingTop: 20 }}>
                                                <Text style={{
                                                    fontSize: 22, fontWeight: 'bold', color: theme.heading,
                                                    margin: 10, textAlign: 'center'
                                                }}>Add Another Location</Text>
                                                <Text style={{
                                                    fontSize: 14, fontWeight: 'bold', color: theme.grey, marginBottom: 5,
                                                    marginLeft: 5, marginRight: 5, textAlign: 'center'
                                                }}>So we can provide you with the most accurate weather alerts and forecasts</Text>

                                                {this.state.selectedCity.length === 0 ?
                                                    <TouchableOpacity style={{ borderBottomColor: 'rgba(179,179,179,1)', borderBottomWidth: 1 }}
                                                        onPress={() => this.setState({ showAutoComplete: true })}>
                                                        <Text style={{ color: theme.grey, fontSize: 18, marginTop: 10, padding: 10 }} >
                                                            Enter your address
                                                        </Text>
                                                    </TouchableOpacity>
                                                    :
                                                    <View style={{ marginTop: 5 }}>
                                                        <Image source={location_icon} style={{ alignSelf: 'center', height: 40, width: 40 }} />
                                                        <Text style={{ color: '#000', textAlign: 'center', margin: 5, fontSize: 18 }}>{this.state.selectedCity}</Text>
                                                        <TouchableOpacity onPress={() => this.setState({ showAutoComplete: true })}>
                                                            <Text style={{ color: '#4281e5', textAlign: 'center', fontWeight: 'bold' }}>Edit</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                }
                                            </View>

                                            <View style={{ height: "18%", justifyContent: 'flex-end' }}>
                                                {this.state.selectedCity.length === 0 ?
                                                    null
                                                    :
                                                    !this.state.loading ?
                                                        <Button style={{ width: width * 0.5, justifyContent: 'center', backgroundColor: theme.brandPrimary, alignSelf: 'center', borderRadius: 10 }}>
                                                            <Text onPress={() => this.validateSave()}>SAVE LOCATION</Text>
                                                        </Button>
                                                        :
                                                        <Image source={loadingspinner} style={{ alignSelf: 'center', height: 75, width: 75 }} />
                                                }
                                                <TouchableOpacity style={{ marginTop: 5 }} onPress={() => this.closeModal()}>
                                                    <Text style={{ color: theme.grey, fontSize: 16, textAlign: 'center' }} >
                                                        Cancel
                                            </Text>
                                                </TouchableOpacity>

                                            </View>
                                        </View>
                                }

                            </View>
                            : null}

                        {/* Edit Home and work location model */}
                        {this.state.buttonType == 'editPersonalLoc' ?

                            <View style={{ width: width * 0.82, height: height * 0.7, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 70, borderWidth: 1, borderRadius: 10 }}>

                                {
                                    this.state.showAutoComplete ?

                                        <View style={{ width: width * 0.7, height: height * 0.75, alignSelf: 'center' }}>
                                            <TouchableOpacity style={{ marginTop: 20, marginBottom: 20 }} onPress={() => this.setState({ showAutoComplete: false })}>
                                                <Image source={cross_icon} style={{ height: 20, width: 20 }} />
                                            </TouchableOpacity>
                                            <GooglePlacesAutocomplete
                                                placeholder={this.state.editedPL == 1 ? 'Enter your home address' : 'Enter your work address'}
                                                minLength={2} // minimum length of text to search
                                                autoFocus={false}
                                                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                                listViewDisplayed='true'    // true/false/undefined
                                                fetchDetails={true}
                                                renderDescription={row => row.description} // custom description render
                                                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                                    this.prepareAddressforEdit(data.description);
                                                }}
                                                getDefaultValue={() => ''}
                                                query={{
                                                    // available options: https://developers.google.com/places/web-service/autocomplete
                                                    key: 'AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg',
                                                    language: 'en', // language of the results
                                                    types: 'address' // default: 'geocode'
                                                }}
                                                styles={{
                                                    textInputContainer: {
                                                        borderTopWidth: 0,
                                                        width: width * 0.70,
                                                        height: 50,
                                                        marginLeft: 10,
                                                        backgroundColor: 'rgba(256,256,256,0)' //
                                                    },
                                                    textInput: {
                                                        marginTop: 0,
                                                        marginLeft: 0,
                                                        marginBottom: 0,
                                                        height: 50,
                                                        color: '#000',
                                                        fontSize: 18,
                                                        borderBottomWidth: 1,
                                                        borderColor: 'rgba(179,179,179,1)',
                                                        backgroundColor: 'rgba(256,256,256,1)' //
                                                    },
                                                    description: {
                                                        fontWeight: '100',
                                                        fontSize: 15,
                                                        color: '#000'
                                                    },
                                                    predefinedPlacesDescription: {
                                                        color: '#1faadb'
                                                    }
                                                }}
                                                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                                GoogleReverseGeocodingQuery={{
                                                    // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                                }}
                                                GooglePlacesSearchQuery={{
                                                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                                    rankby: 'distance',
                                                    types: 'food'
                                                }}
                                            />
                                        </View>
                                        :
                                        <View style={{ width: width * 0.8, height: height * 0.70, alignItems: 'center' }}>
                                            <View style={{ height: "78%", width: width * 0.70, alignSelf: 'center', paddingTop: 20 }}>
                                                {this.state.editedPL == 1 ?
                                                    <View>
                                                        <Image source={home} style={{ alignSelf: 'center', height: 50, width: 50 }} />
                                                        <Text style={{ fontSize: 22, fontWeight: 'bold', color: theme.heading, margin: 10, textAlign: 'center' }}>Set Home Location</Text>
                                                    </View>
                                                    :
                                                    <View>
                                                        <Image source={work} style={{ alignSelf: 'center', height: 50, width: 50 }} />
                                                        <Text style={{ fontSize: 22, fontWeight: 'bold', color: theme.heading, margin: 10, textAlign: 'center' }}>Set Work Location</Text>
                                                    </View>
                                                }
                                                <Text style={{ fontSize: 14, fontWeight: 'bold', color: theme.grey, marginBottom: 5, marginLeft: 5, marginRight: 5, textAlign: 'center' }}>
                                                    So we can provide you with the most accurate weather alerts and forecasts
                                                </Text>

                                                {this.state.selectedAddress.length === 0 ?
                                                    <TouchableOpacity style={{ borderBottomColor: 'rgba(179,179,179,1)', borderBottomWidth: 1 }}
                                                        onPress={() => this.setState({ showAutoComplete: true })}>
                                                        <Text style={{ color: theme.grey, fontSize: 18, marginTop: 10, padding: 10 }} >
                                                            {this.state.editedPL == 1 ? 'Enter your home address' : 'Enter your work address'}
                                                        </Text>
                                                    </TouchableOpacity>
                                                    :
                                                    <View style={{ marginTop: 5 }}>
                                                        <Image source={location_icon} style={{ alignSelf: 'center', height: 40, width: 40 }} />
                                                        <Text style={{ color: '#000', textAlign: 'center', margin: 5, fontSize: 18 }}>{this.state.selectedAddress}</Text>
                                                        <TouchableOpacity onPress={() => this.setState({ showAutoComplete: true })}>
                                                            <Text style={{ color: '#4281e5', textAlign: 'center', fontWeight: 'bold' }}>Edit</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                }
                                            </View>

                                            <View style={{ height: "18%", justifyContent: 'flex-end' }}>
                                                {this.state.selectedAddress.length === 0 ?
                                                    null
                                                    :
                                                    !this.state.loading ?
                                                        <Button style={{ width: width * 0.5, justifyContent: 'center', backgroundColor: theme.brandPrimary, alignSelf: 'center', borderRadius: 10 }}>
                                                            <Text onPress={() => this.validateEdit()}>SAVE LOCATION</Text>
                                                        </Button>
                                                        :
                                                        <Image source={loadingspinner} style={{ alignSelf: 'center', height: 75, width: 75 }} />
                                                }
                                                <TouchableOpacity style={{ marginTop: 10 }} onPress={() => this.closeModal()}>
                                                    <Text style={{ color: theme.grey, fontSize: 16, textAlign: 'center' }} >
                                                        Cancel
                                                    </Text>
                                                </TouchableOpacity>

                                            </View>
                                        </View>
                                }

                            </View>

                            : null}

                        {/* Add Device Model */}
                        {
                            this.state.buttonType === "addTracking" && !this.state.isDeviceNamed && !this.state.isPebblebeeSelected && !this.state.isWeathermanSelected ?
                                <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.65, backgroundColor: 'rgba(256, 256, 256, 1.0)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
                                    <View style={{ width: "100%", height: "10%", alignItems: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => this.closeModal()}>
                                            <Image source={cross_icon} style={{ height: 15, resizeMode: 'contain', marginTop: 15, marginRight: 15 }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ alignSelf: 'center', height: "80%", justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: theme.heading, marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 20, textAlign: 'center' }}> Which device would you like to set up?</Text>

                                        <TouchableOpacity
                                            style={{ width: width * 0.70, borderWidth: 2, borderColor: '#8497ab', borderRadius: 5, marginTop: 10, alignSelf: 'center' }}
                                            onPress={() => this.onPebblebeeSelected()} >
                                            <View style={{ flexDirection: 'row' }}>
                                                <LottieView
                                                    style={styles.image}
                                                    imageAssetsFolder={'images/'}
                                                    source={require('../../animation/pebblebee.json')}
                                                    autoPlay
                                                    loop
                                                />

                                                <View style={{
                                                    padding: 6, marginLeft: 10,
                                                    alignSelf: 'center'
                                                }} >
                                                    <Text style={{
                                                        color: theme.brandPrimary, fontSize: 18, textAlign: 'center'
                                                    }}>PebbleBee</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            style={{ width: width * 0.70, borderWidth: 2, borderColor: '#8497ab', borderRadius: 5, marginTop: 10, alignSelf: 'center' }}
                                            onPress={() => this.onWeathermanSelected()} >
                                            <View style={{ flexDirection: 'row' }}>
                                                <LottieView
                                                    style={styles.image}
                                                    imageAssetsFolder={'images/'}
                                                    source={require('../../animation/weatherman.json')}
                                                    autoPlay
                                                    loop
                                                />

                                                <View style={{
                                                    padding: 6, marginLeft: 10,
                                                    alignSelf: 'center'
                                                }} >
                                                    <Text style={{
                                                        color: theme.brandPrimary, fontSize: 18, textAlign: 'center'
                                                    }}>Weatherman {'\n'}Droplet</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                : null
                        }

                        {
                            this.state.buttonType === "addTracking" && !this.state.isDeviceNamed && this.state.isPebblebeeSelected ?

                                this.state.isScanning || this.state.isPairing ?

                                    <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
                                        <View style={{ width: width * 0.8, height: height * 0.20, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                                            <LottieView
                                                style={styles.image}
                                                imageAssetsFolder={'images/'}
                                                source={require('../../animation/pebblebee_connect.json')}
                                                autoPlay
                                                loop
                                            />

                                        </View>
                                        <View style={{ width: width * 0.8, height: height * 0.30, marginTop: 20, alignItems: 'center' }}>
                                            <View style={{ height: "60%" }}>
                                                <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 10, marginBottom: 10 }}>
                                                    {this.state.isScanning ? 'Scanning your devices' : 'Pairing your devices'}
                                                </Text>
                                                <Progress.Bar style={{ marginTop: 30 }} width={width * 0.65} height={2} indeterminate={true} useNativeDriver={true} color={theme.brandPrimary} />
                                            </View>
                                            <View style={{ height: "40%", justifyContent: 'flex-end' }}>
                                                <TouchableOpacity onPress={() => this.closeModal()}>
                                                    <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 10, marginBottom: 10 }}>
                                                        Cancel
                                                    </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                    :
                                    !this.state.isScanning && !this.state.isPairing && !this.state.isFailed && !this.state.isSuccess ?

                                        <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.65, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10, alignItems: 'center' }}>
                                            <View style={{ width: width * 0.8, height: height * 0.18, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>

                                                <LottieView
                                                    style={styles.image}
                                                    imageAssetsFolder={'images/'}
                                                    source={require('../../animation/pebblebee_connect.json')}
                                                    autoPlay
                                                    loop
                                                />

                                            </View>
                                            <View style={{ width: width * 0.78, height: height * 0.30 }}>

                                                <FlatList
                                                    style={{ marginBottom: 10, height: "50%" }}
                                                    data={this.state.scannedDevices}
                                                    ListEmptyComponent={this.ListEmptyView}
                                                    renderItem={({ item, index }) => {
                                                        console.log(' item ', item)
                                                        return (

                                                            <TouchableOpacity style={{ flexDirection: "row", paddingVertical: 10, paddingHorizontal: 10 }} onPress={() => this.selectDevice(index)}>
                                                                <View style={{ width: 20, height: 20, borderRadius: 20, backgroundColor: "white", borderColor: item.isSelected ? theme.sliderOn : "grey", borderWidth: 6, alignSelf: "center" }} />
                                                                <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, textAlign: 'center', alignSelf: "center", marginHorizontal: 15 }}>Device {index} - {item.status}  </Text>
                                                            </TouchableOpacity>
                                                        )
                                                    }}
                                                    keyExtractor={(item, index) => index.toString()}
                                                    extraData={this.state}
                                                />
                                                <View style={{ height: "50%" }}>
                                                    <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 10, marginBottom: 10, marginLeft: 5, marginRight: 5 }}>
                                                        Select device(s) you would like to connect.
                                                    </Text>

                                                    <Button style={{ width: width * 0.45, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 10, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.connect(this.state.target, this.state.scannedDevices)}>CONNECT</Text></Button>

                                                    <TouchableOpacity style={{ marginTop: 5 }} onPress={() => this.closeModal()}>
                                                        <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 10, marginBottom: 5 }}>
                                                            Cancel
                                                    </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>

                                        :
                                        this.state.isFailed ?

                                            <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.68, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10, alignItems: 'center' }}>
                                                <View style={{ width: width * 0.8, height: "25%", marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                                                    <LottieView
                                                        style={styles.image}
                                                        imageAssetsFolder={'images/'}
                                                        source={require('../../animation/pebblebee_fail.json')}
                                                        autoPlay
                                                        loop
                                                    />

                                                </View>
                                                <View style={{ width: width * 0.7, height: "75%" }}>
                                                    <Text style={{ fontSize: 20, textAlign: 'center', color: 'red', marginTop: 5, marginBottom: 5 }}>
                                                        Pairing Failed
                                                    </Text>
                                                    {this.row(tick, "Bluetooth enabled on phone")}
                                                    {this.row(cross, "Tracker within range")}
                                                    {this.row(cross, "Tracker turned on")}

                                                    <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 20, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.onTryAgain()}>TRY AGAIN</Text></Button>

                                                    <TouchableOpacity onPress={() => this.closeModal()}>
                                                        <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 15, marginBottom: 10 }}>
                                                            Cancel
                                                        </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>

                                            :
                                            this.state.isSuccess ?
                                                <View style={{
                                                    alignSelf: 'center', width: width * 0.82, height: height * 0.60, backgroundColor: 'rgba(256, 256, 256, 1)',
                                                    marginTop: 100, borderWidth: 1, borderRadius: 10, alignItems: 'center'
                                                }}>
                                                    <View style={{ width: width * 0.8, height: "28%", marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                                                        <LottieView
                                                            style={styles.image}
                                                            imageAssetsFolder={'images/'}
                                                            source={require('../../animation/pebblebee_success.json')}
                                                            autoPlay
                                                            loop
                                                        />

                                                    </View>

                                                    <View style={{ width: width * 0.8, height: "70%" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 15, marginBottom: 5 }}>
                                                            Pairing Complete!!
                                                        </Text>

                                                        <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 15, marginBottom: 10, marginRight: 5, marginLeft: 5 }}>
                                                            Please don't forget to place the tracker in the zippered pocket of your umbrella or attach to the item you do not want to lose.
                                                        </Text>

                                                        <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 20, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.onNextStep()}>NEXT STEP</Text></Button>

                                                    </View>
                                                </View>
                                                : null
                                : null
                        }


                        {
                            this.state.buttonType === "addTracking" && !this.state.isDeviceNamed && this.state.isWeathermanSelected ?


                                this.state.insulationSetup ?

                                    <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10, justifyContent: 'center' }}>
                                        <View style={{ width: width * 0.8, height: "20%", marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>

                                            <LottieView
                                                style={styles.image}
                                                imageAssetsFolder={'images/'}
                                                source={require('../../animation/weatherman_main.json')}
                                                autoPlay
                                                loop
                                            />
                                        </View>
                                        <Text style={{ fontSize: 22, textAlign: 'center', color: theme.heading, marginTop: 20, marginBottom: 10, marginRight: 5, marginLeft: 5 }}>
                                            If you have not already, please remove the battery insulated tab.
                                        </Text>

                                        <Button style={{ width: width * 0.5, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 10, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.handleContinueClick()}>CONTINUE</Text></Button>

                                        <TouchableOpacity onPress={() => this.closeModal()}>
                                            <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 10, marginBottom: 10 }}>
                                                Cancel
                                            </Text>
                                        </TouchableOpacity>
                                    </View>

                                    :

                                    this.state.isScanning || this.state.isPairing ?

                                        <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
                                            <View style={{ width: width * 0.8, height: height * 0.20, marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                                                <LottieView
                                                    style={styles.image}
                                                    imageAssetsFolder={'images/'}
                                                    source={require('../../animation/weatherman_connect.json')}
                                                    autoPlay
                                                    loop
                                                />

                                            </View>
                                            <View style={{ width: width * 0.8, height: height * 0.30, marginTop: 20, alignItems: 'center' }}>
                                                <View style={{ height: "60%" }}>
                                                    <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 10, marginBottom: 10 }}>
                                                        {this.state.isScanning ? 'Scanning your devices' : 'Pairing your devices'}
                                                    </Text>
                                                    <Progress.Bar style={{ marginTop: 30 }} width={width * 0.65} height={2} indeterminate={true} useNativeDriver={true} color={theme.brandPrimary} />
                                                </View>
                                                <View style={{ height: "40%", justifyContent: 'flex-end' }}>
                                                    <TouchableOpacity onPress={() => this.closeModal()}>
                                                        <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 10, marginBottom: 10 }}>
                                                            Cancel
                                                        </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        :
                                        !this.state.isScanning && !this.state.isPairing && !this.state.isFailed && !this.state.isSuccess ?

                                            <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.65, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10, alignItems: 'center' }}>
                                                <View style={{ width: width * 0.8, height: height * 0.18, marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>

                                                    <LottieView
                                                        style={styles.image}
                                                        imageAssetsFolder={'images/'}
                                                        source={require('../../animation/weatherman_connect.json')}
                                                        autoPlay
                                                        loop
                                                    />

                                                </View>
                                                <View style={{ width: width * 0.78, height: height * 0.30 }}>

                                                    <FlatList
                                                        style={{ marginBottom: 10, height: "50%" }}
                                                        data={this.state.scannedDevices}
                                                        ListEmptyComponent={this.ListEmptyView}
                                                        renderItem={({ item, index }) => {
                                                            console.log(' item ', item)
                                                            return (

                                                                <TouchableOpacity style={{ flexDirection: "row", paddingVertical: 10, paddingHorizontal: 10 }} onPress={() => this.selectDevice(index)}>
                                                                    <View style={{ width: 20, height: 20, borderRadius: 20, backgroundColor: "white", borderColor: item.isSelected ? theme.sliderOn : "grey", borderWidth: 6, alignSelf: "center" }} />
                                                                    <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, textAlign: 'center', alignSelf: "center", marginHorizontal: 15 }}>Device {index} - {item.status}  </Text>
                                                                </TouchableOpacity>
                                                            )
                                                        }}
                                                        keyExtractor={(item, index) => index.toString()}
                                                        extraData={this.state}
                                                    />
                                                    <View style={{ height: "50%" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 10, marginBottom: 10, marginLeft: 5, marginRight: 5 }}>
                                                            Select device(s) you would like to connect.
                                                    </Text>

                                                        <Button style={{ width: width * 0.45, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 10, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.connect(this.state.target, this.state.scannedDevices)}>CONNECT</Text></Button>

                                                        <TouchableOpacity style={{ marginTop: 5 }} onPress={() => this.closeModal()}>
                                                            <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 10, marginBottom: 5 }}>
                                                                Cancel
                                                    </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>

                                            :
                                            this.state.isFailed ?

                                                <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.68, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10, alignItems: 'center' }}>
                                                    <View style={{ width: width * 0.8, height: "25%", marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                                                        <LottieView
                                                            style={styles.image}
                                                            imageAssetsFolder={'images/'}
                                                            source={require('../../animation/weatherman_fail.json')}
                                                            autoPlay
                                                            loop
                                                        />

                                                    </View>
                                                    <View style={{ width: width * 0.7, height: "75%" }}>
                                                        <Text style={{ fontSize: 20, textAlign: 'center', color: 'red', marginTop: 5, marginBottom: 5 }}>
                                                            Pairing Failed
                                                    </Text>
                                                        {this.row(tick, "Bluetooth enabled on phone")}
                                                        {this.row(cross, "Tracker within range")}
                                                        {this.row(cross, "Tracker turned on")}

                                                        <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 20, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.onTryAgain()}>TRY AGAIN</Text></Button>

                                                        <TouchableOpacity onPress={() => this.closeModal()}>
                                                            <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 15, marginBottom: 10 }}>
                                                                Cancel
                                                        </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>

                                                :
                                                this.state.isSuccess ?
                                                    <View style={{
                                                        alignSelf: 'center', width: width * 0.82, height: height * 0.60, backgroundColor: 'rgba(256, 256, 256, 1)',
                                                        marginTop: 100, borderWidth: 1, borderRadius: 10, alignItems: 'center'
                                                    }}>
                                                        <View style={{ width: width * 0.8, height: "28%", marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>

                                                            <LottieView
                                                                style={styles.image}
                                                                imageAssetsFolder={'images/'}
                                                                source={require('../../animation/weatherman_success.json')}
                                                                autoPlay
                                                                loop
                                                            />

                                                        </View>

                                                        <View style={{ width: width * 0.8, height: "70%" }}>
                                                            <Text style={{ fontSize: 20, textAlign: 'center', color: theme.heading, marginTop: 15, marginBottom: 5 }}>
                                                                Pairing Complete!!
                                                        </Text>

                                                            <Text style={{ fontSize: 15, textAlign: 'center', color: theme.grey, marginTop: 15, marginBottom: 10, marginRight: 5, marginLeft: 5 }}>
                                                            Please don't forget to place the tracker in the zippered pocket of your umbrella or attach to the item you do not want to lose.
                                                        </Text>

                                                            <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 20, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.onNextStep()}>NEXT STEP</Text></Button>

                                                        </View>
                                                    </View>
                                                    : null
                                : null
                        }

                        {
                            this.state.buttonType === "addTracking" && this.state.isDeviceNamed ?
                                <View style={{
                                    alignSelf: 'center', width: width * 0.82, height: height * 0.65, backgroundColor: 'rgba(256, 256, 256, 1)',
                                    marginTop: 100, borderWidth: 1, borderRadius: 10, alignItems: 'center', justifyContent: 'center'
                                }}>
                                    {/* <View style={{ width: width * 0.8, height: "22%", marginTop: 20, alignItems: 'center', justifyContent: 'center'}}>

                                        <LottieView
                                            style={styles.image}
                                            imageAssetsFolder={'images/'}
                                            source={require('../../animation/umbrella_ongoing.json')}
                                            autoPlay
                                            loop
                                        />

                                    </View> */}

                                    <View style={{ width: width * 0.70, height: "40%", flexDirection: 'row', justifyContent: 'center' }}>
                                        <View style={{ marginRight: 2, padding: 5, paddingRight: 18, marginTop: 20 }}>

                                            {this.state.iconToUpdate == 'Umbrella' ? <Image source={umbrella_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                                            {this.state.iconToUpdate == 'Keys' ? <Image source={key_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                                            {this.state.iconToUpdate == 'Bag' ? <Image source={bag_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                                            {this.state.iconToUpdate == 'Car' ? <Image source={car_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                                            {this.state.iconToUpdate == 'Pet' ? <Image source={pet_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                                            {this.state.iconToUpdate == 'Other' ? <Image source={other_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                                            <TouchableOpacity style={{ position: 'absolute', alignSelf: 'flex-end' }} onPress={() => this.showIconDialog()}>
                                                <Animated.Image style={{ height: 25, width: 25, transform: [{ rotate: spin }] }} source={down_arrow} />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={{ margin: 5, width: "80%" }}>
                                            <Text style={{ fontSize: 16, color: theme.heading, marginTop: 20 }}>
                                                Please name your device:
                                            </Text>
                                            <InputGroup style={{ marginBottom: 10, width: width * 0.55, backgroundColor: "rgba(38, 38, 38, 0.0)", borderBottomWidth: 1, borderColor: theme.brandPrimary }} boarderType='round'>
                                                <Input style={{ color: '#000' }}
                                                    maxLength={20}
                                                    value={this.state.newUmbrellaName}
                                                    placeholder='Device Name'
                                                    placeholderTextColor={theme.brandTertiary}
                                                    onChangeText={(val) => { this.setState({ newUmbrellaName: val }) }} />
                                            </InputGroup>
                                        </View>

                                    </View>

                                    {this.state.isIconsVisible ?
                                        <View style={{ width: "96%", flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', marginTop: 5, marginBottom: 5 }}>
                                            <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Umbrella' })}>
                                                {this.state.iconToUpdate == 'Umbrella' ?
                                                    <Image source={umbrella_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                                                    :
                                                    <Image source={umbrella_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}

                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Keys' })}>
                                                {this.state.iconToUpdate == 'Keys' ?
                                                    <Image source={key_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                                                    :
                                                    <Image source={key_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Bag' })}>
                                                {this.state.iconToUpdate == 'Bag' ?
                                                    <Image source={bag_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                                                    :
                                                    <Image source={bag_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Car' })}>
                                                {this.state.iconToUpdate == 'Car' ?
                                                    <Image source={car_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                                                    :
                                                    <Image source={car_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Pet' })}>
                                                {this.state.iconToUpdate == 'Pet' ?
                                                    <Image source={pet_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                                                    :
                                                    <Image source={pet_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Other' })}>
                                                {this.state.iconToUpdate == 'Other' ?
                                                    <Image source={other_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                                                    :
                                                    <Image source={other_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                                            </TouchableOpacity>
                                        </View>
                                        : null}

                                    <Button style={{ width: width * 0.7, justifyContent: 'center', backgroundColor: theme.brandPrimary, marginTop: 10, alignSelf: 'center', borderRadius: 5 }}><Text onPress={() => this.handleSubmit()}>DONE</Text></Button>
                                    <TouchableOpacity onPress={() => this.closeModal()}>
                                        <Text style={{ fontSize: 15, textAlign: 'center', fontWeight: 'bold', color: theme.grey, marginTop: 10, marginBottom: 5 }}>
                                            Cancel
                                        </Text>
                                    </TouchableOpacity>

                                </View>
                                : null
                        }
                    </View>
                </Modal>


                <Footer style={{ height: height * 0.1 }}>
                    <FooterTab style={{ backgroundColor: '#efeff4' }}>
                        <SafeAreaView>
                            <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorWeather === "Active") ? '#fff' : '#efeff4' }}>
                                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("Weather")}>
                                    {this.state.colorWeather == "Active" ?
                                        <LottieView
                                            style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                                            imageAssetsFolder={'images/'}
                                            source={require('../../animation/sun.json')}
                                            autoPlay
                                            loop
                                        />
                                        :
                                        <Image source={weather_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                                    {this.state.colorWeather == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>Weather</Text> :
                                        <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>Weather</Text>}
                                </TouchableOpacity>
                            </View>
                        </SafeAreaView>
                        <SafeAreaView>
                            <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorTracking === "Active") ? '#fff' : '#efeff4' }}>
                                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("Tracking")}>
                                    {this.state.colorTracking == "Active" ?
                                        <LottieView
                                            style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                                            imageAssetsFolder={'images/'}
                                            source={require('../../animation/umbrella.json')}
                                            autoPlay
                                            loop
                                        />
                                        :
                                        <Image source={tracking_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                                    {this.state.colorTracking == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>Devices</Text> :
                                        <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>Devices</Text>}
                                </TouchableOpacity>
                            </View>
                        </SafeAreaView>
                        <SafeAreaView>
                            <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorNotifications === "Active") ? '#fff' : '#efeff4' }}>
                                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("Notifications")}>
                                    {this.state.colorNotifications == "Active" ?
                                        <LottieView
                                            style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                                            imageAssetsFolder={'images/'}
                                            source={require('../../animation/notification.json')}
                                            autoPlay
                                            loop
                                        />
                                        :
                                        <Image source={notifications_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                                    {this.state.colorNotifications == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>Notifications</Text> :
                                        <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>Notifications</Text>}
                                </TouchableOpacity>
                            </View>
                        </SafeAreaView>
                        <SafeAreaView>
                            <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorSettings === "Active") ? '#fff' : '#efeff4' }}>
                                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("AppSettings")}>
                                    {this.state.colorSettings == "Active" ?
                                        <LottieView
                                            style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                                            imageAssetsFolder={'images/'}
                                            source={require('../../animation/settings.json')}
                                            autoPlay
                                            loop
                                        />
                                        :
                                        <Image source={settings_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                                    {this.state.colorSettings == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>Settings</Text> :
                                        <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>Settings</Text>}
                                </TouchableOpacity>
                            </View>
                        </SafeAreaView>
                    </FooterTab>
                </Footer>

            </Container >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.brandPrimary,
        height: 150,
        width: "100%",
    },
    chartView: {
        backgroundColor: 'transparent',
        width: "100%",
        height: 100
    },
    buttonGroup: {
        height: 50,
        width: "100%",
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10
    },
    checkboxImage: {
        height: 40,
        width: 40
    },
    openModel: {
        alignItems: 'center',
        backgroundColor: '#67738d',
        borderRadius: 30,
        paddingTop: 4,
        paddingBottom: 4,
        borderColor: '#eaeaea',
        borderWidth: 1
    },
    closeModel: {
        alignItems: 'center',
        backgroundColor: '#67738d',
        padding: 4,
        borderRadius: 30
    },
    image: {
        width: width * 0.25,
        height: width * 0.25,
        alignSelf: 'center',
        resizeMode: 'cover',
        marginBottom: 20,
    }
});

const mapStateToProps = (state) => {
    return ({
        getUnitData: state.user.getUserIdState,
        location: state.location,
        wUnits: state.user.wUnits,
        deviceToken: state.user.deviceToken,
        notificationId: state.user.notificationId,
        isLoading: state.user.isLoaded,
        wnotify1: state.user.wnotify1,
        wnotify2: state.user.wnotify2,
        wnotifyTime1Show: state.user.wnotifyTime1Show,
        wnotifyTime2Show: state.user.wnotifyTime2Show
    })
}

const mapDispatchToProps = dispatch => ({
    UserActions: bindActionCreators(UserActions, dispatch),
    userLocation: bindActionCreators(userLocation, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

/*
Earlier the code was statically written, so everytime the first card is of current location. But now, We are rendering data dynamically i.e it will not wait for all the data to load, as soon as it get first data wether it is current location or not it will be displayed on the screen.
If i want to forcefully load the current location first ( which depend on geocoder map api's ) , then all the card will only be displayed when i will get the current location from api and till that Loader will be there on the screen.
SOPRA

*/