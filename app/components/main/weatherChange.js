import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    FlatList,
    View,
    Dimensions,
    Platform,
    Image,
    Alert,
    Linking
} from 'react-native';
import Permissions from 'react-native-permissions';
import ListItem from './ListItem';
import Settings from '../../stores/settingsStore'
import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
import { checkPermissions } from "../../utilities/Locations_updated";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import DeviceInfo from 'react-native-device-info';
import { formatDate, getDayoftheWeekHourly, getDayoftheWeek } from '../../functions/datetimefuncs';
import { round, getpercent, getBearing, getCardPercent } from '../../functions/mathandweatherfuncs';
import theme from '../../theme/base-theme';
import Idx from '../../utilities/Idx';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as WeatherActions from '../../redux/modules/weather';
import * as UserActions from '../../redux/modules/user';
import * as userLocation from '../../redux/modules/location';
import { writeToLog } from '../../functions/umbrellafuncs';
import Banner from './banner';

var moment = require('moment-timezone');
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

const pin = require('../../img/currentLocation.png')
const Home = require('../../img/home_icon.png')
const Work = require('../../img/work_Icon.png')
const close = require('../../img/close.png')
const cloud = require('../../img/cloudy.png')
const temp = require('../../img/temp.png')
const humidity = require('../../img/humidity.png')
const wind = require('../../img/wind.png')
const windGust = require('../../img/windGust.png')
const edit_icon = require('../../img/edit_button.png')
const delete_icon = require('../../img/delete_button.png')

const weatherClearDay = require('../../img/weatherClearDay.png')
const weatherClearNight = require('../../img/weatherClearNight.png')
const weatherRain = require('../../img/weatherRain.png')
const weatherSnow = require('../../img/weatherSnow.png')
const weatherSleet = require('../../img/weatherHail.png')
const weatherWind = require('../../img/weatherWind.png')
const weatherFog = require('../../img/weatherFog.png')
const weatherCloudy = require('../../img/weatherCloudy.png')
const weatherPartlyCloudyDay = require('../../img/weatherPartlyCloudyDay.png')
const weatherPartlyCloudyNight = require('../../img/weatherPartlyCloudyNight.png')
const weatherHail = require('../../img/weatherHail.png')
const weatherThunderstorm = require('../../img/weatherThunderstorm.png')
const weatherTornado = require('../../img/weatherTornado.png')
const loadingspinner = require('../../img/loading.gif')
const settings = new Settings();

const subNavColors = {
    active:
        'rgba(256,256,256,0.2)'
    ,
    inactive:
        'rgba(256,256,256,0)'
};

var loadFirstTime = true;

class Weather extends Component {
    constructor(props) {
        super(props)
        this.state = {
            baseUrl: '',
            dsUrl: '',
            myID: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].id : '',
            curTemp: "",
            myUnits: this.props.wUnits || '',
            mylatitude: "",
            mylongitude: "",
            mylocations: 3,
            expand1: 'True',
            expanded1: '180deg',
            myheight1: 180,
            expand2: 'True',
            expanded2: '180deg',
            myheight2: 180,
            expand3: 'True',
            expanded3: '180deg',
            myheight3: 180,
            expand4: 'True',
            expanded4: '180deg',
            myheight4: 180,
            expand5: 'True',
            expanded5: '180deg',
            myheight5: 180,
            expand6: 'True',
            expanded6: '180deg',
            myheight6: 180,
            loading: true,
            weatherLoading: false,
            locationRetry: false,
            forecast: [],
            citynames: [],
            allcities: [],
            personalLoc: [],
            mycities: [],
            mylatlons: [],
            getUnitData: props.getUnitData == [] ? props.userData : props.getUnitData,
            isLoading: true,
            // targetIndex: null
            currentLocationData: props.location.currentLocation,
            // extra state
            dataLenght: '',
            homeAddress: 'null',
            workAddress: 'null',
            enable: true,
            listloading: false,

        }

    }

    componentWillReceiveProps(nextProps) {
        loadFirstTime = false;
        this.setState({
            getUnitData: nextProps.getUnitData,
            wUnits: nextProps.wUnits
        })
        this.forceUpdate();
    }

    showPermissionsDialog() {

        Alert.alert(
            "Location Permissions",
            "We need to access your location. Please go to Settings > Privacy > Weatherman wants to use your location.",
            [{
                text: "Ok",
                onPress: () => {
                    return false;
                }
            }],
            { cancelable: false }
        );

    }

    async componentDidMount() {
        this.state.uniqueId = DeviceInfo.getUniqueID();
        this.state.baseUrl = settings.BaseUrl;
        this.state.dsUrl = settings.DSUrl;
        this.state.forecast.push('');
        this.state.citynames.push('');
        if (Platform.OS === 'android') {
            let hasPermissions = await checkPermissions()
            // var isPermissions = await checkPermissions(this.state.uniqueId, this.state.baseUrl);
            if (hasPermissions) {
                this.locateMe();
            } else {
                this.showPermissionsDialog();
            }
        } else {
            this.locateMe();
        }
        this.getCities();
        this.getCityName(this.state.currentLocationData.latitude, this.state.currentLocationData.longitude, 0);
        this.updateLastOpen();
        writeToLog('', '', DeviceInfo.getUniqueID(), 1, 'appopen')
        //  this.getUnits();
    }

    updateLastOpen() {
        if (this.state.getUnitData[0] != undefined) {
            this.props.UserActions.storeLastOpened({ idToUpdate: this.state.getUnitData[0].id })
        }
    }

    _onRefresh() {
        this.setState({ locationRetry: false });
        this.state.forecast = [];
        this.state.citynames = [];
        this.state.allcities = [];
        this.state.mycities = [];
        this.state.personalLoc = [];
        this.state.forecast.push('');
        this.state.citynames.push('');
        //this.getUnits();
        this.getCities();


    }

    enableGPS = () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
            .then(data => {
                console.log('data ', data)
            }).catch(err => {
                console.log('err ', err)
            });
    }

    locateMe() {
        navigator.geolocation.watchPosition(
            (position) => {
                if (position.coords && position.coords.latitude && position.coords.longitude) {
                    this.getCityName(position.coords.latitude, position.coords.longitude, 0);
                    this.props.getUserLocation(position);
                }
            },
            (error) => {
                this.setState({ locationRetry: true })
            },
            {
                enableHighAccuracy: false,
                timeout: 1000 * 60 * 2,
                maximumAge: 1000,
            }
        );
    }

    getCities() {
        this.setState({ isCitiesLoaded: true })
        let parsed = this.state.getUnitData || this.props.getUnitData;
        let locationData = parsed[0]
        let data = []
        if (parsed.length > 0) {
            locationData.wHomeLoc !== null ? data.push({ Address: locationData.wHomeLoc, type: "Home", latLon: null }) : null
            locationData.wWorkLoc !== null ? data.push({ Address: locationData.wWorkLoc, type: "Work", latLon: null }) : null
            locationData.wCNAdd1 !== null ? data.push({ Address: locationData.wCNAdd1, type: "random1", latLon: locationData.wLLAdd1 }) : null
            locationData.wCNAdd2 !== null ? data.push({ Address: locationData.wCNAdd2, type: "random2", latLon: locationData.wLLAdd2 }) : null
            locationData.wCNAdd3 !== null ? data.push({ Address: locationData.wCNAdd3, type: "random3", latLon: locationData.wLLAdd3 }) : null
            this.setState({ dataLenght: data.length })
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    if (data[i].Address)
                        this.getCNLL(data[i].Address, data[i].type, data[i].latLon)
                }
            }

        }


    }

    async getCityName(lat, lon, id) {
        try {
            let response = await fetch(settings.geourl + 'latlng=' + lat + ',' + lon + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                if (parsed.results[0].address_components[1].types[0] == "locality" || parsed.results[0].address_components[1].types[1] == "sublocality") { // locality type
                    this.getTemp(lat, lon, 1, parsed.results[0].address_components[1].long_name, "Current");

                }
                else if (parsed.results[0].address_components[2].types[0] == "locality" || parsed.results[0].address_components[2].types[1] == "sublocality") { // locality type
                    this.getTemp(lat, lon, 1, parsed.results[0].address_components[2].long_name, "Current");
                }
                else if (parsed.results[0].address_components.length > 3) {
                    if (parsed.results[0].address_components[3].types[0] == "locality" || parsed.results[0].address_components[3].types[1] == "sublocality") { // locality type
                        this.getTemp(lat, lon, 1, parsed.results[0].address_components[3].long_name, "Current");
                    }
                } else {
                    this.getTemp(lat, lon, 1, parsed.results[0].address_components[1].long_name, "Current");
                }
            } else {
                let errors = res;
                throw errors;
            }

        } catch (error) {
            this.setState({ errors: error });

        }
    }


    async getCNLL(address, type, latlong) {
       
        if (latlong === null || latlong === undefined) {
            try {
                let response = await fetch(settings.geourl + 'address=' + address + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                let res = await response.text();
                if (response.status >= 200 && response.status < 300) {
                    let parsed = JSON.parse(res)
                    if (parsed.results[0].address_components[1].types[0] == "locality" || parsed.results[0].address_components[1].types[1] == "sublocality") { // locality type
                        this.getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, 2, parsed.results[0].address_components[1].long_name, type);

                    }
                    else if (parsed.results[0].address_components[2].types[0] == "locality" || parsed.results[0].address_components[2].types[1] == "sublocality") { // locality type
                        this.getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, 2, parsed.results[0].address_components[2].long_name, type);

                    }
                    else if (parsed.results[0].address_components.length > 3) {
                        if (parsed.results[0].address_components[3].types[0] == "locality" || parsed.results[0].address_components[3].types[1] == "sublocality") { // locality type
                            this.getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, 2, parsed.results[0].address_components[3].long_name, type);
                        }
                    } else {
                        this.getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, 2, parsed.results[0].address_components[1].long_name, type);
                    }
                }

            } catch (error) {
                this.setState({ errors: error });
            }
        } else {
            var location = latlong.split(",");
            this.getTemp(location[0], location[1], 3, address, type);
        }
    }

    async getTemp(lat, lon, id, address, type) {
      
        try {
            let response = await fetch(this.state.dsUrl + lat + ',' + lon + '?units=' + this.state.myUnits, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                console.log('parsed ', parsed)
                let DateArray = [];
                parsed.isExpandable = false
                // parsed.isEditable = false
                parsed.subNav = "Current"
                parsed.address = address
                parsed.type = type
                if (type === "Current") {
                    // if (loadFirstTime) {
                    this.props.userLocation.setCurrentLocation(parsed);
                    // }
                    this.setState({ currentLocationData: parsed })
                }
                else {
                    this.setState({ forecast: [...this.state.forecast, parsed] }, () => {
                        var currentData = this.state.forecast
                        for (var i = 1; i <= currentData.length - 1; i++) {
                            if (currentData[i].hourly.data.length !== 0) {
                                for (let key in currentData[i].hourly.data) {
                                    let data = currentData[i].hourly.data
                                    data[key].listIndex = i
                                }
                            }
                            if (currentData[i].daily.data.length !== 0) {
                                for (let key in currentData[i].daily.data) {
                                    let data = currentData[i].daily.data
                                    data[key].listIndex = i
                                }
                            }
                        }
                        this.setState({ forecast: currentData }, () => {
                            console.log('forecast ', this.state.forecast)
                        })
                    }
                    )

                }

                this.storeLocation(id, lat, lon);

            } else {
                let errors = res;
                throw errors;
            }

        } catch (error) {
            this.setState({ errors: error });

        }
    }

    getPLLoc() {

        let parsed = this.props.getUnitData;

        if (this.state.deletedPL.type == 'Home') {
            this.props.UserActions.deleteHomeLocation(this.state.myID);
            this.setState({
                wHomeLoc: null,
                homeLocation: null
            })
        }
        if (this.state.deletedPL.type == 'Work') {
            this.props.UserActions.deleteWorkLocation(this.state.myID);
            this.setState({
                wWorkLoc: null,
                workLocation: null
            })
        }
        setTimeout(() => {
            this._onRefresh()
            this.props.RefreshIndicator(this.state.deletedPL.type)

        }, 2000)
        setTimeout(() => { this.setState({ listloading: false }); }, 2000)
    }

    getALLoc() {
        let parsed = this.props.getUnitData;
        if (this.state.deletedAL.type == 'random1') {
            this.props.UserActions.deleteAdditional1(this.state.myID);
        }
        if (this.state.deletedAL.type == 'random2') {
            this.props.UserActions.deleteAdditional2(this.state.myID);
        }
        if (this.state.deletedAL.type == 'random3') {
            this.props.UserActions.deleteAdditional3(this.state.myID);
        }
        setTimeout(() => {
            this._onRefresh()
        }, 2000)
        setTimeout(() => { this.setState({ listloading: false }); }, 2000)
    }

    editItem = (item, index) => {
        if (item.type == 'Home') {
            this.props.editPersonalLoc(1, "Edit")
        } else {
            this.props.editPersonalLoc(2, "Edit")
        }

    }

    deleteItem = (item, index) => {
        if (item.type == 'Home' || item.type == 'Work') {
            this.deletePersonalLoc(item)
        } else {
            this.deleteAdditionalLoc(item)
        }
    }

    deletePersonalLoc(data) {
        this.setState({ listloading: true, forecast: [] });
        // this.props.deleteItem(data,0)
        this.setState({ deletedPL: data }, () => {
            this.getPLLoc();
            this.forceUpdate();
        });

    }

    deleteAdditionalLoc(data) {
        this.setState({ listloading: true });
        this.setState({ deletedAL: data }, () => {
            this.getALLoc();
            this.forceUpdate();
        });
    }

    selectSubNav(selectedSubNav, item, index) {

        if (index === 0) {
            const { currentLocationData } = this.state
            let forecastData = currentLocationData
            forecastData.subNav = selectedSubNav

            this.setState({ currentLocationData: forecastData })
        } else {
            const { forecast } = this.state
            let forecastData = forecast
            forecastData[index].subNav = selectedSubNav

            this.setState({ forecast: forecastData })
        }

    }

    async storeLocation(LocType, lat, lon) {

        try {

            let response = await fetch(this.state.baseUrl + 'wn_location/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({
                    wuserid: this.state.uniqueId,
                    wloctypeid: LocType,
                    wlat: lat,
                    wlon: lon,
                    wloctime: Date.now(),
                })
            });

            let res = await response.text();
            if (response.status >= 200 && response.status < 300) {
                this.setState({ loading: false });
                this.setState({ error: "" });
                let parsed = JSON.parse(res);
            } else {
                let errors = res;
                throw errors;
            }

        } catch (errors) {
            let formErrors = JSON.parse(errors);
            let errorsArray = [];
            for (let key in formErrors) {
                if (formErrors[key].length > 1) {
                    formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
                } else {
                    errorsArray.push(`${key} ${formErrors[key]}`)
                }
            }
            this.setState({ errors: errorsArray });
        }
    }

    // showMenu(item, index) {

    //     console.log('showMenu')
    //     if (index !== 0) {
    //         const { forecast } = this.state
    //         var data = forecast
    //         data[index].isEditable = !data[index].isEditable
    //         this.setState({ foreCast: data })
    //     }

    // }

    collapsible(item, index) {

        if (index === 0) {
            const { currentLocationData } = this.state
            var data = currentLocationData
            data.isExpandable = !data.isExpandable
            this.setState({ currentLocationData: data })

        } else {
            const { forecast } = this.state
            var data = forecast
            data[index].isExpandable = !data[index].isExpandable
            this.setState({ foreCast: data })
        }

    }

    currentView = (item, index) => {

        return (
            <View>
                <View style={{ width: width - 80, flexDirection: "row", alignSelf: 'center', marginVertical: 20 }}>
                    <View style={{ width: (width - 80) / 3 }}>
                        <Image source={temp} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
                        <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Feels Like</Text>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.apparentTemperature)}˚</Text>
                    </View>
                    <View style={{ width: (width - 80) / 3 }}>
                        <Image source={windGust} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
                        <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Wind / Gust</Text>
                        <Text numberOfLines={1} style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{getBearing(item.currently.windBearing)} {round(item.currently.windSpeed)}/{round(item.currently.windGust)}</Text>
                    </View>
                    <View style={{ width: (width - 80) / 3 }}>
                        <Image source={humidity} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
                        <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Humidity</Text>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.humidity * 100)}%</Text>
                    </View>
                </View>
                <View style={{ width: width - 80, flexDirection: "row", alignSelf: 'center', paddingBottom: 20 }}>
                    <View style={{ width: (width - 80) / 3 }}>
                        <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Pressure</Text>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.pressure)}</Text>
                    </View>
                    <View style={{ width: (width - 80) / 3 }}>
                        <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Visibility</Text>
                        <Text numberOfLines={1} style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>{round(item.currently.visibility)}</Text>
                    </View>
                    <View style={{ width: (width - 80) / 3 }}>
                        <Text style={{ color: 'rgba(256, 256, 256, 0.7)', fontWeight: '200', fontSize: 14, alignSelf: "center", marginVertical: 6 }}>Dew Point</Text>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', alignSelf: "center" }}>
                            {round(item.currently.dewPoint)}˚
                        </Text>
                    </View>
                </View>
            </View>
        )
    }

    hourlyView = (item, index, listIndex) => {
        const { forecast } = this.state
        var timeZone = ''

        if (listIndex === 0) {
            timeZone = this.state.currentLocationData.timezone;

        } else {
            timeZone = forecast[item.listIndex].timezone;
        }

        return (
            <View style={{ alignItems: "center", paddingHorizontal: 15 }}>
                <Text style={{ fontWeight: '400', color: '#fff', fontSize: 15, marginBottom: 2, alignSelf: 'center', }}>{formatDate(item.time, timeZone)}</Text>
                <Text style={{ fontWeight: '400', fontSize: 12, color: '#fff', marginTop: 4, marginBottom: 15, alignSelf: 'center' }}>{getpercent(round(item.precipProbability * 100))}</Text>
                {item.icon == 'clear-day' ? <Image source={weatherClearDay} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'clear-night' ? <Image source={weatherClearNight} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'rain' ? <Image source={weatherRain} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'snow' ? <Image source={weatherSnow} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'sleet' ? <Image source={weatherSleet} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'wind' ? <Image source={weatherWind} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'fog' ? <Image source={weatherFog} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'cloudy' ? <Image source={weatherCloudy} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'hail' ? <Image source={weatherHail} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'thunderstorm' ? <Image source={weatherThunderstorm} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                {item.icon == 'tornado' ? <Image source={weatherTornado} style={{ height: 28, width: 28, alignSelf: "center", marginBottom: 5 }} /> : null}
                <Text style={{ fontWeight: '600', fontSize: 14, color: '#fff', marginBottom: 2, alignSelf: 'center' }}>{round(item.temperature)}˚</Text>
                <Text style={{ fontWeight: '400', color: '#fff', fontSize: 15, marginRight: 5 }}>{getDayoftheWeekHourly(item.time, timeZone)}</Text>
            </View >
        )
    }

    extendedView = (item, index, listIndex) => {
        const { forecast } = this.state

        var timeZone = ''

        if (listIndex === 0) {
            timeZone = this.state.currentLocationData.timezone;

        } else {
            timeZone = forecast[item.listIndex].timezone;
        }

        return (
            <View style={{ alignItems: "center", paddingHorizontal: 15 }}>
                <Text style={{ fontWeight: '400', color: '#fff' }}>{getDayoftheWeek(item.time, timeZone)}</Text>
                <Text style={{ fontWeight: '400', fontSize: 12, color: '#fff', marginTop: 4, marginBottom: 15 }}>{getpercent(round(item.precipProbability * 100))}</Text>
                {item.icon == 'clear-day' ? <Image source={weatherClearDay} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'clear-night' ? <Image source={weatherClearNight} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'rain' ? <Image source={weatherRain} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'snow' ? <Image source={weatherSnow} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'sleet' ? <Image source={weatherSleet} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'wind' ? <Image source={weatherWind} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'fog' ? <Image source={weatherFog} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'cloudy' ? <Image source={weatherCloudy} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'hail' ? <Image source={weatherHail} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'thunderstorm' ? <Image source={weatherThunderstorm} style={{ height: 28, width: 28 }} /> : null}
                {item.icon == 'tornado' ? <Image source={weatherTornado} style={{ height: 28, width: 28 }} /> : null}
                <Text style={{ fontWeight: '700', fontSize: 16, color: '#fff', marginTop: 10 }}>{round(item.apparentTemperatureHigh)}˚</Text>
                <Text style={{ fontWeight: '300', fontSize: 14, color: '#fff', marginTop: 5, paddingBottom: 5 }}>{round(item.apparentTemperatureLow)}˚</Text>
            </View>
        )
    }

    renderForecastData(item, index) {
        console.log(index, ' renderForecastData item ', item)

        return (
            <View style={{
                zIndex: 1, width: width - 30, paddingTop: 20, paddingHorizontal: 20, marginBottom: 10, paddingBottom: item.isExpandable ? 20 : 2,
                borderRadius: 10, overflow: 'hidden', alignSelf: 'center', flexDirection: 'column'
            }}>
                {item.currently.icon == 'clear-day' ? <AnimatedLinearGradient customColors={myWeather.weatherClearDay} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
                {item.currently.icon == 'clear-night' ? <AnimatedLinearGradient customColors={myWeather.weatherClearNight} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'rain' ? <AnimatedLinearGradient customColors={myWeather.weatherRain} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'snow' ? <AnimatedLinearGradient customColors={myWeather.weatherSnow} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'sleet' ? <AnimatedLinearGradient customColors={myWeather.weatherSleet} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'wind' ? <AnimatedLinearGradient customColors={myWeather.weatherWind} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'fog' ? <AnimatedLinearGradient customColors={myWeather.weatherFog} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'cloudy' ? <AnimatedLinearGradient customColors={myWeather.weatherCloudy} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'partly-cloudy-day' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyDay} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'partly-cloudy-night' ? <AnimatedLinearGradient customColors={myWeather.weatherPartlyCloudyNight} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'hail' ? <AnimatedLinearGradient customColors={myWeather.weatherHail} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'thunderstorm' ? <AnimatedLinearGradient customColors={myWeather.weatherThunderstorm} speed={10000} points={direction.vertical} /> : null}
                {item.currently.icon == 'tornado' ? <AnimatedLinearGradient customColors={myWeather.weatherTornado} speed={10000} points={direction.vertical} /> : null}
                <TouchableOpacity style={{ paddingTop: 10, }} onPress={() => this.collapsible(item, index)} >
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={{ flexDirection: "row", marginLeft: 10 }}>
                            {item.type === "Current" || item.type === "Home" || item.type === "Work" ?
                                <Image source={item.type === "Current" ? pin : item.type === "Home" ? Home : item.type === "Work" ? Work : null} style={{ height: 28, width: 28, alignSelf: "center" }} resizeMode="contain" />
                                :
                                null
                            }
                            <Text numberOfLines={1} style={{ fontSize: 28, marginLeft: 15, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>{item.address}</Text>
                        </View>
                        <Image source={close} style={{ alignSelf: "center", height: 28, width: 28, transform: [{ rotate: !item.isExpandable ? '180deg' : '0deg' }] }} />
                    </View>
                    <View style={{ flexDirection: "row", marginVertical: 10, width: "100%", justifyContent: "space-between" }}>
                        <Text
                            style={{ width: "47%", fontSize: 60, paddingLeft: 10, fontWeight: (Platform.OS === 'ios') ? '200' : '300', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#fff', backgroundColor: 'rgba(256, 0, 0, 0)', marginTop: 5 }}
                        >
                            {round(item.currently.temperature)}˚
                        </Text>
                        <View style={{ width: "53%", alignItems: "baseline", justifyContent: "flex-end", marginVertical: 20, marginLeft: 10, marginRight: 10 }}>
                            {item.currently.icon === 'clear-day' ? <Image source={weatherClearDay} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'clear-night' ? <Image source={weatherClearNight} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'rain' ? <Image source={weatherRain} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'snow' ? <Image source={weatherSnow} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'sleet' ? <Image source={weatherSleet} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'wind' ? <Image source={weatherWind} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'fog' ? <Image source={weatherFog} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'cloudy' ? <Image source={weatherCloudy} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'partly-cloudy-day' ? <Image source={weatherPartlyCloudyDay} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'partly-cloudy-night' ? <Image source={weatherPartlyCloudyNight} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'hail' ? <Image source={weatherHail} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'thunderstorm' ? <Image source={weatherThunderstorm} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            {item.currently.icon === 'tornado' ? <Image source={weatherTornado} style={{ height: 40, width: 40, alignSelf: "center" }} resizeMode="contain" /> : null}
                            <Text style={{ fontWeight: '300', fontSize: 22, color: '#fff', backgroundColor: 'rgba(0, 256, 0, 0)', marginTop: 0, alignSelf: "center" }}>{item.currently.summary}</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                {item.isExpandable ?
                    <View>
                        <View style={{ width: width - 80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf: 'center', flexDirection: 'row', borderWidth: 1, borderColor: 'rgba(256,256,256,0.4)', borderRadius: 5 }}>
                            <TouchableOpacity
                                style={{
                                    width: (width - 80) / 3,
                                    paddingVertical: 10,
                                    backgroundColor: item.subNav === "Current" ? subNavColors.active : subNavColors.inactive,
                                    borderWidth: item.subNav === "Current" ? 0.5 : 0,
                                    borderBottomLeftRadius: 5, borderTopLeftRadius: 5,
                                    borderColor: 'rgba(256,256,256,0.4)', alignItems: 'center', justifyContent: 'center'
                                }}
                                onPress={() => this.selectSubNav("Current", item, index)}
                            >
                                <Text style={{ color: '#fff', fontWeight: '200', fontSize: 14 }}>CURRENT</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{
                                    width: (width - 80) / 3,
                                    backgroundColor: item.subNav === "Hourly" ? subNavColors.active : subNavColors.inactive,
                                    borderWidth: item.subNav === "Hourly" ? 0.5 : 0,
                                    borderColor: 'rgba(256,256,256,0.4)', alignItems: 'center', justifyContent: 'center'
                                }}
                                onPress={() => this.selectSubNav("Hourly", item, index)}  >
                                <Text style={{ color: '#fff', fontWeight: '200', fontSize: 14 }}>HOURLY</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{
                                    width: (width - 80) / 3,
                                    borderBottomRightRadius: 5,
                                    borderTopRightRadius: 5,
                                    backgroundColor: item.subNav === "Extended" ? subNavColors.active : subNavColors.inactive,
                                    borderWidth: item.subNav === "Extended" ? 0.5 : 0,
                                    borderColor: 'rgba(256,256,256,0.4)', alignItems: 'center', justifyContent: 'center'
                                }}
                                onPress={() => this.selectSubNav("Extended", item, index)}  >
                                <Text style={{ color: '#fff', fontWeight: '200', fontSize: 14 }}>EXTENDED</Text>
                            </TouchableOpacity>
                        </View>
                        {item.subNav === "Current" ? this.currentView(item, index) :
                            item.subNav === "Hourly" ?
                                <View style={{ marginVertical: 10 }}>
                                    <FlatList
                                        data={item.hourly.data}
                                        renderItem={({ item, subIndex }) => this.hourlyView(item, subIndex, index)}
                                        keyExtractor={(item, subIndex) => subIndex.toString()}
                                        extraData={this.state}
                                        horizontal={true}
                                        contentContainerStyle={{ marginTop: 10 }}
                                    
                                    />
                                </View>
                                :
                                item.subNav === "Extended" ?
                                    <View style={{ marginVertical: 10 }}>
                                        <FlatList
                                            data={item.daily.data}
                                            renderItem={({ item, subIndex }) => this.extendedView(item, subIndex, index)}
                                            keyExtractor={(item, index) => index.toString()}
                                            extraData={this.state}
                                            horizontal={true}
                                            contentContainerStyle={{ marginTop: 10 }}
                                       
                                        />
                                    </View>
                                    :
                                    null}
                    </View>
                    : null
                }
                
            </View>
        )
    }

    setScrollEnabled(enable) {
        this.setState({
            enable,
        });
    }

    renderItem(item, index) {
        console.log(index, ' renderItem ', item)
        return (
            <ListItem
                item={item}
                index={index}
                editItem={this.editItem}
                deleteItem={this.deleteItem}
                screen = {"Weather"}
                renderData={this.renderForecastData.bind(this)}
                setScrollEnabled={enable => this.setScrollEnabled(enable)}
            />
        );
    }

    render() {

        const { forecast } = this.state
        
        if (this.state.listloading) {
            return (
                <View>
                    <Banner />
                    <Image source={loadingspinner} style={{ alignSelf: 'center', height: 200, width: 200, marginTop: 10 }} />
                </View>
            )
        }

        else if (this.state.locationRetry) {
            return (
                <View style={{ width: width, height: height, backgroundColor: '#fff', alignItems: 'center' }}>
                    <Banner />
                    <Text style={{ marginTop: 70, marginLeft: 20, marginRight: 20, marginBottom: 60, textAlign: 'center', color: theme.brandPrimary }}>We were unable to get your current location. If you would like to use the weather features, please ensure location services are tuned on from your settings menu and retry.</Text>
                    <TouchableOpacity>
                        <View style={{ alignSelf: 'center', backgroundColor: 'transparent' }}>
                            <MaterialIcons name="autorenew" size={100} color="#000" onPress={() => this._onRefresh()} />
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
        else
            return (
                <View>
                    <Banner />
                    <View style={{ marginTop: 10 }} >

                        {this.state.currentLocationData == null || this.state.currentLocationData.length == 0 ? null : this.renderForecastData(this.state.currentLocationData, 0)}

                        <FlatList
                            contentContainerStyle={{ marginBottom: 40 }}
                            data={forecast}
                            renderItem={({ item, index }) => index === 0 ? null : this.renderItem(item, index)}
                            scrollEnabled={this.state.enable}
                            keyExtractor={(item, index) => index.toString()}
                            extraData={this.state}
                        />
                       
                    </View>

                </View>
            );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }, row: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 7,
    }
});

const vertical = {
    start: { x: 0.5, y: 0 },
    end: { x: 0.5, y: 1 }
};

const navColors = {
    active:
        '#4f93e2'
    ,
    inactive:
        '#77787a'

};

const direction = {

    vertical: '{start: {x: 0, y: 0.4}, end: {x: 1, y: 0.6}}',
    slant: '{start: {x: 1, y: 0}, end: {x: 0, y: 1}}'
};



const myWeather = {
    weatherClearDay: [
        '#2e61c1', '#7ac5ee'
    ],
    weatherPartlyCloudyDay: [
        '#678ccf', '#8fc8e6'
    ],
    weatherCloudy: [
        '#5770a1', '#90c1dc'
    ],
    weatherClearNight: [
        '#211e57', '#5d59be'
    ],
    weatherPartlyCloudyNight: [
        '#112158', '#443f82', '#9762a2'
    ],
    weatherWind: [
        '#2c74d5', '#82aee9'
    ],
    weatherFog: [
        '#a79bbe', '#cfbed5'
    ],
    weatherRain: [
        '#2b5685', '#6bb2d5'
    ],
    weatherHail: [
        '#3c5c80', '#a1b8d1'
    ],
    weatherSleet: [
        '#67738d', '#aeb5c3'
    ],
    weatherSnow: [
        '#7d9cbc', '#b6c9dc'
    ],
    weatherThunderstorm: [
        '#3d426c', '#b19ab4'
    ],
    weatherTornado: [
        '#21324f', '#b5a8a3'
    ]

};

const mapStateToProps = (state) => ({
    userData: state.user.userDetails,
    getUnitData: state.user.getUserIdState,
    location: state.location,
    wUnits: state.user.wUnits,
})

const mapDispatchToProps = dispatch => ({
    WeatherActions: bindActionCreators(WeatherActions, dispatch),
    UserActions: bindActionCreators(UserActions, dispatch),
    userLocation: bindActionCreators(userLocation, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Weather);
