import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from 'react-navigation';
import Home from './Home';
import Settings from './Settings'

export default createBottomTabNavigator(
  {
    Home: Home,
    Settings: Settings,
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let baseball;
        if (routeName === 'Home') {
          baseball = `ios-information-circle${focused ? '' : '-outline'}`;
        } else if (routeName === 'Settings') {
          baseball = `ios-options${focused ? '' : '-outline'}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={baseball} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
);