/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  NativeEventEmitter,
  NativeModules,
  AppState,
  Animated,
  Easing,
  View, Dimensions, Platform, Image, FlatList, Modal, Alert, Linking
} from 'react-native';
import { Container, Header, Content, Tabs, Tab, TabHeading, Card, CardItem, cardBody, Badge, Body, List, ActionSheet, Spinner, InputGroup, Input } from 'native-base';
import ListItem from './ListItem';
import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/Ionicons';
import GoogleStaticMap from 'react-native-google-static-map';
import { splitlatlon, playSound } from '../../functions/umbrellafuncs';
import RNPbWrapper from 'react-native-pb-wrapper';
import DeviceInfo from 'react-native-device-info';
import Settings from '../../stores/settingsStore';
import theme from '../../theme/base-theme';
import _ from 'lodash';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';
import Banner from './banner';


const settings = new Settings();
var moment = require('moment-timezone');
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const loadingspinner = require('../../img/loading.gif')
const trackingclose = require('../../img/trackingclose.png')
const umbrellaclose = require('../../img/trackingcloseumbrella.png')
const umbrellaopen = require('../../img/trackingumbrella.png')
const trackingdots = require('../../img/trackingdots.png')
const trackingsound1 = require('../../img/trackingsound1.png')
const trackingsound2 = require('../../img/trackingsound2.png')
const trackingsound3 = require('../../img/trackingsound3.png')
const trackinglocation = require('../../img/trackinglocation.png')
const umbrellabanner = require('../../img/umbrellabanner.png')

const umbrella_icon_blue = require('../../img/umbrella_icon_blue.png')
const bag_icon_blue = require('../../img/bag_icon_blue.png')
const car_icon_blue = require('../../img/car_icon_blue.png')
const key_icon_blue = require('../../img/key_icon_blue.png')
const other_icon_blue = require('../../img/other_icon_blue.png')
const pet_icon_blue = require('../../img/pet_icon_blue.png')

const umbrella_icon_white = require('../../img/umbrella_icon_white.png')
const bag_icon_white = require('../../img/bag_icon_white.png')
const car_icon_white = require('../../img/car_icon_white.png')
const key_icon_white = require('../../img/key_icon_white.png')
const other_icon_white = require('../../img/other_icon_white.png')
const pet_icon_white = require('../../img/pet_icon_white.png')

const down_arrow = require('../../img/down_arrow.png')
const full_battery = require('../../img/full_battery_icon.png')
const half_battery = require('../../img/half_battery_icon.png')
const low_battery = require('../../img/low_battery_icon.png')

var BUTTONS = ["Edit Name", "Stop Tracking", "Delete", "Cancel"];
var DESTRUCTIVE_INDEX = 2;
var CANCEL_INDEX = 3;

// GetMyID


//Pull all tracked devices

//Connect New Devices (this will be from the home screen add button)

//Stop Tracking

//Start Tracking

//Remove Devices



class Tracking extends Component {
  constructor(props) {
    super(props)
    this.spinValue = new Animated.Value(0)
    this.state = {
      title: "",
      myTrackinID: '',
      connectedUmbrellas: [],
      newUmbrellaName: '',
      nameIDToUpdate: '',
      iconToUpdate: '',
      modalVisible: false,
      isIconsVisible: false,
      trackingLoading: true,
      nodevices: true,
      trackingVal: props.trackingData,
      enable: true,
      loading: false
    }
  }


  componentWillMount() {
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.baseUrl = settings.BaseUrl;

    console.log('this.state.uniqueId ', this.state.uniqueId)
    this.getMyTrackingId();
  }

  componentDidMount() {
    this.setListeners()
  }

  setListeners() {
    // const mNativeEventEmitter = new NativeEventEmitter(NativeModules.RNPbWrapper)
    // let trackingArr = [];
    // console.log('tracking RNPbWrapper ', RNPbWrapper)
    // mNativeEventEmitter.addListener(RNPbWrapper.ON_RSS_SIGNAL_CHANGED, (data) => {
    //     console.log('tracking ON_RSS_SIGNAL_CHANGED ****** ', data)
    // });


    // mNativeEventEmitter.addListener(RNPbWrapper.ON_DEVICE_ADDED, (data) => {
    //     console.log('tracking inside ON_DEVICE_ADDED ******** ', data)
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STARTED, (data) => {
    //     console.log('tracking ON_SCANNING_STARTED');
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_SUCCESS, (data) => {
    //     console.log('tracking ON_SCANNING_SUCCESS');
    //     // this.setState({ isScanning: false }, () => {
    //     //     this.scanAction();
    //     // });
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_FAILED, (data) => {
    //     console.log('tracking ON_SCANNING_FAILED');
    //     // this.setState({ isScanning: false });
    //     // alert(RNPbWrapper.ON_SCANNING_FAILED);
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_ON, (data) => {
    //     console.log('tracking ON_BLUETOOTH_ON');
    //     //Alert.alert('ON_BLUETOOTH_ON', RNPbWrapper.ON_BLUETOOTH_ON);
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_OFF, (data) => {
    //     console.log('tracking ON_BLUETOOTH_OFF');
    //     alert(RNPbWrapper.ON_BLUETOOTH_OFF);
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STOPPED, (data) => {
    //     console.log('tracking ON_SCANNING_STOPPED');
    //     this.setState({ isScanning: false });
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_OUT_OF_RANGE, (data) => {
    //     console.log(" tracking data aout ssasasa")
    //     //Alert.alert('Home umbrella Out of range handle here.', data)
    // });

    // mNativeEventEmitter.addListener(RNPbWrapper.ON_DEVICE_DISCONNECT, (data) => {
    //     console.log('tracking device disconnected ************ ', data)

    // });
  }

  rotateIcon = () => {
    Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 150,
        easing: Easing.linear
      }
    ).start()
  }

  rotateBackIcon = () => {
    Animated.timing(
      this.spinValue,
      {
        toValue: 0,
        duration: 150,
        easing: Easing.linear
      }
    ).start()
  }

  showIconDialog() {
    this.setState({ isIconsVisible: !this.state.isIconsVisible }, () => { this.state.isIconsVisible ? this.rotateIcon() : this.rotateBackIcon() })

  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  closeModal() {
    this.setState({ modalVisible: false });
  }

  actions(id, tracking, inrange, trackerid, toggle) {
    if (tracking == true && inrange == true) {
      playSound(trackerid)

    }
    if (tracking == true && inrange == false) {
      //Extend Height and Show Location
      this.updateToggle(id, toggle);
    }
    if (tracking == false) {
      //Toggle Tracking
      this.updateTracking(id, tracking)
    }
  }

  options(sheetIndex, id, tracking, trackingname) {
    if (sheetIndex == 0) {
      this.updateName(id);
    }
    if (sheetIndex == 1) {
      this.updateTracking(id, tracking);
    }
    if (sheetIndex == 2) {
      Alert.alert(
        'Remove',
        'Are you sure you want to unpair and remove "' + trackingname + '" ? ',
        [
          { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'Yes', onPress: () => this.removeTracking(id) },
        ],
        { cancelable: false }
      )

    }

  }

  showRemoveDialog(id, trackingname) {
    Alert.alert(
      'Remove',
      'Are you sure you want to unpair and remove "' + trackingname + '" ? ',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.removeTracking(id) },
      ],
      { cancelable: false }
    )
  }

  updateName(item) {
    this.setState({ nameIDToUpdate: item.id, iconToUpdate: item.wicon });
    this.openModal();
  }

  updateTracking(id, tracking) {
    if (tracking == false) {
      this.patchTracking(id, true);
    }
    if (tracking == true) {
      Alert.alert(
        'Stop tracking',
        'This will stop monitoring/notifications for this umbrella',
        [
          { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'Yes', onPress: () => this.patchTracking(id, false) },
        ],
        { cancelable: false }
      )
    }
  }

  updateToggle(id, toggle) {
    if (toggle == false) {
      this.patchToggle(id, true);
    }
    if (toggle == true) {
      this.patchToggle(id, false);

    }
  }

  async getIconsIdByName(name) {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_producticon?filter[where][wicon]=' + name + '&filter[where][wactive]=true', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();

      if (response != null) {
        let parsed = JSON.parse(res)
        return parsed[0].id
      } else {
        return 1
      }

    } catch (error) {
      this.setState({ errors: error, trackingLoading: false, loading: false });
    }
  }

  async getIconsId(id) {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_producticon?filter[where][id]=' + id + '&filter[where][wactive]=true', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();

      if (response != null) {
        let parsed = JSON.parse(res)
        return parsed[0].wicon
      } else {
        return "Umbrella"
      }

    } catch (error) {
      this.setState({ errors: error, trackingLoading: false });
    }
  }

  async getMyTrackingId() {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_tracking?filter[where][wdeviceid]=' + this.state.uniqueId + '&filter[where][wactive]=true', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();

      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        if (parsed.length == 0) {
          this.setState({ nodevices: true, trackingLoading: false });

        } else {

          let DateArray = [];
          let { trackingVal } = this.state;

          if (this.props.trackingData.length > 0) {
            _.each(parsed, async (data) => {

              RNPbWrapper.connectWithDevice(data.wtrackerid, (macAddress) => { console.log(macAddress) });

              _.each(this.props.trackingData, (element) => {
                if (element.macAddress != undefined) {
                  if (data.wtrackerid == element.macAddress) {
                    this.patchRange(data.id, true);
                    this.getUpdatedUmbrellaInfo(data.id);

                  }
                }

              })

              let wicon = await this.getIconsId(data.wprodicon);
              data.wicon = wicon
              _.uniq(DateArray.push(data));
            })
          } else {
            _.each(parsed, async (data) => {
              let wicon = await this.getIconsId(data.wprodicon)
              data.wicon = wicon
              _.uniq(DateArray.push(data));
              // RNPbWrapper.connectWithDevice(data.wtrackerid, (macAddress) => { console.log("macAddress ++++ " + macAddress) });
              this.patchRange(data.id, false);
            })
          }

          this.setState({ nodevices: false, trackingLoading: false, connectedUmbrellas: DateArray }, () => {
            console.log('Tracking connectedUmbrellas: ', this.state.connectedUmbrellas);
          });
        }
      } else {
        let errors = res;
        throw errors;
      }
    } catch (error) {
      this.setState({ errors: error, trackingLoading: false });
    }
  }


  async patchTracking(idToUpdate, toggle) {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_tracking/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wtracking: toggle,
        })
      });

      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        this.setState({ trackingLoading: true });
        this.getMyTrackingId();
      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {
      console.log(errors);
    }
  }

  patchRange(idToUpdate, toggle) {
    this.props.UserActions.patchRangeFunc({ idToUpdate: idToUpdate, winrange: toggle });
    this.refreshTargets();
  }

  async patchToggle(idToUpdate, toggle) {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_tracking/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wtoggle: toggle,
        })
      });

      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        this.setState({ trackingLoading: true });
        this.getMyTrackingId();
      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {
      console.log(errors);
    }
  }

  async patchInRange(latlon, address, idToUpdate) {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_tracking/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wlatlon: latlon,
          waddress: address,
          wlastconnected: Date.now(),
        })
      });

      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        console.log("Updated InRage Umbrella")
      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {
      console.log(errors);
    }
  }

  async patchName() {
    if (!this.state.loading) {
      if (this.state.newUmbrellaName != undefined && this.state.newUmbrellaName.length == 0) {
        alert(" Name Field cannot be empty ")
        return
      }

      this.setState({ loading: true })
      let icon_id = await this.getIconsIdByName(this.state.iconToUpdate)
      try {
        let response = await fetch(this.state.baseUrl + 'wn_tracking/' + this.state.nameIDToUpdate, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({
            wtrackername: this.state.newUmbrellaName,
            wprodicon: icon_id

          })
        });

        let res = await response.text();
        if (response.status >= 200 && response.status < 300) {
          this.closeModal();
          this.setState({ trackingLoading: true, loading: false });
          this.getMyTrackingId();
        } else {
          let errors = res;
          throw errors;
        }
      } catch (errors) {
        this.setState({ loading: false })
        if (errors != null) {
          let parsed = JSON.parse(errors)
          if (parsed != null && parsed.error != null && parsed.error.code != null) {
            alert(parsed.error.code + "\n" + parsed.error.message)
          } else {
            alert(" Error while fetching data ")
          }

        } else {
          alert(" Error while fetching data ")
        }

        console.log("Tracking ", errors);
      }
    }
  }

  async removeTracking(idToUpdate) {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_tracking/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wactive: false,
        })
      });

      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        this.setState({ trackingLoading: true });
        this.getMyTrackingId();
      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {
      console.log(errors);
    }
  }

  async getAddress(latlon, id) {
    try {
      let response = await fetch(settings.geourl + 'latlng=' + latlon + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        this.patchInRange(latlon, parsed.results[0].formatted_address, id);


      } else {
        let errors = res;
        throw errors;
      }

    } catch (error) {
      this.setState({ errors: error });
      console.log("error: " + error);
    }
  }

  refreshTargets() {
    this.setState({ trackingLoading: false });
    this.forceUpdate();
  }

  getUpdatedUmbrellaInfo(id) {

    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.getAddress(position.coords.latitude + ',' + position.coords.longitude, id);
      },
      (error) => this.setState({ locationRetry: true }),
      { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
    );

  }

  setScrollEnabled(enable) {
    this.setState({
      enable,
    });
  }

  renderItem(item, index) {

    return (
      <ListItem
        item={item}
        index={index}
        editItem={this.updateName.bind(this)}
        updateTracking={this.updateTracking.bind(this)}
        deleteItem={this.showRemoveDialog.bind(this)}
        screen={"Tracking"}
        renderData={this.renderList.bind(this)}
        setScrollEnabled={enable => this.setScrollEnabled(enable)}
      />
    );
  }

  peblebeeTrackerUi(item) {

    return (
      <View style={{
        zIndex: 1, height: (item.wtracking == true && item.winrange == false && item.wtoggle == true ? 150 + width * 0.80 : 150),
        width: width * 0.92, marginTop: 5, marginBottom: 10, borderRadius: 5, overflow: 'hidden', alignSelf: 'center', flexDirection: 'column'
      }}>

        {item.wtracking == true && item.winrange == true ? <AnimatedLinearGradient customColors={myTracking.inRange} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
        {item.wtracking == true && item.winrange == false ? <AnimatedLinearGradient customColors={myTracking.outOfRange} speed={10000} points={direction.vertical} /> : null}
        {item.wtracking == false ? <AnimatedLinearGradient customColors={myTracking.untracked} speed={10000} points={direction.vertical} /> : null}

        <View style={{ height: 40, width: width * 0.92, backgroundColor: 'rgba(0, 0, 0, 0)', flexDirection: 'row', marginBottom: 0, marginTop: 10 }}>
          <View style={{ width: width * 0.03, height: 40, backgroundColor: 'rgba(0, 256, 0, 0)' }} />
          <View style={{ width: width * 0.083, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 2 }}>
            {item.wicon == 'Umbrella' ? <Image source={umbrella_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Keys' ? <Image source={key_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Bag' ? <Image source={bag_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Car' ? <Image source={car_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Pet' ? <Image source={pet_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Other' ? <Image source={other_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
          </View>
          <View style={{ width: width * 0.03, height: 40, backgroundColor: 'rgba(0, 256, 0, 0)' }} />
          <View style={{ width: width * 0.65, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 0 }}>
            <Text numberOfLines={1} style={{
              fontSize: 28, fontWeight: (Platform.OS === 'ios') ? '400' : '500',
              fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)'
            }}>{item.wtrackername}</Text>
          </View>

        </View>
        <View style={{ height: 40, width: width * 0.92, backgroundColor: 'rgba(256, 0, 0, 0)', flexDirection: 'row', marginBottom: 0, marginTop: 5 }}>
          <View style={{ width: width * 0.03, height: 40, backgroundColor: 'rgba(0, 256, 0, 0)' }} />
          <View style={{ width: width * 0.075, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 2 }} />
          <View style={{ width: width * 0.03, height: 40, backgroundColor: 'rgba(0, 256, 0, 0)' }} />
          <View style={{ width: width * 0.785, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 0 }}>
            {item.wtracking == true && item.winrange == true ? <Text numberOfLines={1} style={{ fontSize: 16, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>Within range</Text> : null}
            {item.wtracking == true && item.winrange == false ? <Text numberOfLines={1} style={{ fontSize: 16, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>Last connected: {moment(item.wlastconnected).fromNow()}</Text> : null}
            {item.wtracking == false ? <Text numberOfLines={1} style={{ fontSize: 16, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>Untracked</Text> : null}
          </View>
        </View>
        <TouchableOpacity onPress={() => this.actions(item.id, item.wtracking, item.winrange, item.wtrackerid, item.wtoggle)}>
          {item.wtracking == true && item.winrange == false && item.wtoggle == true ? <Image style={{ width: width * 0.80, height: width * 0.80, alignSelf: 'center' }} source={{ uri: 'https://maps.googleapis.com/maps/api/staticmap?center=' + splitlatlon(item.wlatlon, 1) + ',' + splitlatlon(item.wlatlon, 2) + '&zoom=15&scale=false&size=' + Math.floor(width * 0.80) + 'x' + Math.floor(width * 0.80) + '&maptype=roadmap&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg&format=png&visual_refresh=true&markers=icon:http://weathermanlabs.com/app/umbrellaClose.png%7Cshadow:true%7C' + splitlatlon(item.wlatlon, 1) + ',' + splitlatlon(item.wlatlon, 2) }} /> : null}
          <View style={{ height: 40, width: width * 0.92, backgroundColor: 'rgba(0, 0, 0, 0)', flexDirection: 'row', marginBottom: 0, marginTop: 5, borderTopWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)' }}>
            <View style={{ width: width * 0.03, height: 40, backgroundColor: 'rgba(0, 256, 0, 0)' }} />
            <View style={{ width: width * 0.083, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 7 }}>
              {item.wtracking == true && item.winrange == false ? <Image source={trackinglocation} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : <Image source={trackingsound3} style={{ height: 30, width: 30 }} resizeMode={'contain'} />}
            </View>
            <View style={{ width: width * 0.03, height: 40, backgroundColor: 'rgba(0, 256, 0, 0)' }} />
            <View style={{ width: width * 0.65, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12 }}>
              {item.wtracking == true && item.winrange == true ? <Text numberOfLines={1} style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>PLAY SOUND</Text> : null}
              {item.wtracking == true && item.winrange == false ? <Text numberOfLines={1} style={{ fontSize: 10, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 5 }}>{item.waddress}</Text> : null}
              {item.wtracking == false ? <Text numberOfLines={1} style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>START TRACKING</Text> : null}
            </View>
            <View style={{ width: width * 0.1, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop: 7 }}>
              {item.wtracking == true && item.winrange == false ? <Image source={trackingclose} style={{ height: 30, width: 30, transform: [{ rotate: (item.wtoggle) ? '0deg' : '180deg' }] }} /> : null}
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  newTrackerDesign(item) {

    return (
      <View style={{
        zIndex: 1, height: (item.wtracking == true && item.winrange == false && item.wtoggle == true ? 150 + width * 0.80 : 150), padding: 10,
        width: width * 0.92, marginTop: 5, marginBottom: 10, borderRadius: 15, overflow: 'hidden', alignSelf: 'center', flexDirection: 'column'
      }}>

        {item.wtracking == true && item.winrange == true ? <AnimatedLinearGradient customColors={myTracking.inRange} speed={10000} points={direction.vertical} style={{ borderRadius: 5 }} /> : null}
        {item.wtracking == true && item.winrange == false ? <AnimatedLinearGradient customColors={myTracking.outOfRange} speed={10000} points={direction.vertical} /> : null}
        {item.wtracking == false ? <AnimatedLinearGradient customColors={myTracking.untracked} speed={10000} points={direction.vertical} /> : null}

        <View style={{ height: 70, width: width * 0.8, backgroundColor: 'rgba(0, 0, 0, 0)', marginBottom: 0, marginTop: 5, flexDirection: 'row' }}>

          <View style={{ width: width * 0.1, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 2 }}>
            {item.wicon == 'Umbrella' ? <Image source={umbrella_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Keys' ? <Image source={key_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Bag' ? <Image source={bag_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Car' ? <Image source={car_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Pet' ? <Image source={pet_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
            {item.wicon == 'Other' ? <Image source={other_icon_white} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : null}
          </View>

          <View style={{ width: width * 0.6, backgroundColor: 'rgba(0, 0, 0, 0)', marginLeft: 5, marginTop: 5 }}>
            <Text numberOfLines={1} style={{
              width: width * 0.6, fontSize: 25, fontWeight: (Platform.OS === 'ios') ? '400' : '500',
              fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)'
            }}>{item.wtrackername}</Text>
            <View style={{ width: width * 0.6, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 7, marginLeft: 5 }}>
              {item.wtracking == true && item.winrange == true ? <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>Within range</Text> : null}
              {item.wtracking == true && item.winrange == false ? <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>Last connected: {moment(item.wlastconnected).fromNow()}</Text> : null}
              {item.wtracking == false ? <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>Untracked</Text> : null}
            </View>
          </View>

          <View style={{ width: width * 0.2, height: 25, backgroundColor: 'rgba(0, 0, 0, 0)', alignItems: 'center', marginTop: 10, flexDirection: 'row' }}>
            <Text style={{ fontSize: 10, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', marginTop: 4, marginRight: 2 }}>65%</Text>
            <Image source={full_battery} style={{ justifyContent: 'flex-end', height: 30, width: 30 }} />
          </View>

        </View>

        {item.wtracking == true && item.winrange == false && item.wtoggle == true ?
          <Image style={{ width: width * 0.80, height: width * 0.80, alignSelf: 'center', marginBottom: 5 }}
            source={{
              uri: 'https://maps.googleapis.com/maps/api/staticmap?center=' + splitlatlon(item.wlatlon, 1) + ',' +
                splitlatlon(item.wlatlon, 2) + '&zoom=15&scale=false&size=' + Math.floor(width * 0.80) + 'x' + Math.floor(width * 0.80) +
                '&maptype=roadmap&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg&format=png&visual_refresh=true&markers=icon:http://weathermanlabs.com/app/umbrellaClose.png%7Cshadow:true%7C' +
                splitlatlon(item.wlatlon, 1) + ',' + splitlatlon(item.wlatlon, 2)
            }} /> : null}

        <TouchableOpacity style={{
          height: 50, width: width * 0.8, backgroundColor: 'rgba(0, 0, 0, 0)', flexDirection: 'row', marginBottom: 0,
          marginTop: 5, borderTopWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', alignSelf: 'center', justifyContent: 'center',
          alignItems: 'center', paddingTop: 5, paddingBottom: 5
        }}
          onPress={() => this.actions(item.id, item.wtracking, item.winrange, item.wtrackerid, item.wtoggle)} >
          <View style={{ width: width * 0.1, height: 50, backgroundColor: 'rgba(0, 0, 0, 0)', alignItems: 'center', justifyContent: 'center' }}>
            {item.wtracking == true && item.winrange == false ? <Image source={trackinglocation} style={{ height: 30, width: 30 }} resizeMode={'contain'} /> : <Image source={trackingsound3} style={{ height: 30, width: 30 }} resizeMode={'contain'} />}
          </View>
          <View style={{ width: width * 0.55, height: 50, backgroundColor: 'rgba(0, 0, 0, 0)', justifyContent: 'center', alignItems: 'center' }}>
            {item.wtracking == true && item.winrange == true ? <Text numberOfLines={1} style={{ fontSize: 20, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>PLAY SOUND</Text> : null}
            {item.wtracking == true && item.winrange == false ? <Text numberOfLines={2} style={{ fontSize: 12, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 5 }}>{item.waddress}</Text> : null}
            {item.wtracking == false ? <Text numberOfLines={1} style={{ fontSize: 20, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, color: '#fff', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}>START TRACKING</Text> : null}
          </View>
          {item.wtracking == true && item.winrange == false ? <View style={{ width: 1, height: 35, backgroundColor: 'rgba(256, 256, 256, 0.5)' }} /> : null}
          <View style={{ width: width * 0.15, height: 50, backgroundColor: 'rgba(0, 0, 0, 0)', justifyContent: 'center', alignItems: 'center' }} >
            {item.wtracking == true && item.winrange == false ? <Image source={trackingclose} style={{ height: 28, width: 28, transform: [{ rotate: (item.wtoggle) ? '0deg' : '180deg' }] }} /> : null}
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  renderList(item, index) {

    if (item.wproductid == 1) {
      return this.peblebeeTrackerUi(item)
    } else if (item.wproductid == 2) {
      return this.newTrackerDesign(item)
    } else {
      return this.peblebeeTrackerUi(item)
    }

  }

  render() {

    const modalHeight = this.state.isIconsVisible ? height * 0.25 : height * 0.20;

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '-180deg']
    })

    if (this.state.trackingLoading) {
      return (
        <View>
          <Banner />
          <Image source={loadingspinner} style={{ alignSelf: 'center', height: 200, width: 200, marginTop: 10 }} />
        </View>
      )
    }

    else if (this.state.nodevices) {
      return (
        <View>
          <Banner />
          <Image source={umbrellabanner} resizeMode='contain' style={{ alignSelf: 'center', bottom: 200, width: width * 0.90 }} />
        </View>
      )
    }


    else
      return (

        <ScrollView scrollEnabled={true} style={{ flex: 1, width: width }}>
          <Banner />
          {this.state.connectedUmbrellas.length > 0 ?
            <FlatList
              contentContainerStyle={{ marginBottom: 40 }}
              data={this.state.connectedUmbrellas}
              renderItem={({ item, index }) => this.renderItem(item, index)}
              scrollEnabled={this.state.enable}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
            />
            : null
          }
          <Modal
            visible={this.state.modalVisible}
            animationType={'slide'}
            transparent={true}
            onRequestClose={() => this.closeModal()}>
            <View style={{ width: width * 0.82, height: modalHeight, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 60, borderWidth: 1, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomWidth: 0, alignSelf: 'center', marginTop: height * 0.35, backgroundColor: theme.brandWhite }}>

              {this.state.loading ? <Spinner style={{ position: 'absolute', alignSelf: 'center' }} color='#003057' /> :
                <ScrollView scrollEnabled={false} keyboardShouldPersistTaps="always" style={{ backgroundColor: 'rgba(256, 256, 256, 0)', alignSelf: 'center' }}>

                  <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ justifyContent: 'flex-end', marginRight: 2, padding: 5, paddingRight: 18 }}>

                      {this.state.iconToUpdate == 'Umbrella' ? <Image source={umbrella_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                      {this.state.iconToUpdate == 'Keys' ? <Image source={key_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                      {this.state.iconToUpdate == 'Bag' ? <Image source={bag_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                      {this.state.iconToUpdate == 'Car' ? <Image source={car_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                      {this.state.iconToUpdate == 'Pet' ? <Image source={pet_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                      {this.state.iconToUpdate == 'Other' ? <Image source={other_icon_blue} style={{ height: 45, width: 45 }} resizeMode={'contain'} /> : null}
                      <TouchableOpacity style={{ position: 'absolute', alignSelf: 'flex-end' }} onPress={() => this.showIconDialog()}>
                        <Animated.Image style={{ height: 25, width: 25, transform: [{ rotate: spin }] }} source={down_arrow} />
                      </TouchableOpacity>

                    </View>
                    <View style={{ margin: 5, width: "70%" }}>
                      <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 13.5, marginLeft: 2 }}>Edit Tracker Name:</Text>
                      <InputGroup style={{ marginBottom: 10, width: width * 0.55, backgroundColor: "rgba(38, 38, 38, 0.0)", borderBottomWidth: 1, borderColor: theme.brandPrimary }} boarderType='round'>
                        <Input style={{ color: '#000' }}
                          maxLength={20}
                          value={this.state.newUmbrellaName}
                          placeholder='New Name'
                          placeholderTextColor={theme.brandTertiary}
                          onChangeText={(val) => { this.setState({ newUmbrellaName: val }) }} />
                      </InputGroup>
                    </View>

                  </View>

                  {this.state.isIconsVisible ?
                    <View style={{ width: "96%", flexDirection: 'row', alignSelf: 'center', justifyContent: 'center' }}>
                      <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Umbrella' })}>
                        {this.state.iconToUpdate == 'Umbrella' ?
                          <Image source={umbrella_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                          :
                          <Image source={umbrella_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}

                      </TouchableOpacity>
                      <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Keys' })}>
                        {this.state.iconToUpdate == 'Keys' ?
                          <Image source={key_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                          :
                          <Image source={key_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                      </TouchableOpacity>
                      <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Bag' })}>
                        {this.state.iconToUpdate == 'Bag' ?
                          <Image source={bag_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                          :
                          <Image source={bag_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                      </TouchableOpacity>
                      <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Car' })}>
                        {this.state.iconToUpdate == 'Car' ?
                          <Image source={car_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                          :
                          <Image source={car_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                      </TouchableOpacity>
                      <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Pet' })}>
                        {this.state.iconToUpdate == 'Pet' ?
                          <Image source={pet_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                          :
                          <Image source={pet_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                      </TouchableOpacity>
                      <TouchableOpacity style={{ width: "16%" }} onPress={() => this.setState({ iconToUpdate: 'Other' })}>
                        {this.state.iconToUpdate == 'Other' ?
                          <Image source={other_icon_white} style={{ height: 40, width: 40, backgroundColor: theme.brandPrimary, borderRadius: 10 }} resizeMode={'contain'} />
                          :
                          <Image source={other_icon_blue} style={{ height: 40, width: 40 }} resizeMode={'contain'} />}
                      </TouchableOpacity>
                    </View>
                    : null}
                </ScrollView>
              }

            </View>

            <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, flexDirection: 'row', alignSelf: 'center' }}>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'rgba(256, 256, 256, 1.0)', borderBottomLeftRadius: 10, borderLeftWidth: 1, borderBottomWidth: 1, borderTopWidth: 0, alignItems: 'center', overflow: 'hidden' }} onPress={() => this.closeModal()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'rgba(256, 256, 256, 1.0)', borderBottomRightRadius: 10, borderLeftWidth: 1, borderRightWidth: 1, borderBottomWidth: 1, borderTopWidth: 0, alignItems: 'center' }} onPress={() => this.patchName()}>
                <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Save</Text>
              </TouchableOpacity>

            </View>
          </Modal>
        </ScrollView>


      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }, row: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 7,
  },
  profile_container: {
    width: "100%",
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  },
  imageContainer: {
    borderWidth: 2,
    borderColor: 'grey',
    width: 60,
    height: 60,
    borderRadius: 30,

  },
  cicularImage: {
    position: 'absolute',
    height: 40,
    width: 40,
    borderRadius: 25,
    backgroundColor: 'transparent'
  },
  edit_container: {
    marginTop: -10,
    backgroundColor: 'white',
    borderRadius: 5,
    width: 15,
    height: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  edit_image: {
    resizeMode: "contain",
    width: 18,
    height: 18,
  },
});

const direction = {

  vertical: '{start: {x: 0, y: 0.4}, end: {x: 1, y: 0.6}}',
  slant: '{start: {x: 1, y: 0}, end: {x: 0, y: 1}}'
};


const myTracking = {
  inRange: [
    '#368baf', '#68b2d1'
  ],
  outOfRange: [
    '#1a3153', '#274777'
  ],
  untracked: [
    '#828286', '#9b9b9f'
  ]

};

// const mapStateToProps = (state) =>({

// })

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch),
});

export default connect(null, mapDispatchToProps)(Tracking);
/**

For reconnectivity of devices that are allready paired as soon as user opens the Umberalla tab, RNpbWrapper provide function connectWithDevice(macAddress) which allways returns device not found even if the device is allready On.
Until, this function of RNPbWrapper doesent work, I can not ensure The Reconectivity of devices on Launching app.

The BeepDevice function of RNPbWrapper work only once , when we hit the play sound button. Please , let me know if there is another method to handle Sound functionality. I have already checked the ReadMe file of RNPbWrapper and did not found any alternative.
 */