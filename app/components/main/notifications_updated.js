import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  View, Dimensions, Platform, Image, Modal, Alert, Linking, NativeModules
} from 'react-native';
import { Content, Left, Body, ListItem, Button } from 'native-base';
// import NotificationSetting from 'react-native-open-notification';

import MC from 'react-native-vector-icons/MaterialCommunityIcons'
import FlipToggle from 'react-native-flip-toggle-button';
import DeviceInfo from 'react-native-device-info';
import OneDateTimePicker from 'react-native-modal-datetime-picker';
import TwoDateTimePicker from 'react-native-modal-datetime-picker';
import Settings from '../../stores/settingsStore';
import theme from '../../theme/base-theme';
import { registerAppListener } from "../../messaging/Listeners";
import FCM from "react-native-fcm";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';
import Banner from './banner';
// import moment from "moment"
var moment = require('moment-timezone');
let { DateTime } = require('luxon');


var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const dailyForecast = require('../../img/darkRain.png')
const oorUmbrella = require('../../img/darkUmbrella.png')
const loadingspinner = require('../../img/loading.gif')
const settings = new Settings()


class Notifications extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "",
      myNotifyID: "",
      notificationLoading: false,
      subScreenLoad: true,
      varianType: "",
      modalVisible: false,
      testing: false,
      weatherAlerts: true,
      firstNotification: false,
      secondNotification: false,
      displayDays: "Mo, Tu ,We ,Th ,Fr",
      oorAlerts: false,
      daysSunday: false,
      daysMonday: true,
      daysTuesday: true,
      daysWednesday: true,
      daysThursday: true,
      daysFriday: true,
      daysSaturday: false,
      timeOne: '',
      timeTwo: '',
      savetimeOne: '07:00',
      savetimeTwo: '16:00',
      skipped: false,
      isOneDateTimePickerVisible: false,
      isTwoDateTimePickerVisible: false,
    }
  }

  _showOneDateTimePicker = () => this.setState({ isOneDateTimePickerVisible: true });

  _hideOneDateTimePicker = () => this.setState({ isOneDateTimePickerVisible: false });

  _handleOneDatePicked = (date) => {
    var timeConverted = moment.tz(date, DeviceInfo.getTimezone()).format();
    var timeStamp = moment(timeConverted).format("h:mm a")
    console.log(" timeStamp  " + timeStamp)
    this.setState({ savetimeOne: DateTime.fromISO(timeConverted).setZone('utc').minus({ minutes: 15 }).toLocaleString(DateTime.TIME_24_SIMPLE) });
    this.setState({ timeOne: timeStamp });
    setTimeout(() => { this.patchHours1(this.state.myNotifyID); }, 1000)
    this._hideOneDateTimePicker();
  };

  _showTwoDateTimePicker = () => this.setState({ isTwoDateTimePickerVisible: true });

  _hideTwoDateTimePicker = () => this.setState({ isTwoDateTimePickerVisible: false });


  _handleTwoDatePicked = (date) => {
    var timeConverted = moment.tz(date, DeviceInfo.getTimezone()).format();
    // // console.log(DateTime.fromISO(timeConverted).setZone(DeviceInfo.getTimezone()).toLocaleString(DateTime.TIME_SIMPLE));
    // // console.log(DateTime.fromISO(timeConverted).setZone('utc').minus({ minutes: 15 }).toLocaleString(DateTime.TIME_24_SIMPLE));
    // console.log('DeviceInfo.getTimezone() ', DeviceInfo.getTimezone())
    var timeStamp = moment(timeConverted).format("h:mm a")
    console.log(" timeStamp  " + timeStamp)
    this.setState({ savetimeTwo: DateTime.fromISO(timeConverted).setZone('utc').minus({ minutes: 15 }).toLocaleString(DateTime.TIME_24_SIMPLE) });
    this.setState({ timeTwo: timeStamp });
    setTimeout(() => { this.patchHours2(this.state.myNotifyID); }, 1000)
    this._hideTwoDateTimePicker();
  };

  componentWillMount() {

  }

  componentDidMount() {
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.baseUrl = settings.BaseUrl;
    this.setState({ notificationLoading: true }, () =>
      this.getMyNotifyId()
    )
  }



  async getMyNotifyId() {

    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification?filter[where][wdeviceid]=' + this.state.uniqueId, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      console.log(response, "get notification ========>");

      let res = await response.text();
      this.setState({ notificationLoading: false })
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        // console.log("getMyNotifyId parsedd __++__ " + JSON.stringify(parsed))
        if (parsed.length == 0) {


          // this.setState({ skipped: true, notificationLoading: false });  //chnage uncomment
        }

        else {
          //  

          this.setState({ myNotifyID: parsed[0].id })
          if (parsed[0].wnotifySunday != null) {
            this.setState({ daysSunday: parsed[0].wnotifySunday });
          }
          if (parsed[0].wnotifyMonday != null) {
            this.setState({ daysMonday: parsed[0].wnotifyMonday });
          }
          if (parsed[0].wnotifyTuesday != null) {
            this.setState({ daysTuesday: parsed[0].wnotifyTuesday });
          }
          if (parsed[0].wnotifyWednesday != null) {
            this.setState({ daysWednesday: parsed[0].wnotifyWednesday });
          }
          if (parsed[0].wnotifyThursday != null) {
            this.setState({ daysThursday: parsed[0].wnotifyThursday });
          }
          if (parsed[0].wnotifyFriday != null) {
            this.setState({ daysFriday: parsed[0].wnotifyFriday });
          }
          if (parsed[0].wnotifySaturday != null) {
            this.setState({ daysSaturday: parsed[0].wnotifySaturday });
          }
          if (parsed[0].wnotify1 != null) {
            this.setState({ firstNotification: parsed[0].wnotify1 });
          }
          if (parsed[0].wnotify2 != null) {
            this.setState({ secondNotification: parsed[0].wnotify2 });
          }
          if (parsed[0].wnotifyTime1 != null) {
            this.setState({ timeOne: parsed[0].wnotifyTime1Show });
          }
          if (parsed[0].wnotifyTime2 != null) {
            this.setState({ timeTwo: parsed[0].wnotifyTime2Show });
          }
          if (parsed[0].wnotifyOOR != null) {
            this.setState({ oorAlerts: parsed[0].wnotifyOOR });
          }

          this.dayBuilder();
          this.setState({ notificationLoading: false });
        }
      } else {
        let errors = res;
        throw errors;
      }
    } catch (error) {
      this.setState({ errors: error });

    }
  }

  openModal(variant) {

    //
    this.setState({ varianType: variant });

    this.setState({ modalVisible: true, subScreenLoad: false });
  }

  closeModal() {
    this.setState({ modalVisible: false, subScreenLoad: true });

    this.patchDays(this.state.myNotifyID);
  }

  dayToggler(day) {
    if (day == 'Sunday') {
      if (this.state.daysSunday == true) {
        this.setState({ daysSunday: false });
      }
      if (this.state.daysSunday == false) {
        this.setState({ daysSunday: true });
      }
    }
    else if (day == 'Monday') {
      if (this.state.daysMonday == true) {
        this.setState({ daysMonday: false });
      }
      if (this.state.daysMonday == false) {
        this.setState({ daysMonday: true });
      }
    }
    else if (day == 'Tuesday') {
      if (this.state.daysTuesday == true) {
        this.setState({ daysTuesday: false });
      }
      if (this.state.daysTuesday == false) {
        this.setState({ daysTuesday: true });
      }
    }
    else if (day == 'Wednesday') {
      if (this.state.daysWednesday == true) {
        this.setState({ daysWednesday: false });
      }
      if (this.state.daysWednesday == false) {
        this.setState({ daysWednesday: true });
      }
    }
    else if (day == 'Thursday') {
      if (this.state.daysThursday == true) {
        this.setState({ daysThursday: false });
      }
      if (this.state.daysThursday == false) {
        this.setState({ daysThursday: true });
      }
    }
    else if (day == 'Friday') {
      if (this.state.daysFriday == true) {
        this.setState({ daysFriday: false });
      }
      if (this.state.daysFriday == false) {
        this.setState({ daysFriday: true });
      }
    }
    else if (day == 'Saturday') {
      if (this.state.daysSaturday == true) {
        this.setState({ daysSaturday: false });
      }
      if (this.state.daysSaturday == false) {
        this.setState({ daysSaturday: true });
      }
    }
  }

  dayBuilder() {
    this.state.displayDays = ""
    if (this.state.daysSunday == true) {
      this.state.displayDays = this.state.displayDays + 'Su, ';
      //
    }
    if (this.state.daysMonday == true) {
      this.state.displayDays = this.state.displayDays + 'Mo, ';
    }
    if (this.state.daysTuesday == true) {
      this.state.displayDays = this.state.displayDays + 'Tu, ';
    }
    if (this.state.daysWednesday == true) {
      this.state.displayDays = this.state.displayDays + 'We, ';
    }
    if (this.state.daysThursday == true) {
      this.state.displayDays = this.state.displayDays + 'Th, ';
      //
    }
    if (this.state.daysFriday == true) {
      this.state.displayDays = this.state.displayDays + 'Fr, ';
    }
    if (this.state.daysSaturday == true) {
      this.state.displayDays = this.state.displayDays + 'Sa, ';
    }
    this.state.displayDays = this.state.displayDays.substr(0, this.state.displayDays.length - 1)
    this.closeModal();
  }

  async patchDays(idToUpdate) {

    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({

          wnotifySunday: this.state.daysSunday,
          wnotifyMonday: this.state.daysMonday,
          wnotifyTuesday: this.state.daysTuesday,
          wnotifyWednesday: this.state.daysWednesday,
          wnotifyThursday: this.state.daysThursday,
          wnotifyFriday: this.state.daysFriday,
          wnotifySaturday: this.state.daysSaturday,

        })
      });

      console.log(response, "response of setting days");

      let res = await response.text();

      if (response.status >= 200 && response.status < 300) {
        //
      } else {
        let errors = res;
        throw errors;
      }

    } catch (errors) {

    }
  }

  async patchNotify1(idToUpdate) {

    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wnotify1: this.state.firstNotification,
        })
      });
      console.log(response, "response of setting notify1");
      let res = await response.text();

      if (response.status >= 200 && response.status < 300) {
        // console.log(" parsed value" + JSON.stringify(res))
      } else {
        let errors = res;
        console.log('errors er: ', errors)
        throw errors;
      }
    } catch (errors) {
      console.log('errors: ', errors)
    }
  }

  async patchHours1(idToUpdate) {

    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wnotifyTime1: this.state.savetimeOne,
          wnotifyTime1Show: this.state.timeOne,
        })
      });
      console.log(response, "response of setting hour1");
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {

      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {

    }
  }

  async patchNotify2(idToUpdate) {

    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wnotify2: this.state.secondNotification,
        })
      });
      console.log(response, "response of setting notify2");
      let res = await response.text();
      console.log(" patch notify two " + res)
      if (response.status >= 200 && response.status < 300) {

      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {

    }
  }

  async patchHours2(idToUpdate) {

    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wnotifyTime2: this.state.savetimeTwo,
          wnotifyTime2Show: this.state.timeTwo,
        })
      });
      console.log(response, "response of setting hour2");
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {

      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {

    }
  }

  async patchOOR(idToUpdate) {
    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wnotifyOOR: this.state.oorAlerts,
        })
      });
      console.log(response, "response of setting oor");
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {

      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {

    }
  }


  async  requesNotificationPermission() {
    this.setState({ notificationLoading: true });
    registerAppListener();
    try {
      let result = await FCM.requestPermissions({ badge: false, sound: true, alert: true });
    } catch (e) {

    }

    FCM.getFCMToken().then(token => {
      console.log(token, "get token from notification updated");
      this.setState({ token: token || "" });

      this.addToken();
    });

    if (Platform.OS === 'ios') {
      FCM.getAPNSToken().then(token => {

      });
    }
  }

  addToken() {
    console.log('addToken')
    this.props.UserActions.addNotificationTab({ deviceToken: this.state.token })
    this.setState({ notificationLoading: false, skipped: false });
  }

  _renderTimeofDay(isVisible, time, showDateTimePicker) {
    if (isVisible) {
      return (
        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }} onPress={showDateTimePicker} >
          <Text style={{ color: theme.subTitle, fontWeight: "700" }}>Time of day</Text>
          <Text style={{ textAlign: 'right', color: theme.title }}>{time}</Text>
        </TouchableOpacity>
      )
    }
  }

  async setNotification(type) {
    console.log('setNotification noti ')
    let response = ""
    await FCM.requestPermissions().then(val => {
      return response = ""
    }).catch(err => {
      console.log(" get dssfssponse -- " + err)
      return response = err

    })// for iOS
    console.log(" get adadadddda ++ " + response)
    // this.
    if (response === "") {
      FCM.getFCMToken().then(token => {
        console.log(token, "get notificaion token setNotificain")
        if (token) {
          this.props.UserActions.setDeviceToken(token)
          this.setState({ deviceToken: token })
        }
      });
      console.log("token is there")
      registerAppListener(this.props.navigation);
      this.props.UserActions.addNotification({ deviceToken: this.state.deviceToken })
      type === "firstNotify" ? this.patchNotify1(this.state.myNotifyID) : this.patchNotify2(this.state.myNotifyID)

    } else {
      this.setState({ firstNotification: false, secondNotification: false })
      Alert.alert(
        'Notification',
        "Please allow notifications from your mobile settings.",
        [
          { text: 'OK', onPress: () => this.openSettings(type) },
        ],
      )
    }

  }

  openSettings = (type) => {
    if (Platform.OS === 'ios') {
      Linking.openURL('app-settings:')

    } else {
      Linking.openURL('app-settings:')
      // NativeModules.OpenNotification.open();
      // NotificationSetting.open();

    }
  }

  render() {
    if (this.state.notificationLoading) {
      return (
        <View>
          <Banner />
          <Image source={loadingspinner} style={{ alignSelf: 'center', height: 200, width: 200, marginTop: 10 }} />
        </View>
      )
    }

    else if (this.state.skipped) {
      return (<Content style={{ width: width, height: height }}>
        <Banner />
        <View style={{ width: width * 0.92, height: height, marginTop: 25, alignItems: 'center', alignSelf: 'center' }}>
          <View style={{ width: width * 0.92, alignSelf: 'center' }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary, textAlign: 'center', marginTop: 20 }}>Get custom forecasts and reminders to take your umbrella</Text>
          </View>
          <View style={{ alignItems: 'center', marginTop: 30 }}>
            <Text style={{ fontSize: 16, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary, textAlign: 'center' }}>Would you like to receive notifications? </Text>
            <Button style={{ width: width * 0.5, justifyContent: 'center', backgroundColor: theme.brandPrimary, alignSelf: 'center', borderRadius: 10, marginTop: 20 }}><Text style={{ color: theme.brandWhite }} onPress={() => this.requesNotificationPermission()}>SETUP</Text></Button>
          </View>
        </View>
      </Content>)
    }
    else
      return (
        <ScrollView scrollEnabled={false} style={{ width: width, height: (Platform.OS === 'ios') ? height * 0.9 : height * 1 }}>
          <View>
            <Banner />
            <View style={{ flexDirection: 'row', padding: 15 }}>
              <Image source={dailyForecast} style={{ height: 26, width: 26 }} />
              <View style={{ paddingLeft: 10, marginRight: 5 }}>
                <Text style={{ fontSize: 16, color: theme.title, fontWeight: '600' }}>Personalized weather notifications</Text>
                <Text style={{ fontSize: 14, color: theme.subTitle, marginTop: 3, marginRight: 12 }}>Receive forecast alerts for your favorite locations.</Text>
              </View>
            </View>
            <View style={styles.line_separator} />
            <TouchableOpacity style={{ flexDirection: 'row', padding: 18, justifyContent: 'space-between' }} onPress={() => this.openModal('days')} >
              <Text style={{ color: theme.subTitle, fontWeight: '700' }}>Days</Text>
              <Text style={{ textAlign: 'right', color: theme.title }}>{this.state.displayDays}</Text>
            </TouchableOpacity>
            <View style={styles.line_separator} />

            <View style={{ padding: 18 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ color: theme.title, fontSize: 16 }}>First notification</Text>
                <FlipToggle
                  value={this.state.firstNotification}
                  buttonWidth={45}
                  buttonHeight={17}
                  buttonRadius={50}
                  sliderWidth={25}
                  sliderHeight={25}
                  sliderOffColor={'#ecebf2'}
                  sliderOnColor={theme.sliderOn}
                  buttonOffColor={'#aeaeae'}
                  buttonOnColor={theme.buttonOn}
                  // onToggle={(value) => {
                  //   this.setState({ firstNotification: value },()=> this.patchNotify1(this.state.myNotifyID));

                  // }}
                  onToggle={(value) => {
                    this.setState({ firstNotification: value }, () => {
                      this.state.firstNotification ? this.setNotification("firstNotify") : this.patchNotify1(this.state.myNotifyID)
                    });
                  }}
                />
              </View>
              {this._renderTimeofDay(this.state.firstNotification, this.state.timeOne, this._showOneDateTimePicker)}
              {/* <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}
                onPress={this._showOneDateTimePicker}>
                <Text style={{ color: theme.subTitle }}>Time of day</Text>
                <Text style={{ textAlign: 'right', color: theme.title }}>{this.state.timeOne}</Text>
              </TouchableOpacity> */}
            </View>
            <View style={styles.line_separator} />
            <View style={{ padding: 18 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ color: theme.title, fontSize: 16 }}>Second notification</Text>
                <FlipToggle
                  value={this.state.secondNotification}
                  buttonWidth={45}
                  buttonHeight={17}
                  buttonRadius={50}
                  sliderWidth={25}
                  sliderHeight={25}
                  sliderOffColor={'#ecebf2'}
                  sliderOnColor={theme.sliderOn}
                  buttonOffColor={'#aeaeae'}
                  buttonOnColor={theme.buttonOn}
                  onToggle={(value) => {
                    this.setState({ secondNotification: value }, () => {
                      this.state.secondNotification ? this.setNotification("secondNotify") : this.patchNotify2(this.state.myNotifyID);
                    });
                  }}
                // onToggle={(value) => {
                //   this.setState({ secondNotification: value }, ()=> this.patchNotify2(this.state.myNotifyID));
                // }}
                />
              </View>
              {this._renderTimeofDay(this.state.secondNotification, this.state.timeTwo, this._showTwoDateTimePicker)}
              {/* <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }} onPress={this._showTwoDateTimePicker} >
                <Text style={{ color: theme.subTitle }}>Time of day</Text>
                <Text style={{ textAlign: 'right', color: theme.title }}>{this.state.timeTwo}</Text>
              </TouchableOpacity> */}
            </View>
            <View style={styles.line_separator} />
            <View style={{ flexDirection: 'row', padding: 20, width: "100%" }}>
              <View style={{ width: "10%" }}>
                <Image source={oorUmbrella} style={{ height: 26, width: 26 }} />
              </View>
              <View style={{ width: "72%" }}>
                <Text style={{ fontSize: 16, color: theme.title, fontWeight: '600' }}>Receive umbrella out-of-range alerts</Text>
                <Text style={{ fontSize: 14, color: theme.subTitle, marginTop: 5 }}>We’ll let you know that you may have left your umbrella behind.</Text>
              </View>
              <View style={{ width: "18%", alignItems: 'flex-end' }}>
                <FlipToggle
                  value={this.state.oorAlerts}
                  buttonWidth={45}
                  buttonHeight={17}
                  buttonRadius={50}
                  sliderWidth={25}
                  sliderHeight={25}
                  sliderOffColor={'#ecebf2'}
                  sliderOnColor={theme.sliderOn}
                  buttonOffColor={'#aeaeae'}
                  buttonOnColor={theme.buttonOn}
                  onToggle={(value) => {
                    this.setState({ oorAlerts: value });
                    setTimeout(() => { this.patchOOR(this.state.myNotifyID); }, 1000);
                  }}
                  onToggleLongPress={() => {

                  }}
                />
              </View>
            </View>
            <View style={styles.line_separator} />
          </View>
          <ScrollView scrollEnabled={true} style={{ height: (Platform.OS == 'ios') ? height - 120 : height - 100, backgroundColor: '#fff', flexDirection: 'column' }}>
            <Modal
              visible={this.state.modalVisible}
              animationType={'slide'}
              transparent={true}
              onRequestClose={() => this.closeModal()}>
              <View style={{ width: width, height: height, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems: 'center' }}>
                {this.state.varianType == 'days' ? <View style={{ width: width * 0.82, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 60, borderWidth: 1, borderRadius: 10 }}>
                  <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(179,179,179,1)', borderBottomWidth: 0.5, alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5, color: theme.brandPrimary }}>Days</Text>
                  </View>
                  <View>
                    <ListItem onPress={() => this.dayToggler('Sunday')}>
                      <Left>
                        {this.state.daysSunday ? <MC name="checkbox-marked-outline" size={25} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={25} color={theme.brandPrimary} />}
                      </Left>
                      <Body>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary }}>Sunday</Text>
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.dayToggler('Monday')}>
                      <Left>
                        {this.state.daysMonday ? <MC name="checkbox-marked-outline" size={25} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={25} color={theme.brandPrimary} />}
                      </Left>
                      <Body>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary }}>Monday</Text>
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.dayToggler('Tuesday')}>
                      <Left>
                        {this.state.daysTuesday ? <MC name="checkbox-marked-outline" size={25} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={25} color={theme.brandPrimary} />}
                      </Left>
                      <Body>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary }}>Tuesday</Text>
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.dayToggler('Wednesday')}>
                      <Left>
                        {this.state.daysWednesday ? <MC name="checkbox-marked-outline" size={25} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={25} color={theme.brandPrimary} />}
                      </Left>
                      <Body>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary }}>Wednesday</Text>
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.dayToggler('Thursday')}>
                      <Left>
                        {this.state.daysThursday ? <MC name="checkbox-marked-outline" size={25} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={25} color={theme.brandPrimary} />}
                      </Left>
                      <Body>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary }}>Thursday</Text>
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.dayToggler('Friday')}>
                      <Left>
                        {this.state.daysFriday ? <MC name="checkbox-marked-outline" size={25} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={25} color={theme.brandPrimary} />}
                      </Left>
                      <Body>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary }}>Friday</Text>
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.dayToggler('Saturday')}>
                      <Left>
                        {this.state.daysSaturday ? <MC name="checkbox-marked-outline" size={25} color={theme.brandPrimary} /> : <MC name="checkbox-blank-outline" size={25} color={theme.brandPrimary} />}
                      </Left>
                      <Body>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary }}>Saturday</Text>
                      </Body>
                    </ListItem>
                  </View>
                  <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
                    <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                      <Text style={{ fontSize: 18, marginTop: 12, color: theme.brandPrimary }}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.dayBuilder()}>
                      <Text style={{ fontSize: 18, marginTop: 12, color: theme.brandPrimary }}>Save</Text>
                    </TouchableOpacity>
                  </View>
                </View> : null}

              </View>
            </Modal>
            <OneDateTimePicker
              titleIOS='Select a time'
              mode='time'
              isVisible={this.state.isOneDateTimePickerVisible}
              onConfirm={this._handleOneDatePicked}
              onCancel={this._hideOneDateTimePicker}
            />
            <TwoDateTimePicker
              titleIOS='Select a time'
              mode='time'
              isVisible={this.state.isTwoDateTimePickerVisible}
              onConfirm={this._handleTwoDatePicked}
              onCancel={this._hideTwoDateTimePicker}
            />
          </ScrollView>
        </ScrollView >

      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }, row: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 7,
  }, line_separator: {
    width: "100%",
    height: 0.5,
    backgroundColor: theme.separator
  }
});

const mapStateToProps = (state) => {
  console.log(" aletrrrb === ", state.user)
  return {
    deviceToken: state.user.deviceToken
  }
}

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
