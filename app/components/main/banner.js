import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Text,
    Animated,
    Dimensions,
    TouchableOpacity,
    Easing,
    Linking
} from 'react-native';
import Settings from './../../stores/settingsStore.js';
import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient';

const Setting = new Settings();

var width = Dimensions.get('window').width; //full width

export default class Banner extends Component {

    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
        this.state = {
            wbroadcast: 0,
            wMessage: "",
        }
    }

    componentDidMount() {
        this.animates();
        this.getBannerData();
    }

    animates = () => {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 5000,
                easing: Easing.linear
            }
        ).start(() => this.animates())
    }

    async getBannerData() {
        let response = await fetch(`${Setting.baseurl}wn_broadcast`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        let res = await response.text();
        let parsed = JSON.parse(res)
        if (response.status >= 200 && response.status < 300) {
            this.setState({ wbroadcast: parsed[0].wbroadcast, wMessage: parsed[0].wmessage })
        } else {
            alert(" Error ")
        }
    }


    render() {

        const marginLeft = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [width, -this.state.wMessage.length * 8]
        })

        return (
            <View>
                {this.state.wbroadcast === 1 ?
                    <TouchableOpacity onPress={() => Linking.openURL("https://weathermanumbrella.com")} style={{ height: 25, justifyContent: 'center', }}>
                        <AnimatedLinearGradient customColors={['#2e61c1', '#7ac5ee', '#678ccf', '#8fc8e6', '#a79bbe', '#cfbed5']} speed={1000} points={'{start: {x: 0.5, y: 0 }, end: {x: 0.5, y: 1}}'} style={{ borderRadius: 5, marginTop: 20 }} />
                        <Animated.Text numberOfLines={1} style={{ marginLeft, fontSize: 16, color: "#fff" }}>{this.state.wMessage}</Animated.Text>
                    </TouchableOpacity>
                    :
                    null}

            </View>

        );
    }
}