import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, Image, Dimensions, ScrollView, TouchableOpacity, ListView, SafeAreaView, StatusBar, Alert, AppState, RefreshControl,
  NativeEventEmitter,
  NativeModules,
  Modal,
  FlatList,
  Animated,
  Easing
} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Left, Body, Right, List, ListItem, Spinner } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
// import Weather from './weather'
import Tracking from './tracking'
import Notifications from './notifications_updated'
import AppSettings from './settings_updated'
import Settings from '../../stores/settingsStore'
import theme from '../../theme/base-theme';
import DeviceInfo from 'react-native-device-info';
//import PullToRefresh from 'react-native-pull-refresh';
import Svg, { Stop, Defs, LinearGradient } from "react-native-svg";
import { VictoryAxis, VictoryChart, VictoryLine, VictoryArea, VictoryScatter, } from "victory-native";
import { formatDate } from '../../functions/datetimefuncs';
import { multiDimensionalUnique, removeDuplicates, writeToLog } from '../../functions/umbrellafuncs';
import { patchNotifications } from '../../functions/weatherfuncs';
import RNPbWrapper from 'react-native-pb-wrapper';
import * as Progress from 'react-native-progress';
import { getData, getTickValue } from '../../functions/indicatorfuncs';
import _ from 'lodash';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from '../../redux/modules/user';
import * as userLocation from '../../redux/modules/location';
import Idx from '../../utilities/Idx';
import BackgroundTimer from 'react-native-background-timer';
import LottieView from 'lottie-react-native'
import { checkPermissions } from "../../utilities/Locations_updated";
import Weather from './weatherChange'

import FCM from 'react-native-fcm';
import { registerAppListener } from "../../messaging/Listeners";

import AnimatedLinearGradient, { presetColors } from 'react-native-animated-linear-gradient'

var progress = 0;
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const mylogo = require('../../img/wxlogo.png')
const add = require('../../img/add.png')
const edit = require('../../img/edit.png')
const weather_on = require('../../img/weatherActive.png')
const weather_off = require('../../img/weatherInactive.png')
const tracking_on = require('../../img/umbrellaActive.png')
const tracking_off = require('../../img/umbrellaInactive.png')
const pairingconnect = require('../../img/pairingconnect.png')
const pairing = require('../../img/pairing.gif')
const notifications_on = require('../../img/notificationActive.png')
const notifications_off = require('../../img/notificationInactive.png')
const settings_on = require('../../img/settingsActive.png')
const settings_off = require('../../img/settingsInactive.png')
const needUmbrella = require('../../img/rain.gif')
const dontNeedUmbrella = require('../../img/umbrellaClose.png')
const pairingfail = require('../../img/pairingfail.png')
const pairingfound = require('../../img/pairingfound.png')
// const loadingspinner = require('../../img/loading.gif')
const settings = new Settings();

const navColors = {
  active:
    '#092f57'
  ,
  inactive:
    '#77787a'

};


const dataArray0 = [{ time: 4, value: 0 },
{ time: 5, value: 0 },
{ time: 6, value: 10 },
{ time: 7, value: 10 },
{ time: 8, value: 30 },
{ time: 9, value: 20 },
{ time: 10, value: 50 },
{ time: 11, value: 80 },
{ time: 12, value: 100 },
{ time: 1, value: 70 },
{ time: 2, value: 30 },
{ time: 3, value: 0 }];

const buttonImages = [{
  normal: require('../../img/btn1.png'),
  clicked: require('../../img/btn1_clicked.png')
},
{
  normal: require('../../img/btn2.png'),
  clicked: require('../../img/btn2_clicked.png')
},
{
  normal: require('../../img/btn3.png'),
  clicked: require('../../img/btn3_clicked.png')
}];
var fcmUrl = 'https://fcm.googleapis.com/fcm/send';

class Home extends Component {
  constructor(props) {
    super(props);
    console.log('Home');
    this.animatedValue = new Animated.Value(0);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      baseUrl: '',
      dsUrl: '',
      myID: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].id : '',
      myNotifyID: '',
      myUnits: this.props.wUnits || '',
      subScreenLoad: true,
      subScreen: <Weather />,
      colorWeather: "Active",
      colorTracking: "Inactive",
      colorNotifications: "Inactive",
      colorSettings: "Inactive",
      curScreen: 'Weather',
      personalLoc: [],
      mycities: [],
      fullList: [],
      homeLocation: Idx(this.props.location, _ => _.IdAndAllLocationData[0].wHomeLoc) || null,
      workLocation: Idx(this.props.location, _ => _.IdAndAllLocationData[0].wWorkLoc) || null,
      // indicator_personal: Idx(this.props.location,_=>_.IdAndAllLocationData.iwUnitsd) || [],
      indicator_data: [],
      index: 0,
      index_indicator: 0,
      data: getData(-1, dataArray0),
      tickArray: getTickValue(-1, dataArray0),
      modalVisible: false,
      search: '',
      searchText: '',
      buttonType: '',
      uniqueId: '',
      indicatorHeight: 'close',
      selectedItem: undefined,
      selectedAddress: '',
      selectedCity: '',
      selectedLat: '',
      selectedLon: '',
      deletedPL: '',
      deletedAL: '',
      deletedCity: '',
      editedPL: '',
      editedCity: '',
      reults: '',
      copy: '',
      showMax: true,
      refreshing: false,
      doYouNeedUmbrella: false,
      loading: false,
      deviceArray: [],
      // scanning: false,
      fail: false,
      success: false,
      progress: 0,
      // indeterminate: true,
      devicesToConnect: [],
      justDevices: [],
      isDeviceAvailable: false,
      connectedMacAddress: '',
      appState: AppState.currentState,
      isBlutoothEnabled: false,
      isResetEnabled: true,
      isScanning: false,
      appState: AppState.currentState,
      listloading: false,
      initlat: '',
      initlon: '',
      openEditLocModal: false,
      addAdditionalLoc: '',
      selectedHomeAdd: '',
      selectedWorkAdd: '',
      wHomeLoc: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wHomeLoc : null,
      wWorkLoc: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wWorkLoc : null,
      loc1State: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wCNAdd1 : null,
      loc2State: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wCNAdd2 : null,
      loc3State: this.props.getUnitData && this.props.getUnitData.length > 0 ? this.props.getUnitData[0].wCNAdd3 : null,
      sendDataToTracking: [],
      deviceToken: props.deviceToken,
      target: null,
      wbroadcast: 0,
      wMessage: "",
      //setNotiId: props.notificationId
    }
    console.log('tracker id from props ******* ', props)
  }

  componentWillReceiveProps(nextProps) {
    //console.log('updated next props home js ******* ',nextProps)
    if (nextProps.getUnitData && nextProps.getUnitData.length > 0) {
      this.setState({
        myUnits: nextProps.wUnits,
        //getUnitData:nextProps.getUnitData,
        wHomeLoc: this.state.selectedHomeAdd == '' ? nextProps.getUnitData[0].wHomeLoc : this.state.selectedHomeAdd,
        wWorkLoc: this.state.selectedWorkAdd == '' ? nextProps.getUnitData[0].wWorkLoc : this.state.selectedWorkAdd
      })
      this.forceUpdate()
    }
  }

  componentWillMount() {
    //console.log('notification id and device token ******** ',this.props.notificationId, this.props.deviceToken)
    this.state.baseUrl = settings.BaseUrl;
    this.state.uniqueId = DeviceInfo.getUniqueID();
    this.state.dsUrl = settings.DSUrl;

    this.setListeners();
    this.onScanPressed();
    //
    // this.state.uniqueId = DeviceInfo.getUniqueID();
    // this.state.baseUrl = settings.BaseUrl;
    // this.state.dsUrl = settings.DSUrl;
    // this.getMyIdandIndicatorData();

    // if(this.props.notificationId == null){
    //   this.props.UserActions.getNotifcationId();
    // }else{
    //   this.setNotiId()
    // }

    this.getNotiId();
  }

  async getNotiId() {
    console.log('Home getNotiId')
    try {
      let response = await fetch(this.state.baseUrl + 'wn_notification?filter[where][wdeviceid]=' + this.state.uniqueId, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      //console.log(res);
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        if (parsed.length == 0) {
          console.log("No values for this ID");
        } else {
          /// change by leena
          let data = parsed[0]
          if (data.wnotify1 === true || data.wnotify1 === true) {
            FCM.requestPermissions()
            FCM.getFCMToken().then(token => {
              if (token) {
                this.props.UserActions.setDeviceToken(token)
                this.setState({ deviceToken: token })
              }
            });

            console.log("token is there")
            registerAppListener(this.props.navigation);
            this.props.UserActions.addNotification({ deviceToken: this.state.deviceToken })
            // this.props.UserActions.updateDeviceToken({notiId:parsed[0].id, deviceToken:this.state.deviceToken})

          }

        }

      } else {
        let errors = res;
        throw errors;
      }
    } catch (error) {
      this.setState({ errors: error });
      console.log(error);
    }
  }

  // async getNotiId() {

  //   try {
  //     let response = await fetch(this.state.baseUrl + 'wn_notification?filter[where][wdeviceid]=' + this.state.uniqueId, {
  //       method: 'GET',
  //       headers: {
  //         'Content-Type': 'application/json'
  //       }
  //     });
  //     let res = await response.text();
  //     //console.log(res);
  //     if (response.status >= 200 && response.status < 300) {
  //       let parsed = JSON.parse(res)

  //       if (parsed.length == 0) {

  //         console.log("No values for this ID");
  //       } else {
  //         //writeToLog('','',DeviceInfo.getUniqueID(),9,parsed[0].id)
  //         this.props.UserActions.updateDeviceToken({ notiId: parsed[0].id, deviceToken: this.state.deviceToken })
  //       }

  //     } else {
  //       let errors = res;
  //       throw errors;
  //     }
  //   } catch (error) {
  //     this.setState({ errors: error });
  //     console.log(error);
  //   }
  // }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  componentDidMount() {
    checkPermissions(this.state.uniqueId, this.state.baseUrl);
    this.getMyIdandIndicatorData();
    this.animates();
    this.getBannerData();
    //this.onScanPressed();
  }

  animates = () => {
    this.animatedValue.setValue(0)
    Animated.timing(
      this.animatedValue,
      {
        toValue: 1,
        duration: 2000,
        easing: Easing.linear
      }
    ).start(() => this.animates())
  }

  async getBannerData() {
    let response = await fetch(`${settings.baseurl}wn_broadcast`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    let res = await response.text();
    let parsed = JSON.parse(res)
    if (response.status >= 200 && response.status < 300) {
      this.setState({ wbroadcast: parsed[0].wbroadcast, wMessage: parsed[0].wmessage })
    } else {
      alert(" Error ")
    }
  }

  uniqFilterAccordingToProp(prop) {
    if (prop)
      return (ele, i, arr) => arr.map(ele => ele[prop]).indexOf(ele[prop]) === i
    else
      return (ele, i, arr) => arr.indexOf(ele) === i
  }

  setListeners() {
    const mNativeEventEmitter = new NativeEventEmitter(NativeModules.RNPbWrapper)
    AppState.addEventListener('change', this._handleAppStateChange);
    let trackingArr = [];
    console.log('RNPbWrapper ', RNPbWrapper)
    mNativeEventEmitter.addListener(RNPbWrapper.ON_RSS_SIGNAL_CHANGED, (data) => {
      console.log('ON_RSS_SIGNAL_CHANGED ****** ', data)
      // let finalData = data;

      //Array.prototype.push.apply(trackingArr,{...finalData});

      trackingArr.push(data);

      let finalArr = _.uniqBy(trackingArr, 'macAddress')
      //obj.filter(uniqFilterAccordingToProp(macAddress))

      //console.log('trackingArr ********** ',finalArr)
      this.setState({
        sendDataToTracking: finalArr
      })

      let dArray = this.state.deviceArray;
      //if(dArray.length > 0){
      dArray = dArray.map((item) => {
        if (item.macAddress == data.macAddress) {
          return data;
        } else {
          return item;
        }
      });
      // }
      //
      console.log('Home dArray', dArray)
      console.log('Home deviceArray ', this.state.deviceArray)
      //this.setState({ deviceArray: dArray });
    });


    mNativeEventEmitter.addListener(RNPbWrapper.ON_DEVICE_ADDED, (data) => {
      console.log('Home inside ON_DEVICE_ADDED ******** ', data)
      trackingArr.push(data);

      let finalArr = _.uniqBy(trackingArr, 'macAddress')
      //obj.filter(uniqFilterAccordingToProp(macAddress))

      console.log('trackingArr ********** ', JSON.stringify(finalArr))
      this.setState({
        sendDataToTracking: finalArr,
        deviceArray: finalArr,
        isResetEnabled: false
      }, () => {
        console.log('deviceArray ', this.state.deviceArray);
        this.getDevicetoConnect(this.state.deviceArray);
      })

      // let dAddedArray = [].concat(data)
      // console.log('dAddedArray empty', dAddedArray)
      // if (dArray.length == 0) {
      //   dArray.push(data);
      //   this.setState({ deviceArray: dArray, isResetEnabled: false });
      // } else {
      //   dArray = dArray.map((item) => {
      //     console.log('item ', item)
      //     if (item.macAddress == data.macAddress) {
      //       return data;
      //     } else {
      //       return item;
      //     }
      //   });
      // }

      // dAddedArray.push(data);
      // this.setState({ deviceArray: dAddedArray, isResetEnabled: false });
      // console.log('dArray after push', dAddedArray)


    });
    //console.log('listeners are called just before ON_RSS_SIGNAL_CHANGED ******',mNativeEventEmitter)


    /*mNativeEventEmitter.addListener(RNPbWrapper.ON_DEVICE_REMOVED, (data) => {
     /* let dArray = this.state.deviceArray;
      let index = -1;
      for (var i = 0; i < dArray.length; i++) {
        var element = dArray[i];
        if (data.rss_signal == element.rss_signal) {
          index = i;
        }
      }
      if (index != 1) {
        dArray.splice(index, 1);
      }
      this.setState({ deviceArray: dArray });
      //
    });*/

    mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STARTED, (data) => {
      console.log('Home ON_SCANNING_STARTED');
      this.setState({ isScanning: true });
    });

    mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_SUCCESS, (data) => {
      console.log('Home ON_SCANNING_SUCCESS');
      this.setState({ isScanning: false }, () => {
        this.scanAction();
      });
    });

    mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_FAILED, (data) => {
      console.log('Home ON_SCANNING_FAILED');
      this.setState({ isScanning: false });
      alert(RNPbWrapper.ON_SCANNING_FAILED);
    });

    mNativeEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_ON, (data) => {
      console.log('Home ON_BLUETOOTH_ON');
      //Alert.alert('ON_BLUETOOTH_ON', RNPbWrapper.ON_BLUETOOTH_ON);
    });

    mNativeEventEmitter.addListener(RNPbWrapper.ON_BLUETOOTH_OFF, (data) => {
      console.log('Home ON_BLUETOOTH_OFF');
      alert(RNPbWrapper.ON_BLUETOOTH_OFF);
    });

    mNativeEventEmitter.addListener(RNPbWrapper.ON_SCANNING_STOPPED, (data) => {
      console.log('Home ON_SCANNING_STOPPED');
      this.setState({ isScanning: false });
    });

    mNativeEventEmitter.addListener(RNPbWrapper.ON_OUT_OF_RANGE, (data) => {
      //Alert.alert('Home umbrella Out of range handle here.', data)
    });

    mNativeEventEmitter.addListener(RNPbWrapper.ON_DEVICE_DISCONNECT, (data) => {
      console.log('device disconnected ************ ', data)
      trackingArr = [];
      this.setState({
        sendDataToTracking: []
      })
      if (data.device_state != 'Disconnected') {
        trackingArr.push(data);

        let finalArr = _.uniqBy(trackingArr, 'macAddress')
        //obj.filter(uniqFilterAccordingToProp(macAddress))

        console.log('trackingArr ********** ', finalArr)
        this.setState({
          sendDataToTracking: finalArr
        })

        let dArray = this.state.deviceArray;
        //if(dArray.length > 0){
        dArray = dArray.map((item) => {
          if (item.macAddress == data.macAddress) {
            return data;
          } else {
            return item;
          }
        });
        console.log('dArray ', dArray);
        console.log('deviceArray ', this.state.deviceArray)
      }

      // console.log('trackingArr outside if ********** ',trackingArr)

    });
    // Alert.alert('Home umbrella Disconnected.', data)
  }


  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      // RNPbWrapper.onAttach();
      this.setState({ subScreenLoad: false });
      this.closeIndicator();
      this.RefreshIndicator();
      if (Platform.OS === 'ios') {
        BackgroundTimer.stopBackgroundTimer();
      } else {
        BackgroundTimer.clearInterval(this.state.backgroundTimeInterval);
      }

      setTimeout(() => { this.setState({ subScreenLoad: true }); }, 1000);

    }
    else {
      this.locateMe();
      if (Platform.OS === 'ios') {
        BackgroundTimer.runBackgroundTimer(() => {
          patchNotifications(settings.BaseUrl, this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID());
          this.getOutOfRange(this.state.initlat, this.state.initlon);
          writeToLog(this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID(), 2, 'backgroundIOS')
        }, 900000)

      } else {
        intervalId = BackgroundTimer.setInterval(() => {
          //console.log('app in background state ****** ')
          patchNotifications(settings.BaseUrl, this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID());
          this.getOutOfRange(this.state.initlat, this.state.initlon);
          writeToLog(this.state.initlat, this.state.initlon, DeviceInfo.getUniqueID(), 2, 'backgroundAndroid')
        }, 900000)
        this.setState({ backgroundTimeInterval: intervalId });
      }


      // RNPbWrapper.onDettach();
    }
    this.setState({ appState: nextAppState });
  }



  async getOutOfRange(lat, lon) {
    console.log('getOutOfRange lat and long in get out of range ******* ', lat, lon)

    //Fix for 2.0 wdeviceid from wDeviceID
    //console.log(settings.BaseUrl + 'wn_view_oor_curpost?filter[where][wdeviceid]=' + DeviceInfo.getUniqueID());
    try {
      let response = await fetch(settings.BaseUrl + 'wn_view_oor_notifier?filter[where][wdeviceid]=' + DeviceInfo.getUniqueID(), {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      //console.log(res);
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        //writeToLog('','',DeviceInfo.getUniqueID(),5,parsed)
        if (parsed.length == 0) {
          console.log("No one out of range");
        }

        else {

          let { sendDataToTracking } = this.state;
          //console.log('I am inside greater than zero ******** ',DateArray)
          _.each(parsed, (data) => {
            //console.log('data is here ******* in parsed ****** ',data)
            if (sendDataToTracking.length > 0) {
              _.each(sendDataToTracking, (element) => {
                // console.log('element found here ****** ',element)
                if (element.macAddress != undefined) {
                  if (data.wtrackerid == element.macAddress) {
                    //console.log('elment and tracker id match **** ',data.wtrackerid,element.macAddress)
                    this.getUpdatedUmbrellaInfo(data.mytrackerID, lat, lon);
                    this.patchRange(data.mytrackerID, true);
                    //writeToLog('','',DeviceInfo.getUniqueID(),7,data.mytrackerID)
                  } else {
                    this.patchRange(data.mytrackerID, false);
                    this.checkDistancefromLocations(lat, lon, data.whomeLoc, data.wWorkLoc, data.wtoken, data.wtrackername)
                    //writeToLog('','',DeviceInfo.getUniqueID(),8,data.mytrackerID)
                  }
                }
              })
            }
            else {
              //writeToLog('','',DeviceInfo.getUniqueID(),88,data.mytrackerID)
              this.patchRange(data.mytrackerID, false);
              this.checkDistancefromLocations(lat, lon, data.whomeLoc, data.wWorkLoc, data.wtoken, data.wtrackername)
            }
          })
        }
      } else {
        let errors = res;
        throw errors;
      }
    } catch (error) {
      console.log(error);
    }
  }

  getUpdatedUmbrellaInfo(id, lat, lng) {
    let latlng = lat + ',' + lng;
    //console.log('lat and long in get updated umbrella ******* ',latlng,id)

    this.getAddress(latlng, id);
  }

  async getAddress(latlon, id) {
    //console.log('lat and long in get address ******* ',latlon,id)
    try {
      let response = await fetch(settings.geourl + 'latlng=' + latlon + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        // console.log("Formatted Address " + parsed.results[0].formatted_address);
        this.patchInRange(latlon, parsed.results[0].formatted_address, id);


      } else {
        let errors = res;
        throw errors;
      }

    } catch (error) {
      this.setState({ errors: error });
      console.log("error: " + error);
    }
  }

  patchRange(idToUpdate, toggle) {
    console.log('patchRange ', idToUpdate)
    this.props.UserActions.patchRangeFunc({ idToUpdate: idToUpdate, winrange: toggle });
  }


  async patchInRange(latlon, address, idToUpdate) {
    console.log('patchInRange');
    try {
      let response = await fetch(this.state.baseUrl + 'wn_tracking/' + idToUpdate, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          wlatlon: latlon,
          waddress: address,
          wlastconnected: Date.now(),
        })
      });

      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        console.log("Updated InRage Umbrella")
      } else {
        let errors = res;
        throw errors;
      }
    } catch (errors) {
      console.log(errors);
    }
  }

  async checkDistancefromLocations(lat, lon, home, work, token, uname) {
    console.log('getting all values **** ', lat, lon, home, work, token, uname)
    let combinedHomeandWork = [];
    if (home != null) {
      combinedHomeandWork = '[' + home + ']'
      //console.log("Home");
    }

    if (work != null) {
      combinedHomeandWork = '[' + work + ']'
      //console.log("Work");
    }

    if (home != null && work != null) {
      combinedHomeandWork = '[' + home + '|' + work + ']'
      //console.log("Home and Work");

    }
    try {
      //console.log('inside try ******* ')
      let response = await fetch('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + lat + ',' + lon + '&destinations=' + combinedHomeandWork + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();

      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        //console.log('parsed data *********',parsed);
        //writeToLog('','',DeviceInfo.getUniqueID(),99,parsed)

        if (parsed.rows[0].elements.length > 1) {
          if (parsed.rows[0].elements[0].distance.value > 3218 && parsed.rows[0].elements[1].distance.value > 3218) {
            //console.log("Send Notification");
            this.sendNotification(token, uname);
          }
        }
        else if (parsed.rows[0].elements.length == 1) {
          if (parsed.rows[0].elements[0].distance.value > 3218) {
            //console.log("Send Notification")
            this.sendNotification(token, uname);
          }
        }
      } else {
        let errors = res;
        throw errors;
      }

    } catch (error) {
      console.log("error: " + error);
    }
  }


  async sendNotification(token, umbrella) {

    try {

      let response = await fetch(fcmUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'key=AAAAM-j2f4c:APA91bEKm7cfnew5ZHTw3vDI3P66z64oQ9wJa9oXb2LCBJTPj8UTmi9EUtDyfJahe7FvMZw5p6ZfGxNPfuyfOfsH3khkvqy364fPYkd7WGnHu7U95qpAPspHLMv7Ac1upAy6ZNC4tKH7'
        },
        body: JSON.stringify({
          notification: {
            title: "Out Of Range",
            body: 'Looks like you may have left ' + umbrella + ' behind! Open the app to locate it.',
            sound: "default"
          },

          to: token,
          priority: "high"
        })
      });

      let res = await response.text();
      //console.log("res is: " + res);

      if (response.status >= 200 && response.status < 300) {

        let parsed = JSON.parse(res);
      } else {
        let errors = res;
        throw errors;
      }

    } catch (errors) {
      patchRespnse(id, errors);
      console.log(errors);
    }
  }


  onScanPressed() {
    console.log('onScanPressed ', RNPbWrapper)
    RNPbWrapper.isBlutoothEnabled((isBlutoothEnabled) => {
      this.setState({ isBlutoothEnabled: isBlutoothEnabled });
      if (!isBlutoothEnabled) {
        Alert.alert(
          'Bluetooth',
          'Please enable bluetooth.')
        console.log('Please enable bluetooth');
      } else {
        RNPbWrapper.startScanning()
        // this.setState({ scanning: true, progress: 0, indeterminate: true, fail: false, success: false });
        this.setState({ fail: false, success: false });
        // this.onScanPressed();
        // this.animate();
        // this.setTimoutforScan();
      }
    });
  }

  // startscan() {
  //   console.log('startscan ', this.state.isBlutoothEnabled)
  //   if (this.state.isBlutoothEnabled) {
  //     this.setState({ scanning: true, progress: 0, indeterminate: true, fail: false, success: false });
  //     // this.onScanPressed();
  //     this.animate();
  //     this.setTimoutforScan();
  //   }
  //   else {
  //     Alert.alert(
  //       'Bluetooth',
  //       'Please enable bluetooth.')
  //     console.log('Please enable bluetooth')
  //   }

  // }

  getDevicetoConnect(devices) {
    console.log('getDevicetoConnect ', devices.length - 1)
    this.doesTrackerExist(this.state.uniqueId, devices[devices.length - 1].macAddress)
  }

  // setTimoutforScan() {
  //   setTimeout(() => { this.scanAction(); }, 10000);
  // }

  scanAction() {
    console.log('scanAction deviceArray is here ******** ', this.state.deviceArray)
    if (this.state.deviceArray.length > 0) {
      // this.setState({ scanning: false, success: true });
      this.setState({ success: true });
      //alert("Scanning Failed to find a device");
    }
    else {
      // this.setState({ scanning: false, fail: true });
      this.setState({ fail: true });
    }
  }

  connect(target, devices) {
    console.log('Home connect ', devices[target])
    if (!this.state.loading) {
      this.setState({ loading: true });
      if (target !== null) {
        if (devices[target].status == 'Ready to Pair') {
          this.addDevice(this.state.uniqueId, devices[target].id, "My Umbrella" + devices[target].id)
        } else if (devices[target].status == 'Already Paired') {
          this.setState({ loading: false });
          this.closeModal();

        }

      }
    }
    // console.log('Home connect ', devices)
    // if (!this.state.loading) {
    //   console.log('Home isLoading ', this.state.loading)
    //   this.setState({ loading: true });
    //   for (let key in devices) {
    //     if (devices[key].status == 'Ready to Pair') {
    //       this.addDevice(this.state.uniqueId, devices[key].id, "My Umbrella" + key)
    //     }
    //   }
    //   setTimeout(() => {
    //     this.setState({ loading: false });
    //     this.closeModal();
    //   }, 4000);
    //   // setTimeout(() => { this.closeModal(); }, 4000);
    // }
  }

  animate() {
    progress = 0;
    this.setState({ progress });
    // setTimeout(() => {
    //   this.setState({ indeterminate: false });
    //   setInterval(() => {
    //     progress += Math.random() / 5;
    //     if (progress > 1) {
    //       progress = 1;
    //     }
    //     this.setState({ progress });
    //   }, 5500);
    // }, 5500);
    // this.setState({ indeterminate: false });
    // setInterval(() => {
    //   progress += Math.random() / 5;
    //   if (progress > 1) {
    //     progress = 1;
    //   }
    //   this.setState({ progress });
    // }, 1000);

  }

  async doesTrackerExist(deviceID, trackerid) {
    console.log('doesTrackerExist ', trackerid);
    try {
      let response = await fetch(this.state.baseUrl + 'wn_tracking/count?[where][wdeviceid]=' + deviceID + '&[where][wtrackerid]=' + trackerid + '&[where][wactive]=true', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        let newItem = '';
        console.log('parsed ', parsed)
        if (parsed.count == 0) {
          newItem = { id: trackerid, status: "Ready to Pair" };
          this.state.devicesToConnect.push(newItem);
          this.state.justDevices.push(trackerid);
        }
        else {
          newItem = { id: trackerid, status: "Already Paired" };
          this.state.devicesToConnect.push(newItem);
          this.state.justDevices.push(trackerid);
        }

        this.state.devicesToConnect = multiDimensionalUnique(this.state.devicesToConnect);
        //See Which Devices are connected
        //
        //
        //

      } else {
        let errors = res;
        throw errors;
      }
    } catch (error) {

    }
  }

  addDevice(deviceid, trackerid, trackername) {
    this.props.UserActions.addNewTracker({ deviceId: deviceid, trackerId: trackerid, trackerName: trackername, lat: this.state.initlat, lng: this.state.initlon });
  }

  _onRefresh() {
    console.log('on refersh called at home js')
    this.closeIndicator();
    // this.setState({refreshing:true,subScreenLoad:false});
    this.setState({ refreshing: true, subScreenLoad: false });
    // this.onScanPressed();
    setTimeout(() => { this.setState({ subScreenLoad: true, refreshing: false }); }, 1000);

  }

  RefreshIndicator() {
    console.log('dataArray0 ',dataArray0)
    this.setState({ doYouNeedUmbrella: false });
    this.setState({ data: getData(-1, dataArray0), tickArray: getTickValue(-1, dataArray0) });
    this.state.indicator_data = [];
    this.state.indicator_personal = [];
    this.getMyIdandIndicatorData();
    console.log('data ',this.state.data)
  }

  async getMyIdandIndicatorData() {

    try {
      let response = await fetch(this.state.baseUrl + 'wn_users?filter[where][wDeviceID]=' + this.state.uniqueId, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();


      //
      this.locateMe();
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        //console.log("getMyIdandIndicatorData", parsed, this.props.userLocation);
        this.props.userLocation.setDetails(parsed);
        //console.log("this.props.location****home ****", this.props.location)
        //this.setState({myID:parsed[0].id, myUnits:parsed[0].wUnits});
        // this.props.UserActions.storeLastOpened({idToUpdate: this.props.getUnitData[0].id})
        // if (parsed[0].wHomeLoc == null) {
        //   //this.state.indicator_data.push([]);
        // }
        // if (parsed[0].wWorkLoc == null) {
        //   //this.state.indicator_data.push([]);
        // }
        if (parsed[0].wHomeLoc != null) {
          // this.state.homeLocation = parsed[0].wHomeLoc;
          setTimeout(() => { this.getCNLL(this.state.homeLocation, 'Home'); }, 2000)
        }
        if (parsed[0].wWorkLoc != null) {
          // this.state.workLocation = parsed[0].wWorkLoc;
          setTimeout(() => { this.getCNLL(this.state.workLocation, 'Work'); }, 2000)

        }
      } else {
        let errors = res;
        throw errors;
      }
    } catch (error) {
      this.setState({ errors: error });

    }
  }

  locateMe() {

    navigator.geolocation.getCurrentPosition(
      (position) => {
        //
        if (position.coords && position.coords.latitude && position.coords.longitude) {
          this.getTemp(position.coords.latitude, position.coords.longitude, 1, "Cur");
          patchNotifications(this.state.baseUrl, position.coords.latitude, position.coords.longitude, this.state.uniqueId);
          this.setState({
            currentRegion: {
              mylatitude: position.coords.latitude,
              mylongitude: position.coords.longitude,
            }
          });
          this.setState({ initlat: position.coords.latitude, initlon: position.coords.longitude })
        }
      },
      (error) => {
        this.RefreshIndicator();

      },
      { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
    );
  }

  async getCNLL(myAddress, loctype) {

    try {
      let response = await fetch(settings.geourl + 'address=' + myAddress + '&key=AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();

      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        //
        //console.log("getCNLLgetCNLL***", parsed)
        this.getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, 2, loctype);
        //setTimeout(() => {this.setState({weatherLoading: false}) }, 8000)

      } else {
        let errors = res;
        throw errors;
      }

    } catch (error) {
      this.setState({ errors: error });

    }
  }

  async getTemp(lat, lon, id, loctype) {
    //console.log (id )
    try {
      let response = await fetch(this.state.dsUrl + lat + ',' + lon + '?units=' + this.state.myUnits, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
        let parsed = JSON.parse(res)
        let DateArray = [];
        let hourscheck = 0;
        //

        var tempData = [];
        while (hourscheck < 12) {
          //var tempData = [];
          if (parsed.hourly.data[hourscheck].precipIntensity >= 0.02 || parsed.currently.icon == 'rain' || parsed.currently.icon == 'snow') {
            //
            this.setState({ doYouNeedUmbrella: true });
          }
          var newItem = { time: formatDate(parsed.hourly.data[hourscheck].time, parsed.timezone), value: (parsed.hourly.data[hourscheck].precipProbability * 100) };
          tempData.push(newItem)
          hourscheck++;
        }

        if (loctype == 'Cur') {
          //
          this.state.indicator_data[0] = tempData;
        }
        if (loctype == 'Home') {
          //
          this.state.indicator_data[1] = tempData;
        }
        if (loctype == 'Work') {
          //
          this.state.indicator_data[2] = tempData;
        }
        //this.state.indicator_data.push(tempData);
        //
        this.state.index_indicator = this.state.index_indicator + 1;
        this.setState({ index: 0 });


      } else {
        let errors = res;
        throw errors;
      }

    } catch (error) {
      this.setState({ errors: error });

    }
  }

  openModal(variant) {
    //console.log(variant + this.state.curScreen);
    this.setState({ buttonType: variant + this.state.curScreen });
    // this.setState({ scanning: false, progress: 0, indeterminate: true, fail: false, success: false });
    this.setState({ isScanning: false, fail: false, success: false });
    this.setState({ modalVisible: true, subScreenLoad: false, selectedCity: '', selectedAddress: '' });

    setTimeout(() => {

      if (variant == 'edit') {
        //console.log('get cities function',this.getCities)
        this.getCities();
      }
    }, 1000);

    this.closeIndicator();

  }

  closeModal() {
    console.log('closeModal')
    this.setState({ modalVisible: false, subScreenLoad: true });
    this.RefreshIndicator();
    this.state.justDevices = [];
    // this.state.deviceArray = [];
    // RNPbWrapper.resetAll();
    // setTimeout(() => { this.setListeners(); }, 2000)
    // setTimeout(() => { this.startscan(); }, 2000)
  }

  manageIndicator() {
    if (this.state.indicatorHeight == 'close') {
      this.onButtonPress(0);
      this.openIndicator();
    }
    if (this.state.indicatorHeight == 'open') {
      this.closeIndicator();
    }
  }

  openIndicator() {
    this.setState({ indicatorHeight: 'open' })
  }

  closeIndicator() {
    this.setState({ indicatorHeight: 'close' })
  }


  validateField = (myname) => {
    let tName = myname.length.toString();
    if (tName > 0) {
      return true;
    }
    else return false;
  };

  validateSave() {
    if (!this.state.loading) {
      this.setState({ loading: true });
      if (!this.validateField(this.state.selectedCity)) {
        Alert.alert(
          'City Missing',
          'Please enter your city before saving.')
        this.setState({ loading: false });
      }
      else if (1 == 1) {
        this.getAvailability();
      }
    }
  }


  validateEdit() {
    if (!this.state.loading) {
      this.setState({ loading: true });
      if (!this.validateField(this.state.selectedAddress)) {
        Alert.alert(
          'Address Missing',
          'Please enter your address.')
        this.setState({ loading: false });
      }
      else if (1 == 1) {
        if (this.state.editedPL == 1) {
          this.setState({
            selectedHomeAdd: this.state.selectedAddress
          }, () => {
            this.updateHomeLoc();
          })
        }
        if (this.state.editedPL == 2) {
          this.setState({
            selectedWorkAdd: this.state.selectedAddress
          }, () => {
            //console.log('work location 8*********** ',this.state.selectedWorkAdd)
            this.updateWorkLoc();
          })

        }
      }
    }
  }

  updateHomeLoc() {
    this.props.UserActions.addHomeOrWorkLocations({ idToUpdate: this.state.myID, selectedAdd: this.state.selectedAddress, selectedRowToEdit: this.state.editedPL });
    this.setState({ buttonType: 'editWeather', loading: false });
    setTimeout(() => {
      this.getCities();
    }, 2000);

  }

  updateWorkLoc() {
    this.props.UserActions.addHomeOrWorkLocations({ idToUpdate: this.state.myID, selectedAdd: this.state.selectedAddress, selectedRowToEdit: this.state.editedPL });
    this.setState({ buttonType: 'editWeather', loading: false });
    setTimeout(() => {
      this.getCities();
    }, 2000);
  }

  deletePersonalLoc(data) {
    //console.log('inside delete personalLoc **** ',data)
    this.setState({ listloading: true });
    this.setState({ deletedPL: data }, () => {
      this.getPLLoc();
      this.forceUpdate();
    });

  }

  deleteAdditionalLoc(data) {
    //console.log('data to be deleted ******** ',data)
    this.setState({ listloading: true });
    this.setState({ deletedAL: data }, () => {
      this.getALLoc();
      this.forceUpdate();
    });
    //this.forceUpdate()
  }

  editPersonalLoc(row) {
    //console.log('row data updated ****** ',row)
    this.setState({ editedPL: row, buttonType: 'editPersonalLoc' });
  }

  returntoEdit() {
    this.setState({ buttonType: 'editWeather' });
  }

  getPLLoc() {
    // console.log("PLlocationsssss***")

    let parsed = this.props.getUnitData;
    //console.log('inside home deletion ******',parsed[0].wHomeLoc,this.state.deletedPL)

    if (parsed[0].wHomeLoc == this.state.deletedPL) {
      //console.log('inside home deletion if ******',parsed[0].wHomeLoc,this.state.deletedPL)
      this.props.UserActions.deleteHomeLocation(this.state.myID);
    }
    if (parsed[0].wWorkLoc == this.state.deletedPL) {
      this.props.UserActions.deleteWorkLocation(this.state.myID);
    }
    setTimeout(() => { this.getCities(); }, 2000)
    setTimeout(() => { this.setState({ listloading: false }); }, 2000)
  }

  getALLoc() {
    // console.log("ALLlocationsssss***")
    let parsed = this.props.getUnitData;
    //console.log('check cities match ******* ',parsed,this.state.deletedAL)
    if (parsed[0].wCNAdd1 == this.state.deletedAL) {
      this.props.UserActions.deleteAdditional1(this.state.myID);
    }
    if (parsed[0].wCNAdd2 == this.state.deletedAL) {
      this.props.UserActions.deleteAdditional2(this.state.myID);
    }
    if (parsed[0].wCNAdd3 == this.state.deletedAL) {
      this.props.UserActions.deleteAdditional3(this.state.myID);
    }
    setTimeout(() => { this.getCities(); }, 2000)
    setTimeout(() => { this.setState({ listloading: false }); }, 2000)
    //this.forceUpdate();
  }

  switchScreen(screen) {
    if (screen == "Weather") {
      this.setState({ subScreen: <Weather />, colorWeather: "Active", colorTracking: "Inactive", colorNotifications: "Inactive", colorSettings: "Inactive" });

    }
    if (screen == "Tracking") {
      this.setState({ subScreen: <Tracking trackingData={this.state.sendDataToTracking} />, colorWeather: "Inactive", colorTracking: "Active", colorNotifications: "Inactive", colorSettings: "Inactive" });
      // this.onScanPressed();
    }
    if (screen == "Notifications") {
      this.setState({ subScreen: <Notifications />, colorWeather: "Inactive", colorTracking: "Inactive", colorNotifications: "Active", colorSettings: "Inactive" });
    }
    if (screen == "AppSettings") {
      this.setState({ subScreen: <AppSettings />, colorWeather: "Inactive", colorTracking: "Inactive", colorNotifications: "Inactive", colorSettings: "Active" });
    }
    this.setState({ curScreen: screen });
    this.closeIndicator();
  }

  // deleteRow(secId, rowId, rowMap) {
  //   rowMap[`${secId}${rowId}`].props.closeRow();
  //   const newData = [...this.state.mycities];
  //   newData.splice(rowId, 1);
  //   this.setState({ mycities: newData });
  // }

  prepareAddressforEdit(addy) {
    //console.log('updated one for edit personal loc ****** ',addy)
    this.setState({ selectedAddress: addy });
  }

  getCities() {
    //console.log("after deletion additional loc***")
    this.setState({ listloading: true }, () => {
      let { personalLoc, mycities } = this.state;
      this.setState({
        mycities: []
      })
      let parsed = this.props.getUnitData;

      if (parsed.length > 0) {
        //console.log('inside if ********** ')
        if (parsed[0].wHomeLoc != null) {
          personalLoc[1] = this.state.selectedHomeAdd == '' ? this.state.wHomeLoc : this.state.selectedHomeAdd;
          //console.log("parsed personalLoc[1] **********",personalLoc[1],parsed)
        }
        if (parsed[0].wWorkLoc != null) {
          personalLoc[2] = this.state.selectedWorkAdd == '' ? this.state.wWorkLoc : this.state.selectedWorkAdd;
        }

        if (parsed[0].wHomeLoc == null) {
          personalLoc[1] = "Swipe right to enter a home address";
        }
        if (parsed[0].wWorkLoc == null) {
          personalLoc[2] = "Swipe right to enter a work/other address";
        }
        //console.log('parsed city ******* ',parsed[0].wCNAdd1)
        this.setState({
          loc1State: parsed[0].wCNAdd1,
          loc2State: parsed[0].wCNAdd2,
          loc3State: parsed[0].wCNAdd3,
        }, () => {
          //console.log('loc 1 state ****** ',this.state.loc1State)
          if (this.state.loc1State != null || this.state.loc2State != null || this.state.loc3State != null) {
            let citiesArr = [...this.state.mycities];
            citiesArr.push(this.state.loc1State, this.state.loc2State, this.state.loc3State);
            let uniqueArr = _.uniq(citiesArr);
            let finalArr = _.without(uniqueArr, null);
            //console.log('my cities before set state********* ',finalArr)
            this.setState({
              mycities: finalArr
            }, () => {
              //console.log('my cities ********* ',this.state.mycities)
            })
          }

        }
        )
        setTimeout(() => { this.setState({ listloading: false }); }, 2000)
      } else {
        // console.log('inside else ******* ')
        personalLoc[1] = "Swipe right to enter a home address";
        personalLoc[2] = "Swipe right to enter a work/other address";
        setTimeout(() => { this.setState({ listloading: false }); }, 2000)
      }

    });
    // console.log("personalLoc value **********",personalLoc,mycities)
    //this.setState({listloading: false});
  }

  getAvailability() {
    //if(this.state.addAdditionalLoc == 'addLoc1')
    let parsed = this.props.getUnitData;
    if (parsed.length > 0) {
      if (parsed[0].wCNAdd1 == null) {
        this.setState({
          addAdditionalLoc: 'add1'
        }, () => {
          this.AddLoc(parsed[0].id);
        })
      }
      else if (parsed[0].wCNAdd2 == null) {
        this.setState({
          addAdditionalLoc: 'add2'
        }, () => {
          this.AddLoc(parsed[0].id);
        })
      }
      else if (parsed[0].wCNAdd3 == null) {
        this.setState({
          addAdditionalLoc: 'add3'
        }, () => {
          this.AddLoc(parsed[0].id);
        })
      }
      else if (this.state.addAdditionalLoc == '') {
        Alert.alert(
          'Limit Reached',
          'You can only add 3 additional cities at most')
        this.setState({ loading: false });
      }
      else {
        Alert.alert(
          'Limit Reached',
          'You can only add 3 additional cities at most')
        this.setState({ loading: false });
      }
      // this.setState({ loading: false });
    }
  }

  AddLoc(idToUpdate) {
    let { addAdditionalLoc, selectedCity, selectedLat, selectedLon } = this.state;
    this.props.UserActions.addAdditionalLocations({
      addAdditionalLoc: addAdditionalLoc,
      selectedCity: selectedCity,
      selectedLat: selectedLat,
      selectedLon: selectedLon,
      idToUpdate: idToUpdate
    });
    setTimeout(() => {
      this.setState({ loading: false }, () => {
        this.closeModal();
        this.getCities();
        this.forceUpdate();
      });
    }, 1000);

  }

  prepareCity(city, lat, lon) {
    this.setState({ selectedCity: city, selectedLat: lat, selectedLon: lon });
  }

  onButtonPress(i) {
    if (this.state.indicator_data.length > 0) {
      this.setState({
        index: i,
        data: getData(i, this.state.indicator_data[i]),
        tickArray: getTickValue(i, this.state.indicator_data[i])
      });
      setTimeout(() => this.onUpdate(), 500);
    }
  }

  onUpdate() {
    this.forceUpdate();
  }

  getImage(i) {
    if (i == this.state.index) {
      return buttonImages[i].clicked;
    }
    return buttonImages[i].normal;
  }

  selectDevice(id) {
    let deviceData = this.state.devicesToConnect
    if (this.state.target === id) {
      deviceData[id].isSelected = false
      this.setState({ target: null })
    } else {
      if (this.state.target === null) {
        deviceData[id].isSelected = true
        this.setState({ target: id })
      }
      else {
        deviceData[this.state.target].isSelected = false
        deviceData[id].isSelected = true
        this.setState({ target: id })
      }
    }
    //  this.setState({})
    // deviceData[id].isSelected = !deviceData[id].isSelected
    this.setState({ devicesToConnect: deviceData })
  }

  render() {

    const marginLeft = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, width]
    })

    //console.log("isBlutoothEnabled state ******", this.state.isBlutoothEnabled)
    _.set(this.refs, 'Content._scrollview.resetCoords', { x: 0, y: this.state.scrollY })
    return (
      <Container>
        <Header style={{ height: (Platform.OS === 'ios') ? 20 : 0, backgroundColor: theme.brandPrimary }} />
        <StatusBar backgroundColor={theme.brandPrimary} barStyle='light-content' />
        <View style={{ height: 50, backgroundColor: '#fff', borderBottomColor: navColors.inactive, borderBottomWidth: 1, flexDirection: 'row' }}>
          <View style={{ width: width * 0.025, height: 40, backgroundColor: 'rgba(256, 0, 256, 0)', marginTop: 5 }} />
          <View style={{ width: width * 0.1, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 10 }}>
            {this.state.curScreen == 'Tracking' || this.state.curScreen == 'Weather' ?
              <TouchableOpacity onPress={() => this.openModal('add')}>
                <Image source={add} style={{ height: 28, width: 28 }} />
              </TouchableOpacity> : null}
          </View>
          <View style={{ width: width * 0.025, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 5 }} />
          <View style={{ width: width * 0.25, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 10 }}>
            {this.state.curScreen == 'Weather' ? <TouchableOpacity onPress={() => this.openModal('edit')}>
              <Image source={edit} style={{ height: 28, width: 28 }} />
            </TouchableOpacity> : null}
          </View>
          <View style={{ width: width * 0.2, height: 40, backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 5, alignItems: 'center' }}>
            <Image source={mylogo} style={{ marginTop: 2.5, height: 38.285, width: 66.142 }} />
          </View>
          <View style={{ width: width * 0.25, height: 40, backgroundColor: 'rgba(0, 256, 0, 0)', marginTop: 10, alignItems: 'flex-end' }} />
          {this.state.index >= 0 ? <TouchableOpacity onPress={() => this.manageIndicator()}>
            <View style={{ width: width * 0.1, height: 40, backgroundColor: 'rgba(100, 150, 100, 0)', marginTop: 10, alignItems: 'flex-end' }}>
              {this.state.doYouNeedUmbrella ? <Image source={needUmbrella} style={{ height: 28, width: 28 }} /> : <Image source={dontNeedUmbrella} style={{ height: 28, width: 28 }} />}
            </View>
          </TouchableOpacity> : null}
          <View style={{ width: width * 0.125, height: 40, backgroundColor: 'rgba(256, 0, 256, 0)', marginTop: 5 }} />
        </View>
        {this.state.indicatorHeight == 'open' ? <View style={styles.container}>
          <View style={styles.buttonGroup}>
            <TouchableOpacity onPress={() => this.onButtonPress(0)}>
              <Image source={this.getImage(0)} style={styles.checkboxImage} />
            </TouchableOpacity>
            {this.state.homeLocation != null
              ? <TouchableOpacity onPress={() => this.onButtonPress(1)}
                style={{ marginLeft: 10, marginRight: 10 }}>
                <Image source={this.getImage(1)} style={styles.checkboxImage} />
              </TouchableOpacity> : null}
            {this.state.workLocation != null
              ? <TouchableOpacity onPress={() => this.onButtonPress(2)}>
                <Image source={this.getImage(2)} style={styles.checkboxImage} />
              </TouchableOpacity> : null}
          </View>

          <ScrollView style={styles.chartView} horizontal={true} showsHorizontalScrollIndicator={false}>
            <VictoryChart
              height={100}
              width={this.state.data.length * 50}
              domain={{ x: [-1, this.state.data.length], y: [-2, 180] }}
              padding={{ left: 0, top: 0, right: 0, bottom: 40 }}>

              <Defs>
                <LinearGradient id='gradientFill'
                  x1='0%'
                  x2='0%'
                  y1='0%'
                  y2='100%'
                >
                  <Stop offset='0%' stopColor={"#557aa0"} stopOpacity='0.9' />
                  <Stop offset='25%' stopColor={"#557aa0"} stopOpacity='0.7' />
                  <Stop offset='50%' stopColor={"#557aa0"} stopOpacity='0.5' />
                  <Stop offset='70%' stopColor={"#557aa0"} stopOpacity='0.3' />
                  <Stop offset='90%' stopColor={"#557aa0"} stopOpacity='0.1' />
                </LinearGradient>
              </Defs>

              <VictoryArea
                data={this.state.data}
                interpolation="cardinal"
                labels={(data) => `${Math.round(data.y)}%`}
                style={{
                  data: {
                    stroke: theme.brandSecondary,
                    strokeWidth: 1,
                    fill: 'url(#gradientFill)',
                  },
                  labels: { fontFamily: theme.fontFamily, fontWeight: '400', fontSize: 14, fill: "white" },
                }}
              />

              <VictoryScatter
                style={{ data: { fill: theme.brandSecondary } }}
                data={this.state.data}
                size={2} />

              <VictoryAxis
                style={{
                  axis: {
                    stroke: "transparent",
                  },
                  tickLabels: { fontFamily: theme.fontFamily, fontWeight: '400', fontSize: 14, fill: "white" },
                }}
                tickValues={this.state.tickArray}
                tickFormat={(t) => `${Idx(this.state, _ => _.data[t].time)}`}
              />
            </VictoryChart>
          </ScrollView>
        </View> : null}
        {this.state.wbroadcast === 1 ?
          <TouchableOpacity onPress={() => Linking.openURL("https://weathermanumbrella.com")} style={{ height: 25, justifyContent: 'center', }}>
            <AnimatedLinearGradient customColors={['#2e61c1', '#7ac5ee', '#678ccf', '#8fc8e6', '#a79bbe', '#cfbed5']} speed={1000} points={'{start: {x: 0.5, y: 0 }, end: {x: 0.5, y: 1}}'} style={{ borderRadius: 5, marginTop: 20 }} />
            <Animated.Text numberOfLines={1} style={{ marginLeft, fontSize: 16, color: "#fff" }}>{this.state.wMessage}</Animated.Text>
          </TouchableOpacity>
          :
          null}
        {this.state.curScreen == 'Tracking' || this.state.curScreen == 'Weather' ?
          <ScrollView scrollEnabled={true} style={{ height: (Platform.OS == 'ios') ? height - 120 : height - 100, backgroundColor: '#fff', flexDirection: 'column' }} refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
            {this.state.subScreenLoad ? this.state.subScreen : null}
          </ScrollView> : <ScrollView scrollEnabled={true} style={{ height: (Platform.OS == 'ios') ? height - 120 : height - 100, backgroundColor: '#fff', flexDirection: 'column' }}>
            {this.state.subScreenLoad ? this.state.subScreen : null}
          </ScrollView>}


        <Modal
          visible={this.state.modalVisible}
          animationType={'slide'}
          transparent={true}
          onRequestClose={() => this.closeModal()}>
          <View style={{ width: width, height: height, backgroundColor: 'rgba(256, 256, 256, 0)', alignItems: 'center' }}>
            {this.state.buttonType == 'addWeather' ? <View style={{ width: width * 0.82, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
              <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(0, 0, 0, 0.7)', alignItems: 'center' }}>
                <Text style={{
                  fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios')
                    ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5
                }}>Add locations</Text>
              </View>
              <ScrollView keyboardShouldPersistTaps={"always"} scrollEnabled={false} style={{ width: width * 0.80, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)' }}>
                <GooglePlacesAutocomplete
                  placeholder='City'
                  minLength={2} // minimum length of text to search
                  autoFocus={false}
                  returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                  listViewDisplayed='true'    // true/false/undefined
                  fetchDetails={true}
                  renderDescription={row => row.description} // custom description render
                  onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                    this.prepareCity(data.structured_formatting.main_text, details.geometry.location.lat, details.geometry.location.lng);
                  }}
                  getDefaultValue={() => ''}
                  query={{
                    // available options: https://developers.google.com/places/web-service/autocomplete
                    key: 'AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg',
                    language: 'en', // language of the results
                    types: '(cities)' // default: 'geocode'
                  }}
                  styles={{
                    textInputContainer: {
                      borderWidth: 0,
                      width: width * 0.80,
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      backgroundColor: 'rgba(256,256,256,0)'
                    },
                    textInput: {
                      marginTop: 0,
                      marginLeft: 0,
                      marginRight: 0,
                      marginBottom: 0,
                      height: 50,
                      color: '#000',
                      fontSize: 16,
                      borderBottomWidth: 1,
                      borderColor: 'rgba(179,179,179,1)',
                      backgroundColor: 'rgba(256,256,256,1)'
                    },
                    description: {
                      fontWeight: '100'
                    },
                    predefinedPlacesDescription: {
                      color: '#1faadb'
                    }
                  }}
                  nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                  GoogleReverseGeocodingQuery={{
                    // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                  }}
                  GooglePlacesSearchQuery={{
                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                    rankby: 'distance',
                    types: 'food'
                  }}
                />
              </ScrollView>
              {this.state.loading ? <View style={{ position: 'absolute', width: "100%", height: "100%" }}><Spinner style={{ height: "100%", alignItems: 'center', justifyContent: 'center' }} color='#003057' /></View> : null}
              {/* {this.state.loading ? <Spinner color='#003057' /> : null} */}
              <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.validateSave()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Save</Text>
                </TouchableOpacity>
              </View>
            </View> : null}
            {this.state.buttonType == 'editWeather' ?
              <View style={{ width: width * 0.82, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
                <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(179,179,179,1)', borderBottomWidth: 0.5, alignItems: 'center' }}>
                  <Text style={{
                    fontSize: 18, fontWeight: (Platform.OS === 'ios')
                      ? '400' : '500', fontFamily: (Platform.OS === 'ios')
                        ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5
                  }}>Edit locations</Text>
                </View>
                {this.state.listloading ? <Spinner style={{ height: "90%" }} color='#003057' /> : <List scrollEnabled={false} enableEmptySections={true} dataSource={this.ds.cloneWithRows(this.state.personalLoc)} style={{ height: 0, backgroundColor: '#fff' }}
                  renderRow={data =>
                    <ListItem>
                      <View style={{ marginLeft: 15 }}>
                        <Text numberOfLines={2} style={{
                          fontSize: 14, fontWeight: (Platform.OS === 'ios')
                            ? '200' : '300', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'flex-start', backgroundColor: 'rgba(0, 0, 0, 0)', textAlign: 'center'
                        }}> {data} </Text>
                      </View>
                    </ListItem>}
                  renderLeftHiddenRow={(data, secId, rowId, rowMap) =>
                    <Button warning onPress={() => this.editPersonalLoc(rowId)}>
                      <Feather size={20} name="edit" color='#fff' />
                    </Button>}
                  renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                    <Button full danger onPress={() => this.deletePersonalLoc(data)}>
                      <Feather size={20} name="trash-2" color='#fff' />
                    </Button>}
                  leftOpenValue={75}
                  rightOpenValue={-75}
                />}
                {/* {this.state.listloading ? <Spinner color='#003057' /> : <List scrollEnabled={true} enableEmptySections={true} dataSource={this.ds.cloneWithRows(this.state.mycities)} style={{ height: 0, backgroundColor: '#ffffff' }}
                  renderRow={data =>
                    <ListItem>
                      <View style={{ marginLeft: 15 }}>
                        <Text style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '200' : '300', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)' }}> {data} </Text>
                      </View>
                    </ListItem>}
                  renderLeftHiddenRow={data =>
                    <Button full onPress={() => alert(data)}>
                      <Icon active name="information-circle" />
                    </Button>}
                  renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                    <Button full danger onPress={() => this.deleteAdditionalLoc(data)}>
                      <Feather size={20} name="trash-2" color='#fff' />
                    </Button>}
                  leftOpenValue={0}
                  rightOpenValue={-75}
                />} */}
                <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
                  <TouchableOpacity style={{ width: width * 0.82, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                    <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Close</Text>
                  </TouchableOpacity>
                </View>
              </View> : null}
            {this.state.buttonType == 'editPersonalLoc' ? <View style={{ width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 60, borderWidth: 1, borderRadius: 10 }}>
              <View style={{ height: 50, backgroundColor: 'rgba(256, 256, 256, 0)', borderBottomColor: 'rgba(0, 0, 0, 0.7)', alignItems: 'center' }}>
                {this.state.editedPL == 1 ? <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5 }}>Enter Home Address</Text> :
                  <Text style={{ fontSize: 18, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: (Platform.OS === 'ios') ? 'HelveticaNeue-Thin' : 'sans-serif-thin', color: '#000', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0)', marginTop: 12.5 }}>Enter Work Address</Text>}
              </View>
              <ScrollView keyboardShouldPersistTaps={"always"} scrollEnabled={false} style={{ width: width * 0.80, height: height * 0.5, backgroundColor: 'rgba(256, 256, 256, 1)' }}>
                <GooglePlacesAutocomplete
                  placeholder='Enter Address'
                  minLength={2} // minimum length of text to search
                  autoFocus={false}
                  returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                  listViewDisplayed='true'    // true/false/undefined
                  fetchDetails={true}
                  renderDescription={row => row.description} // custom description render
                  onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                    this.prepareAddressforEdit(data.description);
                  }}
                  getDefaultValue={() => ''}
                  query={{
                    // available options: https://developers.google.com/places/web-service/autocomplete
                    key: 'AIzaSyALa0wJlgyVYfOBTBdgwwcP-fstkw5AAAg',
                    language: 'en', // language of the results
                    types: 'address' // default: 'geocode'
                  }}
                  styles={{
                    textInputContainer: {
                      borderWidth: 0,
                      width: width * 0.80,
                      height: 50,
                      marginLeft: 10,
                      marginRight: 0,
                      backgroundColor: 'rgba(256,256,256,0)'
                    },
                    textInput: {
                      marginTop: 0,
                      marginLeft: 0,
                      marginRight: 0,
                      marginBottom: 0,
                      height: 50,
                      color: '#000',
                      fontSize: 12,
                      borderBottomWidth: 1,
                      borderColor: 'rgba(179,179,179,1)',
                      backgroundColor: 'rgba(256,256,256,1)'
                    },
                    description: {
                      fontWeight: '100',
                      fontSize: 12
                    },
                    predefinedPlacesDescription: {
                      color: '#1faadb'
                    }
                  }}
                  nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                  GoogleReverseGeocodingQuery={{
                    // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                  }}
                  GooglePlacesSearchQuery={{
                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                    rankby: 'distance',
                    types: 'food'
                  }}
                />
              </ScrollView>
              {this.state.loading ? <View style={{ position: 'absolute', width: "100%", height: "100%" }}><Spinner style={{ height: "100%", alignItems: 'center', justifyContent: 'center' }} color='#003057' /></View> : null}
              {/* {this.state.loading ? <Spinner color='#003057' /> : null} */}
              <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row' }}>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.returntoEdit()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.validateEdit()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Save</Text>
                </TouchableOpacity>
              </View>
            </View> : null}
            {this.state.buttonType == 'addTracking' && this.state.isScanning == false && this.state.fail == false && this.state.success == false ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1.0)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
              <View style={{ width: width * 0.80, alignSelf: 'center' }}>
                <Image source={pairingconnect} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain' }} />
                <Text style={{ fontSize: 18, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 0, marginBottom: 20, textAlign: 'center' }}>Let’s connect your Weatherman</Text>
                <Text style={{ fontSize: 14, fontWeight: '400', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginBottom: 20, textAlign: 'center' }}>First, enable Bluetooth on your device. Next, turn on your tracker by holding down the button for five seconds until you hear two beeps.</Text>
              </View>
              <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row', bottom: 0, position: 'absolute' }}>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.onScanPressed()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Search</Text>
                </TouchableOpacity>
              </View>
            </View> : null}
            {this.state.buttonType == 'addTracking' && this.state.isScanning == true && this.state.fail == false ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
              <View style={{ width: width * 0.80, alignSelf: 'center', alignItems: 'center' }}>
                <Image source={pairing} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain', marginBottom: 20 }} />
                <Progress.Bar width={200} indeterminate={true} useNativeDriver={true} color={theme.brandPrimary} />
                {/* <Progress.CircleSnail
                  // color={['#F44336', '#2196F3', '#009688']}
                /> */}
              </View>
            </View> : null}
            {this.state.buttonType == 'addTracking' && this.state.isScanning == false && this.state.fail == true ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
              <View style={{ width: width * 0.80, alignSelf: 'center', alignItems: 'center' }}>
                <Image source={pairingfail} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain' }} />
              </View>
              <View style={{ alignItems: 'center', marginTop: 5 }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandDanger, marginBottom: 10 }}>Pairing Failed</Text>
                <Text style={{ fontSize: 16, fontWeight: '300', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 10 }}>Is Bluetooth enabled?</Text>
                <Text style={{ fontSize: 16, fontWeight: '300', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 10 }}>Is tracker turned on?</Text>
                <Text style={{ fontSize: 16, fontWeight: '300', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginTop: 10 }}>Is tracker in range?</Text>
              </View>
              <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row', bottom: 0, position: 'absolute' }}>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.onScanPressed()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Try again</Text>
                </TouchableOpacity>
              </View>
            </View> : null}
            {this.state.buttonType == 'addTracking' && this.state.isScanning == false && this.state.success == true && this.state.fail == false ? <View style={{ alignSelf: 'center', width: width * 0.82, height: height * 0.6, backgroundColor: 'rgba(256, 256, 256, 1)', marginTop: 100, borderWidth: 1, borderRadius: 10 }}>
              <View style={{ width: width * 0.92, alignSelf: 'center', alignItems: 'center' }}>
                <Image source={pairingfound} style={{ alignSelf: 'center', height: 150, resizeMode: 'contain' }} />
              </View>
              <View style={{ alignItems: 'center', marginTop: 5 }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold', fontFamily: theme.fontFamily, color: theme.brandPrimary, marginBottom: 25 }}>Device(s) Found</Text>

                <FlatList
                  data={this.state.devicesToConnect}
                  renderItem={({ item, index }) => {
                    console.log(' item ', item)
                    return (
                      <TouchableOpacity style={{ flexDirection: "row", paddingVertical: 10, paddingHorizontal: 10 }} onPress={() => this.selectDevice(index)}>
                        <View style={{ width: 20, height: 20, borderRadius: 20, backgroundColor: "white", borderColor: item.isSelected ? theme.sliderOn : "grey", borderWidth: 6, alignSelf: "center" }} />
                        <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, textAlign: 'center', alignSelf: "center", marginHorizontal: 15 }}>My Umbrella{index} - {item.status}  </Text>
                      </TouchableOpacity>
                    )
                  }}
                  // renderItem={({ item, index }) => index === 0 ? null : this.renderForecastData(item, index)}
                  keyExtractor={(item, index) => index.toString()}
                  extraData={this.state}
                />
                {/* <ListView scrollEnabled={true} enableEmptySections={true} dataSource={this.ds.cloneWithRows(this.state.devicesToConnect)} style={{ height: 100, width: width * 0.80, backgroundColor: 'transparent' }}
                  renderRow={(data, sectionID, rowID) =>
                    <ListItem>
                      <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: (Platform.OS === 'ios') ? '400' : '500', fontFamily: theme.fontFamily, textAlign: 'center' }}>My Umbrella{rowID} - {data.status}  </Text>
                    </ListItem>}
                /> */}
              </View>
              {this.state.loading ? <View style={{ position: 'absolute', width: "100%", height: "100%" }}><Spinner style={{ height: "100%", alignItems: 'center', justifyContent: 'center' }} color='#003057' /></View> : null}
              <View style={{ width: width * 0.82, height: 50, backgroundColor: 'rgba(256, 256, 256, 0.0)', borderTopWidth: 0.5, borderColor: 'rgba(179,179,179,1)', flexDirection: 'row', bottom: 0, position: 'absolute' }}>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomLeftRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.closeModal()}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: width * 0.41, height: 50, backgroundColor: 'transparent', borderBottomRightRadius: 10, borderRightWidth: 0.5, borderColor: 'rgba(179,179,179,1)', alignItems: 'center' }} onPress={() => this.connect(this.state.target, this.state.devicesToConnect)}>
                  <Text style={{ fontSize: 18, marginTop: 12, color: '#4f93e2' }}>Connect</Text>
                </TouchableOpacity>
              </View>
            </View> : null}
          </View>
        </Modal>

        <Footer style={{ height: height * 0.1 }}>
          <FooterTab style={{ backgroundColor: '#efeff4' }}>
            <SafeAreaView>
              <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorWeather === "Active") ? '#fff' : '#efeff4' }}>
                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("Weather")}>
                  {this.state.colorWeather == "Active" ?
                    <LottieView
                      style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/sun.json')}
                      autoPlay
                      loop
                    />
                    // <Image source={weather_on} style={{height:30, width:30, marginBottom:5}} /> 
                    :
                    <Image source={weather_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                  {this.state.colorWeather == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>Weather</Text> :
                    <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>Weather</Text>}
                </TouchableOpacity>
              </View>
            </SafeAreaView>
            <SafeAreaView>
              <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorTracking === "Active") ? '#fff' : '#efeff4' }}>
                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("Tracking")}>
                  {this.state.colorTracking == "Active" ?
                    <LottieView
                      style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/umbrella.json')}
                      autoPlay
                      loop
                    />
                    // <Image source={tracking_on} style={{ height: 30, width: 30, marginBottom: 5 }} />
                    :
                    <Image source={tracking_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                  {this.state.colorTracking == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>My umbrellas</Text> :
                    <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>My umbrellas</Text>}
                </TouchableOpacity>
              </View>
            </SafeAreaView>
            <SafeAreaView>
              <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorNotifications === "Active") ? '#fff' : '#efeff4' }}>
                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("Notifications")}>
                  {this.state.colorNotifications == "Active" ?
                    <LottieView
                      style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/notification.json')}
                      autoPlay
                      loop
                    />
                    //  <Image source={notifications_on} style={{ height: 30, width: 30, marginBottom: 5 }} /> 
                    :
                    <Image source={notifications_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                  {this.state.colorNotifications == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>Notifications</Text> :
                    <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>Notifications</Text>}
                </TouchableOpacity>
              </View>
            </SafeAreaView>
            <SafeAreaView>
              <View style={{ width: width * 0.25, alignItems: 'center', backgroundColor: (this.state.colorSettings === "Active") ? '#fff' : '#efeff4' }}>
                <TouchableOpacity vertical style={{ width: width * 0.25, height: height * 0.1, alignItems: 'center', marginTop: 10 }} onPress={() => this.switchScreen("AppSettings")}>
                  {this.state.colorSettings == "Active" ?
                    <LottieView
                      style={{ width: width * 0.05, height: height * 0.05, marginTop: 2, marginBottom: 2, alignItems: 'center' }}
                      imageAssetsFolder={'images/'}
                      source={require('../../animation/settings.json')}
                      autoPlay
                      loop
                    />
                    // <Image source={settings_on} style={{ height: 30, width: 30, marginBottom: 5 }} />
                    :
                    <Image source={settings_off} style={{ height: 30, width: 30, marginBottom: 5 }} />}
                  {this.state.colorSettings == "Active" ? <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.active }}>Settings</Text> :
                    <Text style={{ fontFamily: 'Al Nile', fontWeight: 'bold', fontSize: 10, color: navColors.inactive }}>Settings</Text>}
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.brandPrimary,
    height: 150,
    width: "100%",
  },
  chartView: {
    backgroundColor: 'transparent',
    width: "100%",
    height: 100
  },
  buttonGroup: {
    height: 50,
    width: "100%",
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10
  },
  checkboxImage: {
    height: 40,
    width: 40
  }
});

const mapStateToProps = (state) => ({
  getUnitData: state.user.getUserIdState,
  location: state.location,
  wUnits: state.user.wUnits,
  deviceToken: state.user.deviceToken,
  notificationId: state.user.notificationId
})

const mapDispatchToProps = dispatch => ({
  UserActions: bindActionCreators(UserActions, dispatch),
  userLocation: bindActionCreators(userLocation, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);