import React, { Component } from 'React';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';  

class currentHourlyExtTabBtn extends Component {
    constructor(props){
        super(props);
        this.state = {};
    }

    render(){
        const {onPressCurrent, onPressHourly, onPressExtended} = this.props;
        return(
                 <View style={{height:60, width:width*0.80, backgroundColor: 'rgba(256,256,256,0.0)', alignSelf:'center', flexDirection:'row'}}>
                    <TouchableOpacity onPress={onPressCurrent}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navCurrentColor, borderWidth:1, borderTopLeftRadius: 5, borderBottomLeftRadius: 5,  borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>CURRENT</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onPressHourly}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navHourlyColor, borderTopWidth:1, borderBottomWidth:1, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>HOURLY</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onPressExtended}>
                      <View style={{height:50, width:width*0.266, backgroundColor: this.state.navExtendedColor, borderWidth:1, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderColor:'#fff', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color:'#fff', fontWeight:'200', fontSize:14}}>EXTENDED</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
        )
    }
}

const styles = StyleSheet.create({
    
});

export default currentHourlyExtTabBtn;