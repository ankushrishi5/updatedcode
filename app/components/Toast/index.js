import Toast from 'react-native-root-toast';

export default function showToast(msg) {
	Toast.show(`${msg}`, { duration: Toast.durations.LONG, position: -60, animation: true, hideOnPress: true, shadow: true, });
}
