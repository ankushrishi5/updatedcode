import React, { Component } from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import Navigator from "./config/navigator"
import Constants from './constants';
import SplashScreen from 'react-native-splash-screen';
import { Root } from "native-base";

export default class RootMain extends Component{
  componentDidMount() {
    SplashScreen.hide()
  }

  /* *
   * @function: Default render function
   * */
  render(){
    return (
      <Root>
        <Navigator/>
      </Root>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.Colors.White,
  },
  toastStyle:{
     color: Constants.Colors.White,
  }
});

