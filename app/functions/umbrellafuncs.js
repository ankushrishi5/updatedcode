import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
// import RNPbWrapper from 'react-native-pb-wrapper';
import Settings from './../stores/settingsStore.js';

const Setting = new Settings();

export function multiDimensionalUnique(arr) {
  var uniques = [];
  var itemsFound = {};
  for (var i = 0, l = arr.length; i < l; i++) {
    var stringified = JSON.stringify(arr[i]);
    if (itemsFound[stringified]) { continue; }
    uniques.push(arr[i]);
    itemsFound[stringified] = true;
  }
  return uniques;
}

export function playSound(trackerid) {
  console.log('wrapper functuions 8****** ', trackerid)

  // RNPbWrapper.beepDevice(trackerid);

}

/**
* write to log
*/
export async function writeToLog(lat, lng, deviceid, typeid, type) {

  try {

    let response = await fetch(`${Setting.baseurl}wn_log/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        wdeviceid: deviceid,
        wlogtypeid: typeid,
        wlog: type,
        wlogloc: lat + ',' + lng,
        wlogtime: Date.now(),
      })
    });

    let res = await response.text();
    // console.log("res is ******* " + res);

    if (response.status >= 200 && response.status < 300) {

    } else {
      let errors = res;
      throw errors;
    }

  } catch (errors) {
    //console.log("catch errors: " + errors);
  }
}


export function removeDuplicates(arr) {
  let unique_array = arr.filter(function (elem, index, self) {
    return index == self.indexOf(elem);
  });
  return unique_array
}

export function splitlatlon(val, latorlon) {

  if (val == null) {
    return
  }
  else {
    val = val.split(",");

    if (latorlon == 1) {
      return val[0];
    }

    if (latorlon == 2) {
      return val[1];
    }

  }

}