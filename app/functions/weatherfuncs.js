import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

  export function multiDimensionalUnique(arr) {
    var uniques = [];
    var itemsFound = {};
    for(var i = 0, l = arr.length; i < l; i++) {
        var stringified = JSON.stringify(arr[i]);
        if(itemsFound[stringified]) { continue; }
        uniques.push(arr[i]);
        itemsFound[stringified] = true;
    }
    return uniques;
}


  export function removeDuplicates(arr){
        let unique_array = arr.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });
        return unique_array
    }


   export async function patchNotifications(url, lat, lon, deviceid) {
        try {
          let response = await fetch(url + 'wn_notification?filter[where][wdeviceid]=' + deviceid, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
          });
          let res = await response.text();
         if(response.status >= 200 && response.status < 300) {
            let parsed = JSON.parse(res)
            if (parsed.length > 0) {
            //this.setState({myNotifyID: parsed[0].id})
            setTimeout(() => {patchingNotify(url, parsed[0].id, lat, lon);}, 3000)
            }
          } else {
            let errors = res;
            throw errors;
          }
        } catch(error) {
          console.log(error);
        }
      }
    
      export async function patchingNotify(url, idToUpdate, lat, lon) {
        try {
          let response = await fetch(url + 'wn_notification/' +  idToUpdate, {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({
              wcurlat:lat,
              wcurlon:lon,
              })
          });
    
          let res = await response.text();
          if(response.status >= 200 && response.status < 300) {
          } else {
            let errors = res;
            throw errors;
          }
        } catch(errors) {
          console.log("Patching Error " + errors);
        }
      }