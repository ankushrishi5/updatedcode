import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';


export function getData(index, myarray) {
  // console.log("getdata ",myarray);
  var array = [];
  if (myarray && myarray.constructor === Array) {
    for (i = 0; i < myarray.length; i++) {
      var item = myarray[i];
      var newItem = { x: i, y: item.value, time: item.time };
      // console.log(newItem);
      array.push(newItem);
    }
  }
  return array;
}

export function getTickValue(index, myarray) {

  var array = [];
  if (myarray && myarray.constructor === Array) {
    for (i = 0; i < myarray.length; i++) {
      //console.log(i);
      array.push(i);
    }
  }
  return array;
}