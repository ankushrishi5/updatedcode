import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

export async function deleteHomeRecord(url, id) {
  console.log('deleteHomeRecord ',url, id)
  try {

    let response = await fetch(url + 'wn_users/' +  id, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
          wHomeLoc: null,
        })
    });

    let res = await response.text();
    console.log('res ',res)
    if(response.status >= 200 && response.status < 300) {
    } else {
      let errors = res;
      throw errors;
    }

  } catch(errors) {
  }
}

export async function deleteWorkRecord(url, id) {
  try {

    let response = await fetch(url + 'wn_users/' +  id, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
          wWorkLoc: null,
        })
    });

    let res = await response.text();
    //console.log("res is: " + res);

    if(response.status >= 200 && response.status < 300) {
    } else {
      let errors = res;
      throw errors;
    }

  } catch(errors) {
  }
}

export async function deleteAdditional1(url, id) {
  try {

    let response = await fetch(url + 'wn_users/' +  id, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
          wCNAdd1: null,
          wLLAdd1: null,
        })
    });

    let res = await response.text();

    if(response.status >= 200 && response.status < 300) {
    } else {
      let errors = res;
      throw errors;
    }

  } catch(errors) {
  }
}

export async function deleteAdditional2(url, id) {

  try {

    let response = await fetch(url + 'wn_users/' +  id, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
          wCNAdd2: null,
          wLLAdd2: null,
        })
    });

    let res = await response.text();

    if(response.status >= 200 && response.status < 300) {
      console.log("Additional2 Loc Deleted");
    } else {
      let errors = res;
      throw errors;
    }

  } catch(errors) {
  }
}

export async function deleteAdditional3(url, id) {
  try {

    let response = await fetch(url + 'wn_users/' +  id, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
          wCNAdd3: null,
          wLLAdd3: null,
        })
    });

    let res = await response.text();

    if(response.status >= 200 && response.status < 300) {
      console.log("Additional3 Loc Deleted");
    } else {
      let errors = res;
      throw errors;
    }

  } catch(errors) {
  }
}

export async function patchNotifications(url, lat, lon, deviceid) {
  try {
    let response = await fetch(url + 'wn_notification?filter[where][wdeviceid]=' + deviceid, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    let res = await response.text();
    //console.log(res);
    if(response.status >= 200 && response.status < 300) {
      let parsed = JSON.parse(res)
      if (parsed.length > 0) {
      //this.setState({myNotifyID: parsed[0].id})
      setTimeout(() => {patchingNotify(url, parsed[0].id, lat, lon);}, 3000)
      }
    } else {
      let errors = res;
      throw errors;
    }
  } catch(error) {
    console.log(error);
  }
}

export async function patchingNotify(url, idToUpdate, lat, lon) {
    // console.log(url + 'wn_notification/' +  idToUpdate);
  try {
    let response = await fetch(url + 'wn_notification/' +  idToUpdate, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        wcurlat:lat,
        wcurlon:lon,
        })
    });

    let res = await response.text();
    if(response.status >= 200 && response.status < 300) {
      console.log("Updated Notification Current Location")
    } else {
      let errors = res;
      throw errors;
    }
  } catch(errors) {
    console.log("Patching Error " + errors);
  }
}