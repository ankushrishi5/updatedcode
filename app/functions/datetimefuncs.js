import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

var moment = require('moment-timezone');

export function  formatDate(time, timezone) {

        var formatted = 0;
        var daynight = 'am';
        var date = new Date(time*1000);
        date = moment.tz(date, timezone)._d;
        
        var mydate = new Date(Date.parse(moment.tz(date, timezone).format())).toUTCString();
        
        var hours = mydate.substring(17,19);
        var these_hours = date.getHours();
        
        if (hours > 12) {
          formatted = (hours - 12);
          daynight = 'pm';
        }
        
        else if (hours == 0) {
          formatted = 12;
        }
        
        else if (hours < 10) {
          formatted = hours.substring(1,2);
        }
        
        
        else if (hours == 12) {
          formatted = 12
          daynight = 'pm';
        }
        
        else {
          formatted = hours
        }
          return formatted + daynight;
          //return these_hours;
      }

export function  getDayoftheWeekHourly(time, timezone) {
  var n = '';
  var date = new Date(time*1000);
  date = moment.tz(date, timezone)._d;
  var mydate = new Date(Date.parse(moment.tz(date, timezone).format())).toUTCString();
  //console.log(mydate);
  weekday = mydate.substring(0,3);
  //console.log(mydate.substring(0,3));
  //console.log(mydate.substring(17,19));
  var hours = mydate.substring(17,19);

  if (hours == 0) {
    n = weekday;
  }
  return n;
}

export function  getDayoftheWeek(time, timezone) {
  var n = '';
  var date = new Date(time*1000);
  date = moment.tz(date, timezone)._d;
  var mydate = new Date(Date.parse(moment.tz(date, timezone).format())).toUTCString();
  //console.log(mydate);
  weekday = mydate.substring(0,3);
  //console.log(mydate.substring(0,3));
  //console.log(mydate.substring(17,19));
  var hours = mydate.substring(17,19);
  n = weekday;
  return n;
}

export function formatTime(date) {
  var formatted = 0;
  var daynight = 'AM';
  var forReturn = [];
  var hours = date.substring(11,13);

  if (hours > 12) {
    formatted = (hours - 12);
    daynight = 'PM';
  }
  
  else if (hours == 0) {
    formatted = 12;
  }
  
  else if (hours < 10) {
    formatted = hours.substring(1,2);
  }
  
  else if (hours == 12) {
    formatted = 12
    daynight = 'PM';
  }
  
  else {
    formatted = hours
  }

  forReturn[0] = formatted;
  forReturn[1] = daynight;
  return forReturn;

}