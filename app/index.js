import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Clipboard,
  Platform
} from 'react-native';
//import { Navigator } from 'react-native'
import { Provider } from 'react-redux'
import configureStore from "./config/configureStore";
import Root from './Root';
import { pushNotificationInit , pushNotificationRemove }from "./utilities/PushNotification";

// import {
//   Container
// } from "native-base";
//import FCM from "react-native-fcm";

console.disableYellowBox = true;

// const settings = new SettingsStore()
// const user = new UserStore()

const store = configureStore();

export default class AppManager extends Component {
  constructor(props) {
    super(props)
    this.state = {
    
    }
  }

  componentWillMount() {
    /* *
     * @function: Initiliazing push notification utility
     * */
    pushNotificationInit(store);
    
  }

  componentWillUnmount() {
    /* *
     * @function: Stop listening push notification events
     * */
    pushNotificationRemove();
  }

  render() {
      return ( 
        <Provider store={store}>
	        <Root/>
	      </Provider>
      );

  }
}

