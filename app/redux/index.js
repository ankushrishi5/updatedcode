import { combineReducers } from 'redux';
import app from "./modules/app";
import nav from "./modules/nav";
import user from "./modules/user";
import location from "./modules/location";

export default function getRootReducer() {
    return combineReducers({
        app,
        nav,
        user,
        location,
    });
}
