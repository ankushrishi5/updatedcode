'use strict';
// Actions
const LOADING_STOP = "LOADING_STOP";
const LOADING_START = "LOADING_START";

// Action Creators

export const startLoading = () => ({ type: LOADING_START });

export const stopLoading = () => ({ type: LOADING_STOP });


// Reducer

const initialState = {
    isLoading: false,
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOADING_START:
            return { ...state, isLoading: true };

        case LOADING_STOP:
            return { ...state, isLoading: false };

        default:
            return state;
    }
}
