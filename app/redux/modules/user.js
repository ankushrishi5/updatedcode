'use strict';
import {
  Platform,
} from 'react-native';
import _ from "lodash";
import { startLoading, stopLoading, showToast, hideToast } from './app';
import { goBack, reset } from './nav';
import RestClient from '../../utilities/RestClient';
import DeviceInfo from 'react-native-device-info';
// Actions
export const REGISTER_NEW_USER = "REGISTER_NEW_USER";
export const GET_USER_ID = "GET_USER_ID";
export const NAVIGATE_TO_HOME = "NAVIGATE_TO_HOME";
export const UNIQUE_DEVICE = "UNIQUE_DEVICE";
export const UPDATE_ADDITIONAL_LOC = "UPDATE_ADDITIONAL_LOC";
export const UPDATE_HOME_LOC = "UPDATE_HOME_LOC";
export const SET_HOME_LOC = "SET_HOME_LOC";
export const SET_WORK_LOC = "SET_WORK_LOC";
export const UPDATE_WORK_LOC = "UPDATE_WORK_LOC";
export const DEVICE_TOKEN = "DEVICE_TOKEN";
export const CONVERTED_UNITS = "CONVERTED_UNITS";
export const DELETE_HOME_LOC = "DELETE_HOME_LOC";
export const DELETE_WORK_LOC = "DELETE_WORK_LOC";
export const DELETE_ADD_LOC_1 = "DELETE_ADD_LOC_1";
export const DELETE_ADD_LOC_2 = "DELETE_ADD_LOC_2";
export const DELETE_ADD_LOC_3 = "DELETE_ADD_LOC_3";
export const PATCH_RANGE = "PATCH_RANGE";
export const UPDATED_TOKEN = "UPDATED_TOKEN";
export const SET_NOTI_ID = "SET_NOTI_ID";
export const LOAD_TRUE = "LOAD_TRUE"; // New Action Created
export const LOAD_FALSE = "LOAD_FALSE"; // New Action Created
// Action Creators
export const SIGNUP = (data) => ({ type: REGISTER_NEW_USER, data });
export const getUserId = (data) => ({ type: GET_USER_ID, data });
export const GO_TO_HOME = (data) => ({ type: NAVIGATE_TO_HOME, data });
export const uniqueDevice = (data) => ({ type: UNIQUE_DEVICE, data });
export const updateAdditionalLoc = (data) => ({ type: UPDATE_ADDITIONAL_LOC, data });
export const setDeviceToken = (data) => ({ type: DEVICE_TOKEN, data });
export const setConvertedUnits = (data) => ({ type: CONVERTED_UNITS, data });
export const updateHomeLoc = (data) => ({ type: UPDATE_HOME_LOC, data });
export const updateWorkLoc = (data) => ({ type: UPDATE_WORK_LOC, data });
export const deleteHomeLoc = (data) => ({ type: DELETE_HOME_LOC, data });
export const deleteWorkLoc = (data) => ({ type: DELETE_WORK_LOC, data });
export const deleteAddLoc1 = (data) => ({ type: DELETE_ADD_LOC_1, data });
export const deleteAddLoc2 = (data) => ({ type: DELETE_ADD_LOC_2, data });
export const deleteAddLoc3 = (data) => ({ type: DELETE_ADD_LOC_3, data });
export const setHomeLoc = (data) => ({ type: SET_HOME_LOC, data });
export const setWorkLoc = (data) => ({ type: SET_WORK_LOC, data });
export const patchRange = (data) => ({ type: PATCH_RANGE, data });
export const updatedDeviceToken = (data) => ({ type: UPDATED_TOKEN, data });
export const setNotificationId = (data) => ({ type: SET_NOTI_ID, data });

//perform API's

/**
* Check Unique device API
*/
export const deviceUnique = (data) => {
  return dispatch => {
    dispatch(startLoading());
    RestClient.getCount("wn_users/count?[where][wDeviceID]=").then((result) => {
      if (result) {
        dispatch(uniqueDevice(result))
        dispatch(stopLoading());
        // if(result.count > 0){
        //   dispatch(GO_TO_HOME())
        // }
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* Check Unique device API
*/
export const uniqueUser = (data) => {
  return dispatch => {
    dispatch(startLoading());
    RestClient.getCount("wn_users?filter[where][wDeviceID]=", data.uniqueId).then((result) => {
      if (result.length > 0) {
        dispatch(stopLoading());
        dispatch(getUserId(result))
      } else {
        dispatch(reset());
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* Store last opened API
*/
export const storeLastOpened = (data) => {
  let requestObject = {
    wLastOpen: Date.now(),
    wVersion: '2.1.0',
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_users/" + data.idToUpdate, requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        //dispatch(GO_TO_HOME())
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* Register User API
*/
export const registerUser = (data) => {
  let requestObject = {
    wDeviceID: DeviceInfo.getUniqueID(),
    wManufacturer: DeviceInfo.getSystemName(),
    wName: data.fname + ' ' + data.lname,
    wEmail: data.email,
    wUnits: 'us',
    wLastOpen: Date.now(),
    wVersion: '2.1.0',
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.post("wn_users/", requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(SIGNUP(result));
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* Add notification API notification main
*/
export const addNotification = (data) => {
  let requestObject = {
    wdeviceid: DeviceInfo.getUniqueID(),
    wtoken: data.deviceToken
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.post("wn_notification/", requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(setNotificationId(result.id))
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* Add notification API Notification Tab
*/
export const addNotificationTab = (data) => {
  let requestObject = {
    wdeviceid: DeviceInfo.getUniqueID(),
    wtoken: data.deviceToken,
    wnotifyOOR: false,
    wnotifySunday: false,
    wnotifyMonday: true,
    wnotifyTuesday: true,
    wnotifyWednesday: true,
    wnotifyThursday: true,
    wnotifyFriday: true,
    wnotifySaturday: false,
    wnotify1: false,
    wnotifyTime1: '07:00',
    wnotifyTime1Show: '',
    wnotify2: false,
    wnotifyTime2: '16:00',
    wnotifyTime2Show: '',
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.post("wn_notification/", requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(setNotificationId(result.id))
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* Add Device API
*/
export const addNewTracker = (data) => {
  let requestObject = {
    wdeviceid: data.deviceId,
    wtrackerid: data.trackerId,
    wtrackername: data.trackerName,
    wlatlon: data.lat + ',' + data.lng,
    wlastconnected: Date.now(),
    wproductid: 1,
    wcolorid: 1,
    winrange: true,
    wtracking: true,
    wtoggle: false,
    wactive: true,
  };
  return dispatch => {
    // dispatch(startLoading());
    dispatch({ type: LOAD_TRUE })
    RestClient.post("wn_tracking/customizedCreate", requestObject).then((result) => {
      if (result) {
        dispatch({ type: LOAD_FALSE })
        // dispatch(stopLoading());
      }
    }).catch(error => {
      dispatch({ type: LOAD_FALSE })

      // dispatch(stopLoading());
    });
  }
}

/**
* Add Additional Locations API
*/
export const addAdditionalLocations = (data) => {
  let requestObject;
  switch (data.addAdditionalLoc) {
    case 'add1':
      requestObject = {
        wCNAdd1: data.selectedCity,
        wLLAdd1: data.selectedLat + ',' + data.selectedLon,
      }
      break;
    case 'add2':
      requestObject = {
        wCNAdd2: data.selectedCity,
        wLLAdd2: data.selectedLat + ',' + data.selectedLon,
      }
      break;
    case 'add3':
      requestObject = {
        wCNAdd3: data.selectedCity,
        wLLAdd3: data.selectedLat + ',' + data.selectedLon,
      }
      break;
  }
  return dispatch => {
    console.log('dispatch')
    dispatch(startLoading());
    RestClient.patch("wn_users/" + data.idToUpdate, requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(updateAdditionalLoc(result));
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* Add Additional Locations API
*/
export const addHomeOrWorkLocations = (data) => {
  let requestObject;
  if (data.selectedRowToEdit == 1) {
    requestObject = {
      wHomeLoc: data.selectedAdd
    };
  }
  else {
    requestObject = {
      wWorkLoc: data.selectedAdd
    };
  }

  return dispatch => {
    RestClient.patch("wn_users/" + data.idToUpdate, requestObject).then((result) => {
      if (result) {
        if (data.selectedRowToEdit == 1) {
          dispatch(updateHomeLoc(result));
          dispatch(setHomeLoc(result));
        } else {
          dispatch(updateWorkLoc(result));
          dispatch(setWorkLoc(result))
        }
      }
    }).catch(error => {
      console.log("error=> ", error)
    });
  }
}

/**
* delete Home Location
*/
export const deleteHomeLocation = (id) => {
  console.log('deleteHomeLocation ',id)
  let requestObject = {
    wHomeLoc: null,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_users/" + id, requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(deleteHomeLoc(result))
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* delete Work Location
*/
export const deleteWorkLocation = (id) => {
  console.log('deleteWorkLocation ',id)
  let requestObject = {
    wWorkLoc: null,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_users/" + id, requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(deleteWorkLoc(result))
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* delete additional loc1
*/
export const deleteAdditional1 = (data) => {
  let requestObject = {
    wCNAdd1: null,
    wLLAdd1: null,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_users/" + data, requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(deleteAddLoc1(result))
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* delete additional loc1
*/
export const deleteAdditional2 = (data) => {
  let requestObject = {
    wCNAdd2: null,
    wLLAdd2: null,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_users/" + data, requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(deleteAddLoc2(result))
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* delete additional loc3
*/
export const deleteAdditional3 = (data) => {
  let requestObject = {
    wCNAdd3: null,
    wLLAdd3: null,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_users/" + data, requestObject).then((result) => {
      if (result) {
        dispatch(stopLoading());
        dispatch(deleteAddLoc3(result))
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* set patch range api
*/
export const patchRangeFunc = (data) => {
  let requestObject = {
    winrange: data.winrange,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_tracking/" + data.idToUpdate, requestObject).then((result) => {
      dispatch(stopLoading());
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* update device token
*/
export const updateDeviceToken = (data) => {
  let requestObject = {
    wtoken: data.deviceToken,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_notification/" + data.notiId, requestObject).then((result) => {
      dispatch(stopLoading());
      dispatch(updatedDeviceToken(result));
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* get notification id
*/
export const getNotifcationId = (data) => {
  return dispatch => {
    dispatch(startLoading());
    RestClient.getCount("wn_notification?filter[where][wdeviceid]=").then((result) => {
      if (result) {
        dispatch(setNotificationId(result[0].id))
        dispatch(stopLoading());
        // if(result.count > 0){
        //   dispatch(GO_TO_HOME())
        // }
      }
    }).catch(error => {
      console.log("error=> ", error)
      dispatch(stopLoading());
    });
  }
}

/**
* convert units API
*/
export const convertUnits = (data) => {
  let requestObject = {
    wUnits: data,
  };
  return dispatch => {
    dispatch(startLoading());
    RestClient.patch("wn_users/", requestObject).then((result) => {
      // console.log(result, ' result')
      dispatch(setConvertedUnits(result));
    }).catch(error => {
      console.log("error=> ", error)
    });
  }
}

/**
* Initial state
*/
const initialState = {
  userDetails: null,
  count: 2,
  deviceToken: "test",
  firstTimeLogin: false,
  getUserIdState: [],
  wUnits: 'us',
  notificationId: null,
  isLoaded: false,
  wnotify1: false,
  wnotify2: false,
  wnotifyTime1Show: "",
  wnotifyTime2Show: ""
};

/**
* Reducer
*/
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REGISTER_NEW_USER:
      return { ...state, userDetails: action.data, getUserIdState: [action.data] };

    case SET_HOME_LOC:
      return { ...state, userDetails: action.data, getUserIdState: [action.data] };

    case SET_WORK_LOC:
      return { ...state, userDetails: action.data, getUserIdState: [action.data] };

    case UNIQUE_DEVICE:
      return { ...state, count: action.data };

    case GET_USER_ID:
      return { ...state, getUserIdState: action.data };

    case DEVICE_TOKEN:
      return { ...state, deviceToken: action.data }

    case UPDATED_TOKEN:
      //let currentToken = {...state.deviceToken};
      // return { ...state, deviceToken: action.data }
      return {
        ...state, deviceToken: action.data.wtoken, wnotify1: action.data.wnotify1,
        wnotify2: action.data.wnotify2, wnotifyTime1Show: action.data.wnotifyTime1Show,
        wnotifyTime2Show: action.data.wnotifyTime2Show
      }

    case SET_NOTI_ID:
      return { ...state, notificationId: action.data }

    case CONVERTED_UNITS:
      return { ...state, wUnits: action.data.wUnits }

    case UPDATE_ADDITIONAL_LOC:
      let currentState = [...state.getUserIdState];
      currentState.forEach((element) => {
        if (element.id == action.data.id) {
          element.wCNAdd1 = action.data.wCNAdd1;
          element.wCNAdd2 = action.data.wCNAdd2;
          element.wCNAdd3 = action.data.wCNAdd3;
          element.wLLAdd1 = action.data.wLLAdd1;
          element.wLLAdd2 = action.data.wLLAdd2;
          element.wLLAdd3 = action.data.wLLAdd3;
        }
      })
      return { ...state, getUserIdState: currentState };

    case UPDATE_HOME_LOC:
      let currentHomeState = [...state.getUserIdState];
      currentHomeState.forEach((element) => {
        if (element.id == action.data.id) {
          element.wHomeLoc = action.data.wHomeLoc;
        }
      })
      return { ...state, getUserIdState: currentHomeState };

    case UPDATE_WORK_LOC:
      let currentWorkState = [...state.getUserIdState];
      currentWorkState.forEach((element) => {
        if (element.id == action.data.id) {
          element.wWorkLoc = action.data.wWorkLoc;
        }
      })
      return { ...state, getUserIdState: currentWorkState };

    case DELETE_HOME_LOC:
      let currentHomeStateToDelete = [...state.getUserIdState];
      currentHomeStateToDelete.forEach((element) => {
        if (element.id == action.data.id) {
          element.wHomeLoc = null;
        }
      })
      return { ...state, getUserIdState: currentHomeStateToDelete };

    case DELETE_WORK_LOC:
      let currentWorkStateToDelete = [...state.getUserIdState];
      currentWorkStateToDelete.forEach((element) => {
        if (element.id == action.data.id) {
          element.wWorkLoc = null;
        }
      })
      return { ...state, getUserIdState: currentWorkStateToDelete };

    case DELETE_ADD_LOC_1:
      let currentLOC1ToDelete = [...state.getUserIdState];
      currentLOC1ToDelete.forEach((element) => {
        if (element.id == action.data.id) {
          element.wCNAdd1 = null;
          element.wLLAdd1 = null;
        }
      })
      return { ...state, getUserIdState: currentLOC1ToDelete };

    case DELETE_ADD_LOC_2:
      let currentLOC2ToDelete = [...state.getUserIdState];
      currentLOC2ToDelete.forEach((element) => {
        if (element.id == action.data.id) {
          element.wCNAdd2 = null;
          element.wLLAdd2 = null;
        }
      })
      return { ...state, getUserIdState: currentLOC2ToDelete };

    case DELETE_ADD_LOC_3:
      let currentLOC3ToDelete = [...state.getUserIdState];
      currentLOC3ToDelete.forEach((element) => {
        if (element.id == action.data.id) {
          element.wCNAdd3 = null;
          element.wLLAdd3 = null;
        }
      })
      return { ...state, getUserIdState: currentLOC3ToDelete };

    case LOAD_TRUE:
      return { ...state, isLoaded: true }
    case LOAD_FALSE:
      return { ...state, isLoaded: false }
    default:
      return state;
  }
}