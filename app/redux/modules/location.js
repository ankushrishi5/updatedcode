'use strict';
// Actions
const SET_DETAILS = "SET_DETAILS";
const SELECTED_DETAILS = "SELECTED_DETAILS";
const LOCATION_ERROR    = "LOCATION_ERROR";
const LOCATION_SET  = "LOCATION_SET";
const WORK_LOCATION = "WORK_LOCATION";
const HOME_LOCATION = "HOME_LOCATION";
const SET_CURRENT_LOCATION = "SET_CURRENT_LOCATION";

// Action Creators
export const setDetails = (data) => ({ type: SET_DETAILS, data });
export const setCurrentLocation = (data) => ({ type: SET_CURRENT_LOCATION, data });
export const selectLocation = (data) => ({ type: SELECTED_DETAILS, data });
export const locationError = (data) => ({ type: LOCATION_ERROR, data });
export const userLocation = (data) => ({ type: LOCATION_SET, data });
export const setHomeLocation = (data) => ({ type: HOME_LOCATION, data });
export const setworkLocation = (data) => ({ type: WORK_LOCATION, data });

// export const getCities = (data) => {
//     console.log('data ******** is ******* here *******',data)
//     let requestObject={
      
//     };
//     return dispatch => {
//       //console.log('requaet object ******** is ******* here *******',data)
//       dispatch(startLoading());
//       RestClient.get("wn_users/",requestObject).then((result) => {
//         console.log('result is here ********* ',result)
//         if(result){
//           dispatch(stopLoading());
//           dispatch(SIGNUP(result));
//         }
//       }).catch(error => {
//         console.log("error=> " ,error)
//         dispatch(stopLoading());
//       });
//     }
//   }

// Reducer

const initialState = {
    IdAndAllLocationData: [],
    currentLocation     : [],
    selectedLocation    : null,
    workLocation        : null,
    HomeLocation        : null,
    latLongAddress1     : null,
    latLongAddress2     : null,
    latLongAddress3     : null,
    isError             : false
};
 
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_DETAILS:            
            return { ...state, IdAndAllLocationData : action.data};
        case SET_CURRENT_LOCATION:            
            return { ...state, currentLocation : action.data};

        case SELECTED_DETAILS:
        	return { ...state, selectedLocation : action.data};

        case LOCATION_ERROR:
            return {...state, isError : action.data};
        case WORK_LOCATION:
            return {...state,workLocation: "sdddd" }
        case HOME_LOCATION:
            return {...state,HomeLocation: "sdddd" }
        case LOCATION_SET:
            return {...state,currentLocation: "sdddd" }

        default:
            return state;
    }
}
