"use strict";

import Connection  from "../config/Connection";
import querystring from "querystring";
import DeviceInfo from 'react-native-device-info';

let logintoken = "";

class RestClient {
    static post(url, params, token = '',userId='') {
        let context = this,
            logintoken;
        return new Promise(function(fulfill, reject) {
                fetch(Connection.getResturl() + url, {
                    method : "POST",
                    timeout : 1000*1*60,
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        "X-Client-Authorization": token,
                        "x-user-id": userId
                    },
                    body: JSON.stringify(params)
                }).then((response) => {
                    return response.text()
                })
                .then(responseText => {
                    fulfill(JSON.parse(responseText));
                }).catch(error => {
                    fulfill({message:'Please check your internet connectivity or our server is not responding.'});
                    console.warn("eroro",error);
                });
        });
    }

    static patch(url, params, token = '',userId='') {
        let context = this;
        return new Promise(function(fulfill, reject) {
                    fetch(Connection.getResturl() + url, {
                            method: "PATCH",
                            timeout : 1000*1*60,
                            headers: {
                                "Accept": "application/json",
                                "Content-Type": "application/json",
                                "x-auth-token": token,
                                "x-user-id": userId
                            },
                            body: JSON.stringify(params)
                        })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({message:'Please check your internet connectivity or our server is not responding.'});
                        });
                })
    }

    static put(url, params, token = '',userId='') {
        let context = this;
        return new Promise(function(fulfill, reject) {
                    fetch(Connection.getResturl() + url, {
                            method: "PUT",
                            timeout : 1000*1*60,
                            headers: {
                                "Accept": "application/json",
                                "Content-Type": "application/json",
                                "x-auth-token": token,
                                "x-user-id": userId
                            },
                            body: JSON.stringify(params)
                        })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({message:'Please check your internet connectivity or our server is not responding.'});
                        });
                })
    }

    static getCount(url, params, token = '',userId='') {
        let context = this;
        return new Promise(function(fulfill, reject) {
                    let query = querystring.stringify(params);
                    fetch(Connection.getResturl() + url + DeviceInfo.getUniqueID(), {
                            method: "GET",
                            timeout : 1000*1*60,
                            headers: {
                                "Accept": "application/json",
                                "Content-Type": "application/json",
                                "x-auth-token": token,
                                "x-user-id": userId
                            }
                        })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({message:'Please check your internet connectivity or our server is not responding.'});
                        });
                })
    }

    // static getForDs(url, params, token = '',userId='') {
    //     let context = this;
    //     return new Promise(function(fulfill, reject) {
    //                //console.log("url=> ",Connection.getResturl() + url ," requestObject=> ",params, " x-auth-token => ",token, " x-user-id => ",userId )
    //                 let query = querystring.stringify(params);
    //                 console.log('here is the url *********** ',Connection.getDsurl() + url + "?" + query)
    //                 fetch(Connection.getDsurl() + url + "?" + query, {
    //                         method: "GET",
    //                         timeout : 1000*1*60,
    //                         headers: {
    //                             "Accept": "application/json",
    //                             "Content-Type": "application/json",
    //                             "x-auth-token": token,
    //                             "x-user-id": userId
    //                         }
    //                     })
    //                     .then((response) => {
    //                         return response.text()
    //                     })
    //                     .then(responseText => {
    //                         fulfill(JSON.parse(responseText));
    //                     })
    //                     .catch(error => {
    //                         console.warn(error);
    //                         fulfill({message:'Please check your internet connectivity or our server is not responding.'});
    //                     });
    //             })
    // }


    static get(url, params, token = '',userId='') {
        let context = this;
        return new Promise(function(fulfill, reject) {
                    let query = querystring.stringify(params);
                    fetch(Connection.getResturl() + url + "?" + query, {
                            method: "GET",
                            timeout : 1000*1*60,
                            headers: {
                                "Accept": "application/json",
                                "Content-Type": "application/json",
                                "x-auth-token": token,
                                "x-user-id": userId
                            }
                        })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({message:'Please check your internet connectivity or our server is not responding.'});
                        });
                })
    }

    static imageUpload(url, params, token = '',userId='') {
        let context = this,
            logintoken;

        return new Promise(function(fulfill, reject) {
                    fetch(Connection.getResturl() + url, {
                            method: "POST",
                            timeout : 1000*1*60,
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'multipart/form-data;',
                                "x-auth-token": token,
                                "x-user-id": userId
                            },
                            body: params
                        })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        })
                        .catch(error => {
                            console.warn(error);
                            fulfill({message:'Please check your internet connectivity or our server is not responding.'});
                        });
                })
    }

    static delete(url, params, token = '',userId='') {
        let context = this,
            logintoken;
        return new Promise(function(fulfill, reject) {
                    fetch(Connection.getResturl() + url, {
                            method : "DELETE",
                            timeout : 1000*1*60,
                            headers: {
                                Accept: "application/json",
                                "Content-Type": "application/json",
                                "x-auth-token": token,
                                "x-user-id": userId
                            },
                            body: JSON.stringify(params)
                        })
                        .then((response) => {
                            return response.text()
                        })
                        .then(responseText => {
                            fulfill(JSON.parse(responseText));
                        }).catch(error => {
                            fulfill({message:'Please check your internet connectivity or our server is not responding.'});
                            console.warn(error);
                        });
                })
    }

}

export default RestClient;
