
import React, { Component } from "react";
import { Alert, Platform } from 'react-native';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';
import _ from "lodash";
import Idx from './Idx';
import { Root, Toast } from 'native-base';
import moment from "moment";
import * as userActions from '../redux/modules/user';
import { goTo } from '../redux/modules/nav';
import DeviceInfo from 'react-native-device-info';
import Settings from './../stores/settingsStore.js';
let notificationListener, refreshTokenListener;

const Setting = new Settings();

/**
* Initiliazing push notification
*/
export function pushNotificationInit(store) {
    FCM.requestPermissions(); // for iOS
    // FCM token on intial app load.
    FCM.getFCMToken().then(token => {
        console.log(token, "token generated in push notification file");
        if (token) {
            store.dispatch(userActions.setDeviceToken(token));
            //writeToLog('30.7333','76.7794','test',5,'background')
            // setSilentNotification(token)
        }
    });

    // Receive Notification in kill state, inactive state or bankground state.
    FCM.getInitialNotification().then(res => {
        let context = this;
        if (JSON.stringify(res)) {
            setTimeout(function () {
                onNotificationRedirection(res, store);
            }, 500);
        }
    });

    // Receive Notification in forground
    notificationListener = FCM.on(FCMEvent.Notification, async (res) => {
        if (res.type === '1') {
            //handleSilentNotification()
        }
        let context = this;
        if (res.opened_from_tray) {
            setTimeout(function () {
                onNotificationRedirection(res, store);
            }, 500);
        }
    });

    // Fcm token may not be available on first load, catch it here
    refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
        if (token) {
            store.dispatch(userActions.setDeviceToken(token));
        }
    });
}

/**
* Schedule Local Notifications.
*/

export function scheduleNotifications(data) {
    let fire_date = parseInt(moment(data.date).seconds(0).format("x"));  // eliminate secs.
    FCM.scheduleLocalNotification({
        fire_date: fire_date,
        id: data.id,
        body: 'notification body',
        show_in_foreground: true,
        priority: 'high',
        lights: true,
        vibrate: 500,
        notificationType: 6,
        sound: "default",
    });
};

/**
* Get Scheduled Notifications List.
*/

export function getScheduleNotifications(data, callback) {
    FCM.getScheduledLocalNotifications().then(notification => {
        if (_.isFunction(callback)) {
            callback(notification);
        }
    });
};

/**
*  Removes all future local notifications.
*/

export function cancelAllLocalNotifications() {
    FCM.cancelAllLocalNotifications();
};

/**
* Redirection on Notification Tap. 
*/

export function onNotificationRedirection(res, store) {
    // if (res.type == 'driverForm') {
    //     store.dispatch({ type: 'FORMREJECT_VISIBILITY', visibility: true })
    //     store.dispatch(goTo({
    //         route: 'DriverForm',
    //         params: {
    //             notiData: res.driverStatus
    //         }
    //     }));
    // }

    // if (res.type == 'profile') {
    //     //store.dispatch({type:'FORMREJECT_VISIBILITY',visibility:true})
    //     store.dispatch(goTo({
    //         route: 'profile',
    //         params: {
    //             notiData: res.driverStatus
    //         }
    //     }));
    // }
}

/**
* Stop listening push notification events
*/

export function pushNotificationRemove(store) {
    notificationListener.remove();
    refreshTokenListener.remove();
}

export async function patchRange(idToUpdate, toggle) {
    try {
        let response = await fetch(`${Setting.baseurl}wn_tracking/` + idToUpdate, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                winrange: toggle,
            })
        });

        let res = await response.text();
        if (response.status >= 200 && response.status < 300) {
        } else {
            let errors = res;
            throw errors;
        }
    } catch (errors) {
        console.log('patchRange', errors);
    }
}