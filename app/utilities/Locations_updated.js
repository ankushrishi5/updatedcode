'use strict';
import { Alert, InteractionManager, Platform, PermissionsAndroid } from "react-native";
import Permissions from 'react-native-permissions';
import * as LocationActions from '../redux/modules/location';
import Constants from '../constants';
import { patchNotifications } from '../functions/weatherfuncs';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';


export const checkPermissions = async () => {
  try {
    console.log('checkPermissions')
    await enableGPS();
    let response = await Permissions.check('location');
    console.log('response ', response)
    if (response === "authorized") {
      return true;
    } else {
      let request = await Permissions.request('location', { type: 'always' });
      console.log('request ', request)
      if (request !== "authorized") {
        return false;
      } else {
        return true;
      }

    }
  
  } catch (error) {
  }
  return
}

export async function enableGPS() {
  console.log('enableGPS')
  try {
    let result = await RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
    console.log(result, "result from gps enable")
  } catch (error) {
    console.log(error, "error from location updates")
  }
}