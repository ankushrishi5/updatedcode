import React, { Component } from 'react'

const splash = Math.floor((Math.random() * 10) + 1);
const login = Math.floor((Math.random() * 1) + 1);
const reg = Math.floor((Math.random() * 10) + 1);
export default class SettingsStore extends Component {
  constructor(props) {
    super(props)
    this.splashTime = 100
    /*Weatherman Umbrella API*/
    //Development
    //this.baseurl = 'http://0.0.0.0:3000/api/'
    //Staging
    this.baseurl = 'https://wnstgapi01.azurewebsites.net/api/'
    //this.baseurl = 'http://172.16.2.61:3000/api/'
    //Production
    // this.baseurl = 'https://wnprodapi01.azurewebsites.net/api/'

    /* DarkSky API */

    //Development
    //  this.dsurl = 'https://api.darksky.net/forecast/41a31fcd61203d51e90ed53cff1193a2/'
    //Production
    this.dsurl = 'https://api.darksky.net/forecast/027afa942c3b14b60b3fe0984d15bc2e/'

    /* Google API */
    this.geourl = 'https://maps.googleapis.com/maps/api/geocode/json?'

    /* MailChimp */
    /* App 2.0 List */
    this.appmcurl = 'https://us16.api.mailchimp.com/3.0/lists/66ff508fc9/'
    /* Weatherman Main List */
    this.wmmcurl = 'https://us16.api.mailchimp.com/3.0/lists/c08555def8/'
    /* Api Key */
    this.mckey = 'apikey ca74c537c680cb406e9663cc269849e5-us16'

    /*Firebase Messaging */
    this.fcmUrl = 'https://fcm.googleapis.com/fcm/send'
  }
  get LoginBG() {
    return this.loginBG
  }
  get RegisterBG() {
    return this.registerBG
  }
  get SplashTime() {
    return this.splashTime
  }
  get SplashImg() {
    return this.splashImg
  }
  get BaseUrl() {
    return this.baseurl
  }
  get DSUrl() {
    return this.dsurl
  }
  get trackerUrl() {
    return this.trackerurl
  }
}
