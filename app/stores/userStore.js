import React, {Component} from 'react'

export default class UserStore extends Component {
  constructor(props) {
     super(props)
     
    this.myID = ""
    this.deviceID = ""
  }
  get thisID() {
    return this.myID
  }

  get thisDevID() {
    return this.deviceID
  }

}
