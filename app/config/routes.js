
import Welcome from '../components/onboarding/welcome'
import UmbrellaMain from '../components/onboarding/umbrellamain'
import LocationMain from '../components/onboarding/locationmain'
import NotificationMain from '../components/onboarding/notificationmain'
import SelectUmbrella from '../components/onboarding/selectUmbrella'
import ConnectUmbrella from '../components/onboarding/connectumbrella'
import WeathermanUmbrellaMain from '../components/onboarding/weathermanumbrellamain'
import ConnectWeathermanUmbrella from '../components/onboarding/connectweathermanumbrella'
import UmbrellaName from '../components/onboarding/umbrellaname'
import HomeLoc from '../components/onboarding/sethomeloc'
import WorkLoc from '../components/onboarding/setworkloc'
import Notify from '../components/onboarding/setnotification'
import UserDetails from '../components/onboarding/userdetails'
import Walkthrough from '../components/onboarding/walkthrough'
import mytest from '../components/onboarding/mytest'
import Loading from '../components/onboarding/loading'
import Success from '../components/onboarding/success'
import Home from '../components/main/homeChange'

// export list of routes.
export default routes = {
    Home: { screen: Home },
    Loading: { screen: Loading },
    Welcome: { screen: Welcome },
    Walkthrough: { screen: Walkthrough },
    UserDetails: { screen: UserDetails },
    UmbrellaMain: { screen: UmbrellaMain },
    SelectUmbrella: { screen: SelectUmbrella },
    ConnectUmbrella: { screen: ConnectUmbrella },
    WeathermanUmbrellaMain: { screen: WeathermanUmbrellaMain },
    ConnectWeathermanUmbrella: { screen: ConnectWeathermanUmbrella },
    UmbrellaName: { screen: UmbrellaName },
    LocationMain: { screen: LocationMain },
    HomeLoc: { screen: HomeLoc },
    WorkLoc: { screen: WorkLoc },
    NotificationMain: { screen: NotificationMain },
    Notify: { screen: Notify },
    Success: { screen: Success },
    MyTEST: { screen: mytest },
};
