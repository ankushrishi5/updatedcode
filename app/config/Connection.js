'use strict';

const localhost = "localhost:3000",
    staging = "wnstgapi01.azurewebsites.net",
    live = "wnprodapi01.azurewebsites.net";
//dsURL           = "api.darksky.net/forecast";

const running_url = staging,
    http_url = `https://${running_url}`,
    socket_url = `ws://${running_url}/websocket`,
    apiBase_url = `https://${running_url}/api/`,
    staticPagesUrl = `http://${running_url}/`,
    mediaBase_url = `http://${running_url}/store/files/uploads/`;
//ds_url         = `https://${dsURL}/027afa942c3b14b60b3fe0984d15bc2e/`

export default class Connection {
    static getResturl() {
        return apiBase_url;
    };

    // static getDsurl() {
    //     return ds_url;
    // };

    static getSocketResturl() {
        return socket_url;
    };

    static getBaseUrl() {
        return http_url;
    };

    static getMedia(_id) {
        return mediaBase_url + _id;
    }

    static getStaticPage(url) {
        return staticPagesUrl + url;
    }
}