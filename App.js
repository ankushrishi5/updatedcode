import React, { Component } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet } from 'react-native';

const { width } = Dimensions.get('window');


import App from './app/index';
import { Root } from "native-base";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as UserActions from './app/redux/modules/user';

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}


export default class WeathermanUmbrella extends Component {
  state = {
    isConnected: true
  };

componentWillMount(){
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleConnectivityChange
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  handleConnectivityChange = isConnected => {
    if (isConnected) {
      // this.props.UserActions.getNetworkInfo({ isConnected })
      this.setState({ isConnected });
    } else {
      // this.props.UserActions.getNetworkInfo({ isConnected })
      this.setState({ isConnected });
    }
  };

  render() {
    const { isConnected } = this.state;
    return (
      <Root>
        <App />
        {!isConnected ?
          <MiniOfflineSign />
          :
          null
        }
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    position: 'absolute',
    top: 20
  },
  offlineText: { color: '#fff' }
});



// const mapDispatchToProps = dispatch => ({
//   UserActions: bindActionCreators(UserActions, dispatch)
// })
// export default connect(null, mapDispatchToProps)(WeathermanUmbrella)
/*

Changes Made : 1/03/2019

App.js => Offline Status
Index.js


App/index.js-> Comment PushNotification (componentWillMount, componentWillUnMount)
notigficationMain.js -> addToken()
setNotifications.js ->  async setNotification()
PushNotifications -> imported stored instead off passing into functions
*/