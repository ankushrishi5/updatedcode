import React, {Component} from 'react'

const splash = Math.floor((Math.random() * 10) + 1);
const login = Math.floor((Math.random() * 1) + 1);
const reg = Math.floor((Math.random() * 10) + 1);
export default class SettingsStore extends Component {
  constructor(props) {
     super(props)
    this.splashTime = 100
    /*Weatherman Umbrella API*/
    //Development
    //this.baseurl = 'http://0.0.0.0:3000/api/'
    //Staging
      this.baseurl = 'https://wnstgapi01.azurewebsites.net/api/'
      //this.baseurl = 'http://172.16.2.61:3000/api/'
    //Production
    //this.baseurl = 'https://sbprodapi.azurewebsites.net/api/'

    /* DarkSky API */

    //Development
     //this.dsurl = 'https://api.darksky.net/forecast/41a31fcd61203d51e90ed53cff1193a2/'
    //Production
    this.dsurl = 'https://api.darksky.net/forecast/027afa942c3b14b60b3fe0984d15bc2e/'
  }

}
