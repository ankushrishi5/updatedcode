//import variables from '../utils/Environment/variables'
const fetch = require('node-fetch');

//Staging
var baseUrl = 'https://wnstgapi01.azurewebsites.net/api/';

//Productions
// var baseUrl =  'https://wnprodapi01.azurewebsites.net/api/'

var dsUrl = 'https://api.darksky.net/forecast/41a31fcd61203d51e90ed53cff1193a2/'
var geoUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' 
var gaUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address='
var fcmUrl = 'https://fcm.googleapis.com/fcm/send'
var gakey = 'AIzaSyAuoSQI30VfTU3uahg0403mLAUk_9wqiq8'
var schedule = require('node-schedule');
var moment = require('moment-timezone');

var day_to_check = ''
var j = schedule.scheduleJob('*/15 * * * *', function(){
  //onRegister();
  var date = new Date();
  var cronday = date.getDay();
  var cronhour = date.getHours();
  var cronminutes = date.getMinutes();

  //doYouNeedUmbrella();
  //Scheduler Done
  if (cronday == 0) {
    day_to_check = 'wnotifySunday'
  }
  if (cronday == 1) {
    day_to_check = 'wnotifyMonday'
  }
  if (cronday == 2) {
    day_to_check = 'wnotifyTuesday'
  }
  if (cronday == 3) {
    day_to_check = 'wnotifyWednesday'
  }
  if (cronday == 4) {
   day_to_check = 'wnotifyThursday'
   
  }
  if (cronday == 5) {
    day_to_check = 'wnotifyFriday'
  }
  if (cronday == 6) {
    day_to_check = 'wnotifySaturday'
  }

  getNotificationsForToday(date);
});


async function getNotificationsForToday(jobdate) {
  try {
    let response = await fetch(baseUrl + 'wn_notification?filter[where][' + day_to_check + ']=1', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    let res = await response.text();
    if(response.status >= 200 && response.status < 300) {
      let parsed = JSON.parse(res) 
      let looper = 0; 
      
        while (looper < parsed.length) {

             if (parsed[looper].wnotify1 == 1) {
              
               if (parsed[looper].wnotifyTime1.substring(0,2) == jobdate.getHours()) {
                  if (jobdate.getMinutes() >= 0 &&  jobdate.getMinutes() < 15) {
                   if (parsed[looper].wnotifyTime1.substring(3,5) >= 0 && parsed[looper].wnotifyTime1.substring(3,5) < 15) {
                     doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                   }
                  }
                  if (jobdate.getMinutes() >= 15 &&  jobdate.getMinutes() < 30) {
                    if (parsed[looper].wnotifyTime1.substring(3,5) >= 15 && parsed[looper].wnotifyTime1.substring(3,5) < 30) {
                      doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                    }
                   }
                   if (jobdate.getMinutes() >= 30 &&  jobdate.getMinutes() < 45) {
                    if (parsed[looper].wnotifyTime1.substring(3,5) >= 30 && parsed[looper].wnotifyTime1.substring(3,5) < 45) {
                      doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                    }
                   }
                   if (jobdate.getMinutes() >= 45 &&  jobdate.getMinutes() < 59) {
                    if (parsed[looper].wnotifyTime1.substring(3,5) >= 45 && parsed[looper].wnotifyTime1.substring(3,5) < 59) {
                      doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                    }
                   }
                } 
             }

             if (parsed[looper].wnotify2 == 1) {
              
              if (parsed[looper].wnotifyTime2.substring(0,2) == jobdate.getHours()) {
                 if (jobdate.getMinutes() >= 0 &&  jobdate.getMinutes() < 15) {
                  if (parsed[looper].wnotifyTime2.substring(3,5) >= 0 && parsed[looper].wnotifyTime2.substring(3,5) < 15) {
                    doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                  }
                 }
                 if (jobdate.getMinutes() >= 15 &&  jobdate.getMinutes() < 30) {
                   if (parsed[looper].wnotifyTime2.substring(3,5) >= 15 && parsed[looper].wnotifyTime2.substring(3,5) < 30) {
                     doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                   }
                  }
                  if (jobdate.getMinutes() >= 30 &&  jobdate.getMinutes() < 45) {
                   if (parsed[looper].wnotifyTime2.substring(3,5) >= 30 && parsed[looper].wnotifyTime2.substring(3,5) < 45) {
                     doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                   }
                  }
                  if (jobdate.getMinutes() >= 45 &&  jobdate.getMinutes() < 59) {
                   if (parsed[looper].wnotifyTime2.substring(3,5) >= 45 && parsed[looper].wnotifyTime2.substring(3,5) < 59) {
                     doYouNeedUmbrella(parsed[looper].wcurlat, parsed[looper].wcurlon, parsed[looper].wdeviceid, parsed[looper].wtoken);
                   }
                  }
               } 
            }

          looper++;
      }


    } else {
      let errors = res;
      throw errors;
    }
  } catch(error) {
    console.log(error);
  }
}

    function doYouNeedUmbrella(curlat, curlon, device, token) {
      queueNotificationSetup(device, token);
      setTimeout(() => {getCurrentUnits(curlat,curlon,device );}, 10000);
        setTimeout(() => {getCityName(curlat,curlon,device);}, 10000);
        setTimeout(() => {getHomeandWork(device);}, 10000);
        setTimeout(() => {prepareNotification(device);}, 30000);     
      }

      async function getCurrentUnits(curlat, curlon, deviceid) {
        try {
          let response = await fetch(baseUrl + 'wn_users?filter[where][wDeviceID]=' + deviceid, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
          });
          let res = await response.text();
          if(response.status >= 200 && response.status < 300) {
            let parsed = JSON.parse(res)
              var myUnits = parsed[0].wUnits;

              getTemp(curlat, curlon, deviceid, myUnits, 'Cur', '')
         
          } else {
            let errors = res;
            throw errors;
          }
        } catch(error) {
          console.log(error);
        }

      }


      async function getCityName(lat, lon, deviceid) {
        try {
          let response = await fetch(geoUrl + lat + ',' + lon + '&key=' + gakey, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
          });
          let res = await response.text();
        
          if(response.status >= 200 && response.status < 300) {
            let parsed = JSON.parse(res)
              curcity:'';
            if (parsed.results[0].address_components[1].types[0] == "locality" || parsed.results[0].address_components[1].types[1] == "sublocality") { // locality type
              curcity = parsed.results[0].address_components[1].long_name;
             }
             else if (parsed.results[0].address_components[2].types[0] == "locality" || parsed.results[0].address_components[2].types[1] == "sublocality") { // locality type
              curcity = parsed.results[0].address_components[2].long_name;
             }
             else if (parsed.results[0].address_components[3].types[0] == "locality" || parsed.results[0].address_components[3].types[1] == "sublocality") { // locality type
              curcity = parsed.results[0].address_components[3].long_name;
             }
             getMyNotificationIdToPatch(deviceid, 'curCity', curcity, '');

        
          } else {
            let errors = res;
            throw errors;
          }
        
        } catch(error) {
          }
        }
    


      async function getHomeandWork(deviceid) {
        try {
          let response = await fetch(baseUrl + 'wn_users?filter[where][wDeviceID]=' + deviceid, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
          });
          let res = await response.text();
          if(response.status >= 200 && response.status < 300) {
            let parsed = JSON.parse(res)
            if (parsed[0].wHomeLoc != null) {
              getCNLL(parsed[0].wHomeLoc, 'Home', deviceid);
            }
          if (parsed[0].wWorkLoc != null) {
              getCNLL(parsed[0].wWorkLoc, 'Work', deviceid);
    
          }
          } else {
            let errors = res;
            throw errors;
          }
        } catch(error) {
          console.log(error);
        }
      }

      async function getCNLL (myAddress, loctype, deviceid) {

        try {
          let response = await fetch(gaUrl + myAddress + '&key=' + gakey, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
          });
          let res = await response.text();
      
          if(response.status >= 200 && response.status < 300) {
            let parsed = JSON.parse(res)
            var cityname='';
              if (parsed.results[0].address_components[1].types[0] == "locality" || parsed.results[0].address_components[1].types[1] == "sublocality") { // locality type
              cityname = parsed.results[0].address_components[1].long_name;
            }
             else if (parsed.results[0].address_components[2].types[0] == "locality" || parsed.results[0].address_components[2].types[1] == "sublocality") { // locality type
              cityname = parsed.results[0].address_components[2].long_name;
            }
             else if (parsed.results[0].address_components.length > 3) {
                if (parsed.results[0].address_components[3].types[0] == "locality" || parsed.results[0].address_components[3].types[1] == "sublocality") { // locality type
                cityname = parsed.results[0].address_components[3].long_name;
                }
            }
      

            getTemp(parsed.results[0].geometry.location.lat, parsed.results[0].geometry.location.lng, deviceid, 'us', loctype, cityname);
      
          } else {
            let errors = res;
            throw errors;
          }
      
        } catch(error) {
          console.log("error: " + error);
          }
      }



          async function getTemp(lat, lon, id, units, loctype, city) {
            try {
              let response = await fetch(dsUrl  + lat + ',' + lon + '?units=' + units, {
                method: 'GET',
                headers: {
                  'Content-Type': 'application/json'
                }
              });
              let res = await response.text();
              if(response.status >= 200 && response.status < 300) {
                let parsed = JSON.parse(res)
                let will_rain = false;
                let hourscheck = 0;
              
                while (hourscheck < 12) {
                  //var tempData = [];
                  if (parsed.hourly.data[hourscheck].precipIntensity >= 0.02 || parsed.currently.icon == 'rain' ||parsed.currently.icon == 'snow') {
                    will_rain = true;
                    }
                  hourscheck++;
              }

              if (loctype == 'Cur') {
                Math.round(parsed.daily.data[0].temperatureLow) +'˚. '+parsed.hourly.summary;
                var forecast = Math.round(parsed.currently.temperature)+'˚, ' + parsed.currently.summary+'. Today: High '+ Math.round(parsed.daily.data[0].temperatureHigh) +'˚, low: '+ 
                Math.round(parsed.daily.data[0].temperatureLow) +'˚. '+parsed.hourly.summary;
                getMyNotificationIdToPatch(id, 'curForecast', forecast, will_rain);
              }
              if (loctype == 'Home') {
                getMyNotificationIdToPatch(id, 'homerain', city, will_rain);
              }
              if (loctype == 'Work') {
                getMyNotificationIdToPatch(id, 'workrain', city, will_rain);
              }


              } else {
                let errors = res;
                throw errors;
              }

            } catch(error) {
              console.log("error: " + error);
            }
          }

        async function queueNotificationSetup(deviceid, token) {
        var systemTime = moment(new Date()).format();
        //moment(new Date()).format();
        //var queueDate = moment.tz(systemTime, "America/Los_Angeles").format();
        //console.log(queueDate);
        //var queueDate = new Date();
          try {

            let response = await fetch(baseUrl + 'wn_notifyqueue/', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify({
                  wdeviceid:deviceid,
                  wtoken:token,
                  wpostdate: systemTime,
                  wsent: 0,
                })
            });

            let res = await response.text();
            if(response.status >= 200 && response.status < 300) {
              let parsed = JSON.parse(res);
            } else {
              let errors = res;
              throw errors;
            }
          } catch(errors) {
            let formErrors = JSON.parse(errors);
            let errorsArray = [];
            for(let key in formErrors) {
              if(formErrors[key].length > 1) {
                formErrors[key].map(error => errorsArray.push(`${key} ${error}`))
              } else {
                  errorsArray.push(`${key} ${formErrors[key]}`)
              }
            }
          }
        }


        async function getMyNotificationIdToPatch(device, type, nameorforecast, rain) {
          try {
            let response = await fetch(baseUrl + 'wn_notifyqueue?filter[where][wdeviceid]=' + device +'&filter[where][wsent]=false', {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json'
              }
            });
            let res = await response.text();
            if(response.status >= 200 && response.status < 300) {
              let parsed = JSON.parse(res)
              var myID = parsed[0].id;

              if (type == 'curCity') {
                patchCurrentCity(myID, nameorforecast);
              }
              if (type == 'curForecast') {
                patchCurrentForecast(myID, nameorforecast, rain);
              }
              if (type == 'homerain') {
                patchHomeRain(myID, nameorforecast, rain);
              }
            if (type == 'workrain') {
                patchWorkRain(myID, nameorforecast, rain);
      
            }
            } else {
              let errors = res;
              throw errors;
            }
          } catch(error) {
            console.log(error);
          }
        }
      


        async function patchCurrentCity(id, name) {
          try {
            let response = await fetch(baseUrl + 'wn_notifyqueue/' +  id, {
              method: 'PATCH',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify({
                wcurcity: name,
                })
            });

            let res = await response.text();
            if(response.status >= 200 && response.status < 300) {
            } else {
              let errors = res;
              throw errors;
            }

          } catch(errors) {
            console.log(errors);
          }
        }

        async function patchCurrentForecast(id, forecast, rain) {
          try {
            let response = await fetch(baseUrl + 'wn_notifyqueue/' +  id, {
              method: 'PATCH',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify({
                wcurforecast: forecast,
                wcurrain: rain,
                })
            });

            let res = await response.text();
            if(response.status >= 200 && response.status < 300) {
            } else {
              let errors = res;
              throw errors;
            }

          } catch(errors) {
            console.log(errors);
          }
        }

        async function patchHomeRain(id, name, rain) {
          try {
            let response = await fetch(baseUrl + 'wn_notifyqueue/' +  id, {
              method: 'PATCH',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify({
                whomecity: name,
                whomerain: rain,
                })
            });

            let res = await response.text();
            if(response.status >= 200 && response.status < 300) {
            } else {
              let errors = res;
              throw errors;
            }

          } catch(errors) {
            console.log(errors);
          }
        }

        async function patchWorkRain(id, name, rain) {
          try {
            let response = await fetch(baseUrl + 'wn_notifyqueue/' +  id, {
              method: 'PATCH',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify({
                wworkcity: name,
                wworkrain: rain,
                })
            });

            let res = await response.text();
            if(response.status >= 200 && response.status < 300) {
            } else {
              let errors = res;
              throw errors;
            }

          } catch(errors) {
            console.log(errors);
          }
        }

        async function prepareNotification(device, type, nameorforecast, rain) {
          try {
            let response = await fetch(baseUrl + 'wn_notifyqueue?filter[where][wdeviceid]=' + device +'&filter[where][wsent]=false', {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json'
              }
            });
            let res = await response.text();
            if(response.status >= 200 && response.status < 300) {
              let parsed = JSON.parse(res)
              var myID = parsed[0].id;
              var myTitle = 'No precipitation in the forecast';
              var prep = '';
              var precipitation_forecast = '';
              var cities = [];

              if (parsed[0].wcurrain == 1 || parsed[0].whomerain == 1 || parsed[0].wworkrain == 1) {
                myTitle = 'Precipitation is in the forecast';
                prep = 'Grab your umbrella!';
                if (parsed[0].wcurrain == 1) {
                    cities.push(parsed[0].wcurcity);
                }
                if (parsed[0].whomerain == 1) {
                  cities.push(parsed[0].whomecity);
                }
                if (parsed[0].wworkrain == 1) {
                  cities.push(parsed[0].wworkcity);
                }
              }


              if (cities.length == 2) {
                if (cities[0] != cities[1]) {
                precipitation_forecast = 'Precipitation is in the forecast for ' + cities[0] + ' and ' + cities[1] + '.'
                }
                else {
                  precipitation_forecast = 'Precipitation is in the forecast for ' + cities[0] + '.'
                }
              }

              if (cities.length == 1) {
                if (cities[0] != parsed[0].wcurcity) {
                precipitation_forecast = 'Precipitation is in the forecast for ' + cities[0] + '.'
                }
              }


              var message_body = 'Currently in '  + parsed[0].wcurcity + ': ' + parsed[0].wcurforecast + ' ' + precipitation_forecast + ' ' + prep
              sendNotification(myID, parsed[0].wtoken, myTitle, message_body);


            } else {
              let errors = res;
              throw errors;
            }
          } catch(error) {
            console.log(error);
          }
        }

        async function sendNotification(id, token, title, body) {
            try {
              let response = await fetch(fcmUrl, {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': 'key=AAAAM-j2f4c:APA91bEKm7cfnew5ZHTw3vDI3P66z64oQ9wJa9oXb2LCBJTPj8UTmi9EUtDyfJahe7FvMZw5p6ZfGxNPfuyfOfsH3khkvqy364fPYkd7WGnHu7U95qpAPspHLMv7Ac1upAy6ZNC4tKH7'
                },
                body: JSON.stringify({
                  notification:{
                  title:title,
                  body:body,
                  sound:"default"
                  },
                  
                  to:token,
                  priority:"high"
                  })
              });
  
              let res = await response.text();
  
              if(response.status >= 200 && response.status < 300) {
  
                let parsed = JSON.parse(res);
                patchRespnse(id, '200 OK');
              } else {
                let errors = res;
                throw errors;
              }
  
            } catch(errors) {
              patchRespnse(id, errors);
              console.log(errors);
            }
          }


          async function patchRespnse(id, status) {
            try {
              let response = await fetch(baseUrl + 'wn_notifyqueue/' +  id, {
                method: 'PATCH',
                headers: {
                  'Content-Type': 'application/json',
                  'Accept': 'application/json'
                },
                body: JSON.stringify({
                  wresponse: status,
                  wsent: 1,
                  })
              });
  
              let res = await response.text();
              if(response.status >= 200 && response.status < 300) {
              console.log("Response Updated");
              } else {
                let errors = res;
                throw errors;
              }
  
            } catch(errors) {
              console.log(errors);
            }
          }


